<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->text('images')->nullable();
            $table->string('place_of_birth')->nullable()->comment('出身地');
            $table->string('education')->nullable()->comment('学歴');
            $table->string('height')->nullable()->comment('身長');
            $table->string('human_form')->nullable()->comment('体型');
            $table->tinyInteger('sake')->default(0)->nullable()->comment('お酒: 0-No, 1-Yes');
            $table->tinyInteger('smoke')->default(0)->nullable()->comment('タバコ: 0-No, 1-Yes');
            $table->string('blood_type')->nullable()->comment('血液型');
            $table->string('thing_of_marriage')->nullable()->comment('結婚への思い');
            $table->string('annual_income')->nullable()->comment('年収');
            $table->string('job_type')->nullable()->comment('職種');
            $table->string('hair')->nullable()->comment('髪型');
            $table->string('hair_color')->nullable()->comment('髪色');
            $table->string('option_1')->nullable()->comment('同居人');
            $table->string('option_2')->nullable()->comment('会うまで期間');
            $table->string('option_3')->nullable()->comment('兄弟姉妹');
            $table->tinyInteger('option_4')->default(0)->nullable()->comment('子供は欲しいか: 0-No, 1-Yes');
            $table->text('option_5')->nullable()->comment('ひとこと');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            //
        });
    }
}
