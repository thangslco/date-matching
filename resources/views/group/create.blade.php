@php
use App\Helpers\Common;
@endphp
@extends('layouts.default')

@section('title', $title)

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{$title}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">{{$title}}</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="card card-primary">
                    <!-- form start -->
                    <form role="form" id="quickForm" action="{{$action}}" method="POST" novalidate="novalidate" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="input-name">{{__('messages.groups.lbl_name')}}</label>
                                <input type="text" name="name" class="form-control" id="input-name" placeholder="Input name" value="{{ old('name') ?? request()->name }}" autocomplete="off">
                                @if ($errors->has('name'))
                                <span class="error invalid-feedback">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="input-type">{{__('messages.groups.lbl_image')}}</label>
                                <div class="preview">
                                    @if (!is_null($imageUrl))
                                    <img id="previewImg" class="image-preview" src="{{$imageUrl}}" alt="Placeholder">
                                    @else
                                    <img id="previewImg" class="image-preview" src="{{Common::noImage()}}" alt="Placeholder">
                                    @endif
                                </div>
                                <input type="file" name="image" data-error-id="#lbl-image-error" accept=".jpg, .png, .jpeg, .gif|image/*" required>
                                <span class="error invalid-feedback hide" id="lbl-image-error"></span>
                                @if ($errors->has('image'))
                                <span class="error invalid-feedback">{{ $errors->first('image') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>{{__('messages.groups.lbl_description')}}</label>
                                <textarea name="description" class="form-control" rows="3" placeholder="Enter ...">{{ old('description') ?? request()->description }}</textarea>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            @if(isset($id) && !empty($id))
                            <button type="submit" class="btn btn-success float-right">{{__('messages.btn_update')}}</button>
                            @else
                            <button type="submit" class="btn btn-success float-right">{{__('messages.btn_create')}}</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('style')
    @parent
@endsection
@section('script')
    @parent
    @if (Session::has('password'))
    <script>
        $('#modal-default').modal();
    </script>
    @endif
@endsection