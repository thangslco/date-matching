@extends('layouts.default')

@section('title', 'ジャンル管理')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">ジャンル管理</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">ジャンル管理</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-success">{{ Session::get('error') }}</div>
                @endif
                <div class="modal fade" id="modal-delete-group" data-url="{{route('groups.delete')}}" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">{{__('messages.heading_modal')}}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>{{__('messages.content_confirm_delete_group')}}</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{__('messages.btn_close')}}</button>
                                <button type="button" id="btn-delete-group" class="btn btn-danger">{{__('messages.btn_delete')}}</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal fade" id="modal-delete-group-success" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">{{__('messages.heading_modal')}}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{__('messages.btn_close')}}</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="card">
                    <div class="card-header row">
                        <div class="col-md-6">
                            <a href="{{route('groups.create')}}" class="btn btn-success">新規作成</a>
                        </div>
                        <div class="col-md-6">
                            <div class="card-tools fa-pull-right">
                                <form action="" method="GET">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="keyword" class="form-control float-right" value="{!! request()->keyword !!}" placeholder="Search...">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>{{__('messages.groups.lbl_image')}}</th>
                                    <th>{{__('messages.groups.lbl_name')}}</th>
                                    <th>{{__('messages.groups.total_joined')}}</th>
                                    <th>{{__('messages.lbl_created_at')}}</th>
                                    <th>{{__('messages.lbl_updated_at')}}</th>
                                    <th>{{__('messages.lbl_action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td><img width="80" src="{{ $item->getImageUrl(true) }}" alt="{{$item->name}}" /></td>
                                    <td>{!! $item->name !!}</td>
                                    <td>{!! $item->joined !!}</td>
                                    <td>{!! $item->created_at !!}</td>
                                    <td>{!! $item->updated_at !!}</td>
                                    <td>
                                        <a href="{{ route('groups.detail', $item->id)}}" class="btn btn-primary edit-group" data-id="{{$item->id}}">編集</a>
                                        <button type="button" class="btn btn-danger delete-group" data-id="{{$item->id}}">削除</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        {!! $items->links() !!}
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('style')
@parent
@endsection
@section('script')
@parent
@endsection