<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transaction.
 *
 * @package namespace App\Models;
 */
class Transaction extends BaseModel
{
    use SoftDeletes;
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'amount',
        'quantity',
        'type',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function member()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'id');
    }
}
