<?php

namespace App\Services;

use App\Repositories\Contracts\BaseRepositoryContract;
use Illuminate\Support\Facades\Storage;

abstract class BaseService
{
    protected $repository;

    public function __construct(BaseRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    public function create($data)
    {
        return $this->repository->create($data);
    }

    public function update(array $attributes, $id)
    {
        return $this->repository->update($attributes, $id);
    }

    public function updateOrCreate(array $attributes, array $values = [])
    {
        return $this->repository->updateOrCreate($attributes, $values);
    }

    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    public function deleteWhere(array $where)
    {
        return $this->repository->deleteWhere($where);
    }

    public function first($columns = ['*'])
    {
        return $this->repository->first($columns);
    }

    public function firstOrNew(array $attributes = [])
    {
        return $this->repository->firstOrNew($columns);
    }

    public function firstOrCreate(array $attributes = [])
    {
        return $this->repository->firstOrCreate($attributes);
    }

    public function find($id, $columns = ['*'])
    {
        return $this->repository->find($id, $columns);
    }

    public function firstByConditions(array $conditions, $columns = ['*'])
    {
        return $this->repository->firstByConditions($conditions, $columns);
    }

    public function findByField($field, $value = null, $columns = ['*'])
    {
        return $this->repository->findByField($field, $value, $columns);
    }

    public function all($columns = ['*'])
    {
        return $this->repository->all($columns);
    }

    public function paginate($limit = null, $columns = ['*'], $method = "paginate")
    {
        return $this->repository->paginate($limit, $columns, $method);
    }

    public function findWhere(array $where, $columns = ['*'])
    {
        return $this->repository->findWhere($where, $columns);
    }

    public function upload($uploadedFile, $folder = 'public/avatars')
    {
        try {
            if (!file_exists(storage_path("app/$folder"))) {
                @mkdir(storage_path("app/$folder"), 0777, true);
            }
            $filename = time() . '-' . $uploadedFile->getClientOriginalName();
            $path = Storage::disk('local')->putFileAs(
                $folder,
                $uploadedFile,
                $filename
            );
            return $path ? $filename : null;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function removeFile($filePath, $folder = 'public/avatars/')
    {
        try {
            Storage::disk('local')->delete($folder . $filePath);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Forced update function: for only update statement.
     *
     * @param $data
     * @param $conditions
     * @return mixed
     */
    public function forcedUpdate($data, $conditions)
    {
        return $this->repository->where($conditions)->update($data);
    }

    public function filterMessage($message, $ngWords)
    {
        $matches = [];
        $matchFound = preg_match_all(
            "/\b(" . implode("|", $ngWords) . ")\b/i",
            $message,
            $matches
        );
        $listNgWords = [];
        if ($matchFound) {
            $words = array_unique($matches[0]);
            foreach ($words as $word) {
                $listNgWords[] = $word;
            }
        }
        return $listNgWords;
    }
}
