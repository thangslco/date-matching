<?php

namespace App\Repositories\Contracts;

/**
 * Interface SettingRepository.
 *
 * @package namespace App\Repositories;
 */
interface SettingRepository extends BaseRepositoryContract
{
}
