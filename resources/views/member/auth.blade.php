@extends('layouts.default')

@section('title', 'Auth')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">身分証</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active">身分証</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <a>残り: </a><a id="count">{{$memberPendings->total()}}</a><a>件</a>
                <button type="button" id="reload_button" class="btn btn-success" onclick="reload()" style="margin-left: 10px;"><span><i class="fas fa-sync-alt" spin=""></i></span></button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-12">
                <div class="card ui-tabs ui-corner-all ui-widget ui-widget-content" id="tabs">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                            <li class="nav-item"><a id="nav-link-0" class="nav-link {{$tabActive == 'p1' ? 'active' : ''}}" href="#confirmation_waiting" data-toggle="tab">承認待ち</a></li>
                            <li class="nav-item"><a id="nav-link-1" class="nav-link {{$tabActive == 'p2' ? 'active' : ''}}" href="#confirmation_success" data-toggle="tab">承認済み</a></li>
                            <li class="nav-item"><a id="nav-link-2" class="nav-link {{$tabActive == 'p3' ? 'active' : ''}}" href="#confirmation_fail" data-toggle="tab">非承認</a></li>
                        </ul>
                    </div><!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="tab-content">
                            <div class="tab-pane {{$tabActive == 'p1' ? 'active' : ''}}" id="confirmation_waiting">
                                <div class="card-body p-0">
                                    <!-- Table -->
                                    <table class="table table-hover table-bordered table-sm">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Indentification Image</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Birthday</th>
                                                <th>Age</th>
                                                <th>Residence</th>
                                                <th>Last Login</th>
                                                <th>Created</th>
                                                <th style="width: 180px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($memberPendings as $item)
                                            <tr id="{{ $item->id }}">
                                                <td>{{ $item->id }}</td>
                                                <td><img class="zoomable" width="80" src="{{ $item->getImageVerifyUrl(true) }}" alt=" " /></td>
                                                <td><a href="{!! $item->getDetailUrl() !!}">{{ $item->nickname }}</a></td>
                                                <td>{{ $item->phone }}</td>
                                                <td style="color: red;">{{ $item->getBirthday() }}</td>
                                                <td style="color: red;">{{ $item->getAge() }}歳</td>
                                                <td>{{ str_limit($item->address, 50) }}</td>
                                                <td>{{ $item->getLastLogin() }}</td>
                                                <td>{{ $item->getCreatedAt() }}</td>
                                                <td style="width: 180px">
                                                    <button type="button" class="btn btn-primary approval" data-id="{{$item->id}}">承認</button>
                                                    <button type="button" class="btn btn-danger rejection" data-id="{{$item->id}}">非承認</button>
                                                </td>
                                            </tr>
                                            @empty
                                            @endforelse
                                        </tbody>
                                    </table>
                                    <!-- /.table -->
                                </div>
                                <div class="card-footer clearfix">
                                    {!! $memberPendings->links() !!}
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane {{$tabActive == 'p2' ? 'active' : ''}}" id="confirmation_success">
                                <div class="card-body table-responsive p-0">
                                    <!-- Table -->
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Indentification Image</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Birthday</th>
                                                <th>Age</th>
                                                <th>Residence</th>
                                                <th>Last Login</th>
                                                <th>Created</th>
                                                <th style="width: 180px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($memberApproveds as $item)
                                            <tr id="{{ $item->id }}">
                                                <td>{{ $item->id }}</td>
                                                <td><img class="zoomable" width="80" src="{{ $item->getImageVerifyUrl(true) }}" alt=" " /></td>
                                                <td><a href="{!! $item->getDetailUrl() !!}">{{ $item->nickname }}</a></td>
                                                <td>{{ $item->phone }}</td>
                                                <td style="color: red;">{{ $item->getBirthday() }}</td>
                                                <td style="color: red;">{{ $item->getAge() }}歳</td>
                                                <td>{{ str_limit($item->address, 50) }}</td>
                                                <td>{{ $item->getLastLogin() }}</td>
                                                <td>{{ $item->getCreatedAt() }}</td>
                                                <td style="width: 180px">
                                                    <button type="button" class="btn btn-danger rejection" data-id="{{$item->id}}">非承認</button>
                                                </td>
                                            </tr>
                                            @empty
                                            @endforelse
                                        </tbody>
                                    </table>
                                    <!-- /.table -->
                                </div>
                                <div class="card-footer clearfix">
                                    {!! $memberApproveds->links() !!}
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane {{$tabActive == 'p3' ? 'active' : ''}}" id="confirmation_fail">
                                <div class="card-body table-responsive p-0">
                                    <!-- Table -->
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Indentification Image</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Birthday</th>
                                                <th>Age</th>
                                                <th>Residence</th>
                                                <th>Last Login</th>
                                                <th>Created</th>
                                                <th style="width: 180px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($memberRejects as $item)
                                            <tr id="{{ $item->id }}">
                                                <td>{{ $item->id }}</td>
                                                <td><img class="zoomable" width="80" src="{{ $item->getImageVerifyUrl(true) }}" alt=" " /></td>
                                                <td><a href="{!! $item->getDetailUrl() !!}">{{ $item->nickname }}</a></td>
                                                <td>{{ $item->getPhone() }}</td>
                                                <td style="color: red;">{{ $item->getBirthday() }}</td>
                                                <td style="color: red;">{{ $item->getAge() }}歳</td>
                                                <td>{{ str_limit($item->address, 50) }}</td>
                                                <td>{{ $item->getLastLogin() }}</td>
                                                <td>{{ $item->getCreatedAt() }}</td>
                                                <td style="width: 180px">
                                                    <button type="button" class="btn btn-primary approval" data-id="{{$item->id}}">承認</button>
                                                </td>
                                            </tr>
                                            @empty
                                            @endforelse
                                        </tbody>
                                    </table>
                                    <!-- /.table -->
                                </div>
                                <div class="card-footer clearfix">
                                    {!! $memberRejects->links() !!}
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<div class="modal fade" id="notice-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">お知らせ</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default float-right" data-dismiss="modal">OK</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="zoom-image">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default float-right" data-dismiss="modal">OK</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
@section('style')
@parent
@endsection
@section('script')
@parent
<script>
    var urlApprove = "{{ route ('approve')}}";
    var urlReject = "{{ route ('reject')}}";
</script>
<script src="{{ url('/assets/js/auth.js') }}"></script>
@endsection