<?php

namespace App\Repositories;

use App\Models\MemberBlock;
use App\Repositories\Contracts\MemberBlockRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class MemberBlockRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class MemberBlockRepositoryEloquent extends BaseRepository implements MemberBlockRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MemberBlock::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function listUsersBlockedByConditions($conditions)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('userBlocked')
        ->whereHas('userBlocked', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        });
        if (isset($conditions['member_id'])) {
            $model->where([
                'user_block_id' => $conditions['member_id']
            ]);
        }
        if (isset($conditions['user_blocked_id'])) {
            $model->where([
                'user_blocked_id' => $conditions['user_blocked_id']
            ]);
        }
        if (isset($conditions['is_show'])) {
            $model->where([
                'is_show' => $conditions['is_show']
            ]);
        }
        $model = $model->get();
        $this->resetModel();
        return $this->parserResult($model);
    }
}
