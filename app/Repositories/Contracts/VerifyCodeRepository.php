<?php

namespace App\Repositories\Contracts;

/**
 * Interface VerifyCodeRepository.
 *
 * @package namespace App\Repositories;
 */
interface VerifyCodeRepository extends BaseRepositoryContract
{
}
