<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class NgWord.
 *
 * @package namespace App\Models;
 */
class NgWord extends BaseModel
{
    use SoftDeletes;
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['word'];

    protected $dates = ['created_at', 'updated_at'];
}
