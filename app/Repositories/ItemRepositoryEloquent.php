<?php

namespace App\Repositories;

use App\Models\Item;
use App\Repositories\Contracts\ItemRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class ItemRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ItemRepositoryEloquent extends BaseRepository implements ItemRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Item::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getList($conditions = [])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
        ->whereHas('user', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where($conditions);
        $results = $model->paginate(PAGINATE_DEFAULT);
        $results->appends(app('request')->query());
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getTotalLikeItemByUser($id) {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
        ->whereHas('user', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where([
            'member_id' => $id,
            'type' => TYPE_LIKE_ITEM
        ])->sum('total');
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function rewardUser($listMemberId, $like)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->whereIn('member_id', $listMemberId)
            ->update([
                'total' => DB::raw('total + ' . $like)
            ]);
        $this->resetModel();
        return $this->parserResult($model);
    }
}
