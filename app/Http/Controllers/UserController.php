<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangeAccount;
use App\Http\Requests\CreateUserRequest;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Session;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected $service;

    public function __construct(UserService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    public function changeAccount(Request $request)
    {
        $menuActived = 'MENU_CHANGE_ACCOUNT';
        return view('user.change-account', compact('menuActived'));
    }

    public function doChangeAccount(ChangeAccount $request)
    {
        if ($request->isMethod('POST')) {
            DB::beginTransaction();
            try {
                $data = $request->only(['email', 'password']);
                $result = $this->service->update($data, Auth::id());
                DB::commit();
                Auth::setUser($result);
                return redirect()->back()->withInput($request->all)->with('success', 'ログイン情報を変更しました。');
            } catch (Exception $e) {
                DB::rollBack();
                return redirect()->back()->withInput($request->all)->with('error', $e->getMessage());
            }
        }
        return redirect()->route('user.change-account');
    }

    public function index(Request $request)
    {
        $menuActived = 'MENU_USER';
        $currentUser = Auth::user();
        $showButtonCreate = false;
        $showButtonEdit = false;
        $showButtonDelete = false;
        if ($currentUser->type == TYPE_USER_MASTER) {
            $showButtonCreate = true;
            $showButtonEdit = true;
            $showButtonDelete = true;
        } else {
            return redirect()->route('home')->with('error', __('messages.permission_denied'));
        }
        $items = $this->service->getList($request);
        return view('user.index', compact('items', 'menuActived', 'showButtonCreate', 'showButtonEdit', 'showButtonDelete'));
    }

    public function detail(Request $request, $id)
    {
        $currentUser = Auth::user();
        if ($currentUser->type != TYPE_USER_MASTER) {
            return redirect()->route('home')->with('error', __('messages.permission_denied'));
        }
        $menuActived = 'MENU_USER';
        $user = $this->service->find($id);
        if (!$user) {
            return redirect()->route('users')->with('error', __('messages.user_not_found'));
        }
        $request->merge(['email' => $user->email, 'type' => $user->type]);
        $action = route('users.update', ['id' => $id]);
        $title = 'アカウント変更';
        return view('user.create', compact('menuActived', 'action', 'title', 'id'));
    }

    public function create(Request $request)
    {
        $currentUser = Auth::user();
        if ($currentUser->type != TYPE_USER_MASTER) {
            return redirect()->route('home')->with('error', __('messages.permission_denied'));
        }
        $menuActived = 'MENU_USER';
        $action = route('users.store');
        $title = '新規作成';
        return view('user.create', compact('menuActived', 'action', 'title'));
    }
    
    public function store(CreateUserRequest $request)
    {
        $currentUser = Auth::user();
        if ($currentUser->type != TYPE_USER_MASTER) {
            return redirect()->route('home')->with('error', __('messages.permission_denied'));
        }
        $params = $request->only(['email', 'type']);
        $password = Str::random(6);
        $params['password'] = $password;
        $params['email_verified_at'] = date(DATETIME_FORMAT);
        $this->service->create($params);
        Session::flash('message', __('messages.create_user_success'));
        return redirect()->route('users.create')->with('password', $password);
    }

    public function update(CreateUserRequest $request, $id)
    {
        $currentUser = Auth::user();
        if ($currentUser->type != TYPE_USER_MASTER) {
            return redirect()->route('home')->with('error', __('messages.permission_denied'));
        }
        $user = $this->service->find($id);
        if (!$user) {
            return redirect()->route('users')->with('error', __('messages.user_not_found'));
        }
        $params = $request->only(['email', 'type']);
        $this->service->update($params, $id);
        Session::flash('message', __('messages.update_user_success'));
        return redirect()->route('users.detail', ['id' => $id]);
    }

    public function delete(Request $request)
    {
        $currentUser = Auth::user();
        if ($currentUser->type != TYPE_USER_MASTER) {
            return response()->json(['status' => false, 'message' => __('messages.permission_denied')]);
        }
        try {
            $id = $request->get('id');
            if (!$id) {
                return response()->json(['status' => false, 'message' => __('messages.invalid_param')]);
            }
            $user = $this->service->find($id);
            if (!$user) {
                return response()->json(['status' => false, 'message' => __('messages.user_not_found')]);
            }
            if ($user->id == $currentUser->id) {
                return response()->json(['status' => false, 'message' => __('messages.not_delete_account_yourself')]);
            }
            $this->service->delete($id);
            return response()->json(['status' => true, 'message' => __('messages.delete_user_success')]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }
}
