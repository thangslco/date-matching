<?php

namespace App\Services;

use App\Repositories\Contracts\SettingRepository;

class SettingService extends BaseService
{
    /**
     * SettingService constructor.
     * @param SettingRepository $repository
     */
    public function __construct(
        SettingRepository $repository
    ) {
        $this->repository = $repository;
    }

    // $type = 0 (value: value), 1 ( value: 0, 1)
    public function saveSetting($params, $type = 0)
    {
        $data = [];
        foreach ($params as $key => $value) {
            if ($type == 1) {
                $value = $value > 0 ? 1 : 0;
            }
            $result = $this->repository->updateOrCreate([
                'key' => $key
            ], [
                'value' => $value
            ]);
            if ($result) {
                $data[$key] = $value;
            }
        }
        return $data;
    }
}
