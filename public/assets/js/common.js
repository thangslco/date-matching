$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".disable-input").bind("keydown", function(e) {
        e.preventDefault();
    });
    $('input[type="file"]').change(function(e) {
        chooseFileImage($(this), $('#previewImg'));
    });
    var idDelete = null;
    var rowDelete = null;
    var statusDelete = null;
    $('.delete-user').click(function(e) {
        e.preventDefault();
        idDelete = $(this).data('id');
        rowDelete = $(this);
        if (typeof idDelete !== 'undefined' && idDelete != '') {
            $('#modal-delete-user').modal();
        }
    });
    $('#modal-delete-user').on('hidden.bs.modal', function (e) {
        idDelete = null;
    });
    $('#btn-delete-user').click(function(e) {
        e.preventDefault();
        $('#modal-delete-user').modal('hide');
        if (typeof idDelete !== 'undefined' && idDelete != '' && idDelete != null) {
            $.ajax({
                url: $('#modal-delete-user').data('url'),
                type: 'POST',
                data: {id: idDelete},
                success: function(result) {
                    statusDelete = result.status;
                    $('#modal-delete-user-success .modal-body p').html(result.message);
                    $('#modal-delete-user-success').modal();
                }
            })
        }
    });
    $('#modal-delete-user-success').on('hidden.bs.modal', function (e) {
        idDelete = null;
        $('#modal-delete-user-success .modal-body p').html('');
        if (typeof rowDelete !== 'undefined' && statusDelete == true) {
            rowDelete.parents('tr').remove();
        }
    });

    $('.delete-group').click(function(e) {
        e.preventDefault();
        idDelete = $(this).data('id');
        rowDelete = $(this);
        if (typeof idDelete !== 'undefined' && idDelete != '') {
            $('#modal-delete-group').modal();
        }
    });
    $('#modal-delete-group').on('hidden.bs.modal', function (e) {
        idDelete = null;
    });
    $('#btn-delete-group').click(function(e) {
        e.preventDefault();
        $('#modal-delete-group').modal('hide');
        if (typeof idDelete !== 'undefined' && idDelete != '' && idDelete != null) {
            $.ajax({
                url: $('#modal-delete-group').data('url'),
                type: 'POST',
                data: {id: idDelete},
                success: function(result) {
                    statusDelete = result.status;
                    $('#modal-delete-group-success .modal-body p').html(result.message);
                    $('#modal-delete-group-success').modal();
                }
            })
        }
    });
    $('#modal-delete-group-success').on('hidden.bs.modal', function (e) {
        idDelete = null;
        $('#modal-delete-group-success .modal-body p').html('');
        if (typeof rowDelete !== 'undefined' && statusDelete == true) {
            rowDelete.parents('tr').remove();
        }
    });

    $('.delete-favorite').click(function(e) {
        e.preventDefault();
        idDelete = $(this).data('id');
        rowDelete = $(this);
        if (typeof idDelete !== 'undefined' && idDelete != '') {
            $('#modal-delete-favorite').modal();
        }
    });
    $('#modal-delete-favorite').on('hidden.bs.modal', function (e) {
        idDelete = null;
    });
    $('#btn-delete-favorite').click(function(e) {
        e.preventDefault();
        $('#modal-delete-favorite').modal('hide');
        if (typeof idDelete !== 'undefined' && idDelete != '' && idDelete != null) {
            $.ajax({
                url: $('#modal-delete-favorite').data('url'),
                type: 'POST',
                data: {id: idDelete},
                success: function(result) {
                    statusDelete = result.status;
                    $('#modal-delete-favorite-success .modal-body p').html(result.message);
                    $('#modal-delete-favorite-success').modal();
                }
            })
        }
    });
    $('#modal-delete-favorite-success').on('hidden.bs.modal', function (e) {
        idDelete = null;
        $('#modal-delete-favorite-success .modal-body p').html('');
        if (typeof rowDelete !== 'undefined' && statusDelete == true) {
            rowDelete.parents('tr').remove();
        }
    });

    $('.delete-item-message').click(function(e) {
        e.preventDefault();
        idDelete = $(this).data('id');
        rowDelete = $(this);
        if (typeof idDelete !== 'undefined' && idDelete != '') {
            $('#modal-delete-message').modal();
        }
    });
    $('#modal-delete-message').on('hidden.bs.modal', function (e) {
        idDelete = null;
    });
    $('#btn-delete-message').click(function(e) {
        e.preventDefault();
        $('#modal-delete-message').modal('hide');
        if (typeof idDelete !== 'undefined' && idDelete != '' && idDelete != null) {
            $.ajax({
                url: $('#modal-delete-message').data('url'),
                type: 'POST',
                data: {id: idDelete},
                success: function(result) {
                    statusDelete = result.status;
                    $('#modal-delete-message-success .modal-body p').html(result.message);
                    $('#modal-delete-message-success').modal();
                }
            })
        }
    });
    $('#modal-delete-message-success').on('hidden.bs.modal', function (e) {
        idDelete = null;
        $('#modal-delete-message-success .modal-body p').html('');
        if (typeof rowDelete !== 'undefined' && statusDelete == true) {
            rowDelete.parents('tr').remove();
        }
    });

    // force delete message
    $('.delete-force-item-message').click(function (e) {
        e.preventDefault();
        idDelete = $(this).data('id');
        rowDelete = $(this);
        if (typeof idDelete !== 'undefined' && idDelete != '') {
            $('#modal-delete-message').modal();
        }
    });

    // rollback message ng-word
    var idRollBack = rowRollBack = statusRollBack = null;
    $('.rollback-item-message').click(function (e) {
        e.preventDefault();
        idRollBack = $(this).data('id');
        rowRollBack = $(this);
        if (typeof idRollBack !== 'undefined' && idRollBack != '') {
            $('#modal-rollback-message').modal();
        }
    });
    $('#btn-rollback-message').click(function (e) {
        e.preventDefault();
        $('#modal-rollback-message').modal('hide');
        if (typeof idRollBack !== 'undefined' && idRollBack != '' && idRollBack != null) {
            $.ajax({
                url: $('#modal-rollback-message').data('url'),
                type: 'POST',
                data: { id: idRollBack },
                success: function (result) {
                    statusRollBack = result.status;
                    $('#modal-rollback-message-success .modal-body p').html(result.message);
                    $('#modal-rollback-message-success').modal();
                }
            })
        }
    });
    $('#modal-rollback-message-success').on('hidden.bs.modal', function (e) {
        idRollBack = null;
        $('#modal-rollback-message-success .modal-body p').html('');
        if (typeof rowRollBack !== 'undefined' && statusRollBack == true) {
            rowRollBack.parents('tr').remove();
        }
    });

    var typeBuy = null;
    $(document).on('click', '#btn-has-fee', function(e) {
        e.preventDefault();
        var message = '無料にしてよろしいですか？';
        if ($(this).hasClass('is-female')) {
            message = 'VIP未加入にして、よろしいですか？';
        }
        $('#modal-set-buy .modal-body p').html(message);
        $('#modal-set-buy').modal('show');
        typeBuy = 'HAS_FEE';
    });
    $(document).on('click', '#btn-no-fee', function (e) {
        e.preventDefault();
        var message = '１ヵ月有料にしてよろしいですか？';
        if ($(this).hasClass('is-female')) {
            message = '１ヵ月VIPに変更してよろしいですか？';
        }
        $('#modal-set-buy .modal-body p').html(message);
        $('#modal-set-buy').modal('show');
        typeBuy = 'NO_FEE';
    });
    $(document).on('click', '#btn-accept-buy', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        if (typeof url === 'undefined' || typeBuy == null) {
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: { type: typeBuy },
            success: function (result) {
                $('#modal-set-buy .modal-body p').html('');
                $('#modal-set-buy').modal('hide');
                if (result.STATUS == true) {
                    location.href = location.href;
                }
            }
        })
    });
    $('#modal-set-buy').on('hidden.bs.modal', function (e) {
        $('#modal-set-buy .modal-body p').html('');
        typeBuy = null;
    });
});
function chooseFileImage(obj, preview) {
    var file = obj.get(0).files[0];

    var ext = obj.val().split('.').pop().toLowerCase();
    var errorId = obj.data('error-id');
    if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
        if (typeof errorId !== 'undefined' && errorId != '') {
            $(errorId).text('Extension invalid. Only allow extenstions: gif,png,jpg,jpeg');
            $(errorId).removeClass('hide');
        }
        return false;
    } else {
        if (typeof errorId !== 'undefined' && errorId != '') {
            $(errorId).text('').hide();
        }
    }
    if(file){
        var reader = new FileReader();
        if (typeof preview !== 'undefined' && preview != '') {
            reader.onload = function(){
                preview.attr("src", reader.result);
            }
        }
        reader.readAsDataURL(file);
    }
}
function blockOrUnblock(url) {
    if (typeof url !== 'undefined' && url != '') {
        $.ajax({
            url: url,
            type: 'POST',
            success: function(result) {
                $('#modal-block').modal('hide');
                $('#modal-unblock').modal('hide');
                $('#modal-info .modal-body p').text(result.message);
                $('#modal-info').modal('show');
                if (result.STATUS == true) {
                    setTimeout(function() {
                        location.href = location.href;
                    }, 2000);
                }
            }
        });
    }
}