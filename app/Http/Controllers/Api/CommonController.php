<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Common;
use App\Http\Controllers\Controller;
use App\Http\Requests\SendMailQAApiRequest;
use App\Http\Requests\SendVerifyRequest;
use App\Jobs\SendEmailQa;
use App\Services\VerifyCodeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CommonController extends Controller
{
    protected $verifyCodeService;

    public function __construct(VerifyCodeService $service)
    {
        parent::__construct();
        $this->verifyCodeService = $service;
    }

    public function getConstants(Request $request)
    {
        try {
            $listConstants = [
                'LIST_PREFECTURE',
                'LIST_EDUCATION',
                'LIST_HUMAN_FORM',
                'LIST_SAKE',
                'LIST_SMOKE',
                'LIST_BLOOD_TYPE',
                'LIST_THING_OF_MARRIAGE',
                'LIST_ANNUAL_INCOME',
                'LIST_JOB_TYPE',
                'LIST_HAIR_FEMALE',
                'LIST_HAIR_MALE',
                'LIST_HAIR_COLOR',
                'LIST_OPTION_1',
                'LIST_OPTION_2',
                'LIST_OPTION_3',
                'LIST_OPTION_4',
                'LIST_APPEARANCE_MALE',
                'LIST_APPEARANCE_FEMALE',
                'LIST_IMPRESSIVE_MALE',
                'LIST_IMPRESSIVE_FEMALE',
            ];
            $key = $request->get('key');
            $data = [];
            if (!is_null($key)) {
                if (defined($key) && in_array($key, $listConstants)) {
                    $data = [
                        $key => $this->getConstantByKey($key)
                    ];
                } else {
                    $data = [
                        $key => $this->getConstantByKey($key)
                    ];
                }
            } else {
                $listHeights = Common::listHeight();
                $dataHeights = [];
                foreach ($listHeights as $key => $value) {
                    $dataHeights[] = [
                        'index' => $key,
                        'title' => $value
                    ];
                }
                $data = [
                    'LIST_PREFECTURE' => $this->getConstantByKey('LIST_PREFECTURE'),
                    'LIST_EDUCATION' => $this->getConstantByKey('LIST_EDUCATION'),
                    'LIST_HUMAN_FORM' => $this->getConstantByKey('LIST_HUMAN_FORM'),
                    'LIST_SAKE' => $this->getConstantByKey('LIST_SAKE'),
                    'LIST_SMOKE' => $this->getConstantByKey('LIST_SMOKE'),
                    'LIST_BLOOD_TYPE' => $this->getConstantByKey('LIST_BLOOD_TYPE'),
                    'LIST_THING_OF_MARRIAGE' => $this->getConstantByKey('LIST_THING_OF_MARRIAGE'),
                    'LIST_ANNUAL_INCOME' => $this->getConstantByKey('LIST_ANNUAL_INCOME'),
                    'LIST_JOB_TYPE' => $this->getConstantByKey('LIST_JOB_TYPE'),
                    'LIST_HAIR_FEMALE' => $this->getConstantByKey('LIST_HAIR_FEMALE'),
                    'LIST_HAIR_MALE' => $this->getConstantByKey('LIST_HAIR_MALE'),
                    'LIST_HAIR_COLOR' => $this->getConstantByKey('LIST_HAIR_COLOR'),
                    'LIST_OPTION_1' => $this->getConstantByKey('LIST_OPTION_1'),
                    'LIST_OPTION_2' => $this->getConstantByKey('LIST_OPTION_2'),
                    'LIST_OPTION_3' => $this->getConstantByKey('LIST_OPTION_3'),
                    'LIST_OPTION_4' => $this->getConstantByKey('LIST_OPTION_4'),
                    'LIST_APPEARANCE_MALE' => $this->getConstantByKey('LIST_APPEARANCE_MALE'),
                    'LIST_APPEARANCE_FEMALE' => $this->getConstantByKey('LIST_APPEARANCE_FEMALE'),
                    'LIST_IMPRESSIVE_MALE' => $this->getConstantByKey('LIST_IMPRESSIVE_MALE'),
                    'LIST_IMPRESSIVE_FEMALE' => $this->getConstantByKey('LIST_IMPRESSIVE_FEMALE'),
                    'LIST_HEIGHT' => $dataHeights,
                ];
            }
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API get all constants: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    private function getConstantByKey($key)
    {
        $data = null;
        if ($key == 'LIST_HEIGHT') {
            $listHeights = Common::listHeight();
            foreach ($listHeights as $key => $item) {
                $data[] = [
                    'title' => $item,
                    'index' => $key
                ];
            }
        }
        if (defined($key)) {
            $value = constant($key);
            if (is_array($value)) {
                foreach ($value as $key => $item) {
                    $data[] = [
                        'title' => $item,
                        'index' => $key
                    ];
                }
                return $data;
            } else {
                return $value;
            }
        } else {
            return $data;
        }
    }

    public function sendMail(SendMailQAApiRequest $request)
    {
        try {
            $user = request()->user;
            $data = $request->only(['cc', 'bcc', 'title', 'content']);
            $data['member_id'] = $user->id;
            SendEmailQa::dispatch($data);
            Log::info('Pushed email to queue');
            return response()->jsonSuccess(['status' => true, 'message' => 'Pushed email to queue. Mail will send to Admin after']);
        } catch (\Exception $e) {
            return response()->jsonSuccess(['status' => false, 'message' => $e->getMessage()]);
        }
    }

    /**
     * API 68. Send verify code
     *
     * @param SendVerifyRequest $request
     * @return JsonResponse
     */
    public function sendVerify(SendVerifyRequest $request)
    {
        try {
            $phone = $request->get('phone');
            $action = $request->get('action');
            $this->verifyCodeService->generateVerifyCode($phone, $action);
            return response()->jsonSuccess(['status' => true, 'message' => 'Sent verify code to phone number: ' . $phone]);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 68. Send verify code: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonSuccess(['status' => false, 'message' => $e->getMessage()]);
        }
    }
}
