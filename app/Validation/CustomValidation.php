<?php

namespace App\Validation;

use App\Helpers\Common;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;

/**
 * Class CustomValidation
 */
class CustomValidation extends Validator
{
    public function validateCheckNickname($attribute, $value, $parameters)
    {
        return Common::checkNickName($value);
    }

    public function validateCheckOption5($attribute, $value, $parameters)
    {
        return Common::checkOption5($value);
    }

    public function validateCheckMemberCancel($attribute, $value, $parameters)
    {
        if ($attribute == 'email' && !empty($value)) {
            $member = DB::table('members')->where('email', $value)->first();
        } elseif ($attribute == 'phone' && !empty($value)) {
            $member = DB::table('members')->where('phone', $value)->first();
        } elseif ($attribute == 'apple_user_hash' && !empty($value)) {
            $member = DB::table('members')->where('apple_user_hash', $value)->first();
        }
        if ($member) {
            $canceledAt = Carbon::parse($member->canceled_at);
            if ($member->is_canceled != 0 && !is_null($member->canceled_at)) {
                if ($canceledAt->addWeeks(2)->lessThan(now())) {
                    return false;
                }
            }
            if ($member->is_canceled == 0) {
                return false;
            }
        }
        return true;
    }

    public function validateVerifyCode($attribute, $value, $parameters)
    {
        $data = $this->data;
        $phone = $data['phone'] ?? null;
        if (!is_null($phone)) {
            $vc = DB::table('verify_codes')->where([
                'phone' => $phone,
                'type' => 1, // Login
                'code' => $value
            ])
            ->whereRaw("DATE_ADD(created_at, INTERVAL " . TIME_VERIFY_LOGIN . " MINUTE) >= '" . now()->format('Y-m-d H:i:s') . "'")
            ->orderBy('created_at', 'DESC')
            ->first();
            if ($vc) {
                return true;
            }
        }
        return false;
    }

    public function validateVerifyCodeRegister($attribute, $value, $parameters)
    {
        $data = $this->data;
        $phone = $data['phone'] ?? null;
        if (!is_null($phone)) {
            $vc = DB::table('verify_codes')->where([
                'phone' => $phone,
                'type' => 2, // Register
                'code' => $value,
            ])
            ->whereRaw("DATE_ADD(created_at, INTERVAL " . TIME_VERIFY_REGISTER . " MINUTE) >= '" . now()->format('Y-m-d H:i:s') . "'")
            ->orderBy('created_at', 'DESC')
            ->first();
            if ($vc) {
                return true;
            }
        }
        return false;
    }

    public function validateCategoryPost($attribute, $value, $parameters)
    {
        $data = $this->data;
        if ($attribute == 'category1_id') {
            if (!in_array($value, array_keys(LIST_CATEGORY_1))) {
                return false;
            }
        } elseif ($attribute == 'category2_id') {
            $category1 = $data['category1_id'] ?? null;
            if (is_null($category1) || (!is_null($category1) && !in_array($category1, array_keys(LIST_CATEGORY_2)))) {
                return false;
            }
            if (in_array($category1, array_keys(LIST_CATEGORY_2))) {
                if (!in_array($value, array_keys(LIST_CATEGORY_2[$category1]))) {
                    return false;
                }
            } else {
                return false;
            }
        } elseif ($attribute == 'category3_id') {
            $category1 = $data['category1_id'] ?? null;
            $category2 = $data['category2_id'] ?? null;
            if (in_array($category2, [11, 21])) {
                $listCategoryTrend = Common::getCategoryTrend();
                if (empty($listCategoryTrend)) {
                    return false;
                }
                if (!in_array($value, array_keys($listCategoryTrend))) {
                    return false;
                } else {
                    return true;
                }
            }
            if (is_null($category1) || (!is_null($category1) && !in_array($category1, array_keys(LIST_CATEGORY_2)))) {
                return false;
            }
            if (is_null($category2) || (!is_null($category2) && !in_array($category2, array_keys(LIST_CATEGORY_3)))) {
                return false;
            }
            if (in_array($category2, array_keys(LIST_CATEGORY_3))) {
                if (!in_array($value, array_keys(LIST_CATEGORY_3[$category2]))) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }
}
