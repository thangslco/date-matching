<?php

namespace App\Models;

/**
 * Class MemberHide
 *
 * @package namespace App\Models;
 */
class MemberHide extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'user_hided_id'
    ];
    protected $dates = ['created_at', 'updated_at'];

    public function userHide()
    {
        return $this->belongsTo('App\Models\Member', 'user_id', 'id');
    }

    public function userHided()
    {
        return $this->belongsTo('App\Models\Member', 'user_hided_id', 'id');
    }
}
