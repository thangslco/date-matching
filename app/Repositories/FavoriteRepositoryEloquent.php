<?php

namespace App\Repositories;

use App\Models\Favorite;
use App\Repositories\Contracts\FavoriteRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class FavoriteRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class FavoriteRepositoryEloquent extends BaseRepository implements FavoriteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Favorite::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getListByType($conditions, $columns = ['*'], $pageName = 'page', $page = null)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where($conditions);
        $results = $model->paginate(PAGINATE_DEFAULT, $columns, $pageName, $page);
        $results->appends(app('request')->query());
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getAll()
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->get();
        $this->resetModel();
        return $this->parserResult($model);
    }
}
