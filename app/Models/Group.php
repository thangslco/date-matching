<?php

namespace App\Models;

use App\Helpers\Common;

/**
 * Class Group
 *
 * @package namespace App\Models;
 */
class Group extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'image_id', 'joined'];

    protected $dates = ['created_at', 'updated_at'];

    public function image()
    {
        return $this->belongsTo('App\Models\Image', 'image_id', 'id');
    }

    public function getImageUrl($isDefault = false)
    {
        if (!is_null($this->image)) {
            return $this->image->getUrl();
        }
        if ($isDefault) {
            return Common::noImage();
        }
        return null;
    }
}
