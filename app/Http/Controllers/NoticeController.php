<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNoticeRequest;
use App\Services\NoticeService;
use Illuminate\Http\Request;
use Session;

class NoticeController extends Controller
{
    protected $noticeService;
    public function __construct(NoticeService $service)
    {
        parent::__construct();
        $this->noticeService = $service;
    }

    public function index(Request $request)
    {
        $menuActived = 'MENU_NOTICE';
        $items = $this->noticeService->getList($request);
        return view('notice.list', compact('items', 'menuActived'));
    }

    public function create(Request $request)
    {
        $menuActived = 'MENU_NOTICE';
        $action = route('notices.store');
        $title = __('messages.notices.lbl_heading_create');
        return view('notice.create', compact('menuActived', 'action', 'title'));
    }

    public function store(CreateNoticeRequest $request)
    {
        $this->noticeService->register($request);
        Session::flash('message', __('messages.create_notice_success'));
        return redirect()->route('notices');
    }

    public function detail(Request $request, $id)
    {
        $menuActived = 'MENU_NOTICE';
        $notice = $this->noticeService->find($id);
        if (!$notice) {
            return redirect()->route('notices')->with('error', __('messages.group_not_found'));
        }
        $request->merge($notice->toArray());
        $action = route('notices.update', ['id' => $id]);
        $title = __('messages.notices.lbl_heading_update');
        return view('notice.create', compact('menuActived', 'action', 'title', 'id'));
    }

    public function update(CreateNoticeRequest $request, $id)
    {
        $notice = $this->noticeService->find($id);
        if (!$notice) {
            return redirect()->route('notices')->with('error', __('messages.group_not_found'));
        }
        $params = $request->only(['title', 'content', 'category']);
        $this->noticeService->update($params, $id);
        Session::flash('message', __('messages.update_notice_success'));
        return redirect()->route('notices');
    }
}
