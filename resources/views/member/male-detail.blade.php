@php
use App\Helpers\Common;
@endphp
@extends('layouts.default')

@section('title', '登録詳細情報')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">登録詳細情報</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">登録詳細情報</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @if ($item->is_block == 1)
        <div class="alert alert-warning">このユーザがブロックされました。</div>
        @endif
        <form action="{{ route('member.update', request()->id )}}" method="POST">
            @csrf
            @if (session('message'))
            <div class="alert alert-success" role="alert">
                {!! session('message') !!}
            </div>
            @endif
            @if (session('error'))
            <div class="alert alert-danger" role="alert">
                {!! session('error') !!}
            </div>
            @endif
            <input type="hidden" name="type" value="male">
            <div class="row">
                <div class="col-sm-3">
                    @if ($imagesSecondary->count())
                        <div class="slider single-item">
                            <div><img width="100%" src="{!! $item->getAvatarUrl() !!}" class="rounded mx-auto d-block" alt="{{ $item->nickname }}"></div>
                            @foreach ($imagesSecondary as $keyImage => $itemImageSecondary)
                                <div><img width="100%" src="{!! $itemImageSecondary->getUrl() !!}" class="rounded mx-auto d-block" alt="image-secondary-{{ $keyImage }}"></div>
                            @endforeach
                        </div>
                    @else
                        <img width="100%" src="{!! $item->getAvatarUrl() !!}" class="rounded mx-auto d-block" alt="{{ $item->nickname }}">
                    @endif
                </div>
                <div class="col-sm-9">
                    <div class="form-group row">
                        <label for="input-nickname" class="col-sm-2 col-form-label">ニックネーム：</label>
                        <div class="col-sm-5">
                            <input type="text" name="nickname" class="form-control" value="{{ old('nickname') ?? $item->nickname }}" id="input-nickname" placeholder="ニックネーム">
                            @if ($errors->has('nickname'))
                            <span class="error invalid-feedback">{{ $errors->first('nickname') }}</span>
                            @endif
                        </div>
                        <div class="col-sm-5">
                            @php $date = null; @endphp
                           @if ($item->isHasFee($date))
                            <button type="button" id="btn-has-fee" class="btn btn-block btn-success btn-flat">有料プラン（{{ $date->format('Y/m/d') }}まで）</button>
                            @else
                            <button type="button" id="btn-no-fee" class="btn btn-block btn-secondary btn-flat">無料プラン</button>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-email" class="col-sm-2 col-form-label">年齢：</label>
                        <div class="col-sm-10">
                            <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                <input type="text" name="birthday" class="form-control datetimepicker-input" value="{{ old('birthday') ?? $item->birthday }}" data-target="#reservationdate" />
                                <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                                @if ($errors->has('birthday'))
                                <span class="error invalid-feedback">{{ $errors->first('birthday') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-address" class="col-sm-2 col-form-label">居住地：</label>
                        <div class="col-sm-10">
                            <input type="text" name="address" class="form-control" value="{{ old('address') ?? $item->address }}" id="input-address" placeholder="居住地">
                            @if ($errors->has('address'))
                            <span class="error invalid-feedback">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-email" class="col-sm-2 col-form-label">メール：</label>
                        <div class="col-sm-10">
                            <input type="text" name="email" class="form-control" value="{{ old('email') ?? $item->email }}" id="input-email" placeholder="メール">
                            @if ($errors->has('email'))
                            <span class="error invalid-feedback">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-phone" class="col-sm-2 col-form-label">電話番号：</label>
                        <div class="col-sm-10">
                            <input type="text" name="phone" class="form-control" value="{{ old('phone') ?? $item->phone }}" id="input-phone" placeholder="電話番号">
                            @if ($errors->has('phone'))
                            <span class="error invalid-feedback">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slider -->
            @if ($images && $images->count())
            <div class="row slider">
                <div class="image-slider">
                    @foreach($images as $itemImage)
                    <div class="item-slider">
                        <img src="{{ $itemImage->getUrl() }}" class="rounded mx-auto d-block" alt="{!! $itemImage->filename !!}">
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
            <!-- Categories -->
            @if($categories && $categories->count())
            <div class="row" id="categories">
                <div class="col-sm-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Categories</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @foreach($categories as $itemCate)
                            <div class="item">
                                <a href="#">
                                    <img src="{{$itemCate->getImageUrl()}}" class="rounded mx-auto d-block" alt="{!! $itemCate->name !!}">
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <!-- List peoples liked -->
            @if($tags && $tags->count())
            <div class="row" id="list-peoples-liked">
                <div class="col-sm-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">好きなタイプ</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @foreach($tags as $itemTag)
                            <a href="#">{!! $itemTag->name !!}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <!-- 基本プロフィール -->
            <div class="row" id="block-1">
                <div class="col-sm-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">基本プロフィール</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="input-place_of_birth" class="col-sm-4 col-form-label">出身地：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('place_of_birth', LIST_PREFECTURE, old('place_of_birth') ?? $item->place_of_birth, ['class' => 'form-control', 'id' => 'input-place_of_birth']) !!}
                                            @if ($errors->has('place_of_birth'))
                                            <span class="error invalid-feedback">{{ $errors->first('place_of_birth') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-education" class="col-sm-4 col-form-label">学歴：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('education', LIST_EDUCATION, old('education') ?? $item->education, ['class' => 'form-control', 'id' => 'input-education']) !!}
                                            @if ($errors->has('education'))
                                            <span class="error invalid-feedback">{{ $errors->first('education') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-height" class="col-sm-4 col-form-label">身長：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('height', Common::listHeight(), old('height') ?? $item->height, ['class' => 'form-control', 'id' => 'input-height']) !!}
                                            @if ($errors->has('height'))
                                            <span class="error invalid-feedback">{{ $errors->first('height') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-human_forms" class="col-sm-4 col-form-label">体型：</label>
                                        <div class="col-sm-8">
                                            @php
                                            $listHumanForms = old('human_forms') ?? explode(',', $item->human_forms);
                                            @endphp
                                            @foreach (LIST_HUMAN_FORM as $key => $value)
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="human_forms[]" value="{{$key}}" id="inlineCheckbox-human-{{$key}}" {{ in_array($key, $listHumanForms) ? "checked='checked'" : '' }}>
                                                <label class="form-check-label" for="inlineCheckbox-human-{{$key}}">{!! $value !!}</label>
                                            </div>
                                            @endforeach
                                            @if ($errors->has('human_forms'))
                                            <span class="error invalid-feedback">{{ $errors->first('human_forms') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-sake" class="col-sm-4 col-form-label">お酒：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('sake', LIST_SAKE, old('sake') ?? $item->sake, ['class' => 'form-control', 'id' => 'input-sake']) !!}
                                            @if ($errors->has('sake'))
                                            <span class="error invalid-feedback">{{ $errors->first('sake') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-smoke" class="col-sm-4 col-form-label">タバコ：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('smoke', LIST_SMOKE, old('smoke') ?? $item->smoke, ['class' => 'form-control', 'id' => 'input-smoke']) !!}
                                            @if ($errors->has('smoke'))
                                            <span class="error invalid-feedback">{{ $errors->first('smoke') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-blood_type" class="col-sm-4 col-form-label">血液型：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('blood_type', LIST_BLOOD_TYPE, old('blood_type') ?? $item->blood_type, ['class' => 'form-control', 'id' => 'input-blood_type']) !!}
                                            @if ($errors->has('blood_type'))
                                            <span class="error invalid-feedback">{{ $errors->first('blood_type') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-thing_of_marriage" class="col-sm-4 col-form-label">結婚への思い：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('thing_of_marriage', LIST_THING_OF_MARRIAGE, old('thing_of_marriage') ?? $item->thing_of_marriage, ['class' => 'form-control', 'id' => 'input-thing_of_marriage']) !!}
                                            @if ($errors->has('thing_of_marriage'))
                                            <span class="error invalid-feedback">{{ $errors->first('thing_of_marriage') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-appearances" class="col-sm-4 col-form-label">見た目：</label>
                                        <div class="col-sm-8">
                                            @php
                                            $appearances = old('appearances') ?? explode(',', $item->appearances);
                                            @endphp
                                            @foreach (LIST_APPEARANCE_MALE as $key => $value)
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="appearances[]" value="{{$key}}" id="inlineCheckbox-appearances-{{$key}}" {{ in_array($key, $appearances) ? "checked='checked'" : '' }}>
                                                <label class="form-check-label" for="inlineCheckbox-appearances-{{$key}}">{!! $value !!}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="input-annual_income" class="col-sm-4 col-form-label">年収：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('annual_income', LIST_ANNUAL_INCOME, old('annual_income') ?? $item->annual_income, ['class' => 'form-control', 'id' => 'input-annual_income']) !!}
                                            @if ($errors->has('annual_income'))
                                            <span class="error invalid-feedback">{{ $errors->first('annual_income') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-job_type" class="col-sm-4 col-form-label">職種：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('job_type', LIST_JOB_TYPE, old('job_type') ?? $item->job_type, ['class' => 'form-control', 'id' => 'input-job_type']) !!}
                                            @if ($errors->has('job_type'))
                                            <span class="error invalid-feedback">{{ $errors->first('job_type') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-hair" class="col-sm-4 col-form-label">髪型：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('hair', LIST_HAIR_MALE, old('hair') ?? $item->hair, ['class' => 'form-control', 'id' => 'input-hair']) !!}
                                            @if ($errors->has('hair'))
                                            <span class="error invalid-feedback">{{ $errors->first('hair') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-hair_color" class="col-sm-4 col-form-label">髪色：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('hair_color', LIST_HAIR_COLOR, old('hair_color') ?? $item->hair_color, ['class' => 'form-control', 'id' => 'input-hair_color']) !!}
                                            @if ($errors->has('hair_color'))
                                            <span class="error invalid-feedback">{{ $errors->first('hair_color') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-option_1" class="col-sm-4 col-form-label">同居人：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('option_1', LIST_OPTION_1, old('option_1') ?? $item->option_1, ['class' => 'form-control', 'id' => 'input-option_1']) !!}
                                            @if ($errors->has('option_1'))
                                            <span class="error invalid-feedback">{{ $errors->first('option_1') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-option_2" class="col-sm-4 col-form-label">会うまで期間：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('option_2', LIST_OPTION_2, old('option_2') ?? $item->option_2, ['class' => 'form-control', 'id' => 'input-option_2']) !!}
                                            @if ($errors->has('option_2'))
                                            <span class="error invalid-feedback">{{ $errors->first('option_2') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-option_3" class="col-sm-4 col-form-label">兄弟姉妹：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('option_3', LIST_OPTION_3, old('option_3') ?? $item->option_3, ['class' => 'form-control', 'id' => 'input-option_3']) !!}
                                            @if ($errors->has('option_3'))
                                            <span class="error invalid-feedback">{{ $errors->first('option_3') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-option_4" class="col-sm-4 col-form-label">子供は欲しいか：</label>
                                        <div class="col-sm-8">
                                            {!! Form::select('option_4', LIST_OPTION_4, old('option_4') ?? $item->option_4, ['class' => 'form-control', 'id' => 'input-option_4']) !!}
                                            @if ($errors->has('option_4'))
                                            <span class="error invalid-feedback">{{ $errors->first('option_4') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-impressives" class="col-sm-4 col-form-label">イメージ：</label>
                                        <div class="col-sm-8">
                                            @php
                                            $impressives = old('impressives') ?? explode(',', $item->impressives);
                                            @endphp
                                            @foreach (LIST_IMPRESSIVE_MALE as $key => $value)
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="impressives[]" value="{{$key}}" id="inlineCheckbox-impressives-{{$key}}" {{ in_array($key, $impressives) ? "checked='checked'" : '' }}>
                                                <label class="form-check-label" for="inlineCheckbox-impressives-{{$key}}">{!! $value !!}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label for="input-notes" class="col-sm-2 col-form-label">ひとこと：</label>
                                        <div class="col-sm-10">
                                            <textarea name="notes" class="form-control" id="input-notes" placeholder="自己紹介">{{ old('notes') ?? $item->notes }}</textarea>
                                            @if ($errors->has('notes'))
                                            <span class="error invalid-feedback">{{ $errors->first('notes') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-option_5" class="col-sm-2 col-form-label">自己紹介：</label>
                                        <div class="col-sm-10">
                                            <textarea name="option_5" class="form-control" id="input-option_5" placeholder="ひとこと">{{ old('option_5') ?? $item->option_5 }}</textarea>
                                            @if ($errors->has('option_5'))
                                            <span class="error invalid-feedback">{{ $errors->first('option_5') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-notes" class="col-sm-12 col-form-label">悪い言葉✖️{{$totalNgWords}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">マッチングユーザ一覧</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nickname</th>
                                        <th>Birthday</th>
                                        <th>Status</th>
                                        <th>{{__('messages.lbl_action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($listMemberMatching))
                                    @foreach ($listMemberMatching as $itemMatching)
                                    <tr class="{!! $itemMatching->getStatusBackground() !!}">
                                        <td>{{ $itemMatching->id }}</td>
                                        @if ($itemMatching->gender == 1)
                                        <td><a href="{{ route('male.detail', $itemMatching->id) }}">{!! $itemMatching->nickname !!}</a></td>
                                        @else
                                        <td><a href="{{ route('female.detail', $itemMatching->id) }}">{!! $itemMatching->nickname !!}</a></td>
                                        @endif
                                        <td>{!! $itemMatching->birthday !!}</td>
                                        <td>{!! $itemMatching->getStatusText() !!}</td>
                                        <td>
                                            <a href="{{ $itemMatching->link }}" class="btn btn-success btn-sm btn-approve">メッセージ履歴</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        @if (!empty($listMemberMatching))
                        <div class="card-footer clearfix">
                            {!! $listMemberMatching->links() !!}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-info">
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info float-right">Update</button>
                            @if ($item->is_block == 1)
                            <button type="button" class="btn btn-success float-right" id="btn-unblock" style="margin-right: 6px">ブロックを解除</button>
                            @else
                            <button type="button" class="btn btn-danger float-right" id="btn-block" style="margin-right: 6px">ブロック</button>
                            @endif
                            <a href="{{ route('male.list') }}" class="btn btn-default">Back</a>
                        </div>
                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div><!-- /.container-fluid -->
</section>
<div class="modal fade" id="modal-block">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{!! __('messages.heading_modal') !!}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>本当にブロックしたいですか。</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-do-block" data-url="{{route('member.block_unblock', ['id' => request()->id, 'status' => 'block'])}}">OK</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modal-unblock">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{!! __('messages.heading_modal') !!}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>本当にブロック解除したいですか。</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-do-unblock" data-url="{{route('member.block_unblock', ['id' => request()->id, 'status' => 'unblock'])}}">OK</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modal-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{!! __('messages.heading_modal') !!}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default float-right" data-dismiss="modal">{!! __('messages.btn_close') !!}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-set-buy">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{!! __('messages.heading_modal') !!}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-accept-buy" data-url="{{route('members.set-buy', $item->id)}}">OK</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('style')
@parent
<link rel="stylesheet" type="text/css" href="{{ url('/assets/slick/slick.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('/assets/slick/slick-theme.css') }}" />
<style>
    .slider.row {
        padding: 15px;
        margin: 10px 0px;
        background: #fff;
    }

    .image-slider {
        width: 100%;
        margin: 0px;
    }

    .image-slider .slick-list {
        width: 100%;
        height: 140px;
        margin: 0px;
        padding: 10px 0px;
    }

    .image-slider div.item-slider {
        width: auto;
        height: 140px;
        background: white;
        border: 1px solid #ccc;
        margin: 0px 10px;
        padding: 5px;
    }

    .image-slider div.item-slider img {
        height: 100%;
        width: auto;
    }

    #categories .item {
        float: left;
    }

    #categories .item img {
        height: 140px;
        width: auto;
        padding: 6px;
    }

    #list-peoples-liked a {
        display: inline-block;
        margin: 0px 6px;
        border: 1px solid #db26de;
        border-radius: 20px;
        padding: 5px 10px;
        color: #db26de;
    }

    #list-peoples-liked a:hover {
        color: blue;
        border: 1px solid blue;
    }
</style>
@endsection
@section('script')
@parent
<script src="{{ url('/assets/admin-lte/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script type="text/javascript" src="{{ url('/assets/slick/slick.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '#btn-do-block', function(e) {
            e.preventDefault();
            var url = $(this).data('url');
            blockOrUnblock(url);
        });
        $(document).on('click', '#btn-do-unblock', function(e) {
            e.preventDefault();
            var url = $(this).data('url');
            blockOrUnblock(url);
        });
        $(document).on('click', '#btn-block', function(e) {
            e.preventDefault();
            $('#modal-block').modal('show');
        });
        $(document).on('click', '#btn-unblock', function(e) {
            e.preventDefault();
            $('#modal-unblock').modal('show');
        });
        $('#reservationdate').datetimepicker({
            format: 'YYYY/MM/DD'
        });
        $("input[data-bootstrap-switch]").each(function() {
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });
        $('.single-item').slick({
            infinite: false
        });
        $('.image-slider').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            centerMode: true,
            variableWidth: true,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
</script>
@endsection