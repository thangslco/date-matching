<?php

namespace App\Console\Commands;

use App\Services\MemberService;
use Illuminate\Console\Command;

class UpdateTimeForItemRocket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_time_for_item_rocket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update time for members use item rocket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(MemberService $service)
    {
        try {
            $service->forcedUpdate([
                'item_rocket_date' => null
            ], [
                ['item_rocket_date', '<', now()->format(DATETIME_FORMAT)]
            ]);
            $this->info('Update time for member use item rocket success');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
