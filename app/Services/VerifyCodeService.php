<?php

namespace App\Services;

use App\Repositories\Contracts\VerifyCodeRepository;
use Telnyx\Message;
use Telnyx\Telnyx;

class VerifyCodeService extends BaseService
{
    /**
     * VerifyCodeService constructor.
     * @param VerifyCodeRepository $repository
     */
    public function __construct(
        VerifyCodeRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function generateVerifyCode($phone, $action)
    {
        $key = env('TELNYS_KEY');
        $masterPhone = env('TELNYS_PHONE');
        if (empty($key) || empty($masterPhone)) {
            throw new \Exception("Not found Phone or Key API", 400);
        }
        Telnyx::setApiKey($key);

        $your_telnyx_number = $masterPhone;
        $code = rand(100000, 999999);
        $message = 'Your verify code is ' . $code;
        Message::Create(['from' => $your_telnyx_number, 'to' => $phone, 'text' => $message]);
        $this->repository->updateOrCreate([
            'phone' => $phone,
            'type' => $action,
            'created_at' => date('Y-m-d H:i:s')
        ], [
            'phone' => $phone,
            'code' => $code,
            'type' => $action,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }

}
