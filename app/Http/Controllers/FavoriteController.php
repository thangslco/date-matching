<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFavoriteRequest;
use App\Services\FavoriteService;
use Illuminate\Http\Request;
use Session;

class FavoriteController extends Controller
{
    protected $favoriteService;
    public function __construct(FavoriteService $service)
    {
        parent::__construct();
        $this->favoriteService = $service;
    }

    public function index(Request $request)
    {
        $menuActived = 'MENU_FAVORITE';
        $listPinks = $this->favoriteService->getListByType($request, TYPE_FAVORITE_PINK, 'p1');
        $listBlues = $this->favoriteService->getListByType($request, TYPE_FAVORITE_BLUE, 'p2');
        $listOranges = $this->favoriteService->getListByType($request, TYPE_FAVORITE_ORANGE, 'p3');
        $listGreens = $this->favoriteService->getListByType($request, TYPE_FAVORITE_GREEN, 'p4');
        $listReds = $this->favoriteService->getListByType($request, TYPE_FAVORITE_RED, 'p5');
        $tabActive = 'p1';
        if (!is_null($request->get('p2'))) {
            $tabActive = 'p2';
        } elseif (!is_null($request->get('p3'))) {
            $tabActive = 'p3';
        } elseif (!is_null($request->get('p4'))) {
            $tabActive = 'p4';
        } elseif (!is_null($request->get('p5'))) {
            $tabActive = 'p5';
        }
        if (!is_null($request->get('tab'))) {
            $tabActive = $request->get('tab');
        }
        return view('favorite.list', compact('listPinks', 'listBlues', 'listOranges', 'listGreens', 'listReds', 'menuActived', 'tabActive'));
    }

    public function create(Request $request)
    {
        $menuActived = 'MENU_FAVORITE';
        $action = route('favorites.store');
        $title = __('messages.favorites.lbl_heading_create');
        $type = 1;
        if ($request->get('tab') == 'p2') {
            $type = 2;
        } elseif ($request->get('tab') == 'p3') {
            $type = 3;
        } elseif ($request->get('tab') == 'p4') {
            $type = 4;
        } elseif ($request->get('tab') == 'p5') {
            $type = 5;
        }
        $request->merge(['type' => $type]);
        return view('favorite.create', compact('menuActived', 'action', 'title'));
    }

    public function store(CreateFavoriteRequest $request)
    {
        $type = $request->get('type');
        $this->favoriteService->register($request);
        Session::flash('message', __('messages.create_favorites_success'));
        return redirect()->route('favorites', ['tab' => 'p' . $type]);
    }

    public function detail(Request $request, $id)
    {
        $menuActived = 'MENU_FAVORITE';
        $favorite = $this->favoriteService->find($id);
        if (!$favorite) {
            return redirect()->route('favorites')->with('error', __('messages.favorites_not_found'));
        }
        $request->merge($favorite->toArray());
        $action = route('favorites.update', ['id' => $id]);
        $title = __('messages.favorites.lbl_heading_update');
        return view('favorite.create', compact('menuActived', 'action', 'title', 'id'));
    }

    public function update(CreateFavoriteRequest $request, $id)
    {
        $favorite = $this->favoriteService->find($id);
        if (!$favorite) {
            return redirect()->route('favorites')->with('error', __('messages.favorites_not_found'));
        }
        $this->favoriteService->updateFavorite($request, $favorite);
        Session::flash('message', __('messages.update_favorites_success'));
        $type = $request->get('type');
        return redirect()->route('favorites', ['tab' => 'p' . $type]);
    }

    public function delete(Request $request)
    {
        try {
            $id = $request->get('id');
            if (!$id) {
                return response()->json(['status' => false, 'message' => __('messages.invalid_param')]);
            }
            $favorite = $this->favoriteService->find($id);
            if (!$favorite) {
                return response()->json(['status' => false, 'message' => __('messages.favorites_not_found')]);
            }
            $this->favoriteService->delete($id);
            return response()->json(['status' => true, 'message' => __('messages.delete_favorites_success')]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }
}
