<?php

namespace App\Services;

use App\Helpers\Common;
use App\Repositories\Contracts\LikeRepository;
use App\Repositories\Contracts\PostLikeRepository;
use App\Repositories\Contracts\PostRepository;
use Illuminate\Support\Facades\Log;

class PostService extends BaseService
{
    /**
     * PostService constructor
     * @param PostRepository $repository
     * @param PostLikeRepository $postLikeRepository
     * @param LikeRepository $likeRepository
     */
    public function __construct(
        PostRepository $repository,
        PostLikeRepository $postLikeRepository,
        LikeRepository $likeRepository
    ) {
        $this->repository = $repository;
        $this->postLikeRepository = $postLikeRepository;
        $this->likeRepository = $likeRepository;
    }

    public function replyPost($params)
    {
        $post = $this->repository->firstByConditions([
            'id' => $params['id'],
            'status' => 1
        ]);
        if (!$post) {
            throw new \Exception('Post not found', 400);
        }
        $user = request()->user;
        $listMemberBlocked = Common::getListMemberBlocked($user->id, $post->member_id);
        if (!is_array($listMemberBlocked) && $listMemberBlocked == $post->member_id) {
            throw new \Exception(trans('message.member_create_post_blocked'), 400);
        }
        $listMemberHided = Common::getListMemberHided($user->id, $post->member_id);
        if (!is_array($listMemberHided) && $listMemberHided == $post->member_id) {
            throw new \Exception(trans('message.member_create_post_hided'), 400);
        }
        $checkMaxPostOnOneDay = $this->repository->getTotalPostOnOneDay($post, $user);
        if ($checkMaxPostOnOneDay >= 10) {
            throw new \Exception("よびかけでお誘いできる人数は一日10人までです", 400);
        }
        $data = [
            'post_id' => $post->id,
            'member_id' => $params['member_id'],
            'owner_id' => $post->member_id,
            'content' => $params['message'],
            'is_viewed' => 0
        ];
        return $this->repository->create($data);
    }

    public function stopPost($member, $postId)
    {
        $post = $this->repository->firstByConditions(['id' => $postId]);
        if (!$post) {
            throw new \Exception('Post not found', 400);
        }
        if ($post->member_id != $member->id) {
            throw new \Exception("Permission deny", 400);
        }
        if ($post->status == 0) {
            throw new \Exception("This post stoped", 400);
        }
        return $this->repository->forcedUpdate([
            'status' => 0
        ], ['id' => $postId]);
    }

    public function search($member, $params)
    {
        $conditions = [
            ['members.gender', '=', 0]
        ];
        if (isset($params['age_min']) && !is_null($params['age_min'])) {
            $ageMin = $params['age_min'];
            $dateMin = now()->subYears($ageMin)->format(DATE_FORMAT);
            $conditions[] = ['members.birthday', '<=', $dateMin];
        }
        if (isset($params['age_max']) && !is_null($params['age_max'])) {
            $ageMax = $params['age_max'];
            $dateMax = now()->subYears($ageMax)->format(DATE_FORMAT);
            $conditions[] = ['members.birthday', '>=', $dateMax];
        }
        if (isset($params['address']) && !is_null($params['address'])) {
            $address = $params['address'];
            $conditions[] = ['members.address', 'LIKE', "%$address%"];
        }
        $conditions[] = ['members.is_pause', '=', 0];
        $conditions[] = ['members.is_canceled', '=', 0];
        $items = $this->repository->search($conditions);
        $results = [];
        if ($items) {
            $data = [];
            foreach ($items as $item) {
                $data[] = $this->getInfoPost($item);
            }
            $results = [
                'total' => $items->total(),
                'per_page' => $items->perPage(),
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'more_page' => $items->hasMorePages(),
                'data' => $data
            ];
        }
        return $results;
    }

    public function like($user, $postId)
    {
        $post = $this->repository->firstByConditions([
            'id' => $postId,
            'status' => 1
        ]);
        if (!$post) {
            throw new \Exception('Post not found', 400);
        }
        $listMemberBlocked = Common::getListMemberBlocked($user->id, $post->member_id);
        if (!is_array($listMemberBlocked) && $listMemberBlocked == $post->member_id) {
            throw new \Exception(trans('message.member_blocked'), 400);
        }
        $listMemberHided = Common::getListMemberHided($user->id, $post->member_id);
        if (!is_array($listMemberHided) && $listMemberHided == $post->member_id) {
            throw new \Exception(trans('message.member_hided'), 400);
        }

        if ($user->gender != 1) {
            $this->forceMatchingMember($user->id, $post->member_id);
        }

        $data = [
            'post_id' => $postId,
            'member_id' => $user->id,
            'target_id' => $post->member_id
        ];
        $item = $this->postLikeRepository->firstByConditions($data);
        if ($item) {
            throw new \Exception('You liked this post', 400);
        }
        $result = $this->postLikeRepository->create($data);
        if ($result) {
            $this->repository->forcedUpdate([
                'is_liked' => 1,
                'member_id_liked' => $user->id
            ], ['id' => $post->id]);
            return $result;
        }
        return false;
    }

    public function forceMatchingMember($user1, $user2)
    {
        $checkMatching = $this->likeRepository->checkMatching($user1, $user2);
        $dateMatching = date(DATETIME_FORMAT);
        if ($checkMatching == 1) {
            $this->likeRepository->create([
                'id_from' => $user2,
                'id_to' => $user1,
                'date_matching' => $dateMatching,
                'id_matching' => $user2
            ]);
            $this->likeRepository->forcedUpdate([
                'is_viewed' => 1,
                'date_matching' => $dateMatching
            ], [
                'id_from' => $user1,
                'id_to' => $user2,
            ]);
        } elseif ($checkMatching == 3) {
            $this->likeRepository->create([
                'id_from' => $user1,
                'id_to' => $user2,
                'date_matching' => $dateMatching,
                'id_matching' => $user1
            ]);
            $this->likeRepository->forceUpdate([
                'is_viewed' => 1,
                'date_matching' => $dateMatching
            ], [
                'id_from' => $user2,
                'id_to' => $user1,
            ]);
        } elseif ($checkMatching == 0) {
            $this->likeRepository->create([
                'id_from' => $user1,
                'id_to' => $user2,
                'date_matching' => $dateMatching,
                'is_viewed' => 1,
            ]);
            $this->likeRepository->create([
                'id_from' => $user2,
                'id_to' => $user1,
                'date_matching' => $dateMatching,
                'id_matching' => $user2
            ]);
        }
    }

    public function updatePostsLiked()
    {
        try {
            $listLikesPosts = $this->postLikeRepository->all(['post_id', 'member_id']);
            foreach ($listLikesPosts as $itemLike) {
                $this->repository->update([
                    'is_liked' => 1,
                    'member_id_liked' => $itemLike->member_id
                ], $itemLike->post_id);
            }
            Log::info('Update liked posts success');
        } catch (\Exception $ex) {
            Log::error('Exception: ' . $ex->getMessage());
        }
    }

    public function viewed($member, $postId)
    {
        $post = $this->repository->firstByConditions(['id' => $postId, 'owner_id' => $member->id]);
        if (!$post) {
            throw new \Exception('Post not found', 400);
        }
        $postParent = $this->repository->firstByConditions(['id' => $post->post_id, 'member_id' => $member->id]);
        if (!$postParent) {
            throw new \Exception('Post parent not found', 400);
        }
        $listMemberBlocked = Common::getListMemberBlocked($member->id, $post->member_id);
        if (!is_array($listMemberBlocked) && $listMemberBlocked == $post->member_id) {
            throw new \Exception(trans('message.member_blocked'), 400);
        }
        $listMemberHided = Common::getListMemberHided($member->id, $post->member_id);
        if (!is_array($listMemberHided) && $listMemberHided == $post->member_id) {
            throw new \Exception(trans('message.member_hided'), 400);
        }
        return $this->repository->forcedUpdate(['is_viewed' => 0], ['id' => $postId]);
    }

    public function listMessageByPostId($member, $postId)
    {
        $post = $this->repository->firstByConditions([
            'id' => $postId,
            'status' => 1
        ]);
        if (!$post) {
            throw new \Exception('Post not found', 400);
        }
        $conditions = [
            'post_id' => $postId
        ];
        $listMemberBlocked = Common::getListMemberBlocked($member->id);
        if (!empty($listMemberBlocked) && in_array($post->member_id, $listMemberBlocked)) {
            throw new \Exception(trans('message.member_blocked'), 400);
        }
        $listMemberHided = Common::getListMemberHided($member->id);
        if (!empty($listMemberHided) && in_array($post->member_id, $listMemberHided)) {
            throw new \Exception(trans('message.member_hided'), 400);
        }
        if (!empty($listMemberBlocked)) {
            $conditions['member_blocked'] = $listMemberBlocked;
        }
        if (!empty($listMemberHided)) {
            $conditions['member_hided'] = $listMemberHided;
        }
        return $this->repository->listMessageByPostId($conditions);
    }

    public function listPosts()
    {
        $conditions = $results = [];
        $user = request()->user;
        $listMemberBlocked = Common::getListMemberBlocked($user->id);
        if (is_array($listMemberBlocked) && !empty($listMemberBlocked)) {
            $conditions['member_blocked'] = $listMemberBlocked;
        }
        $listMemberHided = Common::getListMemberHided($user->id);
        if (is_array($listMemberHided) && !empty($listMemberHided)) {
            $conditions['member_hided'] = $listMemberHided;
        }
        $listPostLiked = Common::getListPostLiked($user->id);
        if (is_array($listPostLiked) && !empty($listPostLiked)) {
            $conditions['post_liked'] = $listPostLiked;
        }
        $listPostIdReplied = $this->repository->getListPostReplied($user);
        if ($listPostIdReplied->count()) {
            $conditions['post_replied'] = $listPostIdReplied->toArray();
        }
        $items = $this->repository->listPosts($conditions);
        if ($items) {
            $data = [];
            foreach ($items as $item) {
                $data[] = $this->getInfoPost($item);
            }
            $results = [
                'total' => $items->total(),
                'per_page' => $items->perPage(),
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'more_page' => $items->hasMorePages(),
                'data' => $data
            ];
        }
        return $results;
    }

    private function getInfoPost($item)
    {
        return [
            'id' => $item->id,
            'category_1' => $item->getCategory1(),
            'category_2' => $item->getCategory2(),
            'category_3' => $item->getCategory3(),
            'content' => $item->content,
            'type_time' => $item->type_time,
            'type_time_text' => $item->getTypeTimeText(),
            'type_time_current' => $item->getTypeTimeCurrent(),
            'time_remain' => $item->getTimeRemain(),
            'date' => !is_null($item->date) ? $item->date->format(DATETIME_FORMAT) : null,
            'status' => $item->status,
            'created_at' => !is_null($item->created_at) ? $item->created_at->format(DATETIME_FORMAT) : null,
            'updated_at' => !is_null($item->updated_at) ? $item->updated_at->format(DATETIME_FORMAT) : null,
            'member' => !is_null($item->member) ? Common::getInfoMember($item->member, [], true) : []
        ];
    }

    public function getPostLatest($memberId)
    {
        $data = [];
        $post = $this->repository->getPostLatest($memberId);
        if ($post) {
            if ($post->status != 1) {
                $data = [];
            } else {
                $conditions = [
                    'post_id' => $post->id
                ];
                $listMemberBlocked = Common::getListMemberBlocked($memberId);
                if (!empty($listMemberBlocked)) {
                    $conditions['member_blocked'] = $listMemberBlocked;
                }
                $listMemberHided = Common::getListMemberHided($memberId);
                if (!empty($listMemberHided)) {
                    $conditions['member_hided'] = $listMemberHided;
                }
                $listMessages = $this->repository->listMessageByPostId($conditions, false);
                $messages = [];
                foreach ($listMessages as $item) {
                    $itemData = [
                        'id' => $item->id,
                        'post_id' => $item->post_id,
                        'owner_id' => $item->owner_id,
                        'content' => $item->content,
                        'is_viewed' => $item->is_viewed,
                        'is_liked' => $item->is_liked,
                        'member' => Common::getInfoMember($item->member, [], true),
                        'created_at' => !is_null($item->created_at) ? $item->created_at->format(DATETIME_FORMAT) : null,
                        'updated_at' => !is_null($item->updated_at) ? $item->updated_at->format(DATETIME_FORMAT) : null,
                    ];
                    $messages[] = $itemData;
                }
                $data = [
                    'post' => $this->getInfoPost($post),
                    'messages' => $messages
                ];
            }
        }
        return $data;
    }

    public function getTotalPosts()
    {
        return $this->repository->getTotalPosts();
    }
}
