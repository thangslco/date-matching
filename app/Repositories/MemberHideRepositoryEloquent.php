<?php

namespace App\Repositories;

use App\Models\MemberHide;
use App\Repositories\Contracts\MemberHideRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class MemberHideRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class MemberHideRepositoryEloquent extends BaseRepository implements MemberHideRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MemberHide::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function listMemberHided($memberId)
    {
        $this->applyCriteria();
        $this->applyScope();
        $results = $this->model->with('userHided')
                ->whereHas('userHided', function ($query) {
                    $query->where('is_pause', 0)
                          ->where('is_canceled', 0);
                })
                ->where('user_id', $memberId)->get();
        $this->resetModel();
        return $this->parserResult($results);
    }
}
