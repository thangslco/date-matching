<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\DB;

class MemberFindApi extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $params = $this->route()->parameters();
        $id = $params['id'] ?? null;
        $gender = 0;
        if (!is_null($id)) {
            $member = DB::table('members')->select('gender')->where('id', $id)->first();
            if ($member) {
                $gender = $member->gender;
            }
        }
        $rules = [
            'age' => 'integer',
            'height' => '',
            'address' => '',
            'option_2' => '',
        ];
        $dataHumanForms = $this->get('human_forms');
        if (!is_null($dataHumanForms)) {
            $listKeysDataHumanForms = array_keys($dataHumanForms);
            foreach ($listKeysDataHumanForms as $key) {
                $rules['human_forms.' . $key] = 'nullable|in:' . implode(',', array_keys(LIST_HUMAN_FORM));
            }
        }
        $listAppearances = LIST_APPEARANCE_MALE;
        if ($gender != 1) {
            $listAppearances = LIST_APPEARANCE_FEMALE;
        }
        $dataAppearances = $this->get('appearances');
        if (!is_null($dataAppearances)) {
            $listKeysDataAppearances = array_keys($dataAppearances);
            foreach ($listKeysDataAppearances as $key) {
                $rules['appearances.' . $key] = 'nullable|in:' . implode(',', array_keys($listAppearances));
            }
        }
        $listImpressives = LIST_IMPRESSIVE_MALE;
        if ($gender != 1) {
            $listImpressives = LIST_IMPRESSIVE_FEMALE;
        }
        $dataImpressives = $this->get('impressives');
        if (!is_null($dataImpressives)) {
            $listKeysDataImpressives = array_keys($dataImpressives);
            foreach ($listKeysDataImpressives as $key) {
                $rules['impressives.' . $key] = 'nullable|in:' . implode(',', array_keys($listImpressives));
            }
        }
        return $rules;
    }

    public function messages()
    {
        $messages = [];
        $dataHumanForms = $this->get('human_forms');
        if (!is_null($dataHumanForms)) {
            $listKeysDataHumanForms = array_keys($dataHumanForms);
            foreach ($listKeysDataHumanForms as $key) {
                $messages['human_forms.' . $key] = "Value of 体格 at {$key} invalid";
            }
        }
        $dataAppearances = $this->get('appearances');
        if (!is_null($dataAppearances)) {
            $listKeysDataAppearances = array_keys($dataAppearances);
            foreach ($listKeysDataAppearances as $key) {
                $messages['appearances.' . $key] = "Value of 見た目 at {$key} invalid";
            }
        }
        $dataImpressives = $this->get('impressives');
        if (!is_null($dataImpressives)) {
            $listKeysDataImpressives = array_keys($dataImpressives);
            foreach ($listKeysDataImpressives as $key) {
                $messages['impressives.' . $key] = "Value of イメージ at {$key} invalid";
            }
        }
        return $messages;
    }
}
