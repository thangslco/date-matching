<?php

namespace App\Repositories;

use App\Models\VerifyCode;
use App\Repositories\Contracts\VerifyCodeRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class VerifyCodeRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class VerifyCodeRepositoryEloquent extends BaseRepository implements VerifyCodeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return VerifyCode::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
