<?php

namespace App\Repositories;

use App\Models\Transaction;
use App\Repositories\Contracts\TransactionRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class TransactionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class TransactionRepositoryEloquent extends BaseRepository implements TransactionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Transaction::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getTotalMoney()
    {
        $this->applyCriteria();
        $this->applyScope();
        $totalMoney = $this->model
                        ->select('amount')
                        ->whereIn('type', [TRANSACTION_TYPE_LIKE, TRANSACTION_TYPE_VIP, TRANSACTION_TYPE_PAID])
                        ->sum('amount');
        $this->resetModel();
        return $this->parserResult($totalMoney);
    }

    public function getTotalMoneyByYear($year)
    {
        $this->applyCriteria();
        $this->applyScope();
        $totalMoney = $this->model
                        ->select('amount')
                        ->whereIn('type', [TRANSACTION_TYPE_LIKE, TRANSACTION_TYPE_VIP, TRANSACTION_TYPE_PAID])
                        ->whereYear('created_at', $year)
                        ->sum('amount');
        $this->resetModel();
        return $this->parserResult($totalMoney);
    }

    public function getTotalMoneyByMonth($year, $month)
    {
        $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        $this->applyCriteria();
        $this->applyScope();
        $totalMoney = $this->model
                        ->select('amount')
                        ->whereIn('type', [TRANSACTION_TYPE_LIKE, TRANSACTION_TYPE_VIP, TRANSACTION_TYPE_PAID])
                        ->whereRaw(DB::raw("YEAR(created_at) = $year"))
                        ->whereRaw(DB::raw("MONTH(created_at) = $month"))
                        ->sum('amount');
        $this->resetModel();
        return $this->parserResult($totalMoney);
    }

    public function getTotalMoneyByType($type, $date)
    {
        $this->applyCriteria();
        $this->applyScope();
        $totalMoney = $this->model
                        ->select('amount')
                        ->whereIn('type', [TRANSACTION_TYPE_LIKE, TRANSACTION_TYPE_VIP, TRANSACTION_TYPE_PAID])
                        ->where('created_at', ">=", $date)
                        ->where('type', $type)
                        ->sum('amount');
        $this->resetModel();
        return $this->parserResult($totalMoney);
    }

    public function getTotalLikeByType($memberId, $type)
    {
        $this->applyCriteria();
        $this->applyScope();
        $totalLike = $this->model
                        ->select('quantity')
                        ->where('member_id', $memberId)
                        ->where('type', $type)
                        ->sum('quantity');
        $this->resetModel();
        return $this->parserResult($totalLike);
    }
}
