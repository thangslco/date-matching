<?php

namespace App\Services;

use App\Helpers\Common;
use App\Http\Requests\MemberFindApi;
use App\Http\Requests\MemberRegister;
use App\Http\Requests\MemberUpdateApi;
use App\Jobs\PushNotificationAndroid;
use App\Jobs\PushNotificationIos;
use App\Repositories\Contracts\GroupRepository;
use App\Repositories\Contracts\ImageRepository;
use App\Repositories\Contracts\ItemRepository;
use App\Repositories\Contracts\LikeRepository;
use App\Repositories\Contracts\MailBoxRepository;
use App\Repositories\Contracts\MemberBlockRepository;
use App\Repositories\Contracts\MemberHideRepository;
use App\Repositories\Contracts\MemberRepository;
use App\Repositories\Contracts\MessageRepository;
use App\Repositories\Contracts\NextRepository;
use App\Repositories\Contracts\SettingRepository;
use App\Repositories\Contracts\TransactionRepository;
use App\Repositories\Contracts\UserTokenRepository;
use App\Repositories\Contracts\VerifyCodeRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class MemberService extends BaseService
{
    protected $imageRepository;
    protected $groupRepository;
    protected $userTokenRepository;
    protected $likeRepository;
    protected $nextRepository;
    protected $itemRepository;
    protected $mailBoxRepository;
    protected $memberBlockRepository;
    protected $memberHideRepository;
    protected $messageRepository;
    protected $settingRepository;
    protected $transactionRepository;
    protected $verifyCodeRepository;
    /**
     * MemberService constructor.
     * @param MemberRepository $repository
     * @param ImageRepository $imageRepository
     * @param GroupRepository $groupRepository
     * @param UserTokenRepository $userTokenRepository
     * @param LikeRepository $likeRepository
     * @param NextRepository $nextRepository
     * @param ItemRepository $itemRepository
     * @param MailBoxRepository $mailBoxRepository
     * @param MemberBlockRepository $memberBlockRepository
     * @param MemberHideRepository $memberHideRepository
     * @param MessageRepository $messageRepository
     * @param SettingRepository $settingRepository
     * @param TransactionRepository $transactionRepository
     * @param VerifyCodeRepository $verifyCodeRepository
     */
    public function __construct(
        MemberRepository $repository,
        ImageRepository $imageRepository,
        GroupRepository $groupRepository,
        UserTokenRepository $userTokenRepository,
        LikeRepository $likeRepository,
        NextRepository $nextRepository,
        ItemRepository $itemRepository,
        MailBoxRepository $mailBoxRepository,
        MemberBlockRepository $memberBlockRepository,
        MemberHideRepository $memberHideRepository,
        MessageRepository $messageRepository,
        SettingRepository $settingRepository,
        TransactionRepository $transactionRepository,
        VerifyCodeRepository $verifyCodeRepository
    ) {
        $this->repository = $repository;
        $this->imageRepository = $imageRepository;
        $this->groupRepository = $groupRepository;
        $this->userTokenRepository = $userTokenRepository;
        $this->likeRepository = $likeRepository;
        $this->nextRepository = $nextRepository;
        $this->itemRepository = $itemRepository;
        $this->mailBoxRepository = $mailBoxRepository;
        $this->memberBlockRepository = $memberBlockRepository;
        $this->memberHideRepository = $memberHideRepository;
        $this->messageRepository = $messageRepository;
        $this->settingRepository = $settingRepository;
        $this->transactionRepository = $transactionRepository;
        $this->verifyCodeRepository = $verifyCodeRepository;
    }

    public function register(MemberRegister $request, $gender)
    {
        DB::beginTransaction();
        try {
            $data = $request->only(['nickname', 'birthday', 'address', 'phone', 'job_type', 'height', 'smoke', 'sake', 'email', 'option_1', 'groups_join', 'image_verify', 'apple_user_hash']);
            $user = $this->repository->findMemberByEmailPhone($data);
            if ($user) {
                throw new \Exception("Email or Phone registed", 400);
            }
            $uploadedFile = $request->file('avatar');
            if ($uploadedFile) {
                $filename = $this->upload($uploadedFile);
                $image = $this->imageRepository->create([
                    'filename' => $filename,
                    'type' => IMAGE_TYPE_MEMBER
                ]);
                $data['avatar_id'] = !is_null($image) ? $image->id : null;
            }
            if ($gender == 1) {
                $data['gender'] = 1;
                $data['status'] = STATUS_DEFAULT;
                $uploadedImageVerify = $request->file('image_verify');
                if ($uploadedImageVerify) {
                    $filename = $this->upload($uploadedImageVerify);
                    $image = $this->imageRepository->create([
                        'filename' => $filename,
                        'type' => IMAGE_TYPE_MEMBER
                    ]);
                    $data['image_verify_id'] = !is_null($image) ? $image->id : null;
                }
            } else {
                $data['gender'] = 0;
                $data['status'] = STATUS_DEFAULT;
                $data['human_forms'] = Common::setFieldMultiData($request->get('human_forms'));
                $data['appearances'] = Common::setFieldMultiData($request->get('appearances'));
                $data['impressives'] = Common::setFieldMultiData($request->get('impressives'));
            }
            $groupJoined = $request->get('groups_join');
            $data['groups_join'] = Common::setFieldMultiData($groupJoined);
            $result = $this->create($data);
            if ($result) {
                $this->groupRepository->updateTotalJoined($groupJoined);
                if ($gender == 1) {
                    $listImages = $request->file('images');
                    $images = [];
                    if (!empty($listImages)) {
                        foreach ($listImages as $itemImage) {
                            $filename = $this->upload($itemImage);
                            $img = $this->imageRepository->create([
                                'filename' => $filename,
                                'target_id' => $result->id,
                                'type' => IMAGE_TYPE_MEMBER
                            ]);
                            $images[] = [
                                'id' => $img->id,
                                'url' => $img->getUrl()
                            ];
                        }
                    }
                }
                $this->verifyCodeRepository->deleteWhere([
                    'type' => 2,
                    'phone' => $result->phone
                ]);
            }
            DB::commit();
            return $result;
        } catch (Exception $e) {
            DB::rollBack();
            Log::error('Exception: ' . $e->getMessage());
            throw new Exception("Create member fail", 400);
        }
    }

    public function checkMemberCancel($request)
    {
        $email = $request->get('email');
        $phone = $request->get('phone');
        $appleUserHash = $request->get('apple_user_hash');
        $table = DB::table('members')->whereNotNull('canceled_at');
        if (!empty($email)) {
            $member = $table->where('email', $email)->first();
        } elseif (!empty($phone)) {
            $member = $table->where('phone', $phone)->first();
        } elseif (!empty($appleUserHash)) {
            $member = $table->where('apple_user_hash', $appleUserHash)->first();
        }
        if ($member) {
            $canceledAt = Carbon::parse($member->canceled_at);
            if ($member->is_canceled != 0 && !is_null($member->canceled_at)) {
                if ($canceledAt->addWeeks(2)->greaterThan(now())) {
                    $this->forceDeleteMember($member->id);
                    Log::info('Deleted member canceled > 14 days');
                }
            }
        }
    }

    public function forceDeleteMember($memberId) {
        DB::table('images')->where('target_id', $memberId)->delete();
        DB::table('items')->where('member_id', $memberId)->delete();
        DB::table('likes')->where('id_from', $memberId)->orWhere('id_to', $memberId)->delete();
        DB::table('mail_boxes')->where('user1_id', $memberId)->orWhere('user2_id', $memberId)->delete();
        DB::table('member_blocks')->where('user_block_id', $memberId)->orWhere('user_blocked_id', $memberId)->delete();
        DB::table('member_category')->where('member_id', $memberId)->delete();
        DB::table('member_favorite')->where('member_id', $memberId)->delete();
        DB::table('member_hides')->where('user_hided_id', $memberId)->orWhere('user_id', $memberId)->delete();
        DB::table('member_tag')->where('member_id', $memberId)->delete();
        DB::table('messages')->where('user_no', $memberId)->delete();
        DB::table('nexts')->where('owner_id', $memberId)->orWhere('id_next', $memberId)->delete();
        DB::table('posts')->where('owner_id', $memberId)->orWhere('member_id', $memberId)->delete();
        DB::table('post_likes')->where('owner_id', $memberId)->orWhere('target_id', $memberId)->delete();
        DB::table('reports')->where('user_report_id', $memberId)->orWhere('user_reported_id', $memberId)->delete();
        DB::table('transactions')->where('member_id', $memberId)->delete();
        DB::table('user_tokens')->where('user_id', $memberId)->delete();
        DB::table('members')->where('id', $memberId)->delete();
    }

    public function updateProfile(MemberUpdateApi $request, $id)
    {
        DB::beginTransaction();
        try {
            $item = $this->find($id);
            if (!$item) {
                throw new Exception("Not found member", 400);
            }
            $data = $request->only([
                'nickname', 'email', 'birthday', 'address', 'phone', 'place_of_birth', 'education', 'height', 'human_forms', 'sake', 'smoke', 'blood_type', 'thing_of_marriage', 'annual_income', 'job_type', 'hair', 'hair_color', 'option_1', 'option_2', 'option_3', 'option_4', 'option_5', 'appearances', 'impressives'
            ]);
            $data['human_forms'] = Common::setFieldMultiData($data['human_forms']);
            $data['appearances'] = Common::setFieldMultiData($data['appearances']);
            $data['impressives'] = Common::setFieldMultiData($data['impressives']);
            $avatarOld = $item->avatar_id ?? null;
            $uploadedFile = $request->file('avatar');
            if ($uploadedFile) {
                $filename = $this->upload($uploadedFile);
                $image = $this->imageRepository->create([
                    'filename' => $filename,
                    'type' => IMAGE_TYPE_MEMBER
                ]);
                $data['avatar_id'] = !is_null($image) ? $image->id : null;
            }
            $result = $this->update($data, $id);
            if ($result) {
                if ($uploadedFile && $data['avatar_id'] != $avatarOld && !is_null($avatarOld)) {
                    $this->imageRepository->delete($avatarOld);
                }
                $data = $result->toArray();
                unset($data['avatar_id'], $data['gender'], $data['status'], $data['deleted_at']);
                unset($data['human_forms'], $data['appearances'], $data['impressives']);
                $data['birthday'] = $result->birthday ? $result->birthday->format(DATE_FORMAT) : null;
                $data['avatar'] = $result->getAvatarUrl();
                $data['created_at'] = $result->created_at ? $result->created_at->format(DATETIME_FORMAT) : null;
                $data['updated_at'] = $result->updated_at ? $result->updated_at->format(DATETIME_FORMAT) : null;
                if ($item->gender == 1) {
                    $listImages = $request->file('images');
                    $images = [];
                    if (!empty($listImages)) {
                        foreach ($listImages as $itemImage) {
                            $filename = $this->upload($itemImage);
                            $this->imageRepository->create([
                                'filename' => $filename,
                                'target_id' => $result->id,
                                'type' => IMAGE_TYPE_MEMBER
                            ]);
                        }
                    }
                    $imageDeleted = $request->get('images_deleted');
                    if (!empty($imageDeleted)) {
                        $imageDeleted = explode(',', $imageDeleted);
                        $this->imageRepository->deleteMulti([
                            'id' => $imageDeleted,
                            'target_id' => $result->id,
                            'type' => IMAGE_TYPE_MEMBER
                        ]);
                    }
                    $images = $this->imageRepository->findWhere([
                        'target_id' => $result->id,
                        'type' => IMAGE_TYPE_MEMBER
                    ]);
                    if ($images && $images->count()) {
                        $dataImage = [];
                        foreach ($images as $itemImage) {
                            $dataImage[] = [
                                'id' => $itemImage->id,
                                'url' => $itemImage->getUrl()
                            ];
                        }
                        $data['images'] = $dataImage;
                    }
                    $data['status'] = $result->status;
                } else {
                    $data['human_forms'] = $result->getHumanForms(true);
                    $data['appearances'] = $result->getAppearances(true);
                    $data['impressives'] = $result->getImpressives(true);
                }
                DB::commit();
                return $data;
            }
            return false;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception("Update profile fail", 400);
        }
    }

    public function updateStatus($id, $status)
    {
        DB::beginTransaction();
        try {
            $result = $this->repository->update(['status' => $status], $id);
            DB::commit();
            if ($result) {
                if ($status == STATUS_REJECTED) {
                    $message = '本人確認は承認されました。';
                } elseif ($status == STATUS_APROVED) {
                    $message = '本人確認は非承認されました。';
                }
                $otherPush = $this->settingRepository->getSettingByKey('SETTING_NOTIFICATION_OTHER');
                if (!empty($message) && !is_null($otherPush)) {
                    $dataNotification = [
                        'title' => $message,
                        'body' => $message,
                        'data_custom' => [
                            'type' => 'OTHER',
                        ]
                    ];
                    // PushNotificationAndroid::dispatch($dataNotification, $id);
                    PushNotificationIos::dispatch($dataNotification, $id);
                }
            }
            return $result;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception("Update member fail", 400);
        }
    }

    public function getList($request, $gender, $isBlock = false)
    {
        $keyword = $request->get('keyword');
        $page = $request->get('page');
        $conditions = [
            ['gender', '=', $gender],
        ];
        if ($isBlock !== false) {
            $conditions['is_block'] = $isBlock;
        }
        if ($keyword) {
            $conditions[] = ['nickname', 'LIKE', "%{$keyword}%"];
        }
        if ($gender == 1) {
            $route = route('male.list');
        } else {
            $route = route('female.list');
        }
        $data = $this->repository->findWhere($conditions);
        return $this->repository->paging($data, $page, PAGINATE_DEFAULT, 'page', $route);
    }

    public function getListByStatus($request, $status, $pageName = 'page', $isBlock = false)
    {
        $keyword = $request->get('keyword');
        $page = $request->get($pageName);
        $conditions = [
            ['status', '=', $status],
        ];
        if ($isBlock !== false) {
            $conditions['is_block'] = $isBlock;
        }
        if ($keyword) {
            $conditions[] = ['nickname', 'LIKE', "%{$keyword}%"];
        }
        return $this->repository->getListByStatus($conditions, ['*'], $pageName, $page);
    }

    public function getInfoDetail($id, $gender, $isBlock = false)
    {
        $where = [
            'id' => $id,
            'gender' => $gender
        ];
        if ($isBlock !== false) {
            $where['is_block'] = $isBlock;
        }
        $item = $this->repository
                    ->with(['avatar', 'images', 'tags', 'categories', 'imagesSecondary'])
                    ->findWhere($where);
        $item = $item->first();
        if (!$item) {
            abort(404);
        }
        $avatar = $item->avatar;
        $images = $item->images;
        $tags = $item->tags;
        $categories = $item->categories;
        $imagesSecondary = $item->imagesSecondary;
        return [$item, $avatar, $images, $tags, $categories, $imagesSecondary];
    }

    public function getListMatchingByMemberId($memberId)
    {
        $result = $this->likeRepository->getListMatching(['member_id' => $memberId]);
        $data = [];
        foreach ($result as $item) {
            if (in_array($item->id_from, $data) || in_array($item->id_to, $data)) {
                continue;
            }
            if ($item->id_from != $memberId) {
                $data[] = $item->id_from;
            } elseif ($item->id_to != $memberId) {
                $data[] = $item->id_to;
            }
        }
        $listMembers = [];
        if (!empty($data)) {
            $listMembers = $this->repository->getListByConditions([
                ['id', 'IN', $data]
            ]);
            foreach ($listMembers as &$item) {
                $mailbox = $this->mailBoxRepository->getMailBoxByUserId($memberId, $item->id);
                $link = '#';
                if ($mailbox) {
                    $link = route('messages.detail', ['id' => $mailbox->id]);
                }
                $item->link = $link;
            }
        }
        return $listMembers;
    }

    public function getTotalNgWordByMember($memberId)
    {
        $totalMessageNgWord = $this->messageRepository->getTotalNgWordByMember($memberId);
        $totalLikeNgWord = $this->likeRepository->getTotalNgWordByMember($memberId);
        return $totalMessageNgWord + $totalLikeNgWord;
    }

    public function findMember(MemberFindApi $request, $id, $isBlock = false)
    {
        $params = $request->only(['appearances', 'impressives', 'age', 'height', 'human_forms', 'address', 'option_2']);
        $where = [
            'id' => $id,
            'is_pause' => 0,
            'is_canceled' => 0
        ];
        if ($isBlock !== false) {
            $params['is_block'] = $isBlock;
            $where['is_block'] = $isBlock;
        }
        $item = $this->repository->firstByConditions($where);
        if (!$item) {
            throw new Exception("Not found", 400);
        }
        $gender = $item->gender;
        $params['gender'] = 1;
        $params['is_pause'] = 0;
        $params['is_canceled'] = 0;
        if ($gender == 1) {
            $params['gender'] = 0;
        }
        $listMemberBlocked = Common::getListMemberBlocked($id);
        if (is_array($listMemberBlocked) && !empty($listMemberBlocked)) {
            $params['member_blocked'] = $listMemberBlocked;
        }
        $listMemberHided = Common::getListMemberHided($id);
        if (is_array($listMemberHided) && !empty($listMemberHided)) {
            $params['member_hided'] = $listMemberHided;
        }
        $data = $this->repository->findMember($params);
        $result = [];
        if ($data && $data->count()) {
            foreach ($data as $item) {
                $itemData = $item->toArray();
                $itemData['birthday'] = $item->birthday ? $item->birthday->format(DATE_FORMAT) : null;
                $itemData['avatar'] = $item->getAvatarUrl();
                $itemData['created_at'] = $item->created_at ? $item->created_at->format(DATETIME_FORMAT) : null;
                $itemData['updated_at'] = $item->updated_at ? $item->updated_at->format(DATETIME_FORMAT) : null;
                $images = $this->imageRepository->findWhere([
                    'target_id' => $item->id,
                    'type' => IMAGE_TYPE_MEMBER
                ]);
                $dataImage = [];
                if ($images && $images->count()) {
                    foreach ($images as $itemImage) {
                        $dataImage[] = [
                            'id' => $itemImage->id,
                            'url' => $itemImage->getUrl()
                        ];
                    }
                }
                $itemData['images'] = $dataImage;
                $tags = $item->tags;
                $listTags = [];
                foreach ($tags as $itemTag) {
                    $listTags[] = [
                        'id' => $itemTag->id,
                        'name' => $itemTag->name
                    ];
                }
                $itemData['tags'] = $listTags;
                $categories = $item->categories;
                $listCatgories = [];
                foreach ($categories as $itemCate) {
                    $listCatgories[] = [
                        'id' => $itemCate->id,
                        'name' => $itemCate->name,
                        'image' => $itemCate->getImageUrl()
                    ];
                }
                $itemData['categories'] = $listCatgories;
                $itemData['status'] = $item->status;
                if ($gender == 1) {
                    $itemData['human_forms'] = $item->getHumanForms(true);
                    $itemData['appearances'] = $item->getAppearances(true);
                    $itemData['impressives'] = $item->getImpressives(true);
                    unset($itemData['status']);
                }
                unset($itemData['avatar_id'], $itemData['gender'], $itemData['human_forms']);
                unset($itemData['appearances'], $itemData['impressives']);
                $result[] = $itemData;
            }
        }
        return $result;
    }

    public function blockOrUnblock($id, $status)
    {
        DB::beginTransaction();
        try {
            $result = $this->repository->update(['is_block' => $status], $id);
            DB::commit();
            return $result;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception("Block member fail", 400);
        }
    }

    public function login($request)
    {
        $conditions = [
            'is_canceled' => 0
        ];
        $type = $request->get('type');
        if ($type == TYPE_LOGIN_PHONE) {
            $conditions['phone'] = $request->get('phone');
            $result = $this->repository->login($conditions);

            if ($result) {
                if ($result->gender == 1 && $result->status != STATUS_APROVED) {
                    return false;
                }
                $this->verifyCodeRepository->deleteWhere([
                    'type' => 1,
                    'phone' => $conditions['phone']
                ]);
                return $result;
            }
            return false;
        } elseif ($type == TYPE_LOGIN_FACEBOOK || $type == TYPE_LOGIN_APPLE) {
            $email = $request->get('email');
            $conditions['email'] = $email;
            if ($type == TYPE_LOGIN_APPLE && empty($email)) {
                unset($conditions['email']);
                $conditions['apple_user_hash'] = $request->get('apple_user_hash');
            }
            $result = $this->repository->login($conditions);
            if ($result->gender == 1 && $result->status != STATUS_APROVED) {
                return false;
            }
            return $result;
        } elseif ($type == TYPE_LOGIN_EMAIL) {
            $conditions['email'] = $request->get('email');
            $password = $request->get('password');
            $member = $this->repository->login($conditions);
            if ($member->gender == 1 && $member->status != STATUS_APROVED) {
                return false;
            }
            if ($member && $member->password == Hash::check($password, $member->password)) {
                return $member;
            }
        } else {
            throw new \Exception("Type invalid", 400);
        }
        return null;
    }

    public function createUserToken($data)
    {
        if (isset($data['apns_id'])) {
            $conditions = [
                'apns_id' => $data['apns_id']
            ];
        } else {
            $conditions = [
                'token' => $data['token']
            ];
        }
        $userToken = $this->userTokenRepository->firstByConditions($conditions);
        if ($userToken) {
            return $this->userTokenRepository->update($data, $userToken->id);
        } else {
            return $this->userTokenRepository->create($data);
        }
    }

    public function getListByGender($request, $gender, $groupId = false)
    {
        $conditions = [
            ['gender', '=', $gender],
            ['is_pause', '=', 0],
            ['is_canceled', '=', 0],
        ];
        $user = request()->user;
        $conditionsUserLiked = [
            'id_to' => $user->id
        ];
        $listMemberBlocked = Common::getListMemberBlocked($user->id);
        if (!empty($listMemberBlocked) && is_array($listMemberBlocked)) {
            $conditions[] = [
                'id',
                'NOT-IN',
                $listMemberBlocked
            ];
            $conditionsUserLiked['member_blocked'] = $listMemberBlocked;
        }
        $listMemberHided = Common::getListMemberHided($user->id);
        if (!empty($listMemberHided) && is_array($listMemberHided)) {
            $conditions[] = [
                'id',
                'NOT-IN',
                $listMemberHided
            ];
            $conditionsUserLiked['member_hided'] = $listMemberHided;
        }
        // Users liked then ignore
        $listUserIdLiked = $this->likeRepository->listUsersLikedNotView($conditionsUserLiked, -1, false);
        if (!empty($listUserIdLiked) && is_array($listUserIdLiked)) {
            $conditions[] = [
                'id',
                'NOT-IN',
                $listUserIdLiked
            ];
        }
        $conditions[] = [
            'id',
            'NOT-IN',
            [$user->id]
        ];
        $isTagsNotSearch = false;
        $conditionsNotSearch = $conditions;
        if (isset($params['age_min']) && !is_null($params['age_min'])) {
            $ageMin = $params['age_min'];
            $dateMin = now()->subYears($ageMin)->format(DATE_FORMAT);
            $conditions[] = ['birthday', '<=', $dateMin];
            $conditionsNotSearch[] = ['birthday', '<=', $dateMin];
        }
        if (isset($params['age_max']) && !is_null($params['age_max'])) {
            $ageMax = $params['age_max'];
            $dateMax = now()->subYears($ageMax)->format(DATE_FORMAT);
            $conditions[] = ['birthday', '>=', $dateMax];
            $conditionsNotSearch[] = ['birthday', '>=', $dateMax];
        }
        if ($request->get('place_of_birth')) {
            $placeOfBirth = $request->get('place_of_birth');
            if (is_array($placeOfBirth)) {
                $listPlaceOfBirth = [];
                foreach (LIST_PREFECTURE as $key => $name) {
                    if (in_array($name, $placeOfBirth)) {
                        $listPlaceOfBirth[] = $key;
                    }
                }
                if (!empty($listPlaceOfBirth)) {
                    $conditions[] = ['place_of_birth', 'IN', $listPlaceOfBirth];
                }
            } else {
                $conditions[] = ['place_of_birth', '=', $request->get('place_of_birth')];
            }
        }
        if ($request->get('address')) {
            $address = $request->get('address');
            if (is_array($address)) {
                foreach ($address as $itemAddress) {
                    $conditions[] = ['address', 'LIKE%%', $itemAddress];
                    $conditionsNotSearch[] = ['address', 'LIKE%%', $itemAddress];
                }
            } else {
                $conditions[] = ['address', '=', $request->get('address')];
                $conditionsNotSearch[] = ['address', '=', $request->get('address')];
            }
        }
        if ($request->get('education')) {
            $conditions[] = ['education', '=', $request->get('education')];
        }
        if ($request->get('height')) {
            $heights = explode('-', $request->get('height'));
            if (count($heights) == 2) {
                $conditions[] = ['height', 'BETWEEN', $heights];
            } else {
                $conditions[] = ['height', '=', $request->get('height')];
            }
        }
        if ($request->get('human_forms')) {
            $conditions[] = ['human_forms', 'LIKE%%', $request->get('human_forms')];
        }
        if ($request->get('sake')) {
            $conditions[] = ['sake', '=', $request->get('sake')];
        }
        if ($request->get('smoke')) {
            $conditions[] = ['smoke', '=', $request->get('smoke')];
        }
        if ($request->get('blood_type')) {
            $conditions[] = ['blood_type', '=', $request->get('blood_type')];
        }
        if ($request->get('thing_of_marriage')) {
            $conditions[] = ['thing_of_marriage', '=', $request->get('thing_of_marriage')];
        }
        if ($request->get('appearances')) {
            $appearances = $request->get('appearances');
            $isTagsNotSearch = true;
            if (is_array($appearances)) {
                foreach ($appearances as $itemAppearances) {
                    $conditions[] = ['appearances', 'LIKE', "%,$itemAppearances,%"];
                }
            } else {
                $conditions[] = ['appearances', 'LIKE', "%,$appearances,%"];
            }
        }
        if ($request->get('annual_income')) {
            $conditions[] = ['annual_income', '=', $request->get('annual_income')];
        }
        if ($request->get('job_type')) {
            $conditions[] = ['job_type', '=', $request->get('job_type')];
        }
        if ($request->get('hair')) {
            $conditions[] = ['hair', '=', $request->get('hair')];
        }
        if ($request->get('hair_color')) {
            $conditions[] = ['hair_color', '=', $request->get('hair_color')];
        }
        if ($request->get('option_1')) {
            $conditions[] = ['option_1', '=', $request->get('option_1')];
        }
        if ($request->get('option_2')) {
            $conditions[] = ['option_2', '=', $request->get('option_2')];
        }
        if ($request->get('option_3')) {
            $conditions[] = ['option_3', '=', $request->get('option_3')];
        }
        if ($request->get('option_4')) {
            $conditions[] = ['option_4', '=', $request->get('option_4')];
        }
        if ($request->get('impressives')) {
            $isTagsNotSearch = true;
            $impressives = $request->get('impressives');
            if (is_array($impressives)) {
                foreach ($impressives as $itemImpressives) {
                    $conditions[] = ['impressives', 'LIKE', "%,$itemImpressives,%"];
                }
            } else {
                $conditions[] = ['impressives', 'LIKE', "%,$impressives,%"];
            }
        }
        if ($request->get('option_5')) {
            $option5 = $request->get('option_5');
            $conditions[] = ['option_5', 'LIKE', "%$option5%"];
        }
        if ($request->get('new_create') && $request->get('new_create') == 1) {
            $isTagsNotSearch = true;
            $conditions[] = ['created_at', '>=', now()->subDays(14)->format(DATETIME_FORMAT)];
        }
        $user = request()->user;
        if ($groupId) {
            $group = $this->groupRepository->firstByConditions(['id' => $groupId]);
            if (!$group) {
                Log::info('Exception1: This group not exists', [$groupId]);
                throw new \Exception("This group not exists", 400);
            }
            $groupJoined = trim($user->groups_join, ',');
            $listGroupsJoined = explode(',', $groupJoined);
            if (!in_array($groupId, $listGroupsJoined)) {
                Log::info('Exception1: This group not joined', $listGroupsJoined);
                throw new \Exception("This group not joined", 400);
            }
            $conditions[] = ['groups_join', 'LIKE', "%,$groupId,%"];
        }
        Log::info('Debug');
        Log::info('Conditions:', $conditions);
        Log::info('GroupId', [$groupId]);
        $items = $this->repository->getListByGender($conditions, ['*']);
        Log::info('Total', [$items->count()]);
        if ($items->count() == 0 && $isTagsNotSearch != true) {
            $conditionsNotSearch[] = ['is_pause', '=', 0];
            $conditionsNotSearch[] = ['is_canceled', '=', 0];
            $items = $this->repository->getListByGender($conditionsNotSearch, ['*']);
        }
        $data = [];
        if ($items->count()) {
            $listMemberNextedThisMember = $this->nextRepository->listUsersNextedByConditions(
                ['id_next' => $user->id]
            );
            $listMemberNextedThisMember = $listMemberNextedThisMember->count() ? $listMemberNextedThisMember->pluck('owner_id')->toArray() : [];
            foreach ($items as $item) {
                $checkMatching = $this->likeRepository->checkMatching($user->id, $item->id);
                $likeOrMatching = false;
                if ($checkMatching == 2) {
                    $likeOrMatching = 'matching';
                    // If is matching then ignore
                    continue;
                } elseif ($checkMatching == 1) {
                    if (!in_array($item->id, $listMemberNextedThisMember)) {
                        continue;
                    }
                    $likeOrMatching = 'liked';
                }
                $dataAppend = [
                    'liked_or_matching' => $likeOrMatching,
                ];
                $data[] = $this->getInfoMember($item, $dataAppend, true);
            }
        }
        shuffle($data);
        return $data;
    }

    public function getInfoMember($member, $dataAppend = [], $groupJoinedDetail = false)
    {
        $data = [
            'id' => $member->id,
            'user_id' => str_pad($member->id, 8, '0', STR_PAD_LEFT),
            'nickname' => $member->nickname,
            'email' => $member->email,
            'birthday' => $member->getBirthday(DATE_FORMAT),
            'age' => $member->getAge(),
            'address' => $member->address,
            'place_of_birth' => $member->getPrefecture(),
            'education' => $member->getEducation(),
            'blood_type' => $member->getBloodType(),
            'thing_of_marriage' => $member->getThingOfMarriage(),
            'annual_income' => $member->getAnnualIncome(),
            'hair' => $member->getHair(),
            'hair_color' => $member->getHairColor(),
            'avatar' => $member->getAvatarUrl(),
            'phone' => $member->phone,
            'gender' => $member->gender,
            'job_type' => $member->getJobTypes(),
            'height' => $member->getHeights(),
            'smoke' => $member->getSmokes(),
            'sake' => $member->getSakes(),
            'option_1' => $member->getOption1(),
            'option_2' => $member->getOption2(),
            'option_3' => $member->getOption3(),
            'option_4' => $member->getOption4(),
            'option_5' => $member->option_5,
            'groups_join' => $member->getGroupsJoin($groupJoinedDetail),
            'is_pause' => $member->is_pause,
            'reason_pause' => $member->reason_pause,
            'type_pause' => $member->type_pause,
            'created_at' => $member->created_at ? $member->created_at->format(DATETIME_FORMAT) : null
        ];
        $listCategories = !is_null($member->categories) ? $member->categories : [];
        $categories = [];
        if (!empty($listCategories)) {
            foreach ($listCategories as $item) {
                $categories[] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'image' => $item->getImageUrl()
                ];
            }
        }
        $data['categories'] = $categories;
        $listFavorites = !is_null($member->favorites) ? $member->favorites : [];
        $favorites = [];
        if (!empty($listFavorites)) {
            foreach ($listFavorites as $item) {
                $favorites[] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'type' => [
                        'index' => $item->type,
                        'name' => LIST_TYPE_FAVORITES[$item->type] ?? null
                    ]
                ];
            }
        }
        $data['favorites'] = $favorites;
        $listTags = !is_null($member->tags) ? $member->tags : [];
        $tags = [];
        if (!empty($listTags)) {
            foreach ($listTags as $item) {
                $tags[] = [
                    'id' => $item->id,
                    'name' => $item->name
                ];
            }
        }
        $data['tags'] = $tags;
        if ($member->gender == 1) {
            $data['image_verify'] = $member->getImageVerifyUrl(true);
            $listImages = !is_null($member->images) ? $member->images : [];
            $images = [];
            if (!empty($listImages)) {
                foreach ($listImages as $itemImage) {
                    $images[] = [
                        'id' => $itemImage->id,
                        'url' => $itemImage->getUrl()
                    ];
                }
            }
            $data['images'] = $images;
        }
        $listImagesSecondary = !is_null($member->imagesSecondary) ? $member->imagesSecondary : [];
        $imagesSecondary = [];
        if (!empty($listImagesSecondary)) {
            foreach ($listImagesSecondary as $itemImage) {
                $imagesSecondary[] = [
                    'id' => $itemImage->id,
                    'url' => $itemImage->getUrl()
                ];
            }
        }
        $data['images_secondary'] = $imagesSecondary;
        $data['human_forms'] = $member->getHumanForms(true);
        $data['appearances'] = $member->getAppearances(true);
        $data['impressives'] = $member->getImpressives(true);
        $data['status'] = $member->status;

        return array_merge($data, $dataAppend);
    }

    public function like($user, $memberId, $message, $useRose)
    {
        $limitLikedHasMessage = null;
        $likedHasMessage = $this->settingRepository->getSettingByKey('LIMIT_LIKE_HAS_MESSAGE');
        if (!is_null($likedHasMessage)) {
            $limitLikedHasMessage = $likedHasMessage->value;
        }
        Common::checkExistMember($memberId, true);
        $listMemberBlocked = Common::getListMemberBlocked($user->id, $memberId);
        if (!empty($listMemberBlocked) && !is_array($listMemberBlocked) && $listMemberBlocked == $memberId) {
            throw new \Exception(trans('messages.member_blocked'), 400);
        }
        $listMemberHided = Common::getListMemberHided($user->id, $memberId);
        if (!empty($listMemberHided) && !is_array($listMemberHided) && $listMemberHided == $memberId) {
            throw new \Exception(trans('messages.member_hided'), 400);
        }
        if ($user->id == $memberId) {
            throw new \Exception("Don't like yourself", 400);
        }
        $totalLikedHasMessage = 0;
        $pointItem = 1;
        $hasNgWord = 0;
        if (!is_null($message)) {
            $totalLikedHasMessage = $this->likeRepository->getTotalLikedByUser($memberId, true);
            $pointItem = 30;
            $hasNgWord = count($this->filterMessage($message, LIST_NGWORD_LIKE_HAS_MESSAGE));
        }
        if (!is_null($useRose) && $useRose == 1) {
            $pointItem = 0;
        }
        if ((!is_null($limitLikedHasMessage) && $totalLikedHasMessage >= $limitLikedHasMessage)) {
            throw new \Exception("受信可能なメッセージ付きいいね！の数を超えましたので、送信はできませんでした。", 400);
        }
        $itemLike = $this->itemRepository->firstByConditions([
            'type' => TYPE_LIKE_ITEM,
            'member_id' => $user->id
        ]);
        $totalLikeRemain = 0;
        if ($itemLike && $itemLike->total >= $pointItem) {
            $totalLikeRemain = $itemLike->total - $pointItem;
            $dataUpdateItem = [
                'total' => $totalLikeRemain
            ];
            $this->itemRepository->update($dataUpdateItem, $itemLike->id);
        } else {
            throw new \Exception("Not found item like or quantity item like no enough", 400);
        }
        // A like B, B next A then when A like again B => delete like old, delete B next A
        $this->nextRepository->deleteWhere([
            'owner_id' => $user->id,
            'id_next' => $memberId
        ]);
        $this->likeRepository->deleteWhere([
            'id_from' => $user->id,
            'id_to' => $memberId,
        ]);
        $data = [
            'id_from' => $user->id,
            'id_to' => $memberId,
            'message' => $message,
            'is_viewed' => 0,
            'is_ngword' => $hasNgWord > 0 ? 1 : 0
        ];
        $like = $this->likeRepository->create($data);
        $isNotificationLiked = $this->settingRepository->getSettingByKey('SETTING_NOTIFICATION_LIKE');
        $checkMatching = $this->likeRepository->checkMatching($user->id, $memberId);
        // A like B => create record 1
        // and B like A => create record 2
        // then record 2 set is_viewed = 0 else record 1 set is_viewed = 1
        if ($checkMatching == 2) {
            // Set datetime matching
            $dateMatching = null;
            $this->likeRepository->makedMatching($user->id, $memberId, $dateMatching);
            $this->likeRepository->update([
                'id_matching' => $memberId
            ], $like->id);
            $this->likeRepository->forcedUpdate([
                'is_viewed' => 1,
            ], [
                'id_from' => $memberId,
                'id_to' => $user->id,
                'date_matching' => $dateMatching
            ]);
        }
        $dataPushCustom = [
            'member_id' => $user->id,
            'nickname' => $user->nickname,
            'email' => $user->email,
            'phone' => $user->phone,
            'apple_user_hash' => $user->apple_user_hash,
            'is_matching' => $checkMatching == 2 ? true : false
        ];
        if ($isNotificationLiked && $isNotificationLiked->value == 1) {
            $dataPushCustom['type'] = 'LIKE';
            $dataNotification = [
                'title' => $user->nickname . 'さんにいいね！されました。',
                'body' => $user->nickname . 'さんにいいね！されました。',
                'data_custom' => $dataPushCustom
            ];
            // PushNotificationAndroid::dispatch($dataNotification, $memberId);
            PushNotificationIos::dispatch($dataNotification, $memberId);
        }
        $isNotificationMatching = $this->settingRepository->getSettingByKey('SETTING_NOTIFICATION_MATCHING');
        if ($checkMatching == 2 && $isNotificationMatching && $isNotificationMatching->value == 1) {
            $dataPushCustom['type'] = 'MATCHING';
            $dataPushCustom['user_matching'] = $user;
            $dataNotification = [
                'title' => $user->nickname . 'さんとマッチングしました。',
                'body' => $user->nickname . 'さんとマッチングしました。',
                'data_custom' => $dataPushCustom
            ];
            // PushNotificationAndroid::dispatch($dataNotification, $memberId);
            PushNotificationIos::dispatch($dataNotification, $memberId);
        }
        $like['total_like_remain'] = $totalLikeRemain;
        $like['message'] = $this->getMessageContent($like, $memberId);
        return $like;
    }

    private function getMessageContent($item, $memberId)
    {
        $message = $item->message;
        if ($item->is_ngword == 1 && $item->user_from == $memberId) {
            $message = '運営によってメッセージが削除されました。';
        } elseif ($item->is_ngword == 1 && $item->user_to == $memberId) {
            $message = 'メッセージが削除されました。';
        }
        return $message;
    }

    public function next($user, $memberId, $useRose)
    {
        Common::checkExistMember($memberId, true);
        $listMemberBlocked = Common::getListMemberBlocked($user->id, $memberId);
        if (!empty($listMemberBlocked) && !is_array($listMemberBlocked) && $listMemberBlocked == $memberId) {
            throw new \Exception(trans('messages.member_blocked'), 400);
        }
        $listMemberHided = Common::getListMemberHided($user->id, $memberId);
        if (!empty($listMemberHided) && !is_array($listMemberHided) && $listMemberHided == $memberId) {
            throw new \Exception(trans('messages.member_hided'), 400);
        }
        if ($user->id == $memberId) {
            throw new \Exception("Don't next yourself", 400);
        }
        $next = $this->nextRepository->firstByConditions([
            'owner_id' => $user->id,
            'id_next' => $memberId
        ]);
        if ($next) {
            throw new \Exception("Don't next this member again", 400);
        } else {
            // Check A like B
            $checkLikeMember = $this->likeRepository->checkLikedMember($user->id, $memberId);
            // Check B not like A
            $checkLikedDouble = $this->likeRepository->checkLikedMember($memberId, $user->id);
            $pointItem = 1;
            if (!is_null($useRose) && $useRose == 1) {
                $pointItem = 0;
            }
            $itemLike = $this->itemRepository->firstByConditions([
                'type' => TYPE_LIKE_ITEM,
                'member_id' => $user->id
            ]);
            $totalLikeRemain = 0;
            if ($itemLike && $itemLike->total >= $pointItem) {
                $totalLikeRemain = $itemLike->total - $pointItem;
                $dataUpdateItem = [
                    'total' => $totalLikeRemain
                ];
                $this->itemRepository->update($dataUpdateItem, $itemLike->id);
            } else {
                throw new \Exception("Not found item like or quantity item like no enough", 400);
            }
            if ($checkLikeMember == 1 && $checkLikedDouble === 0) {
                $data = [
                    'owner_id' => $user->id,
                    'id_next' => $memberId,
                ];
                return $this->nextRepository->create($data);
            }
            throw new \Exception("You and this member liked.", 400);
        }
    }

    public function listUsersLikedNotView($memberId, $hasMessage)
    {
        Common::checkExistMember($memberId, true);
        $conditions = [
            'id_to' => $memberId
        ];
        $listMemberBlocked = Common::getListMemberBlocked($memberId);
        if (is_array($listMemberBlocked) && !empty($listMemberBlocked)) {
            $conditions['member_blocked'] = $listMemberBlocked;
        }
        $listMemberHided = Common::getListMemberHided($memberId);
        if (is_array($listMemberHided) && !empty($listMemberHided)) {
            $conditions['member_hided'] = $listMemberHided;
        }

        $listMemberNexted = Common::getListMemberNexted($memberId);
        if (is_array($listMemberNexted) && !empty($listMemberNexted)) {
            $conditions['member_nexted'] = $listMemberNexted;
        }
        $listUsersLiked = $this->likeRepository->listUsersLikedNotView($conditions, $hasMessage, false);
        $data = [];
        if (!empty($listUsersLiked)) {
            $memberSorted = $this->repository->getMemberSortedByDateVip($listUsersLiked);
            $memberSorted = $memberSorted->pluck('id')->toArray();
            $items = $this->repository->findMember([
                'liked' => $memberSorted,
                'is_pause' => 0,
                'is_canceled' => 0,
            ]);
            if ($items->count()) {
                $listLikedId = [];
                foreach ($items as $item) {
                    $checkMatching = $this->likeRepository->checkMatching($memberId, $item->id);
                    if ($checkMatching == 2) {
                        continue;
                    }
                    $itemLike = $this->likeRepository->firstByConditions([
                        'id_to' => $memberId,
                        'id_from' => $item->id,
                    ]);
                    $listLikedId[] = $itemLike->id;
                    $dataAppend = [
                        'is_matching' => $checkMatching == 2 ? true : false
                    ];
                    if ($hasMessage) {
                        $dataAppend['message'] = $itemLike ? $itemLike->message : null;
                    }
                    $data[] = Common::getInfoMember($item, $dataAppend, true);
                }
                if (!empty($listLikedId)) {
                    $this->likeRepository->forcedUpdate([
                        'is_viewed' => 1
                    ], [
                        'id' => $listLikedId,
                        'id_to' => $memberId
                    ]);
                }
            }
        }
        return $data;
    }

    public function listUsersLikedNoMessage($memberId)
    {
        Common::checkExistMember($memberId, true);
        $conditions = [
            'member_id' => $memberId
        ];
        $listMemberBlocked = Common::getListMemberBlocked($memberId);
        if (is_array($listMemberBlocked) && !empty($listMemberBlocked)) {
            $conditions['member_blocked'] = $listMemberBlocked;
        }
        $listMemberHided = Common::getListMemberHided($memberId);
        if (is_array($listMemberHided) && !empty($listMemberHided)) {
            $conditions['member_hided'] = $listMemberHided;
        }
        $items = $this->likeRepository->listUsersLikedNoMessage($conditions);
        $data = [];
        foreach ($items as $item) {
            $checkMatching = $this->likeRepository->checkMatching($memberId, $item->id);
            if (!is_null($item->userTo) && $checkMatching != 2) {
                $userTo = $item->userTo;
                $data[] = [
                    'id' => $userTo->id,
                    'nickname' => $userTo->nickname,
                    'birthday' => $userTo->getBirthday(DATE_FORMAT),
                    'age' => $userTo->getAge(),
                    'address' => $userTo->address,
                    'date_vip' => $userTo->date_vip ? $userTo->date_vip->format(DATETIME_FORMAT) : null,
                    'avatar' => $userTo->getAvatarUrl()
                ];
            }
        }
        Common::array_sort_by_column($data, 'date_vip', SORT_DESC);
        return $data;
    }

    public function listUsersLikedHasMessage($memberId)
    {
        Common::checkExistMember($memberId, true);
        $conditions = [
            'member_id' => $memberId
        ];
        $listMemberBlocked = Common::getListMemberBlocked($memberId);
        if (is_array($listMemberBlocked) && !empty($listMemberBlocked)) {
            $conditions['member_blocked'] = $listMemberBlocked;
        }
        $listMemberHided = Common::getListMemberHided($memberId);
        if (is_array($listMemberHided) && !empty($listMemberHided)) {
            $conditions['member_hided'] = $listMemberHided;
        }

        $listUserNotLikeAgain = $this->likeRepository->getMemberLikedOrOtherMemberLiked($memberId);
        if (!empty($listUserNotLikeAgain)) {
            $conditions['member_not_like_again'] = $listUserNotLikeAgain;
        }
        $listMemberNexted = Common::getListMemberNexted($memberId);
        if (is_array($listMemberNexted) && !empty($listMemberNexted)) {
            $conditions['member_nexted'] = $listMemberNexted;
        }
        $items = $this->likeRepository->listUsersLikedHasMessage($conditions);
        $data = [];
        foreach ($items as $item) {
            if (!is_null($item->userTo)) {
                $userTo = $item->userTo;
                $data[] = [
                    'id' => $userTo->id,
                    'nickname' => $userTo->nickname,
                    'avatar' => $userTo->getAvatarUrl(),
                    'birthday' => $userTo->getBirthday(DATE_FORMAT),
                    'age' => $userTo->getAge(),
                    'address' => $userTo->address,
                    'date_vip' => $userTo->date_vip ? $userTo->date_vip->format(DATETIME_FORMAT) : null,
                    'status' => $this->getStatus($memberId, $item->id_to, $listMemberBlocked, $listMemberHided)
                ];
            }
        }
        Common::array_sort_by_column($data, 'date_vip', SORT_DESC);
        return $data;
    }

    public function listUsersNexted($memberId)
    {
        Common::checkExistMember($memberId, true);
        $conditions = [
            'member_id' => $memberId
        ];
        $listMemberBlocked = Common::getListMemberBlocked($memberId);
        if (is_array($listMemberBlocked) && !empty($listMemberBlocked)) {
            $conditions['member_blocked'] = $listMemberBlocked;
        }
        $listMemberHided = Common::getListMemberHided($memberId);
        if (is_array($listMemberHided) && !empty($listMemberHided)) {
            $conditions['member_hided'] = $listMemberHided;
        }
        $items = $this->nextRepository->listUsersNexted($conditions);
        $data = [];
        foreach ($items as $item) {
            if (!is_null($item->userNext)) {
                $userNext = $item->userNext;
                $data[] = [
                    'id' => $userNext->id,
                    'nickname' => $userNext->nickname,
                    'avatar' => $userNext->getAvatarUrl(),
                    'birthday' => $userNext->getBirthday(DATE_FORMAT),
                    'age' => $userNext->getAge(),
                    'address' => $userNext->address
                ];
            }
        }
        return $data;
    }

    public function getStatus($userId1, $userId2, $listMemberBlocked = [], $listMemberHided = [])
    {
        $checkMatching = $this->likeRepository->checkMatching($userId1, $userId2);
        $flag = null;
        if ($checkMatching == 2) {
            $flag = 1;
        } else {
            $conditions = [
                'member_blocked' => $listMemberBlocked,
                'member_hided' => $listMemberHided,
                'member_id' => $userId1,
                'id_to' => $userId2
            ];
            $itemLike = $this->likeRepository->listUsersLikedByConditions($conditions);
            if ($itemLike->count()) {
                $conditions = [
                    'member_blocked' => $listMemberBlocked,
                    'member_hided' => $listMemberHided,
                    'owner_id' => $userId1,
                    'id_next' => $userId2
                ];
                $itemNext = $this->nextRepository->listUsersNextedByConditions($conditions);
                if ($itemNext->count()) {
                    $flag = 2;
                }
            }
        }
        if (is_null($flag)) {
            $conditions = [
                'member_blocked' => $listMemberBlocked,
                'member_hided' => $listMemberHided,
                'member_id' => $userId2,
                'id_to' => $userId1
            ];
            $itemLike = $this->likeRepository->listUsersLikedByConditions($conditions);
            $conditions = [
                'member_blocked' => $listMemberBlocked,
                'member_hided' => $listMemberHided,
                'owner_id' => $userId2,
                'id_next' => $userId1
            ];
            $itemNext = $this->nextRepository->listUsersNextedByConditions($conditions);
            if ($itemLike->count() == 0 && $itemNext->count() == 0) {
                $flag = 3;
            }
        }
        switch ($flag) {
            case 1:
                return 'マッチング';
            case 2:
                return 'ごめんなさい';
            case 3:
                return '結果待ち';
            default:
                return null;
        }
    }

    public function detail($id, $owner = null)
    {
        if (!is_null($owner)) {
            $listMemberBlocked = Common::getListMemberBlocked($owner->id, $id);
            if (!empty($listMemberBlocked) && !is_array($listMemberBlocked) && $listMemberBlocked == $id) {
                throw new \Exception(trans('messages.member_blocked'), 400);
            }
            $listMemberHided = Common::getListMemberHided($owner->id, $id);
            if (!empty($listMemberHided) && !is_array($listMemberHided) && $listMemberHided == $id) {
                throw new \Exception(trans('messages.member_hided'), 400);
            }
        }
        $item = $this->repository->detail($id);
        if (!$item) {
            throw new \Exception(trans('messages.member_not_found'), 400);
        }
        $totalLiked = $this->likeRepository->getTotalLikedByUser($id);
        $totalLikeItems = $checkMatching = $isSentMessage = 0;
        if (!is_null($owner)) {
            $totalLikeItems = $this->itemRepository->getTotalLikeItemByUser($owner->id);
            if (!is_null($owner)) {
                $checkMatching = $this->likeRepository->checkMatching($id, $owner->id);
                $isSentMessage = $this->mailBoxRepository->checkMemberSentMessageOrNot($id, $owner->id);
            }
        }

        $listItems = $this->itemRepository->findWhere(['member_id' => $item->id]);
        $totalLike = 0;
        $totalRose = 0;
        $totalRocket = 0;
        if ($listItems->count()) {
            foreach ($listItems as $itemObj) {
                if ($itemObj->type == TYPE_LIKE_ITEM) {
                    $totalLike = $itemObj->total;
                } elseif ($itemObj->type == TYPE_ROSE_ITEM) {
                    $totalRose = $itemObj->total;
                } elseif ($itemObj->type == TYPE_ROCKET_ITEM) {
                    $totalRocket = $itemObj->total;
                }
            }
        }
        $dataAppend = [
            'total_liked' => $totalLiked,
            'total_can_like' => $totalLikeItems,
            'is_liked' => false,
            'is_matching' => $checkMatching == 2 ? true : false,
            'is_sent_message' => $isSentMessage > 1 ? true : false,
            'date_vip' => $item->getDateVip(),
            'date_paid' => $item->getDatePaid(),
            'total_like' => $totalLike,
            'total_rose' => $totalRose,
            'total_rocket' => $totalRocket,
        ];
        return $this->getInfoMember($item, $dataAppend, true);
    }

    public function updateMember($request, $id)
    {
        $item = $this->repository->firstByConditions([
            'id' => $id,
            'is_pause' => 0,
            'is_canceled' => 0,
        ]);
        if (!$item) {
            throw new \Exception(trans('messages.member_not_found'), 400);
        }
        $params = $request->only([
            'nickname',
            'birthday',
            'address',
            'place_of_birth',
            'education',
            'height',
            'human_forms',
            'blood_type',
            'thing_of_marriage',
            'annual_income',
            'hair',
            'hair_color',
            'phone',
            'gender',
            'job_type',
            'smoke',
            'sake',
            'appearances',
            'impressives',
            'option_1',
            'option_2',
            'option_3',
            'option_4',
            'option_5',
            'groups_join',
            'categories',
            'tags',
            'favorites',
        ]);
        $data = [];
        $uploadedFile = $request->file('avatar');
        if ($uploadedFile) {
            $filename = $this->upload($uploadedFile);
            $image = $this->imageRepository->create([
                'filename' => $filename,
                'type' => IMAGE_TYPE_MEMBER
            ]);
            $data['avatar_id'] = !is_null($image) ? $image->id : null;
            $this->imageRepository->deleteMulti([
                'id' => [$item->avatar_id]
            ]);
        } else {
            $this->imageRepository->deleteMulti([
                'id' => [$item->avatar_id]
            ]);
        }
        if (isset($params['birthday']) && !is_null($params['birthday'])) {
            $data['birthday'] = $params['birthday'];
        }
        if (isset($params['nickname']) && !is_null($params['nickname'])) {
            $data['nickname'] = $params['nickname'];
        }
        if (isset($params['place_of_birth']) && !is_null($params['place_of_birth'])) {
            $data['place_of_birth'] = $params['place_of_birth'];
        }
        if (isset($params['education']) && !is_null($params['education'])) {
            $data['education'] = $params['education'];
        }
        if (isset($params['height']) && !is_null($params['height'])) {
            $data['height'] = $params['height'];
        }
        if (isset($params['human_forms']) && !is_null($params['human_forms'])) {
            $data['human_forms'] = ',' . implode(',', $params['human_forms']) . ',';
        }
        if (isset($params['sake']) && !is_null($params['sake'])) {
            $data['sake'] = $params['sake'];
        }
        if (isset($params['smoke']) && !is_null($params['smoke'])) {
            $data['smoke'] = $params['smoke'];
        }
        if (isset($params['blood_type']) && !is_null($params['blood_type'])) {
            $data['blood_type'] = $params['blood_type'];
        }
        if (isset($params['thing_of_marriage']) && !is_null($params['thing_of_marriage'])) {
            $data['thing_of_marriage'] = $params['thing_of_marriage'];
        }
        if (isset($params['appearances']) && !is_null($params['appearances'])) {
            $data['appearances'] = ',' . implode(',', $params['appearances']) . ',';
        }
        if (isset($params['annual_income']) && !is_null($params['annual_income'])) {
            $data['annual_income'] = $params['annual_income'];
        }
        if (isset($params['job_type']) && !is_null($params['job_type'])) {
            $data['job_type'] = $params['job_type'];
        }
        if (isset($params['hair']) && !is_null($params['hair'])) {
            $data['hair'] = $params['hair'];
        }
        if (isset($params['hair_color']) && !is_null($params['hair_color'])) {
            $data['hair_color'] = $params['hair_color'];
        }
        if (isset($params['option_1']) && !is_null($params['option_1'])) {
            $data['option_1'] = $params['option_1'];
        }
        if (isset($params['option_2']) && !is_null($params['option_2'])) {
            $data['option_2'] = $params['option_2'];
        }
        if (isset($params['option_3']) && !is_null($params['option_3'])) {
            $data['option_3'] = $params['option_3'];
        }
        if (isset($params['option_4']) && !is_null($params['option_4'])) {
            $data['option_4'] = $params['option_4'];
        }
        if (isset($params['impressives']) && !is_null($params['impressives'])) {
            $data['impressives'] = ',' . implode(',', $params['impressives']) . ',';
        }
        if (isset($params['option_5']) && !is_null($params['option_5'])) {
            $data['option_5'] = $params['option_5'];
        }
        if (isset($params['groups_join']) && !is_null($params['groups_join'])) {
            $data['groups_join'] = ',' . implode(',', $params['groups_join']) . ',';
        }
        $result = $this->repository->update($data, $id);
        if ($result) {
            $listImages = $request->file('images');
            $images = [];
            if (!empty($listImages)) {
                foreach ($listImages as $itemImage) {
                    $filename = $this->upload($itemImage);
                    $img = $this->imageRepository->create([
                        'filename' => $filename,
                        'target_id' => $result->id,
                        'type' => IMAGE_TYPE_MEMBER
                    ]);
                    $images[] = [
                        'id' => $img->id,
                        'url' => $img->getUrl(false)
                    ];
                }
            }
            $data['images'] = $images;
            $listImages = $request->file('images_secondary');
            $images = [];
            if (!empty($listImages)) {
                $dataImage = [];
                foreach ($listImages as $itemImage) {
                    $filename = $this->upload($itemImage);
                    $dataImage[] = [
                        'filename' => $filename,
                        'target_id' => $result->id,
                        'type' => IMAGE_TYPE_MEMBER_SECONDARY
                    ];
                }
                if (!empty($dataImage)) {
                    $this->imageRepository->deleteMulti([
                        'type' => IMAGE_TYPE_MEMBER_SECONDARY,
                        'target_id' => $item->id
                    ]);
                    foreach ($dataImage as $itemImage) {
                        $img = $this->imageRepository->create($itemImage);
                        $images[] = [
                            'id' => $img->id,
                            'url' => $img->getUrl(false)
                        ];
                    }
                }
            } else {
                $this->imageRepository->deleteMulti([
                    'type' => IMAGE_TYPE_MEMBER_SECONDARY,
                    'target_id' => $item->id
                ]);
            }
            $data['images_secondary'] = $images;
            if (isset($params['categories']) && !is_null($params['categories'])) {
                $this->repository->sync($id, 'categories', $params['categories']);
            }
            if (isset($params['tags']) && !is_null($params['tags'])) {
                $this->repository->sync($id, 'tags', $params['tags']);
            }
            if (isset($params['favorites']) && !is_null($params['favorites'])) {
                $this->repository->sync($id, 'favorites', $params['favorites']);
            }
        }
        $item = $this->repository->detail($id);
        $totalLiked = $this->likeRepository->getTotalLikedByUser($id);
        $dataAppend = [
            'total_liked' => $totalLiked
        ];
        return $this->getInfoMember($item, $dataAppend, true);
    }

    public function joinGroup($user, $id)
    {
        $group = $this->groupRepository->firstByConditions(['id' => $id]);
        if (!$group) {
            throw new \Exception("Group not found", 400);
        }
        $groupJoined = trim($user->groups_join, ',');
        $listGroupsJoined = [];
        if (!empty($groupJoined)) {
            $listGroupsJoined = explode(',', $groupJoined);
        }
        if (in_array($id, $listGroupsJoined)) {
            throw new \Exception('This group joined', 400);
        }
        $listGroupsJoined[] = $id;
        $result = $this->repository->update([
            'groups_join' => ',' . implode(',', $listGroupsJoined) . ','
        ], $user->id);
        if ($result) {
            return true;
        }
        return false;
    }

    public function mypage($user)
    {
        $item = $this->repository->detail($user->id);
        if (!$item) {
            throw new \Exception(trans('messages.member_not_found'), 400);
        }

        $totalLiked = $this->likeRepository->getTotalLikedByUser($user->id);
        $totalLikeItems = $this->itemRepository->getTotalLikeItemByUser($user->id);
        $listItems = $this->itemRepository->findWhere(['member_id' => $user->id]);
        $totalLike = 0;
        $totalRose = 0;
        $totalRocket = 0;
        if ($listItems->count()) {
            foreach ($listItems as $itemObj) {
                if ($itemObj->type == TYPE_LIKE_ITEM) {
                    $totalLike = $itemObj->total;
                } elseif ($itemObj->type == TYPE_ROSE_ITEM) {
                    $totalRose = $itemObj->total;
                } elseif ($itemObj->type == TYPE_ROCKET_ITEM) {
                    $totalRocket = $itemObj->total;
                }
            }
        }

        $dataAppend = [
            'total_liked' => $totalLiked,
            'total_can_like' => $totalLikeItems,
            'is_liked' => false,
            'is_matching' => false,
            'is_sent_message' => false,
            'date_vip' => $item->getDateVip(),
            'date_paid' => $item->getDatePaid(),
            'total_like' => $totalLike,
            'total_rose' => $totalRose,
            'total_rocket' => $totalRocket,
        ];
        return $this->getInfoMember($item, $dataAppend, true);
    }

    public function blockMember($memberBlockId, $memberBlockedId)
    {
        $item = $this->memberBlockRepository->firstByConditions([
            'user_block_id' => $memberBlockId,
            'user_blocked_id' => $memberBlockedId
        ]);
        if ($item) {
            throw new \Exception("You blocked this member. Can't blocked this member any more.", 400);
        }
        return $this->memberBlockRepository->create([
            'user_block_id' => $memberBlockId,
            'user_blocked_id' => $memberBlockedId
        ]);
    }

    public function hideMemberBlocked($memberBlockId, $memberBlockedId)
    {
        $item = $this->memberBlockRepository->firstByConditions([
            'user_block_id' => $memberBlockId,
            'user_blocked_id' => $memberBlockedId
        ]);
        if (!$item) {
            throw new \Exception("This member not blocked.", 400);
        }
        return $this->memberBlockRepository->forcedUpdate([
            'is_show' => 0
        ], [
            'id' => $item->id
        ]);
    }

    public function unBlockMember($memberBlockId, $memberBlockedId)
    {
        $item = $this->memberBlockRepository->firstByConditions([
            'user_block_id' => $memberBlockId,
            'user_blocked_id' => $memberBlockedId,
            'is_show' => 1
        ]);
        if (!$item) {
            throw new \Exception("This member not blocked.", 400);
        }
        return $this->memberBlockRepository->deleteWhere([
            'id' => $item->id
        ]);
    }

    public function listMemberBlocked($memberBlockId)
    {
        $items = $this->memberBlockRepository->listUsersBlockedByConditions([
            'member_id' => $memberBlockId
        ]);
        $data = [];
        foreach ($items as $item) {
            if (!is_null($item->userBlocked)) {
                $memberBlocked = $item->userBlocked;
                $data[] = [
                    'id' => $memberBlocked->id,
                    'nickname' => $memberBlocked->nickname,
                    'avatar' => $memberBlocked->getAvatarUrl(),
                    'birthday' => $memberBlocked->getBirthday(DATE_FORMAT),
                    'age' => $memberBlocked->getAge(),
                    'address' => $memberBlocked->address,
                    'is_show' => $item->is_show
                ];
            }
        }
        return $data;
    }

    public function hideMember($memberId, $memberHideId)
    {
        $item = $this->memberHideRepository->firstByConditions([
            'user_id' => $memberId,
            'user_hided_id' => $memberHideId
        ]);
        if ($item) {
            throw new \Exception("You hided this member. Can't hide this member any more.", 400);
        }
        return $this->memberHideRepository->create([
            'user_id' => $memberId,
            'user_hided_id' => $memberHideId
        ]);
    }

    public function unHideMember($memberId, $memberHideId)
    {
        $item = $this->memberHideRepository->firstByConditions([
            'user_id' => $memberId,
            'user_hided_id' => $memberHideId
        ]);
        if (!$item) {
            throw new \Exception("This user not hide", 400);
        }
        $result = $this->memberHideRepository->deleteWhere([
            'user_id' => $memberId,
            'user_hided_id' => $memberHideId
        ]);
        if ($result) {
            $memberHided = $this->repository->firstByConditions([
                'id' => $memberHideId
            ]);
            return [
                'id' => $memberHided->id,
                'nickname' => $memberHided->nickname,
                'birthday' => $memberHided->getBirthday(DATE_FORMAT),
                'age' => $memberHided->getAge(),
                'address' => $memberHided->address,
                'avatar' => $memberHided->getAvatarUrl()
            ];
        }
        return [];
    }

    public function listMemberHided($memberId)
    {
        $items = $this->memberHideRepository->listMemberHided($memberId);
        $data = [];
        foreach ($items as $item) {
            if (!is_null($item->userHided)) {
                $memberHided = $item->userHided;
                $data[] = [
                    'id' => $memberHided->id,
                    'nickname' => $memberHided->nickname,
                    'birthday' => $memberHided->getBirthday(DATE_FORMAT),
                    'age' => $memberHided->getAge(),
                    'address' => $memberHided->address,
                    'avatar' => $memberHided->getAvatarUrl()
                ];
            }
        }
        return $data;
    }

    public function listMatching($member)
    {
        $listMemberBlocked = Common::getListMemberBlocked($member->id);
        $listMemberHided = Common::getListMemberHided($member->id);
        $listLiked = $this->likeRepository->getMemberLikedOrOtherMemberLiked($member->id);
        $listLikedNotBlocked = [];
        foreach ($listLiked as $memberIdLiked) {
            if (!in_array($memberIdLiked, $listMemberBlocked) && !in_array($memberIdLiked, $listMemberHided)) {
                $listLikedNotBlocked[] = $memberIdLiked;
            }
        }
        $listMemberSentMessages = $this->mailBoxRepository->getMemberSentOrOtherMemberSent($member->id);
        $listMemberSentMessageNotBlocked = [];
        foreach ($listMemberSentMessages as $memberIdSent) {
            if (!in_array($memberIdSent, $listMemberBlocked) && !in_array($memberIdSent, $listMemberHided)) {
                $listMemberSentMessageNotBlocked[] = $memberIdSent;
            }
        }
        $listMember1 = $listMember2 = [];
        if (empty($listMemberSentMessageNotBlocked)) {
            $listMember1 = $listLikedNotBlocked;
        } else {
            foreach ($listLikedNotBlocked as $memberIdLiked) {
                if (!in_array($memberIdLiked, $listMemberSentMessageNotBlocked)) {
                    $listMember1[] = $memberIdLiked;
                } else {
                    $listMember2[] = $memberIdLiked;
                }
            }
        }
        $listMailbox = [];
        if (!empty($listMemberSentMessageNotBlocked)) {
            $listMailbox = $this->mailBoxRepository->getMaiboxIdBySenterAndReceiver($member->id, $listMemberSentMessageNotBlocked);
        }
        $data = [
            'members_liked_not_sent' => [],
            'members_liked_sent' => []
        ];
        if (!empty($listMember1)) {
            $memberSortedVIP = $this->repository->getMemberSortedByDateVip($listMember1, true);
            $memberSortedVIP = $memberSortedVIP->pluck('id')->toArray();
            if (!empty($memberSortedVIP)) {
                $listMemberVip = $this->repository->findMember([
                    'liked' => $memberSortedVIP,
                    'is_pause' => 0,
                    'is_canceled' => 0,
                ]);
                foreach ($listMemberVip as $item) {
                    $data['members_liked_not_sent'][] = [
                        'id' => $item->id,
                        'avatar' => $item->getAvatarUrl(),
                        'age' => $item->getAge(),
                        'nickname' => $item->nickname,
                        'address' => $item->address
                    ];
                }
            }

            $listMemberByMatching = array_diff($listMember1, $memberSortedVIP);
            $memberSortedDateMatching = $this->likeRepository->getMemberSortedByDateMatching($member->id, $listMemberByMatching);
            if (!empty($listMemberByMatching) && $memberSortedDateMatching) {
                $memberSortedDateMatching = array_keys($memberSortedDateMatching);
                $listMemberMatching = $this->repository->findMember([
                    'liked' => $memberSortedDateMatching,
                    'is_pause' => 0,
                    'is_canceled' => 0,
                ]);
                foreach ($listMemberMatching as $item) {
                    $data['members_liked_not_sent'][] = [
                        'id' => $item->id,
                        'avatar' => $item->getAvatarUrl(),
                        'age' => $item->getAge(),
                        'nickname' => $item->nickname,
                        'address' => $item->address
                    ];
                }
            }
        }
        if (!empty($listMember2)) {
            $memberSortedVIP = $this->repository->getMemberSortedByDateVip($listMember2, true);
            $memberSortedVIP = $memberSortedVIP->pluck('id')->toArray();
            $listMemberByMatching = array_diff($listMember2, $memberSortedVIP);
            if (!empty($memberSortedVIP)) {
                $listMemberVip = $this->repository->findMember([
                    'liked' => $memberSortedVIP,
                    'is_pause' => 0,
                    'is_canceled' => 0,
                ]);
                foreach ($listMemberVip as $item) {
                    $mailboxId = isset($listMailbox[$member->id][$item->id]) ? $listMailbox[$member->id][$item->id] : [];
                    $messageLatest = [];
                    if (!empty($mailboxId)) {
                        $itemMessageLatest = $this->messageRepository->getMessageLatest($mailboxId, $member->id, $item->id);
                        if ($itemMessageLatest) {
                            $messageLatest = $itemMessageLatest;
                        }
                    }
                    $data['members_liked_sent'][] = [
                        'id' => $item->id,
                        'avatar' => $item->getAvatarUrl(),
                        'age' => $item->getAge(),
                        'nickname' => $item->nickname,
                        'address' => $item->address,
                        'message_latest' => $messageLatest
                    ];
                }
            }
            $memberSortedDateMatching = $this->likeRepository->getMemberSortedByDateMatching($member->id, $listMemberByMatching);
            if (!empty($listMemberByMatching) && $memberSortedDateMatching) {
                $memberSortedDateMatching = array_keys($memberSortedDateMatching);
                $listMemberMatching = $this->repository->findMember([
                    'liked' => $memberSortedDateMatching,
                    'is_pause' => 0,
                    'is_canceled' => 0,
                ]);
                $indexDefault = -1;
                foreach ($listMemberMatching as $item) {
                    $mailboxId = isset($listMailbox[$member->id][$item->id]) ? $listMailbox[$member->id][$item->id] : [];
                    $messageLatest = [];
                    $createdAt = null;
                    if (!empty($mailboxId)) {
                        $itemMessageLatest = $this->messageRepository->getMessageLatest($mailboxId, $member->id, $item->id);
                        if ($itemMessageLatest) {
                            if (is_null($createdAt)) {
                                $createdAt = $itemMessageLatest->created_at;
                            } elseif ($createdAt < $itemMessageLatest->created_at) {
                                $createdAt = $itemMessageLatest->created_at;
                            }
                            
                            $messageLatest = $itemMessageLatest;
                        }
                    }
                    if (!is_null($createdAt)) {
                        $keyIndex = strtotime($createdAt);
                    } else {
                        $keyIndex = $indexDefault--;
                    }
                    $data['members_liked_sent'][$keyIndex] = [
                        'id' => $item->id,
                        'avatar' => $item->getAvatarUrl(),
                        'age' => $item->getAge(),
                        'nickname' => $item->nickname,
                        'address' => $item->address,
                        'message_latest' => $messageLatest
                    ];
                }
                $dataLikedSent = $data['members_liked_sent'];

                krsort($dataLikedSent, SORT_NUMERIC);
                $data['members_liked_sent'] = array_values($dataLikedSent);
            }
        }
        return $data;
    }

    /**
     * API 70: list matching not view
     *
     * @param $member
     * @return JsonResponse
     */
    public function listMatchingNotView($member)
    {
        $listMatching = $this->likeRepository->getMemberLikedOrOtherMemberLiked($member->id, [
            'date_matching' => 1
        ]);
        $data = [];
        if (!empty($listMatching)) {
            $listMemberMatching = $this->repository->findMember([
                'liked' => $listMatching,
                'is_pause' => 0,
                'is_canceled' => 0,
            ]);
            foreach ($listMemberMatching as $item) {
                $memberMatching = $this->likeRepository->getMatchingMemberNotView($member->id, $item->id);
                if ($memberMatching) {
                    $data[] = [
                        'matching_id' => $memberMatching->id,
                        'member_id' => $item->id,
                        'avatar' => $item->getAvatarUrl(),
                        'age' => $item->getAge(),
                        'nickname' => $item->nickname,
                        'address' => $item->address
                    ];
                }
            }
        }
        return $data;
    }

    /**
     * API 71: set viewed for matching
     *
     * @param $member
     * @param $ids
     * @return JsonResponse
     */
    public function setViewedMatching($member, $ids)
    {
        $listMatching = $this->likeRepository->findWhereIn('id', $ids, ['id']);
        if (empty($listMatching)) {
            throw new \Exception('Not found member matching', 400);
        }
        $listIdViewed = $listMatching->pluck('id')->toArray();
        $result = false;
        if (!empty($listIdViewed)) {
            $result = $this->likeRepository->setViewedMatching($listIdViewed);
        }
        return [
            'status' => $result,
            'ids_viewed' => $listIdViewed
        ];
    }

    // type: 1: VIP, 2: Pay has fee
    public function buy($member, $params, $type)
    {
        $data = [];
        $month = $params['month'];
        $date = now()->addMonths($month);
        $transactionType = 0;
        $amount = $params['amount'] ?? 0;
        $date = $date->format(DATETIME_FORMAT);
        $totalLike = 0;
        if ($type == 1) {
            $data = [
                'date_vip' => $date
            ];
            $transactionType = TRANSACTION_TYPE_VIP;
        } elseif ($type == 2) {
            $data = [
                'date_paid' => $date
            ];
            $totalLike = $params['like_reward'] ?? 0;
            $transactionType = TRANSACTION_TYPE_PAID;
        }
        if (!empty($data)) {
            $data = $this->repository->update($data, $member->id);
            if ($totalLike > 0) {
                $itemLike = $this->itemRepository->firstByConditions([
                    'member_id' => $member->id,
                    'type' => TYPE_LIKE_ITEM
                ]);
                if ($itemLike) {
                    $totalLike = $itemLike->total + $totalLike;
                    $this->itemRepository->update([
                        'total' => $totalLike
                    ], $itemLike->id);
                } else {
                    $this->itemRepository->create([
                        'member_id' => $member->id,
                        'type' => TYPE_LIKE_ITEM,
                        'total' => $totalLike
                    ]);
                }
            }
            $this->transactionRepository->create([
                'member_id' => $member->id,
                'amount' => $amount,
                'quantity' => $month,
                'type' => $transactionType
            ]);
            return $data;
        }
        return false;
    }

    public function buyLike($member, $points, $amount)
    {
        $data = [];
        $itemLike = $this->itemRepository->firstByConditions([
            'member_id' => $member->id,
            'type' => TYPE_LIKE_ITEM
        ]);
        $result = false;
        if (!$itemLike) {
            $data = [
                'member_id' => $member->id,
                'type' => TYPE_LIKE_ITEM,
                'total' => $points
            ];
            $result = $this->itemRepository->create($data);
        } else {
            $dataUpdate = [
                'total' => $itemLike->total + $points
            ];
            $result = $this->itemRepository->update($dataUpdate, $itemLike->id);
        }
        $this->transactionRepository->create([
            'member_id' => $member->id,
            'amount' => $amount,
            'quantity' => $points,
            'type' => TRANSACTION_TYPE_LIKE
        ]);
        return $result;
    }

    public function tranferLike($member, $params)
    {
        $itemLike = $this->itemRepository->firstByConditions([
            'type' => TYPE_LIKE_ITEM,
            'member_id' => $member->id
        ]);
        if (!$itemLike || ($itemLike && $itemLike->total <= 0)) {
            throw new \Exception('Item like not exists or quantity not enough', 400);
        }
        if ($itemLike->total < $params['like_number']) {
            throw new \Exception('Quantity item like not enough', 400);
        }
        $dataOriginal = [
            'total' => $itemLike->total - $params['like_number']
        ];
        $this->itemRepository->update($dataOriginal, $itemLike->id);
        $itemTranfer = $this->itemRepository->firstByConditions([
            'type' => $params['type'],
            'member_id' => $member->id
        ]);
        if ($itemTranfer) {
            $dataTranfer = [
                'total' => $itemTranfer->total + $params['item_number'],
                'type' => $params['type'],
            ];
            $this->itemRepository->update($dataTranfer, $itemTranfer->id);
        } else {
            $this->itemRepository->create([
                'member_id' => $member->id,
                'type' => $params['type'],
                'total' => $params['item_number']
            ]);
        }

        $listItems = $this->itemRepository->findWhere(['member_id' => $member->id]);
        $totalRose = 0;
        $totalRocket = 0;
        if ($listItems->count()) {
            foreach ($listItems as $itemObj) {
                if ($itemObj->type == TYPE_ROSE_ITEM) {
                    $totalRose = $itemObj->total;
                } elseif ($itemObj->type == TYPE_ROCKET_ITEM) {
                    $totalRocket = $itemObj->total;
                }
            }
        }

        return [
            'total_rose' => $totalRose,
            'total_rocket' => $totalRocket
        ];
    }

    public function statisticByAge($labelsAge)
    {
        $result = [];
        foreach ($labelsAge as $label) {
            switch ($label) {
                case '18歳~25歳':
                    $from = 25;
                    $to = 18;
                    break;
                case '26歳~33歳':
                    $from = 33;
                    $to = 26;
                    break;
                case '34歳~41歳':
                    $from = 41;
                    $to = 34;
                    break;
                case '42歳~49歳':
                    $from = 49;
                    $to = 42;
                    break;
                case '50歳~57歳':
                    $from = 57;
                    $to = 50;
                    break;
                case '58歳~65歳':
                    $from = 65;
                    $to = 58;
                    break;
                default:
                    $from = $to = 0;
                    break;
            }
            $startDate = now()->subYears($from)->startOfYear()->format(DATETIME_FORMAT);
            $endDate = now()->subYears($to)->endOfYear()->format(DATETIME_FORMAT);
            $total = $this->repository->statisticByAge($startDate, $endDate);
            $result[$label] = $total;
        }
        return $result;
    }

    public function statisticByGender($labelsGender)
    {
        $totalMale = $this->repository->statisticByGender(1);
        $totalFemale = $this->repository->statisticByGender(0);
        return [
            '男性' => $totalMale,
            '女性' => $totalFemale
        ];
    }

    public function statisticByLike($labelsGender)
    {
        $totalMale = $this->repository->statisticByLike(1);
        $totalFemale = $this->repository->statisticByLike(0);
        $total = $totalMale + $totalFemale;
        return [
            'いいね（男性）' => $total ? $totalMale / $total * 100 : 0,
            'いいね（女性）' => $total ? $totalFemale / $total * 100 : 0
        ];
    }

    public function getListPaused()
    {
        return $this->repository->getListPaused();
    }

    public function statisticMemberPaused()
    {
        $items = $this->repository->statisticMemberPaused();
        $data = $dataLabel = [];
        $dataColor = [
            '#f56954',
            '#00a65a',
            '#f39c12',
            '#00c0ef',
            '#3c8dbc',
            '#d2d6de',
            '#dc3545',
            '#17a2b8'
        ];
        foreach (LIST_REASON_PAUSE as $type => $reason) {
            $data[$type] = 0;
            $dataLabel[$reason] = isset($dataColor[$type - 1]) ? $dataColor[$type - 1] : '#fff';
        }
        if ($items->count()) {
            foreach ($items as $item) {
                $data[$item->type_pause] = $item->total;
            }
        }
        return [$data, $dataLabel];
    }

    public function getListCanceled()
    {
        return $this->repository->getListCanceled();
    }

    public function statisticMemberCanceled()
    {
        $items = $this->repository->statisticMemberCanceled();
        $data = $dataLabel = [];
        $dataColor = [
            '#f56954',
            '#00a65a',
            '#f39c12',
            '#00c0ef',
            '#3c8dbc',
            '#d2d6de',
            '#dc3545',
            '#17a2b8'
        ];
        foreach (LIST_REASON_CANCELED as $type => $reason) {
            $data[$type] = 0;
            $dataLabel[$reason] = isset($dataColor[$type - 1]) ? $dataColor[$type - 1] : '#fff';
        }
        if ($items->count()) {
            foreach ($items as $item) {
                $data[$item->type_canceled] = $item->total;
            }
        }
        return [$data, $dataLabel];
    }

    public function getTotalLikeByType($memberId, $type)
    {
        return $this->transactionRepository->getTotalLikeByType($memberId, $type);
    }

    public function uploadImageVerify($request)
    {
        $member = request()->user;
        $uploadedImageVerify = $request->file('image_verify');
        if ($uploadedImageVerify) {
            $filename = $this->upload($uploadedImageVerify);
            $image = $this->imageRepository->create([
                'filename' => $filename,
                'type' => IMAGE_TYPE_MEMBER
            ]);
            $data['image_verify_id'] = !is_null($image) ? $image->id : null;
            if (!is_null($data['image_verify_id'])) {
                $data['status'] = STATUS_PENDING;
            }
            $result = $this->update($data, $member->id);
            if ($result) {
                $item = $this->repository->detail($member->id);
                $dataAppend = [];
                return $this->getInfoMember($item, $dataAppend, true);
            }
        }
        return null;
    }

    // Total users has fee
    public function getTotalUserHasFee()
    {
        $result = DB::table('members')
        ->selectRaw(DB::raw('COUNT(id) AS total'))
        ->whereNull('deleted_at')
        ->where([
            ['date_paid', '>=', now()->format(DATETIME_FORMAT)],
            ['is_block', '=', 0],
            ['is_pause', '=', 0],
            ['is_canceled', '=', 0]
        ])->first();
        if ($result) {
            return $result->total;
        }
        return 0;
    }

    // Total users not has fee or expired
    public function getTotalUserNotHasFeeOrExpired()
    {
        $result = DB::table('members')
        ->selectRaw(DB::raw('COUNT(id) AS total'))
        ->whereNull('deleted_at')
        ->where([
            ['is_block', '=', 0],
            ['is_pause', '=', 0],
            ['is_canceled', '=', 0]
        ])
        ->where(function ($query) {
            $query->where([
                ['date_paid', '<', now()->format(DATETIME_FORMAT)]
            ])
            ->orWhereNull('date_paid');
        })
        ->first();
        if ($result) {
            return $result->total;
        }
        return 0;
    }

    // Total users buy VIP and not expired
    public function getTotalUserBuyVIP()
    {
        $result = DB::table('members')
        ->selectRaw(DB::raw('COUNT(id) AS total'))
        ->whereNull('deleted_at')
        ->where([
            ['date_vip', '>=', now()->format(DATETIME_FORMAT)],
            ['is_block', '=', 0],
            ['is_pause', '=', 0],
            ['is_canceled', '=', 0]
        ])->first();
        if ($result) {
            return $result->total;
        }
        return 0;
    }
    
    // Total users is female and has image verify and approved
    // $type = 1: approved, 0: not approved
    public function getTotalUserByVerify($isVerify = 1, $isApproved = 1)
    {
        $conditions = [
            ['gender', '=', 0],
            ['is_block', '=', 0],
            ['is_pause', '=', 0],
            ['is_canceled', '=', 0],
            ['status', '=', $isApproved]
        ];
        $result = DB::table('members')
        ->selectRaw(DB::raw('COUNT(id) AS total'))
        ->whereNull('deleted_at')
        ->where($conditions);
        if ($isVerify) {
            $result = $result->whereNotNull('image_verify_id');
        } else {
            $result = $result->whereNull('image_verify_id');
        }
        $result = $result->first();
        if ($result) {
            return $result->total;
        }
        return 0;
    }

    public function setBuy($request, $memberId)
    {
        $type = $request->get('type');
        $member = $this->repository->firstByConditions([
            'id' => $memberId
        ]);
        if (!$member) {
            throw new \Exception('User not found', 400);
        }
        $result = false;
        if ($type == 'HAS_FEE') {
            $data = [
                'date_paid' => null,
                'date_vip' => null
            ];
            $result = $this->repository->update($data, $memberId);
        } elseif ($type == 'NO_FEE') {
            $date = now()->addMonths(1);
            if ($member->gender == 0) {
                $data = [
                    'date_vip' => $date
                ];
                $transactionType = TRANSACTION_TYPE_VIP;
            } else {
                $data = [
                    'date_paid' => $date
                ];
                $transactionType = TRANSACTION_TYPE_PAID;
            }
            $result = $this->repository->update($data, $memberId);
            $this->transactionRepository->create([
                'member_id' => $memberId,
                'amount' => 0,
                'quantity' => 1,
                'type' => $transactionType
            ]);
        }
        return $result;
    }

    public function listUsersByOption($request)
    {
        $user = request()->user;
        $listAppearances = $listImpressives = null;
        if ($user->gender == 1) {
            $listAppearances = array_keys(LIST_APPEARANCE_MALE);
            $listImpressives = array_keys(LIST_IMPRESSIVE_MALE);
        } else {
            $listAppearances = array_keys(LIST_APPEARANCE_FEMALE);
            $listImpressives = array_keys(LIST_IMPRESSIVE_FEMALE);
        }
        list($appearances, $impressives) = $this->repository->listUsersByOption($listAppearances, $listImpressives);
        $data = [
            'appearances' => [],
            'impressives' => []
        ];
        foreach ($appearances as $item) {
            $data['appearances'][] = $this->getInfoMember($item, [], true);
        }
        foreach ($impressives as $item) {
            $data['impressives'][] = $this->getInfoMember($item, [], true);
        }

        return $data;
    }

    public function rewardUserVip()
    {
        $members = $this->repository->findUserVip();
        $limit = 50;
        $like = 10;
        if ($members->count()) {
            $listMemberId = [];
            $totalMemberRewarded = 0;
            foreach ($members as $item) {
                $listMemberId[] = $item->id;
                if (count($listMemberId) == $limit) {
                    $this->itemRepository->rewardUser($listMemberId, $like);
                    $totalMemberRewarded += count($listMemberId);
                    $listMemberId = [];
                }
            }
            if (!empty($listMemberId)) {
                $this->itemRepository->rewardUser($listMemberId, $like);
                $totalMemberRewarded += count($listMemberId);
            }
            Log::info("Rewarded $totalMemberRewarded member");
        } else {
            Log::info('Not found user VIP');
        }
    }

    public function checkInfo(Request $request)
    {
        $member = request()->user;
        $memberId = $member->id;

        $conditionsUserLiked = [
            'id_to' => $memberId
        ];
        $listMemberBlocked = Common::getListMemberBlocked($memberId);
        if (is_array($listMemberBlocked) && !empty($listMemberBlocked)) {
            $conditions['member_blocked'] = $listMemberBlocked;
            $conditionsUserLiked['member_blocked'] = $listMemberBlocked;
        }
        $listMemberHided = Common::getListMemberHided($memberId);
        if (is_array($listMemberHided) && !empty($listMemberHided)) {
            $conditions['member_hided'] = $listMemberHided;
            $conditionsUserLiked['member_hided'] = $listMemberHided;
        }

        $listMemberNexted = Common::getListMemberNexted($memberId);
        if (is_array($listMemberNexted) && !empty($listMemberNexted)) {
            $conditions['member_nexted'] = $listMemberNexted;
            $conditionsUserLiked['member_nexted'] = $listMemberNexted;
        }

        $listMessageNotView = $this->mailBoxRepository->getTotalMessageNotViewedByMember($memberId);
        $data['message_not_viewed'] = $listMessageNotView ?: 0;
        $conditions['id_to'] = $memberId;
        $listMatchingNotView = $this->likeRepository->getListMembersMatchingNotView($conditions);
        $data['matching_not_viewed'] = !empty($listMatchingNotView) ? count($listMatchingNotView) : 0;
        $listUserIdLikedNotView = $this->likeRepository->getListMembersLikedButTheyNotLikeAgain($conditionsUserLiked);
        $data['liked_not_viewed'] = !empty($listUserIdLikedNotView) ? count($listUserIdLikedNotView) : 0;
        return $data;
    }
}
