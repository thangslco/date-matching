$(function () {
    'use strict';

    /* Chart.js Charts */
  // Sales chart
  var salesChartCanvas = document.getElementById('revenue-chart-canvas').getContext('2d');
  //$('#revenue-chart').get(0).getContext('2d');
  var salesChartData = {
    labels  : labelsGraph,
    datasets: [
      {
        label               : 'Sales',
        backgroundColor     : 'rgba(60,141,188,0.9)',
        borderColor         : 'rgba(60,141,188,0.8)',
        pointRadius          : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : dataGraph
      }
    ]
  }

  var salesChartOptions = {
    maintainAspectRatio : false,
    responsive : true,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        gridLines : {
          display : false,
        },
        ticks: {
          beginAtZero: true,
          min: 0
        }
      }],
      yAxes: [{
        gridLines : {
          display : false,
        },
        ticks: {
          beginAtZero: true,
          min: 0,
          callback: function (value, index, values) {
            return value + ' 円';
          }
        }
      }]
    }
  }

  // This will get the first returned node in the jQuery collection.
  var salesChart = new Chart(salesChartCanvas, { 
      type: 'line', 
      data: salesChartData, 
      options: salesChartOptions
    }
  );

  // ---------------------------
  // - END MONTHLY SALES CHART -
  // ---------------------------
});

function create_pie_chart(id, labels = ['Chrome', 'IE', 'Navigator'], data = [700, 500, 400]) {
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart-' + id).get(0).getContext('2d')
    // set size of chart
    pieChartCanvas.width = 1000;
    pieChartCanvas.height = 1000;

    var pieData = {
        labels: labels,
        datasets: [
            {
                data: data,
                backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
            }
        ]
    }
    var pieOptions = {
        legend: {
            display: false
        },
        // 2 options for config size of chart
        responsive: true,
        maintainAspectRatio: false,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas, {
        type: 'doughnut',
        data: pieData,
        options: pieOptions
    })

    //-----------------
    //- END PIE CHART -
    //-----------------
}
