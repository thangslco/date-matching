<?php

namespace App\Http\Controllers;

use App\Helpers\Common;
use App\Http\Requests\MemberUpdate;
use App\Jobs\PushNotificationAndroid;
use App\Jobs\PushNotificationIos;
use App\Services\MemberService;
use App\Services\SendPushNotificationService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    protected $memberService;
    protected $pushService;

    public function __construct(MemberService $service, SendPushNotificationService $pushService)
    {
        parent::__construct();
        $this->memberService = $service;
        $this->pushService = $pushService;
    }

    public function maleList(Request $request)
    {
        $menuActived = 'MENU_MALE';
        $items = $this->memberService->getList($request, 1);
        return view('member.male-list', compact('items', 'menuActived'));
    }

    public function maleDetail(Request $request, $id)
    {
        $menuActived = 'MENU_MALE';
        list($item, $avatar, $images, $tags, $categories, $imagesSecondary) = $this->memberService->getInfoDetail($id, 1);
        $totalNgWords = $this->memberService->getTotalNgWordByMember($id);
        $listMemberMatching = $this->memberService->getListMatchingByMemberId($id);
        return view('member.male-detail', compact('item', 'avatar', 'images', 'tags', 'categories', 'imagesSecondary', 'menuActived', 'totalNgWords', 'listMemberMatching'));
    }

    public function approve(Request $request)
    {
        $id = $request->get('id');
        $result = $this->memberService->updateStatus($id, STATUS_APROVED);
        if ($result) {
            return response()->json(['STATUS' => true, 'message' => 'This request is approved']);
        }
        return response()->json(['STATUS' => false, 'message' => 'Approve fail']);
    }

    public function reject(Request $request)
    {
        $id = $request->get('id');
        $result = $this->memberService->updateStatus($id, STATUS_REJECTED);
        if ($result) {
            return response()->json(['STATUS' => true, 'message' => 'This request is rejected']);
        }
        return response()->json(['STATUS' => false, 'message' => 'Reject fail']);
    }

    public function femaleList(Request $request)
    {
        $menuActived = 'MENU_FEMALE';
        $items = $this->memberService->getList($request, 0);
        return view('member.female-list', compact('items', 'menuActived'));
    }

    public function femaleDetail(Request $request, $id)
    {
        $menuActived = 'MENU_FEMALE';
        list($item, $avatar, $images, $tags, $categories, $imagesSecondary) = $this->memberService->getInfoDetail($id, 0);
        $totalNgWords = $this->memberService->getTotalNgWordByMember($id);
        $listMemberMatching = $this->memberService->getListMatchingByMemberId($id);
        return view('member.female-detail', compact('item', 'avatar', 'images', 'tags', 'categories', 'imagesSecondary', 'menuActived', 'totalNgWords', 'listMemberMatching'));
    }

    public function update(MemberUpdate $request, $id)
    {
        $data = $request->only(['nickname', 'email', 'birthday', 'address', 'notes', 'phone', 'place_of_birth', 'education', 'height', 'human_forms', 'sake', 'smoke', 'blood_type', 'thing_of_marriage', 'annual_income', 'job_type', 'hair', 'hair_color', 'option_1', 'option_2', 'option_3', 'option_4', 'option_5', 'appearances', 'impressives']);
        if (!isset($data['human_forms'])) {
            $data['human_forms'] = null;
        } else {
            $data['human_forms'] = Common::setFieldMultiData($data['human_forms']);
        }
        if (!isset($data['appearances'])) {
            $data['appearances'] = null;
        } else {
            $data['appearances'] = Common::setFieldMultiData($data['appearances']);
        }
        if (!isset($data['impressives'])) {
            $data['impressives'] = null;
        } else {
            $data['impressives'] = Common::setFieldMultiData($data['impressives']);
        }
        if (isset($data['birthday'])) {
            $data['birthday'] = Carbon::parse($data['birthday'])->format('Y-m-d');
        }
        $result = $this->memberService->update($data, $id);
        if ($result) {
            return redirect()->back()->with('message', 'Update success');
        }
        return redirect()->back()->with('error', 'Update fail!');
    }

    public function auth(Request $request)
    {
        $menuActived = 'MENU_AUTH';
        $memberPendings = $this->memberService->getListByStatus($request, STATUS_PENDING, 'p1');
        $memberApproveds = $this->memberService->getListByStatus($request, STATUS_APROVED, 'p2');
        $memberRejects = $this->memberService->getListByStatus($request, STATUS_REJECTED, 'p3');
        $tabActive = 'p1';
        if (!is_null($request->get('p2'))) {
            $tabActive = 'p2';
        } elseif (!is_null($request->get('p3'))) {
            $tabActive = 'p3';
        }
        return view('member.auth', compact('menuActived', 'memberPendings', 'memberApproveds', 'memberRejects', 'tabActive'));
    }

    public function blockOrUnblock(Request $request, $id, $status)
    {
        $member = $this->memberService->firstByConditions(['id' => $id]);
        if (!$member) {
            return response()->json(['STATUS' => false, 'message' => __('messages.user_not_found')]);
        }
        $status = $status == 'block' ? 1 : 0;
        $result = $this->memberService->blockOrUnblock($id, $status);
        $message = $status == 'block' ? 'Block' : 'Unblock';
        if ($result) {
            $message = $member->username . 'さんをブロックしました';
            return response()->json(['STATUS' => true, 'message' => $message]);
        }
        $message = $member->username . 'さんをアンブロックしました';
        return response()->json(['STATUS' => false, 'message' => $message]);
    }

    public function sendNotification(Request $request)
    {
        $data = [
            'title' => 'Test notification',
            'body' => 'This is body of notification'
        ];
        // PushNotificationAndroid::dispatch($data);
        PushNotificationIos::dispatch($data);
        dd('Success');
    }

    public function listPaused(Request $request)
    {
        $menuActived = 'MENU_MEMBER_PAUSED';
        $items = $this->memberService->getListPaused();
        list($dataPaused, $labelPaused) = $this->memberService->statisticMemberPaused();
        return view('member.paused', compact('items', 'menuActived', 'dataPaused', 'labelPaused'));
    }

    public function listCanceled(Request $request)
    {
        $menuActived = 'MENU_MEMBER_CANCELED';
        $items = $this->memberService->getListCanceled();
        list($dataCanceled, $labelCanceled) = $this->memberService->statisticMemberCanceled();
        return view('member.canceled', compact('items', 'menuActived', 'dataCanceled', 'labelCanceled'));
    }

    public function setBuy(Request $request, $memberId)
    {
        $result = $this->memberService->setBuy($request, $memberId);
        if ($result) {
            return response()->json(['STATUS' => true, 'message' => 'Set package success']);
        }
        return response()->json(['STATUS' => false, 'message' => 'Set package faild']);
    }
}
