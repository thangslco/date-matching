<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Member;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Member::class, function (Faker $faker) {
    return [
        'nickname' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'birthday' => $faker->date($format = 'Y-m-d', 'now'),
        'address' => $faker->address(),
        'notes' => $faker->realText(500),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'avatar_id' => 1,
        'phone' => $faker->phoneNumber,
        'gender' => rand(0, 1),
        'status' => rand(0, 2),
        'images' => null,
        'place_of_birth' => null,
        'education' => null,
        'height' => null,
        'human_forms' => null,
        'sake' => null,
        'smoke' => null,
        'blood_type' => null,
        'thing_of_marriage' => null,
        'annual_income' => null,
        'job_type' => null,
        'hair' => null,
        'hair_color' => null,
        'option_1' => null,
        'option_2' => null,
        'option_3' => null,
        'option_4' => null,
        'option_5' => null,
        'appearances' => null,
        'impressives' => null,
    ];
});
