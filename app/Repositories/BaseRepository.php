<?php

namespace App\Repositories;

use App\Repositories\Contracts\BaseRepositoryContract;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Prettus\Repository\Eloquent\BaseRepository as Repository;
use Prettus\Repository\Events\RepositoryEntityDeleted;

abstract class BaseRepository extends Repository implements BaseRepositoryContract
{
    /**
     * Check exists with given conditions
     *
     * @param array $conditions
     * @param array $orWhere
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function exists(array $conditions, array $orWhere = [])
    {
        $this->applyCriteria();
        $this->applyScope();
        $query = $this->model->where($conditions);

        if (!empty($orWhere)) {
            list($column, $operator, $value) = $orWhere;
            $query->orWhere($column, $operator, $value);
        }
        $model = $query->exists();

        $this->resetModel();
        return $this->parserResult($model);
    }

    /**
     * Find first data with given conditions
     *
     * @param array $conditions
     * @param array $columns
     *
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function firstByConditions(array $conditions, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where($conditions)->first($columns);
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function paging($collections, $page, $perPage = null, $pageName = 'page', $path = null)
    {
        if (!$collections) {
            return null;
        }
        $currentPage = Paginator::resolveCurrentPage() - 1;
        $perPage = $perPage ?? 20;
        $currentPageSearchResults = $collections->slice($currentPage * $perPage, $perPage)->all();
        $paginate = new LengthAwarePaginator($currentPageSearchResults, count($collections), $perPage, $page, ['pageName' => $pageName]);
        if (!is_null($path)) {
            $paginate->setPath($path);
        }
        $paginate->appends(app('request')->query());
        return $paginate;
    }

    /**
     * Forced update function: for only update statement.
     *
     * @param $data
     * @param $conditions
     * @return mixed
     */
    public function forcedUpdate($data, $conditions)
    {
        return $this->model->where($conditions)->update($data);
    }

    /**
     * Increase amount of a column
     *
     * @param $where
     * @param $column
     * @param $amount
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function increment($where, $column, $amount)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where($where)->increment($column, $amount);
        $this->resetModel();
        return $this->parserResult($model);
    }

    /**
     * Update value when given conditions is satisfied
     *
     * @param array $where
     * @param array $value
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function whereUpdate(array $where, array $value)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where($where)->update($value);
        $this->resetModel();
        return $this->parserResult($model);
    }

    /**
     * Delete value when given conditions is satisfied
     *
     * @param array $where
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function deleteMulti(array $where)
    {
        $this->applyCriteria();
        $this->applyScope();
        $temporarySkipPresenter = $this->skipPresenter;
        $model = $this->model;
        foreach ($where as $key => $value) {
            if (is_array($value)) {
                $model = $model->whereIn($key, $value);
            } else {
                $model = $model->where($key, $value);
            }
        }
        $deleted = $model->delete();

        event(new RepositoryEntityDeleted($this, $this->model->getModel()));

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        return $deleted;
    }

    /**
     * Forced delete function
     *
     * @param $conditions
     * @return mixed
     */
    public function forceDelete($conditions)
    {
        return $this->model->where($conditions)->forceDelete();
    }
}
