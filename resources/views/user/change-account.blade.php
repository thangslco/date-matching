@php
use App\Helpers\Common;
@endphp
@extends('layouts.default')

@section('title', 'Change account')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Change account</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Change account</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                      {!! session('error') !!}
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                      {!! session('success') !!}
                    </div>
                @endif
                <div class="card card-primary">
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="card-body">
                        <form class="form-horizontal" method="POST" action="{{ route('user.do-change-account') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="input-email" class="col-sm-2 col-form-label">Email address</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="input-email" name="email" placeholder="Email address" value="{{ old('email') ?? request()->email }}">
                                    @if ($errors->has('email'))
                                    <span class="error invalid-feedback">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-password" class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="password" id="input-password" placeholder="Password">
                                    @if ($errors->has('password'))
                                    <span class="error invalid-feedback">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-confirm-password" class="col-sm-2 col-form-label">Confirm password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="password_confirmation" id="input-confirm-password" placeholder="Confirm password">
                                    @if ($errors->has('password_confirmation'))
                                    <span class="error invalid-feedback">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('style')
    @parent
@endsection
@section('script')
    @parent
@endsection