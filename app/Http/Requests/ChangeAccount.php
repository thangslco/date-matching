<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;

class ChangeAccount extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:150|unique:users,email,' . Auth::id(),
            'password' => 'required|confirmed|min:6'
        ];
    }
}
