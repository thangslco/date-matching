<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('/login', 'LoginController@showLoginForm')->name('login');
Route::post('/login', 'LoginController@login')->name('doLogin');
Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('/logout', 'LoginController@logout')->name('logout');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/male/list', 'MemberController@maleList')->name('male.list');
    Route::get('/male/detail/{id}', 'MemberController@maleDetail')->name('male.detail');
    Route::post('/approve', 'MemberController@approve')->name('approve');
    Route::post('/reject', 'MemberController@reject')->name('reject');
    Route::get('/female/list', 'MemberController@femaleList')->name('female.list');
    Route::get('/female/detail/{id}', 'MemberController@femaleDetail')->name('female.detail');
    Route::post('/member/update/{id}', 'MemberController@update')->name('member.update');
    Route::post('/member/{id}/{status}', 'MemberController@blockOrUnBlock')->name('member.block_unblock');
    Route::get('/user/change-account', 'UserController@changeAccount')->name('user.change-account');
    Route::post('/user/change-account', 'UserController@doChangeAccount')->name('user.do-change-account');
    Route::prefix('members')->group(function () {
        Route::get('auth', 'MemberController@auth')->name('members.auth');
        Route::get('sendNotification', 'MemberController@sendNotification')->name('members.sendNotification');
        Route::get('list-paused', 'MemberController@listPaused')->name('members.list-paused');
        Route::get('list-canceled', 'MemberController@listCanceled')->name('members.list-canceled');
        Route::post('set-buy/{memberId}', 'MemberController@setBuy')->name('members.set-buy');
    });
    Route::prefix('users')->group(function () {
        Route::get('list', 'UserController@index')->name('users');
        Route::get('create', 'UserController@create')->name('users.create');
        Route::post('store', 'UserController@store')->name('users.store');
        Route::post('update/{id}', 'UserController@update')->name('users.update');
        Route::post('delete', 'UserController@delete')->name('users.delete');
        Route::get('detail/{id}', 'UserController@detail')->name('users.detail');
    });
    Route::prefix('messages')->group(function () {
        Route::get('list', 'MessageController@index')->name('messages');
        Route::get('ng-words', 'MessageController@listMessageNgWord')->name('messages.ng_words');
        Route::get('detail/{id}', 'MessageController@detail')->name('messages.detail');
        Route::post('delete', 'MessageController@delete')->name('messages.delete');
        Route::post('delete-force', 'MessageController@deleteForce')->name('messages.delete-force');
        Route::post('rollback', 'MessageController@rollback')->name('messages.rollback');
    });
    Route::prefix('groups')->group(function () {
        Route::get('list', 'GroupController@index')->name('groups');
        Route::get('create', 'GroupController@create')->name('groups.create');
        Route::post('store', 'GroupController@store')->name('groups.store');
        Route::post('update/{id}', 'GroupController@update')->name('groups.update');
        Route::post('delete', 'GroupController@delete')->name('groups.delete');
        Route::get('detail/{id}', 'GroupController@detail')->name('groups.detail');
    });
    Route::prefix('notices')->group(function () {
        Route::get('list', 'NoticeController@index')->name('notices');
        Route::get('create', 'NoticeController@create')->name('notices.create');
        Route::post('store', 'NoticeController@store')->name('notices.store');
        Route::post('update/{id}', 'NoticeController@update')->name('notices.update');
        Route::get('detail/{id}', 'NoticeController@detail')->name('notices.detail');
    });

    Route::prefix('favorites')->group(function () {
        Route::get('list', 'FavoriteController@index')->name('favorites');
        Route::get('create', 'FavoriteController@create')->name('favorites.create');
        Route::post('store', 'FavoriteController@store')->name('favorites.store');
        Route::post('update/{id}', 'FavoriteController@update')->name('favorites.update');
        Route::post('delete', 'FavoriteController@delete')->name('favorites.delete');
        Route::get('detail/{id}', 'FavoriteController@detail')->name('favorites.detail');
    });
});
