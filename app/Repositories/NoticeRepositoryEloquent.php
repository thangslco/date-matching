<?php

namespace App\Repositories;

use App\Models\Notice;
use App\Repositories\Contracts\NoticeRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class NoticeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class NoticeRepositoryEloquent extends BaseRepository implements NoticeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Notice::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getList($conditions = [])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where($conditions)
        ->orderBy('created_at', 'DESC');
        $results = $model->paginate(PAGINATE_DEFAULT);
        $results->appends(app('request')->query());
        $this->resetModel();
        return $this->parserResult($results);
    }
}
