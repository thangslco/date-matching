<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateColumnsToMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->text('appearance')->nullable();
            $table->text('impressive')->nullable();
        });
        DB::statement('ALTER TABLE members MODIFY human_form TEXT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->dropColumn('appearance');
            $table->dropColumn('impressive');
        });
        DB::statement('ALTER TABLE members MODIFY human_form INT;');
    }
}
