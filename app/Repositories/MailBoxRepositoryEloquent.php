<?php

namespace App\Repositories;

use App\Models\MailBox;
use App\Repositories\Contracts\MailBoxRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class MailBoxRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class MailBoxRepositoryEloquent extends BaseRepository implements MailBoxRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MailBox::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function checkMemberSentMessageOrNot($memberId1, $memberId2)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where(function ($query) use ($memberId1, $memberId2) {
            $query->where('user1_id', $memberId1)
                  ->where('user2_id', $memberId2);
        })
        ->orWhere(function ($query) use ($memberId1, $memberId2) {
            $query->where('user1_id', $memberId2)
                  ->where('user2_id', $memberId1);
        })->count();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getMailBoxByUserId($memberId1, $memberId2)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where(function ($query) use ($memberId1, $memberId2) {
            $query->where('user1_id', $memberId1)
                  ->where('user2_id', $memberId2);
        })
        ->orWhere(function ($query) use ($memberId1, $memberId2) {
            $query->where('user1_id', $memberId2)
                  ->where('user2_id', $memberId1);
        })->first();
        $this->resetModel();
        return $this->parserResult($model);
    }

    // Get list members sent message or member other sent message
    public function getMemberSentOrOtherMemberSent($memberId)
    {
        $list1 = DB::table('mail_boxes')->where('user1_id', $memberId)
                                ->select('user2_id AS member_id')
                                ->distinct();
        $list2 = DB::table('mail_boxes')->where('user2_id', $memberId)
                                ->select('user1_id AS member_id')
                                ->distinct()
                                ->union($list1)
                                ->pluck('member_id')
                                ->toArray();
        return array_unique($list2);
    }

    // Get list mailbox_id by member sent and member receive
    public function getMaiboxIdBySenterAndReceiver($memberId, $listMemberIdSent)
    {
        $list = DB::table('mail_boxes')
            ->where(function ($query) use ($memberId, $listMemberIdSent) {
                $query->where('user1_id', $memberId)
                    ->whereIn('user2_id', $listMemberIdSent);
            })
            ->orWhere(function ($query) use ($memberId, $listMemberIdSent) {
                $query->where('user2_id', $memberId)
                    ->whereIn('user1_id', $listMemberIdSent);
            })
            ->select('id', 'user1_id', 'user2_id')
            ->get();
        $result = [];
        foreach ($list as $item) {
            if ($item->user1_id == $memberId) {
                $result[$memberId][$item->user2_id][] = $item->id;
            } elseif ($item->user2_id == $memberId) {
                $result[$memberId][$item->user1_id][] = $item->id;
            }
        }
        return $result;
    }

    public function getTotalMessageNotViewedByMember($memberId)
    {
        return DB::table('mail_boxes')
        ->join('messages', 'messages.mailbox_id', '=', 'mail_boxes.id')
        ->where(function ($query) use ($memberId) {
            $query->orWhere('user1_id', $memberId)
                ->orWhere('user2_id', $memberId);
        })
        ->where([
            ['user_no', '!=', $memberId],
            ['viewed', '=', 0],
            ['has_ng_word', '=', 0]
        ])
        ->count();
    }

}
