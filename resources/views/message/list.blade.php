@extends('layouts.default')

@section('title', 'メッセージ履歴')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">メッセージ履歴</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">メッセージ履歴</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-success">{{ Session::get('error') }}</div>
                @endif
                <div class="modal fade" id="modal-delete-message" data-url="{{route('messages.delete')}}" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">{{__('messages.heading_modal')}}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>{{__('messages.content_confirm_delete_message')}}</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{__('messages.btn_close')}}</button>
                                <button type="button" id="btn-delete-message" class="btn btn-danger">{{__('messages.btn_delete')}}</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal fade" id="modal-delete-message-success" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">{{__('messages.heading_modal')}}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{__('messages.btn_close')}}</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="card">
                    <div class="card-header row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="card-tools fa-pull-right">
                                <form action="" method="GET">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="keyword" class="form-control float-right" value="{!! request()->keyword !!}" placeholder="Search...">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    @if (!is_null($keyword))
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>Message</th>
                                        <th>Time</th>
                                        <th>{{__('messages.lbl_action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($items as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{!is_null($item->user) ? $item->user->email : ''}}</td>
                                        <td>
                                            @if (!is_null($item->content))
                                                {!! $item->content !!}
                                            @else
                                                <img width="100" height="75" src="{{ $item->image }}"/>
                                            @endif
                                        </td>
                                        <td>{{$item->created_at}}</td>
                                        <td>
                                            <button type="button" class="btn btn-danger delete-item-message" data-id="{{$item->id}}">削除</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            {!! $items->links() !!}
                        </div>
                    @else
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>ユーザ１</th>
                                        <th>ユーザ２</th>
                                        <th>最後のメッセージ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($items as $item)
                                    <tr class="">
                                        <td><a href="{{ route('messages.detail', ['id' => $item->mailbox_id]) }}">{{$item->mailbox_id}}</a></td>
                                        <td><a href="{{ generate_link_member_detail($item->member_id_1, $item->gender_1) }}">{{(!empty($item->nickname_1)) ? $item->nickname_1 : '-'}}</a></td>
                                        <td><a href="{{ generate_link_member_detail($item->member_id_2, $item->gender_2) }}">{{(!empty($item->nickname_2)) ? $item->nickname_2 : '-'}}</a></td>
                                        <td>{{$item->content}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer clearfix">
                            {!! $items->links() !!}
                        </div>
                    @endif

                </div>
                <!-- /.card -->
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('style')
@parent
@endsection
@section('script')
@parent
@endsection