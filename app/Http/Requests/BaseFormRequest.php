<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class BaseFormRequest extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        $isJson = $this->header('Accept');
        if (config('app.debug') && $isJson != 'application/json') {
            throw (new ValidationException($validator))
                    ->errorBag($this->errorBag)
                    ->redirectTo($this->getRedirectUrl());
        }
        $errors = (new ValidationException($validator))->errors();
        if ($this->segment(1) == 'api') {
            $messages = [];
            foreach ($errors as $itemError) {
                if (is_array($itemError)) {
                    foreach ($itemError as $e) {
                        $messages[] = $e;
                    }
                } else {
                    $messages[] = reset($itemError);
                }
            }

            throw new HttpResponseException(response()->json(
                [
                    'message' => implode(',', $messages),
                    'status_code' => 422,
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            ));
        }
        throw new HttpResponseException(response()->json(
            [
                'error' => $errors,
                'status_code' => 422,
            ],
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY
        ));
    }
}
