<?php

namespace App\Repositories\Contracts;

/**
 * Interface UserTokenRepository
 *
 * @package namespace App\Repositories;
 */
interface UserTokenRepository extends BaseRepositoryContract
{
}
