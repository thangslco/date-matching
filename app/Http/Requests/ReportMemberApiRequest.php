<?php

namespace App\Http\Requests;

class ReportMemberApiRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_reported_id' => 'required|integer|exists:members,id',
            'type' => 'required|integer|in:' . implode(',', array_keys(LIST_TYPE_REPORT)),
            'position' => 'required|integer|in:' . implode(',', array_keys(LIST_POSITION_REPORT)),
            'content' => 'nullable',
        ];
    }
}
