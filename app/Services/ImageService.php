<?php

namespace App\Services;

use App\Repositories\Contracts\ImageRepository;

class ImageService extends BaseService
{
    /**
     * ImageService constructor.
     * @param ImageRepository $repository
     */
    public function __construct(
        ImageRepository $repository
    ) {
        $this->repository = $repository;
    }
}
