<?php

namespace App\Services;

use App\Repositories\Contracts\GroupRepository;
use App\Repositories\Contracts\ImageRepository;
use App\Repositories\Contracts\MemberRepository;
use Illuminate\Support\Facades\DB;

class GroupService extends BaseService
{
    protected $imageRepository;
    /**
     * GroupService constructor.
     * @param GroupRepository $repository
     * @param ImageRepository $imageRepository
     * @param MemberRepository $memberRepository
     */
    public function __construct(
        GroupRepository $repository,
        ImageRepository $imageRepository,
        MemberRepository $memberRepository
    ) {
        $this->repository = $repository;
        $this->imageRepository = $imageRepository;
        $this->memberRepository = $memberRepository;
    }

    public function getList($request)
    {
        $conditions = [];
        $keyword = $request->get('keyword');
        if ($keyword) {
            $conditions[] = ['name', 'LIKE', "%{$keyword}%"];
        }
        return $this->repository->getList($conditions);
    }

    public function register($request)
    {
        $params = $request->only('name', 'description');
        $imageFile = $request->file('image');
        if ($imageFile) {
            $filename = $this->upload($imageFile);
            $image = $this->imageRepository->create([
                'filename' => $filename,
                'type' => IMAGE_TYPE_GROUP
            ]);
            $params['image_id'] = !is_null($image) ? $image->id : null;
        }
        return $this->create($params);
    }

    public function updateGroup($request, $group)
    {
        $params = $request->only('name', 'description');
        $imageFile = $request->file('image');
        $filename = null;
        DB::beginTransaction();
        try {
            if ($imageFile) {
                $filename = $this->upload($imageFile);
                $image = $this->imageRepository->create([
                    'filename' => $filename,
                    'type' => IMAGE_TYPE_GROUP
                ]);
                $params['image_id'] = !is_null($image) ? $image->id : null;
            }
            $result = $this->update($params, $group->id);
            if ($result) {
                $image = $group->image()->first();
                if ($imageFile && !is_null($image)) {
                    $this->removeFile($image->filename);
                }
            } else {
                if (!is_null($filename)) {
                    $this->removeFile($filename);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            if (!is_null($filename)) {
                $this->removeFile($filename);
            }
        }
    }

    public function getAll()
    {
        $results = $this->repository->getAll();
        $data = [];
        foreach ($results as $item) {
            $itemData = $item->toArray();
            unset($itemData['image_id']);
            $itemData['image'] = !is_null($item->image) ? $item->image->getUrl() : null;
            $data[] = $itemData;
        }
        return $data;
    }

    public function updateTotalJoined($groupJoined)
    {
        if (!empty($groupJoined)) {
            $this->repository->updateTotalJoined($groupJoined);
        }
    }

    public function getListGroups($request, $user)
    {
        $groupJoined = trim($user->groups_join, ',');
        $listGroupsJoined = [];
        if (!empty($groupJoined)) {
            $listGroupsJoined = explode(',', $groupJoined);
        }
        $data = [
            'joined' => [],
            'not_joined' => []
        ];
        if (!empty($listGroupsJoined)) {
            $listJoined = $this->repository->getListGroups($listGroupsJoined, true);
            if ($listJoined) {
                $dataJoined = [];
                foreach ($listJoined as $item) {
                    $itemData = $item->toArray();
                    unset($itemData['image_id']);
                    $itemData['image'] = !is_null($item->image) ? $item->image->getUrl() : null;
                    $dataMembers = $this->getMemberByGroupId($item->id, $user->gender);
                    $itemData['members'] = $dataMembers;
                    $dataJoined[] = $itemData;
                }
                $data['joined'] = $dataJoined;
            }
            $listNotJoin = $this->repository->getListGroups($listGroupsJoined, false);
            if ($listNotJoin) {
                $dataNotJoin = [];
                foreach ($listNotJoin as $item) {
                    $itemData = $item->toArray();
                    unset($itemData['image_id']);
                    $itemData['image'] = !is_null($item->image) ? $item->image->getUrl() : null;
                    $dataMembers = $this->getMemberByGroupId($item->id, $user->gender);
                    $itemData['members'] = $dataMembers;
                    $dataNotJoin[] = $itemData;
                }
                $data['not_joined'] = $dataNotJoin;
            }
        } else {
            $data['not_joined'] = $this->getAll();
        }
        return $data;
    }

    private function getMemberByGroupId($groupId, $gender)
    {
        $listMembers = $this->memberRepository->getMembersByGroupId($groupId, $gender);
        $dataMembers = [];
        foreach ($listMembers as $itemMember) {
            $images = $itemMember->images;
            $listImages = [];
            if ($images) {
                $countImage = 0;
                foreach ($images as $itemImage) {
                    if ($countImage++ == 4) {
                        break;
                    }
                    $listImages[] = $itemImage->getUrl();
                }
            }
            $dataMembers[] = [
                'id' => $itemMember->id,
                'nickname' => $itemMember->nickname,
                'phone' => $itemMember->phone,
                'email' => $itemMember->email,
                'images' => $listImages
            ];
        }
        return $dataMembers;
    }

    public function detail($id)
    {
        $item = $this->repository->detail($id);
        if (!$item) {
            throw new \Exception("Group not found", 400);
        }
        return [
            'id' => $item->id,
            'name' => $item->name,
            'description' => $item->description,
            'joined' => $item->joined,
            'image' => !is_null($item->image) ? $item->image->getUrl() : null
        ];
    }
}
