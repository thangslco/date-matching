<?php

namespace App\Http\Requests;

class PauseMemberRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|integer|in:' . implode(',', array_keys(LIST_REASON_PAUSE))
        ];
    }
}
