<?php

namespace App\Services;

use App\Repositories\Contracts\TransactionRepository;

class TransactionService extends BaseService
{
    /**
     * TransactionService constructor.
     * @param TransactionRepository $repository
     */
    public function __construct(
        TransactionRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function getTotalMoney()
    {
        return $this->repository->getTotalMoney();
    }

    public function getTotalMoneyByYear($year)
    {
        return $this->repository->getTotalMoneyByYear($year);
    }

    public function getTotalMoneyByMonth($currentYear, $currentMonth)
    {
        return $this->repository->getTotalMoneyByMonth($currentYear, $currentMonth);
    }

    public function getTotalMoneyByType($type, $date)
    {
        return $this->repository->getTotalMoneyByType($type, $date);
    }
}
