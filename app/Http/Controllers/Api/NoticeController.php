<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\NoticeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class NoticeController extends Controller
{
    protected $noticeService;

    public function __construct(NoticeService $service)
    {
        parent::__construct();
        $this->noticeService = $service;
    }

    /**
     * API 41. Get list notices
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request)
    {
        try {
            $member = $request->user;
            $items = $this->noticeService->getList($request, $member);
            $data = [];
            foreach ($items as $item) {
                $itemData = $item->toArray();
                if (!is_null($item->category) && in_array($item->category, array_keys(LIST_CATEGORY_NOTICES))) {
                    $itemData['category'] = [
                        'index' => $item->category,
                        'title' => LIST_CATEGORY_NOTICES[$item->category]
                    ];
                } else {
                    $itemData['category'] = null;
                }
                
                $data[] = $itemData;
            }
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 41. Get list notices: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 42. Notice detail
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function detail(Request $request, $id)
    {
        try {
            $notice = $this->noticeService->firstByConditions(['id' => $id]);
            if (!$notice) {
                throw new \Exception("Notice not found", 400);
            }
            if (!is_null($notice->category) && in_array($notice->category, array_keys(LIST_CATEGORY_NOTICES))) {
                $notice['category'] = [
                    'index' => $notice->category,
                    'title' => LIST_CATEGORY_NOTICES[$notice->category]
                ];
            } else {
                $notice['category'] = null;
            }
            return response()->jsonSuccess($notice);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 41. Get list notices: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }
}
