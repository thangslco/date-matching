<?php

namespace App\Services;

use App\Helpers\Common;
use App\Jobs\PushNotificationAndroid;
use App\Jobs\PushNotificationIos;
use App\Repositories\Contracts\ImageRepository;
use App\Repositories\Contracts\MailBoxRepository;
use App\Repositories\Contracts\MemberRepository;
use App\Repositories\Contracts\MessageRepository;
use App\Repositories\Contracts\SettingRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class MessageService extends BaseService
{
    protected $mailBoxRepository = null;
    protected $imageRepository;
    protected $memberRepository;
    protected $settingRepository;
    /**
     * MessageService constructor.
     * @param MessageRepository $repository
     * @param MailBoxRepository $mailBoxRepository
     * @param ImageRepository $imageRepository
     * @param MemberRepository $memberRepository
     * @param SettingRepository $settingRepository
     */
    public function __construct(
        MessageRepository $repository,
        MailBoxRepository $mailBoxRepository,
        ImageRepository $imageRepository,
        MemberRepository $memberRepository,
        SettingRepository $settingRepository
    ) {
        $this->repository = $repository;
        $this->mailBoxRepository = $mailBoxRepository;
        $this->imageRepository = $imageRepository;
        $this->memberRepository = $memberRepository;
        $this->settingRepository = $settingRepository;
    }

    public function getList($request, $keyword = null)
    {
        $conditions = [];
        return $this->repository->getList($conditions, $keyword);
    }

    public function getListNgWord($request, $keyword = null)
    {
        $conditions = [];
        return $this->repository->getListNgWord($conditions, $keyword);
    }
    
    public function getListByMailBoxId($mailBoxId)
    {
        return $this->repository->getListByMailBoxId($mailBoxId);
    }

    public function deleteMessage($id)
    {
        return $this->repository->delete($id);
    }

    public function forceDeleteMessage($id)
    {
        return $this->repository->forceDelete(['id' => $id]);
    }

    public function listMessagesById($memberId, $targetId)
    {
        $listMemberBlocked = Common::getListMemberBlocked($memberId, $targetId);
        if (!empty($listMemberBlocked) && !is_array($listMemberBlocked) && $listMemberBlocked == $memberId) {
            throw new \Exception(trans('message.member_blocked'), 400);
        }
        $listMemberHided = Common::getListMemberHided($memberId, $targetId);
        if (!empty($listMemberHided) && !is_array($listMemberHided) && $listMemberHided == $memberId) {
            throw new \Exception(trans('message.member_hided'), 400);
        }
        $mailbox = $this->mailBoxRepository->getMailBoxByUserId($memberId, $targetId);
        if (!$mailbox) {
            $mailbox = $this->mailBoxRepository->create([
                'user1_id' => $memberId,
                'user2_id' => $targetId,
            ]);
        }
        $messages = $this->repository->getListByMailBoxId($mailbox->id, false);
        $user = $this->memberRepository->detail($targetId);
        if (!$user) {
            throw new \Exception(trans('message.member_not_found'), 400);
        }
        $dataUser = [
            'id' => $user->id,
            'mailbox_id' => $mailbox->id,
            'nickname' => $user->nickname,
            'birthday' => $user->getBirthday(DATE_FORMAT),
            'age' => $user->getAge(),
            'address' => $user->address,
            'avatar' => $user->getAvatarUrl(),
            'phone' => $user->phone,
            'gender' => $user->gender,
            'human_forms' => $user->getHumanForms(true),
            'appearances' => $user->getAppearances(true),
            'impressives' => $user->getImpressives(true),
            'status' => $user->status,
        ];

        $listTags = !is_null($user->tags) ? $user->tags : [];
        $tags = [];
        if (!empty($listTags)) {
            foreach ($listTags as $item) {
                $tags[] = [
                    'id' => $item->id,
                    'name' => $item->name
                ];
            }
        }
        $dataUser['tags'] = $tags;
        $data = [
            'user' => $dataUser,
            'messages' => []
        ];
        if ($messages->count()) {
            $listMessages = [];
            foreach ($messages as $item) {
                $viewed = $item->viewed;
                if ($item->user_no == $targetId) {
                    $viewed = 1;
                }
                $listMessages[] = [
                    'id' => $item->id,
                    'mailbox_id' => $item->mailbox_id,
                    'user_id' => $item->user_no,
                    'content' => $this->getMessageContent($item, $memberId),
                    'image' => $item->getImageUrl(),
                    'viewed' => $viewed,
                    'updated_at' => !is_null($item->updated_at) ? $item->updated_at->format(DATETIME_FORMAT) : null,
                    'created_at' => !is_null($item->created_at) ? $item->created_at->format(DATETIME_FORMAT) : null,
                ];
            }
            $this->setViewed($mailbox->id, $targetId);
            $data['messages'] = $listMessages;
        }
        return $data;
    }

    public function createMessage($member, $request)
    {
        $params = $request->only('user_id', 'content', 'image');
        $listMemberBlocked = Common::getListMemberBlocked($member->id, $params['user_id']);
        if (!empty($listMemberBlocked) && !is_array($listMemberBlocked) && $listMemberBlocked == $member->id) {
            throw new \Exception(trans('message.member_blocked'), 400);
        }
        $listMemberHided = Common::getListMemberHided($member->id, $params['user_id']);
        if (!empty($listMemberHided) && !is_array($listMemberHided) && $listMemberHided == $member->id) {
            throw new \Exception(trans('message.member_blocked'), 400);
        }
        $mailbox = $this->mailBoxRepository->getMailBoxByUserId($member->id, $params['user_id']);
        $listNgWords = Cache::rememberForever('list_ng_words', function () {
            return DB::table('ng_words')->pluck('word');
        });
        if (!$mailbox) {
            $mailbox = $this->mailBoxRepository->create([
                'user1_id' => $member->id,
                'user2_id' => $params['user_id'],
            ]);
        }
        $imageFile = $request->file('image');
        $data = [
            'mailbox_id' => $mailbox->id,
            'user_no' => $member->id
        ];
        if (isset($params['content'])) {
            $data['content'] = $params['content'];
            $checkNgWord = $this->filterMessage($data['content'], $listNgWords->toArray());
            if (!empty($checkNgWord)) {
                $data['has_ng_word'] = 1;
                $data['total_ng_words'] = count($checkNgWord);
            }
        }
        if ($imageFile) {
            $filename = $this->upload($imageFile, 'public/messages');
            $image = $this->imageRepository->create(['filename' => $filename]);
            $data['image'] = !is_null($image) ? $image->filename : null;
        }
        $item = $this->repository->create($data);
        $isNotificationMessage = $this->settingRepository->getSettingByKey('SETTING_NOTIFICATION_MESSAGE');
        if ($isNotificationMessage && $isNotificationMessage->value == 1) {
            $dataNotification = [
                'title' => $member->nickname . 'さんから新しいメッセージが届きました。',
                'body' => $member->nickname . 'さんから新しいメッセージが届きました。',
                'data_custom' => [
                    'type' => 'MESSAGE',
                ]
            ];
            // PushNotificationAndroid::dispatch($dataNotification, $params['user_id']);
            PushNotificationIos::dispatch($dataNotification, $params['user_id']);
        }
        $data = $item;
        $data['user_id'] = $item->user_no;
        unset($data['user_no']);
        $data['image'] = $item->getImageUrl();
        return $data;
    }

    private function getMessageContent($item, $memberId)
    {
        $content = $item->content;
        if ($item->has_ng_word == 1 && $item->user_no == $memberId) {
            $content = '運営によってメッセージが削除されました。';
        } elseif ($item->has_ng_word == 1 && $item->user_no != $memberId) {
            $content = 'メッセージが削除されました。';
        }
        return $content;
    }

    public function messageLatest($member, $mailBoxId, $messageLatestId = null)
    {
        $conditions = [];
        if (!is_null($messageLatestId)) {
            $conditions = [
                ['id', '>', $messageLatestId]
            ];
        }
        $messages = $this->repository->getListByMailBoxId($mailBoxId, false, $conditions);
        $listMessages = [];
        if ($messages->count()) {
            foreach ($messages as $item) {
                $viewed = $item->viewed;
                if ($item->user_no != $member->id) {
                    $viewed = 1;
                }
                $listMessages[] = [
                    'id' => $item->id,
                    'mailbox_id' => $item->mailbox_id,
                    'user_id' => $item->user_no,
                    'avatar' => !is_null($item->user) ? $item->user->getAvatarUrl() : '',
                    'content' => $this->getMessageContent($item, $member->id),
                    'image' => $item->getImageUrl(),
                    'viewed' => $viewed,
                    'updated_at' => !is_null($item->updated_at) ? $item->updated_at->format(DATETIME_FORMAT) : null,
                    'created_at' => !is_null($item->created_at) ? $item->created_at->format(DATETIME_FORMAT) : null,
                ];
            }
            $this->setViewed($mailBoxId, $member->id, false);
        }
        return $listMessages;
    }

    // detected = false when no detect user_no
    public function setViewed($mailBoxId, $memberId, $detected = true)
    {
        $conditions = [];
        if ($detected) {
            $conditions = [
                'mailbox_id' => $mailBoxId,
                'user_no' => $memberId
            ];
        } else {
            $conditions = [
                ['mailbox_id', '=', $mailBoxId],
                ['user_no', '!=', $memberId]
            ];
        }
        return $this->repository->forcedUpdate([
            'viewed' => 1
        ], $conditions);
    }
}
