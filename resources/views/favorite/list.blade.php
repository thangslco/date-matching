@extends('layouts.default')

@section('title', 'すきなものバッジ管理')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">すきなものバッジ管理</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">すきなものバッジ管理</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-success">{{ Session::get('error') }}</div>
                @endif
                <div class="modal fade" id="modal-delete-favorite" data-url="{{route('favorites.delete')}}" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">{{__('messages.heading_modal')}}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>{{__('messages.content_confirm_delete_favorite')}}</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{__('messages.btn_close')}}</button>
                                <button type="button" id="btn-delete-favorite" class="btn btn-danger">{{__('messages.btn_delete')}}</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal fade" id="modal-delete-favorite-success" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">{{__('messages.heading_modal')}}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>{{__('messages.content_confirm_delete_favorite')}}</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{__('messages.btn_close')}}</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="card">
                    <div class="card-header row">
                        <div class="col-md-6">
                            <a href="{{route('favorites.create', ['tab' => request()->tab])}}" class="btn btn-success">新規作成</a>
                        </div>
                        <div class="col-md-6">
                            <div class="card-tools fa-pull-right">
                                <form action="" method="GET">
                                    <input type="hidden" name='tab' value="{!! request()->tab !!}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="keyword" class="form-control float-right" value="{!! request()->keyword !!}" placeholder="Search...">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card ui-tabs ui-corner-all ui-widget ui-widget-content tab-favorites" id="tabs">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                <li class="nav-item"><a id="nav-link-0" class="nav-link pink {{$tabActive == 'p1' ? 'active' : ''}}" href="#tab-1" data-toggle="tab">動物</a></li>
                                <li class="nav-item"><a id="nav-link-1" class="nav-link blue {{$tabActive == 'p2' ? 'active' : ''}}" href="#tab-2" data-toggle="tab">アウトドア</a></li>
                                <li class="nav-item"><a id="nav-link-2" class="nav-link orange {{$tabActive == 'p3' ? 'active' : ''}}" href="#tab-3" data-toggle="tab">趣味</a></li>
                                <li class="nav-item"><a id="nav-link-3" class="nav-link green {{$tabActive == 'p4' ? 'active' : ''}}" href="#tab-4" data-toggle="tab">休日</a></li>
                                <li class="nav-item"><a id="nav-link-4" class="nav-link red {{$tabActive == 'p5' ? 'active' : ''}}" href="#tab-5" data-toggle="tab">飲食</a></li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body p-0">
                            <div class="tab-content">
                                <div class="tab-pane {{$tabActive == 'p1' ? 'active' : ''}}" id="tab-1">
                                    <div class="card-body p-0">
                                        <!-- Table -->
                                        <table class="table table-hover table-bordered table-sm">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>{{__('messages.favorites.lbl_name')}}</th>
                                                    <th>{{__('messages.lbl_created_at')}}</th>
                                                    <th>{{__('messages.lbl_updated_at')}}</th>
                                                    <th>{{__('messages.lbl_action')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($listPinks as $item)
                                                <tr>
                                                    <td>{{ $item->id }}</td>
                                                    <td>{!! $item->name !!}</td>
                                                    <td>{!! $item->created_at !!}</td>
                                                    <td>{!! $item->updated_at !!}</td>
                                                    <td>
                                                        <a href="{{ route('favorites.detail', $item->id)}}" class="btn btn-primary edit-favorite" data-id="{{$item->id}}">編集</a>
                                                        <button type="button" class="btn btn-danger delete-favorite" data-id="{{$item->id}}">削除</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <!-- /.table -->
                                    </div>
                                    <div class="card-footer clearfix">
                                        {!! $listPinks->links() !!}
                                    </div>
                                </div>
                                <div class="tab-pane {{$tabActive == 'p2' ? 'active' : ''}}" id="tab-2">
                                    <div class="card-body p-0">
                                        <!-- Table -->
                                        <table class="table table-hover table-bordered table-sm">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>{{__('messages.favorites.lbl_name')}}</th>
                                                    <th>{{__('messages.lbl_created_at')}}</th>
                                                    <th>{{__('messages.lbl_updated_at')}}</th>
                                                    <th>{{__('messages.lbl_action')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($listBlues as $item)
                                                <tr>
                                                    <td>{{ $item->id }}</td>
                                                    <td>{!! $item->name !!}</td>
                                                    <td>{!! $item->created_at !!}</td>
                                                    <td>{!! $item->updated_at !!}</td>
                                                    <td>
                                                        <a href="{{ route('favorites.detail', $item->id)}}" class="btn btn-primary edit-favorite" data-id="{{$item->id}}">編集</a>
                                                        <button type="button" class="btn btn-danger delete-favorite" data-id="{{$item->id}}">削除</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <!-- /.table -->
                                    </div>
                                    <div class="card-footer clearfix">
                                        {!! $listBlues->links() !!}
                                    </div>
                                </div>
                                <div class="tab-pane {{$tabActive == 'p3' ? 'active' : ''}}" id="tab-3">
                                    <div class="card-body p-0">
                                        <!-- Table -->
                                        <table class="table table-hover table-bordered table-sm">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>{{__('messages.favorites.lbl_name')}}</th>
                                                    <th>{{__('messages.lbl_created_at')}}</th>
                                                    <th>{{__('messages.lbl_updated_at')}}</th>
                                                    <th>{{__('messages.lbl_action')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($listOranges as $item)
                                                <tr>
                                                    <td>{{ $item->id }}</td>
                                                    <td>{!! $item->name !!}</td>
                                                    <td>{!! $item->created_at !!}</td>
                                                    <td>{!! $item->updated_at !!}</td>
                                                    <td>
                                                        <a href="{{ route('favorites.detail', $item->id)}}" class="btn btn-primary edit-favorite" data-id="{{$item->id}}">編集</a>
                                                        <button type="button" class="btn btn-danger delete-favorite" data-id="{{$item->id}}">削除</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <!-- /.table -->
                                    </div>
                                    <div class="card-footer clearfix">
                                        {!! $listOranges->links() !!}
                                    </div>
                                </div>
                                <div class="tab-pane {{$tabActive == 'p4' ? 'active' : ''}}" id="tab-4">
                                    <div class="card-body p-0">
                                        <!-- Table -->
                                        <table class="table table-hover table-bordered table-sm">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>{{__('messages.favorites.lbl_name')}}</th>
                                                    <th>{{__('messages.lbl_created_at')}}</th>
                                                    <th>{{__('messages.lbl_updated_at')}}</th>
                                                    <th>{{__('messages.lbl_action')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($listGreens as $item)
                                                <tr>
                                                    <td>{{ $item->id }}</td>
                                                    <td>{!! $item->name !!}</td>
                                                    <td>{!! $item->created_at !!}</td>
                                                    <td>{!! $item->updated_at !!}</td>
                                                    <td>
                                                        <a href="{{ route('favorites.detail', $item->id)}}" class="btn btn-primary edit-favorite" data-id="{{$item->id}}">編集</a>
                                                        <button type="button" class="btn btn-danger delete-favorite" data-id="{{$item->id}}">削除</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <!-- /.table -->
                                    </div>
                                    <div class="card-footer clearfix">
                                        {!! $listGreens->links() !!}
                                    </div>
                                </div>
                                <div class="tab-pane {{$tabActive == 'p5' ? 'active' : ''}}" id="tab-5">
                                    <div class="card-body p-0">
                                        <!-- Table -->
                                        <table class="table table-hover table-bordered table-sm">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>{{__('messages.favorites.lbl_name')}}</th>
                                                    <th>{{__('messages.lbl_created_at')}}</th>
                                                    <th>{{__('messages.lbl_updated_at')}}</th>
                                                    <th>{{__('messages.lbl_action')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($listReds as $item)
                                                <tr>
                                                    <td>{{ $item->id }}</td>
                                                    <td>{!! $item->name !!}</td>
                                                    <td>{!! $item->created_at !!}</td>
                                                    <td>{!! $item->updated_at !!}</td>
                                                    <td>
                                                        <a href="{{ route('favorites.detail', $item->id)}}" class="btn btn-primary edit-favorite" data-id="{{$item->id}}">編集</a>
                                                        <button type="button" class="btn btn-danger delete-favorite" data-id="{{$item->id}}">削除</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <!-- /.table -->
                                    </div>
                                    <div class="card-footer clearfix">
                                        {!! $listReds->links() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('style')
@parent
@endsection
@section('script')
@parent
@endsection