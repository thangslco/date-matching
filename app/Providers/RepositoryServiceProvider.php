<?php

namespace App\Providers;

use App\Repositories\Contracts\FavoriteRepository;
use App\Repositories\Contracts\GroupRepository;
use App\Repositories\Contracts\ImageRepository;
use App\Repositories\Contracts\ItemRepository;
use App\Repositories\Contracts\LikeRepository;
use App\Repositories\Contracts\MailBoxRepository;
use App\Repositories\Contracts\MemberBlockRepository;
use App\Repositories\Contracts\MemberHideRepository;
use App\Repositories\Contracts\MemberRepository;
use App\Repositories\Contracts\MessageRepository;
use App\Repositories\Contracts\NextRepository;
use App\Repositories\Contracts\NgWordRepository;
use App\Repositories\Contracts\NoticeRepository;
use App\Repositories\Contracts\PostLikeRepository;
use App\Repositories\Contracts\PostRepository;
use App\Repositories\Contracts\ReportRepository;
use App\Repositories\Contracts\SettingRepository;
use App\Repositories\Contracts\TransactionRepository;
use App\Repositories\Contracts\UserRepository;
use App\Repositories\Contracts\UserTokenRepository;
use App\Repositories\Contracts\VerifyCodeRepository;
use App\Repositories\FavoriteRepositoryEloquent;
use App\Repositories\GroupRepositoryEloquent;
use App\Repositories\ImageRepositoryEloquent;
use App\Repositories\ItemRepositoryEloquent;
use App\Repositories\LikeRepositoryEloquent;
use App\Repositories\MailBoxRepositoryEloquent;
use App\Repositories\MemberBlockRepositoryEloquent;
use App\Repositories\MemberHideRepositoryEloquent;
use App\Repositories\MemberRepositoryEloquent;
use App\Repositories\MessageRepositoryEloquent;
use App\Repositories\NextRepositoryEloquent;
use App\Repositories\NgWordRepositoryEloquent;
use App\Repositories\NoticeRepositoryEloquent;
use App\Repositories\PostLikeRepositoryEloquent;
use App\Repositories\PostRepositoryEloquent;
use App\Repositories\ReportRepositoryEloquent;
use App\Repositories\SettingRepositoryEloquent;
use App\Repositories\TransactionRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\UserTokenRepositoryEloquent;
use App\Repositories\VerifyCodeRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register repositories for app
     *
     * @var array
     */
    protected $repositories = [
        UserRepository::class => UserRepositoryEloquent::class,
        MemberRepository::class => MemberRepositoryEloquent::class,
        ImageRepository::class => ImageRepositoryEloquent::class,
        MessageRepository::class => MessageRepositoryEloquent::class,
        GroupRepository::class => GroupRepositoryEloquent::class,
        ItemRepository::class => ItemRepositoryEloquent::class,
        UserTokenRepository::class => UserTokenRepositoryEloquent::class,
        NoticeRepository::class => NoticeRepositoryEloquent::class,
        FavoriteRepository::class => FavoriteRepositoryEloquent::class,
        LikeRepository::class => LikeRepositoryEloquent::class,
        MailBoxRepository::class => MailBoxRepositoryEloquent::class,
        PostRepository::class => PostRepositoryEloquent::class,
        PostLikeRepository::class => PostLikeRepositoryEloquent::class,
        ReportRepository::class => ReportRepositoryEloquent::class,
        MemberBlockRepository::class => MemberBlockRepositoryEloquent::class,
        MemberHideRepository::class => MemberHideRepositoryEloquent::class,
        NextRepository::class => NextRepositoryEloquent::class,
        NgWordRepository::class => NgWordRepositoryEloquent::class,
        SettingRepository::class => SettingRepositoryEloquent::class,
        TransactionRepository::class => TransactionRepositoryEloquent::class,
        VerifyCodeRepository::class => VerifyCodeRepositoryEloquent::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->repositories as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }
}
