@extends('layouts.default')

@section('title', 'デートマッチング')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">ダッシュボード</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active">ダッシュボード</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">会員数</span>
                        <span class="info-box-number">113名</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-comments"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">メッセージ数</span>
                        <span class="info-box-number">51</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-thumbs-up"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">いいね！数</span>
                        <span class="info-box-number">{{$totalLiked}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-bullhorn"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">呼びだけ数</span>
                        <span class="info-box-number">{{$totalPosts}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">収入金額</h5>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                                    <i class="fas fa-wrench"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" role="menu">
                                    <a href="#" class="dropdown-item">Action</a>
                                    <a href="#" class="dropdown-item">Another action</a>
                                    <a href="#" class="dropdown-item">Something else here</a>
                                    <a class="dropdown-divider"></a>
                                    <a href="#" class="dropdown-item">Separated link</a>
                                </div>
                            </div>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <p class="text-center">
                                    <strong>売り上げ {{$rangeDate}}</strong>
                                </p>

                                <div class="chart">
                                    <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>
                                </div>
                                <!-- /.chart-responsive -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <p class="text-center">
                                </p>

                                <div class="progress-group">
                                    いいね｜購入した金額
                                    <span class="float-right"><b>{{$totalMoneyLike}}</b>円/{{$totalMoney12MonthAgo}}円</span>
                                    <div class="progress progress-sm">
                                        @if ($totalMoney12MonthAgo > 0)
                                        <div class="progress-bar bg-primary" style="width: {{round($totalMoneyLike/$totalMoney12MonthAgo, 2) * 100}}%"></div>
                                        @else
                                        <div class="progress-bar bg-primary" style="width: 0%"></div>
                                        @endif
                                    </div>
                                </div>
                                <!-- /.progress-group -->

                                <div class="progress-group">
                                    VIP｜購入した金額
                                    <span class="float-right"><b>{{$totalMoneyVip}}</b>円/{{$totalMoney12MonthAgo}}円</span>
                                    <div class="progress progress-sm">
                                        @if ($totalMoney12MonthAgo > 0)
                                        <div class="progress-bar bg-danger" style="width: {{round($totalMoneyVip/$totalMoney12MonthAgo, 2) * 100}}%"></div>
                                        @else
                                        <div class="progress-bar bg-danger" style="width: 0%"></div>
                                        @endif
                                    </div>
                                </div>

                                <!-- /.progress-group -->
                                <div class="progress-group">
                                    <span class="progress-text">有料｜購入した金額</span>
                                    <span class="float-right"><b>{{$totalMoneyPaid}}</b>円/{{$totalMoney12MonthAgo}}円</span>
                                    <div class="progress progress-sm">
                                        @if ($totalMoney12MonthAgo > 0)
                                        <div class="progress-bar bg-success" style="width: {{round($totalMoneyPaid/$totalMoney12MonthAgo, 2) * 100}}%"></div>
                                        @else
                                        <div class="progress-bar bg-success" style="width: 0%"></div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- ./card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-sm-4 col-6">
                                <div class="description-block border-right">
                                    <span class="description-percentage text-warning"><i class="fas"></i></span>
                                    <h5 class="description-header">{{$totalMoney}}円</h5>
                                    <span class="description-text">全て期間</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 col-6">
                                <div class="description-block border-right">
                                    @if ($rateYear > 0)
                                    <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> {{$rateYear}}%</span>
                                    @elseif ($rateYear < 0)
                                    <span class="description-percentage text-warning"><i class="fas fa-caret-down"></i> {{$rateYear}}%</span>
                                    @else
                                    <span class="description-percentage text-success"><i class="fas"></i> {{$rateYear}}%</span>
                                    @endif
                                    <h5 class="description-header">{{$totalMoneyCurrentYear}}円</h5>
                                    <span class="description-text">今年</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 col-6">
                                <div class="description-block">
                                    @if ($rateMonth > 0)
                                    <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> {{$rateMonth}}%</span>
                                    @elseif ($rateMonth < 0)
                                    <span class="description-percentage text-warning"><i class="fas fa-caret-down"></i> {{$rateMonth}}%</span>
                                    @else
                                    <span class="description-percentage text-success"><i class="fas"></i> {{$rateMonth}}%</span>
                                    @endif
                                    <h5 class="description-header">{{$totalMoneyCurrentMonth}}円</h5>
                                    <span class="description-text">今月</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-6">
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-footer -->

                    <div class="row" style="margin-top: 20px; padding: 0px 10px;">
                        <div class="col-12 col-sm-6 col-md-3" style="max-width: 20%">
                            <div class="info-box" style="background: #eee">
                                <div class="info-box-content">
                                    <span class="info-box-number bold">男性の有料プラン加入者人数</span>
                                <span class="info-box-text">{{ $totalUserHasFee }}人</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-12 col-sm-6 col-md-3" style="max-width: 20%">
                            <div class="info-box mb-3" style="background: #eee">
                                <div class="info-box-content">
                                    <span class="info-box-number">男性の無料プランの人数</span>
                                    <span class="info-box-text">{{ $totalUserNotHasFeeOrExpired }}人</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix hidden-md-up"></div>

                        <div class="col-12 col-sm-6 col-md-3" style="max-width: 20%">
                            <div class="info-box mb-3" style="background: #eee">
                                <div class="info-box-content">
                                    <span class="info-box-number">女性の本人確認済みの人数</span>
                                    <span class="info-box-text">{{$totalUserFemaleVerified}}人</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-12 col-sm-6 col-md-3" style="max-width: 20%">
                            <div class="info-box mb-3" style="background: #eee">
                                <div class="info-box-content">
                                    <span class="info-box-number">女性の本人未確認の人数</span>
                                    <span class="info-box-text">{{$totalUserFemaleNotVerified}}人</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-12 col-sm-6 col-md-3" style="max-width: 20%">
                            <div class="info-box mb-3" style="background: #eee">
                                <div class="info-box-content">
                                    <span class="info-box-number">VIPプランの加入人数</span>
                                    <span class="info-box-text">{{$totalUserBuyVIP}}人</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <!-- Info Boxes Style 2 -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">年齢割合</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="chart-responsive">
                                            <canvas id="pieChart-age" height="150"></canvas>
                                        </div>
                                        <!-- ./chart-responsive -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-4">
                                        <ul class="chart-legend clearfix">
                                            @foreach ($labelsAge as $labelAge => $color)
                                            <li><i class="far fa-circle" style="color: {{$color}}!important;"></i> {{$labelAge}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4">
                        <!-- Info Boxes Style 2 -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">男女割合</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="chart-responsive">
                                            <canvas id="pieChart-gender" height="150"></canvas>
                                        </div>
                                        <!-- ./chart-responsive -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-4">
                                        <ul class="chart-legend clearfix">
                                            @foreach ($labelsGender as $labelGender => $color)
                                            <li><i class="far fa-circle" style="color: {{$color}}!important;"></i> {{$labelGender}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4">
                        <!-- Info Boxes Style 2 -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">いいね！割合</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="chart-responsive">
                                            <canvas id="pieChart-like" height="150"></canvas>
                                        </div>
                                        <!-- ./chart-responsive -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-4">
                                        <ul class="chart-legend clearfix">
                                            @foreach ($labelsLike as $labelLike => $color)
                                            <li><i class="far fa-circle" style="color: {{$color}}!important;"></i> {{$labelLike}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!--/. container-fluid -->
</section>
<!-- /.content -->
@endsection
@section('style')
@parent
@endsection
@section('script')
@parent
<script src="{{ url('/assets/js/Chart.min.js') }}"></script>
<script>
    var pieAgeLabels = JSON.parse('{!!json_encode(array_keys($dataAge))!!}');
    var pieAgeData = JSON.parse('{!!json_encode(array_values($dataAge))!!}');
    var pieGenderLabels = JSON.parse('{!!json_encode(array_keys($dataGender))!!}');
    var pieGenderData = JSON.parse('{!!json_encode(array_values($dataGender))!!}');
    var pieLikeLabels = JSON.parse('{!!json_encode(array_keys($dataLike))!!}');
    var pieLikeData = JSON.parse('{!!json_encode(array_values($dataLike))!!}');
    var labelsGraph = JSON.parse('{!!json_encode($labelsGraph)!!}');
    var dataGraph = JSON.parse('{!!json_encode(array_values($dataGraph))!!}');
</script>
<script src="{{ url('/assets/js/dashboard.js') }}"></script>
<script type="text/javascript">
    create_pie_chart('age', pieAgeLabels, pieAgeData);
    create_pie_chart('gender', pieGenderLabels, pieGenderData);
    create_pie_chart('like', pieLikeLabels, pieLikeData);
</script>
@endsection