<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

/**
 * Class User.
 *
 * @package namespace App\Models;
 */
class User extends Authenticatable
{
    use Notifiable;
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'type', 'password', 'email_verified_at', 'remember_token', 'login_at', 'logout_at'];
    protected $hidden = ['password'];
    protected $dates = ['created_at', 'updated_at', 'login_at', 'logout_at'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getTypeName()
    {
        if (isset(LIST_TYPE_USER[$this->type])) {
            return LIST_TYPE_USER[$this->type];
        }
        return 'Unknown';
    }

    public function getLoginAt($format = 'Y/m/d H:i:s')
    {
        if (!is_null($this->login_at)) {
            return $this->login_at->format($format);
        }
        return '';
    }

    public function getLogoutAt($format = 'Y/m/d H:i:s')
    {
        if (!is_null($this->logout_at)) {
            return $this->logout_at->format($format);
        }
        return '';
    }
}
