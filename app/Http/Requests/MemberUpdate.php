<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use DateTime;

class MemberUpdate extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type = $this->get('type');
        $gender = 0;
        if (!is_null($type) && $type == 'male') {
            $gender = 1;
        }
        $rules = [
            'nickname' => 'required|max:150|unique:members,nickname,' . $this->id,
            'email' => 'required|max:255|email|unique:members,email,' . $this->id,
            'birthday' => 'date|date_format:Y-m-d|before:now',
            'address' => 'nullable|string|max:255',
            'password' => 'nullable|max:255',
            'phone' => 'nullable|string|max:30|unique:members,phone,' . $this->id,
            'place_of_birth' => 'nullable|integer',
            'education' => 'nullable|integer',
            'height' => 'nullable|integer',
            'sake' => 'nullable|integer',
            'smoke' => 'nullable|integer',
            'blood_type' => 'nullable|integer',
            'thing_of_marriage' => 'nullable|integer',
            'annual_income' => 'nullable|integer',
            'job_type' => 'nullable|integer',
            'hair' => 'nullable|integer',
            'hair_color' => 'nullable|integer',
            'option_1' => 'nullable|integer',
            'option_2' => 'nullable|integer',
            'option_3' => 'nullable|integer',
            'option_4' => 'nullable|integer',
        ];
        $dataHumanForms = $this->get('human_forms');
        if (!is_null($dataHumanForms)) {
            $listKeysDataHumanForms = array_keys($dataHumanForms);
            foreach ($listKeysDataHumanForms as $key) {
                $rules['human_forms.' . $key] = 'nullable|in:' . implode(',', array_keys(LIST_HUMAN_FORM));
            }
        }
        $listAppearances = LIST_APPEARANCE_FEMALE;
        if ($gender == 1) {
            $listAppearances = LIST_APPEARANCE_MALE;
        }
        $dataAppearances = $this->get('appearances');
        if (!is_null($dataAppearances)) {
            $listKeysDataAppearances = array_keys($dataAppearances);
            foreach ($listKeysDataAppearances as $key) {
                $rules['appearances.' . $key] = 'nullable|in:' . implode(',', array_keys($listAppearances));
            }
        }
        $listImpressives = LIST_IMPRESSIVE_FEMALE;
        if ($gender == 1) {
            $listImpressives = LIST_IMPRESSIVE_MALE;
        }
        $dataImpressives = $this->get('impressives');
        if (!is_null($dataImpressives)) {
            $listKeysDataImpressives = array_keys($dataImpressives);
            foreach ($listKeysDataImpressives as $key) {
                $rules['impressives.' . $key] = 'nullable|in:' . implode(',', array_keys($listImpressives));
            }
        }
        return $rules;
    }

    public function messages()
    {
        $messages = [];
        $dataHumanForms = $this->get('human_forms');
        if (!is_null($dataHumanForms)) {
            $listKeysDataHumanForms = array_keys($dataHumanForms);
            foreach ($listKeysDataHumanForms as $key) {
                $messages['human_forms.' . $key] = "Value of 体格 at {$key} invalid";
            }
        }
        $dataAppearances = $this->get('appearances');
        if (!is_null($dataAppearances)) {
            $listKeysDataAppearances = array_keys($dataAppearances);
            foreach ($listKeysDataAppearances as $key) {
                $messages['appearances.' . $key] = "Value of 見た目 at {$key} invalid";
            }
        }
        $dataImpressives = $this->get('impressives');
        if (!is_null($dataImpressives)) {
            $listKeysDataImpressives = array_keys($dataImpressives);
            foreach ($listKeysDataImpressives as $key) {
                $messages['impressives.' . $key] = "Value of イメージ at {$key} invalid";
            }
        }
        return $messages;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $d = DateTime::createFromFormat('Y/m/d', $this->birthday);
        if ($d && $d->format('Y/m/d') === $this->birthday) {
            $this->merge([
                'birthday' => Carbon::parse($this->birthday)->format(DATE_FORMAT)
            ]);
        }
    }
}
