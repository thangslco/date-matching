<?php

namespace App\Models;

/**
 * Class MailBox
 *
 * @package namespace App\Models;
 */
class MailBox extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user1_id', 'user2_id', 'created_at', 'updated_at'];
    protected $dates = ['created_at', 'updated_at'];

    public function user1()
    {
        return $this->belongsTo('App\Models\Member', 'user1_id', 'id');
    }

    public function user2()
    {
        return $this->belongsTo('App\Models\Member', 'user2_id', 'id');
    }
}
