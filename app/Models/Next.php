<?php

namespace App\Models;

/**
 * Class Next
 *
 * @package namespace App\Models;
 */
class Next extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['owner_id', 'id_next'];

    protected $dates = ['created_at', 'updated_at'];

    public function owner()
    {
        return $this->belongsTo('App\Models\Member', 'owner_id', 'id');
    }

    public function userNext()
    {
        return $this->belongsTo('App\Models\Member', 'id_next', 'id');
    }
}
