<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $groups = [
            'オシャレ好き',
            '食べ歩き',
            '映画好き',
            'ショッピング好き',
            'お酒好き',
            'カフェ好き',
            '動物好き',
            'アニメ好き',
            '旅行好き',
            '音楽好き',
            '料理好き',
            'トレーニング',
            'ドラマ好き',
            '将来を考えて',
            'マンガ好き',
            '車好き',
            'ゲーム好き',
            '海好き',
            'お笑い好き',
            'インドア派',
            'アウトドア派',
       ];
       $data = [];
       foreach ($groups as $item) {
            $data[] = [
                'name' => $item,
                'joined' => rand(1, 100),
                'description' => Str::random(200),
                'image_id' => rand(1, 10),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
       }
       DB::table('groups')->insert($data);
    }
}
