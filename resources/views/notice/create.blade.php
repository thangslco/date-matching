@php
use App\Helpers\Common;
@endphp
@extends('layouts.default')

@section('title', $title)

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{$title}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active">{{$title}}</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<section class="content">
    <div class="container-fluid">
        @if (Session::has('message'))
            <div class="alert alert-success">{{ Session::get('message') }}</div>
        @endif
        <form action="{{$action}}" method="POST">
            @csrf
            <!-- 基本プロフィール -->
            <div class="row" id="block-1">
                <div class="col-sm-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Info</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label for="self_introduction" class="col-sm-2 col-form-label"><label for="title">タイトル</label>：</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" id="title" name="title" placeholder="タイトル" required type="text" value="{{ old('title') ?? request()->title }}">
                                            @if ($errors->has('title'))
                                                <span class="error invalid-feedback">{{ $errors->first('title') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label for="self_introduction" class="col-sm-2 col-form-label"><label for="content">内容</label>：</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="content" name="content" placeholder="内容" required rows="4">{{ old('content') ?? request()->content }}</textarea>
                                            @if ($errors->has('content'))
                                                <span class="error invalid-feedback">{{ $errors->first('content') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label for="self_introduction" class="col-sm-2 col-form-label"><label for="input-category">{{__('messages.notices.lbl_category')}}</label>：</label>
                                        <div class="col-sm-10">
                                            <select id="input-category" name="category" class="form-control" data-fillr="bound" autocomplete="off">
                                                @php
                                                    $category = old('category') ?? request()->category;
                                                @endphp
                                                @foreach(LIST_CATEGORY_NOTICES as $key => $item)
                                                <option value="{{$key}}" {{$category == $key ? 'selected' : '' }}>{{$item}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('category'))
                                            <span class="error invalid-feedback">{{ $errors->first('category') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-info">
                        <div class="card-footer">
                            @if(isset($id) && !empty($id))
                            <button type="submit" class="btn btn-success float-right">{{__('messages.btn_update')}}</button>
                            @else
                            <button type="submit" class="btn btn-success float-right">{{__('messages.btn_create')}}</button>
                            @endif
                            <a href="{{route('notices')}}" class="btn btn-default">Back</a>
                        </div>
                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('style')
    @parent
@endsection
@section('script')
    @parent
@endsection