<?php

namespace App\Http\Requests;


class SavePasswordRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|string|min:6'
        ];
    }

    public function messages()
    {
        return [
            'password.min' => '半角英数字で6文字以上を入力してください。'
        ];
    }
}
