<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Common;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\ReplyPostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Services\PostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostController extends Controller
{
    protected $postService;

    public function __construct(PostService $service)
    {
        parent::__construct();
        $this->postService = $service;
    }

    /**
     * Get category trend
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function categoryTrend(Request $request)
    {
        try {
            $items = Common::getCategoryTrend();
            $data = [
                'category_1' => [
                    'index' => 1,
                    'title' => LIST_CATEGORY_1[1],
                    'data' => []
                ],
                'category_2' => [
                    'index' => 2,
                    'title' => LIST_CATEGORY_1[2],
                    'data' => []
                ]
            ];
            $listCategories1 = LIST_CATEGORY_3[12] + LIST_CATEGORY_3[13];
            $listCategories1Id = array_keys($listCategories1);
            $listCategories2 = LIST_CATEGORY_3[22] + LIST_CATEGORY_3[23];
            $listCategories2Id = array_keys($listCategories2);
            foreach ($items as $categoryId => $total) {
                if (in_array($categoryId, $listCategories1Id)) {
                    $data['category_1']['data'][] = [
                        'category_id' => $categoryId,
                        'name' => Common::getCategoryNameById($categoryId),
                        'total' => $total
                    ];
                    continue;
                }
                if (in_array($categoryId, $listCategories2Id)) {
                    $data['category_2']['data'][] = [
                        'category_id' => $categoryId,
                        'name' => Common::getCategoryNameById($categoryId),
                        'total' => $total
                    ];
                }
            }
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API get category trend: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Create new post
     *
     * @param CreatePostRequest $request
     * @return JsonResponse
     */
    public function store(CreatePostRequest $request)
    {
        try {
            $member = request()->user;
            if ($member->gender == 1) {
                throw new \Exception("Only female called this API", 400);
            }
            $params = $request->only('category1_id', 'category2_id', 'category3_id', 'type_time', 'content');
            if (!isset($params['type_time'])) {
                $params['type_time'] = null;
            }
            $date = $request->get('date');
            $params['date'] = !is_null($date) ? $date : date(DATETIME_FORMAT);
            $params['member_id'] = $member->id;
            $params['status'] = 1;
            $data = $this->postService->create($params);
            $data['type_time_text'] = $data->getTypeTimeText();
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API create new post: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Update post
     *
     * @param UpdatePostRequest $request
     * @return JsonResponse
     */
    public function update(UpdatePostRequest $request, $postId)
    {
        try {
            $member = request()->user;
            if ($member->gender == 1) {
                throw new \Exception("Only female called this API", 400);
            }
            $post = $this->postService->firstByConditions(['id' => $postId, 'member_id' => $member->id]);
            if (!$post) {
                throw new \Exception('Post not found', 400);
            }
            $paramsUpdate = ['category1_id', 'category2_id', 'category3_id', 'type_time', 'content'];
            $params = $request->all();
            $dataUpdate = [];
            foreach ($paramsUpdate as $itemParam) {
                if (isset($params[$itemParam])) {
                    $dataUpdate[$itemParam] = $params[$itemParam];
                }
            }
            $dataUpdate['status'] = 1;
            $result = $this->postService->update($params, $postId);
            $data = [
                'status' => false,
                'message' => 'Update fail'
            ];
            if ($result) {
                $data = [
                    'id' => $result->id,
                    'member_id' => $result->member_id,
                    'category_1' => $result->getCategory1(),
                    'category_2' => $result->getCategory2(),
                    'category_3' => $result->getCategory3(),
                    'content' => $result->content,
                    'type_time' => $result->getTypeTimeText(),
                    'date' => !is_null($result->date) ? $result->date->format(DATE_FORMAT) : null,
                    'status' => $result->status,
                    'created_at' => !is_null($result->created_at) ? $result->created_at->format(DATETIME_FORMAT) : null,
                    'updated_at' => !is_null($result->updated_at) ? $result->updated_at->format(DATETIME_FORMAT) : null,
                ];
            }
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API update post: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Create reply post
     *
     * @param ReplyPostRequest $request
     * @return JsonResponse
     */
    public function reply(ReplyPostRequest $request, $id)
    {
        try {
            $member = request()->user;
            if ($member->gender == 0) {
                throw new \Exception("Only male called this API", 400);
            }
            $params = $request->only('message');
            $params['id'] = $id;
            $params['member_id'] = $member->id;
            $data = $this->postService->replyPost($params);
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API reply post: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Like post
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function like(Request $request, $postId)
    {
        DB::beginTransaction();
        try {
            $member = request()->user;
            $data = $this->postService->like($member, $postId);
            DB::commit();
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('========== Exception ==============');
            Log::error('API like post: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Change status of post to viewed
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function viewed(Request $request, $postId)
    {
        try {
            $member = request()->user;
            $data = $this->postService->viewed($member, $postId);
            if ($data) {
                return response()->jsonSuccess(['status' => true, 'message' => 'Viewed this post']);
            }
            return response()->jsonSuccess(['status' => false, 'message' => "This post don't viewed"]);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API viewed: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Create stop post
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function stop(Request $request, $id)
    {
        try {
            $member = request()->user;
            $result = $this->postService->stopPost($member, $id);
            $data = [
                'status' => false,
                'message' => "Can't stop this post"
            ];
            if ($result) {
                $data = [
                    'status' => true,
                    'message' => 'This post stoped'
                ];
            }
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API stop post: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Search post
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request)
    {
        try {
            $member = request()->user;
            if ($member->gender == 0) {
                throw new \Exception("Only male called this API", 400);
            }
            $params = $request->only('age_min', 'age_max', 'address');
            $results = $this->postService->search($member, $params);
            return response()->jsonSuccess($results);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API stop post: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * List message by postId
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listMessageByPostId(Request $request, $postId)
    {
        try {
            $member = request()->user;
            $results = $this->postService->listMessageByPostId($member, $postId);
            $data = [];
            foreach ($results as $item) {
                $itemData = [
                    'id' => $item->id,
                    'post_id' => $item->post_id,
                    'owner_id' => $item->owner_id,
                    'content' => $item->content,
                    'is_viewed' => $item->is_viewed,
                    'member' => Common::getInfoMember($item->member, [], true),
                    'created_at' => !is_null($item->created_at) ? $item->created_at->format(DATETIME_FORMAT) : null,
                    'updated_at' => !is_null($item->updated_at) ? $item->updated_at->format(DATETIME_FORMAT) : null,
                ];
                $data[] = $itemData;
            }
            $results = [
                'total' => $results->total(),
                'per_page' => $results->perPage(),
                'current_page' => $results->currentPage(),
                'last_page' => $results->lastPage(),
                'more_page' => $results->hasMorePages(),
                'data' => $data
            ];
            return response()->jsonSuccess($results);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API list message by postId: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * List posts
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listPosts(Request $request)
    {
        try {
            $results = $this->postService->listPosts();
            return response()->jsonSuccess($results);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API list all posts: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * List message latest
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function latest(Request $request)
    {
        try {
            $member = request()->user;
            $results = $this->postService->getPostLatest($member->id);
            return response()->jsonSuccess($results);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API list message latest: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }
}
