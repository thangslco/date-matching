<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $menuActived = null;
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // TODO: Define some common middleware in here
        $listGroups = DB::table('groups')
                        ->leftJoin('images', 'images.id', '=', 'groups.image_id')
                        ->select('groups.*', 'images.filename', 'images.target_id')
                        ->get();
        $data = [];
        foreach ($listGroups as $item) {
            $image = null;
            if (!is_null($item->filename) && file_exists('storage/avatars/' . $item->filename)) {
                $image = asset('storage/avatars/' . $item->filename);
            }
            $data[] = [
                'id' => $item->id,
                'name' => $item->name,
                'description' => $item->description,
                'joined' => $item->joined,
                'image' => $image,
                'created_at' => date(DATETIME_FORMAT),
                'updated_at' => date(DATETIME_FORMAT)
            ];
        }
        config(['app.groups' => $data]);
    }
}
