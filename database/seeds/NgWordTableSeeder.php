<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class NgWordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            'SEX',
            'sex',
            'セックス',
            'せっくす',
            'エッチ',
            'えっち',
            'マンコ',
            'まんこ',
            'ちんこ',
            'チンコ',
            'おっぱい',
            'オッパイ',
            '性行為',
            '乱交',
            '淫乱',
            'フェラチオ',
            'フェラ',
            'ブス',
            '死ね',
            'おっぱい',
            'オッパイ',
            '性行為',
            '乱交',
            '淫乱',
            'フェラチオ',
            'フェラ',
            'ブス',
            '死ね',
            '援助交際',
            '円光',
            '援交',
            'パパカツ',
            'パパ活',
            'お手当て',
            '割り切った関係',
            '割り切り',
            'ソフレ',
            'そふれ',
            'シネ',
            'キモい',
            'キモイ',
            'きもい',
            'ブサイク',
            'デブ',
            'ぶす',
            'でぶ',
            '乳首',
            '殺す',
            'ころす',
            'コロス',
            'せふれ',
            'セフレ',
            '何カップ',
        ];
        $data = [];
        foreach ($groups as $item) {
            $data[] = [
                'word' => $item,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        DB::table('ng_words')->insert($data);
        Cache::rememberForever('list_ng_words', function () {
            return DB::table('ng_words')->pluck('word');
        });
    }
}
