<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_blocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_block_id');
            $table->bigInteger('user_blocked_id');
            $table->timestamps();
            $table->unique(['user_block_id', 'user_blocked_id'], 'un_member_blocks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_blocks');
    }
}
