@php
use App\Helpers\Common;
@endphp
@extends('layouts.default')

@section('title', $title)

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{$title}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">{{$title}}</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif

                <div class="card card-primary">
                    <!-- form start -->
                    <form role="form" id="quickForm" action="{{$action}}" method="POST" novalidate="novalidate" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="input-name">{{__('messages.favorites.lbl_name')}}</label>
                                <input type="text" name="name" class="form-control" id="input-name" placeholder="Input name" value="{{ old('name') ?? request()->name }}" autocomplete="off">
                                @if ($errors->has('name'))
                                <span class="error invalid-feedback">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="input-type">{{__('messages.favorites.lbl_type')}}</label>
                                <select id="input-type" name="type" class="form-control" data-fillr="bound" autocomplete="off">
                                    @php
                                        $type = old('type') ?? request()->type;
                                    @endphp
                                    @foreach(LIST_TYPE_FAVORITES as $key => $item)
                                    <option value="{{$key}}" {{$type == $key ? 'selected' : '' }}>{{$item}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('type'))
                                <span class="error invalid-feedback">{{ $errors->first('type') }}</span>
                                @endif
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            @if(isset($id) && !empty($id))
                            <button type="submit" class="btn btn-success float-right">{{__('messages.btn_update')}}</button>
                            @else
                            <button type="submit" class="btn btn-success float-right">{{__('messages.btn_create')}}</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('style')
    @parent
@endsection
@section('script')
    @parent
    @if (Session::has('password'))
    <script>
        $('#modal-default').modal();
    </script>
    @endif
@endsection