@php
use App\Helpers\Common;
@endphp
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
        <img src="/assets/admin-lte/dist/img/AdminLTELogo.png" alt="DATING" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">DATING</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/assets/admin-lte/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ get_email() }}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            ダッシュボード
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('male.list') }}" class="nav-link {{ Common::checkMenuActive('MENU_MALE', $menuActived)}}">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            男性登録一覧
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('female.list') }}" class="nav-link {{ Common::checkMenuActive('MENU_FEMALE', $menuActived)}}">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            女性登録一覧
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('members.list-paused') }}" class="nav-link {{ Common::checkMenuActive('MENU_MEMBER_PAUSED', $menuActived)}}">
                        <i class="nav-icon fas fa-pause"></i>
                        <p>
                            一時停止ユーザ一覧
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('members.list-canceled') }}" class="nav-link {{ Common::checkMenuActive('MENU_MEMBER_CANCELED', $menuActived)}}">
                        <i class="nav-icon fas fa-window-close"></i>
                        <p>
                            退会したユーザ一覧
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('messages') }}" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            メッセージ履歴
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('messages.ng_words') }}" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            悪い言葉フィルター
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('members.auth') }}" class="nav-link {{ Common::checkMenuActive('MENU_AUTH', $menuActived)}}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            身分証
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('groups') }}" class="nav-link {{ Common::checkMenuActive('MENU_GROUP', $menuActived)}}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            ジャンル管理
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('favorites') }}" class="nav-link {{ Common::checkMenuActive('MENU_FAVORITE', $menuActived)}}">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            すきなものバッジ管理
                        </p>
                    </a>
                </li>
                <li class="nav-item hr-bottom">
                    <a href="{{ route('notices') }}" class="nav-link {{ Common::checkMenuActive('MENU_NOTICE', $menuActived)}}">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            お知らせ
                        </p>
                    </a>
                </li>
                <li class="nav-item {{Common::checkPermission() ? '' : 'hide' }}">
                    <a href="{{ route('users') }}" class="nav-link {{ Common::checkMenuActive('MENU_USER', $menuActived)}}">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            管理者アカウント一覧
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('user.change-account') }}" class="nav-link {{ Common::checkMenuActive('MENU_CHANGE_ACCOUNT', $menuActived)}}">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            ログイン情報変更
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link">
                        <i class="nav-icon fas fa-tree"></i>
                        <p>
                            ログアウト
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>