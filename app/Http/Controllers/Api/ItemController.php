<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddItemRequest;
use App\Http\Requests\UseItemRequest;
use App\Services\ItemService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class ItemController extends Controller
{
    protected $itemService;
    public function __construct(ItemService $service)
    {
        parent::__construct();
        $this->itemService = $service;
    }

    /**
     * Get add item to user
     *
     * @param AddItemRequest $request
     * @return JsonResponse
     */
    public function add(AddItemRequest $request)
    {
        try {
            $this->itemService->addItem($request, request()->user);
            return response()->jsonSuccess(['message' => 'Add item success']);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API add item to user: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 65: Use item
     *
     * @param UseItemRequest $request
     * @return JsonResponse
     */
    public function useItem(UseItemRequest $request)
    {
        try {
            $result = $this->itemService->useItem($request, request()->user);
            if ($result) {
                return response()->jsonSuccess($result);
            }
            return response()->jsonError(400, 'Use item faild.');
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 65: Use item: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }
}
