<?php

namespace App\Http\Requests;

class BuyPackRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'month' => 'required|integer|min:1',
            'amount' => 'required|numeric|min:1'
        ];
    }
}
