<?php

namespace App\Models;

/**
 * Class Message
 *
 * @package namespace App\Models;
 */
class Message extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['mailbox_id', 'user_no', 'content', 'image', 'viewed', 'created_at', 'updated_at', 'has_ng_word', 'total_ng_words'];
    protected $dates = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\Member', 'user_no', 'id');
    }

    public function getImageUrl()
    {
        if (!is_null($this->image) && file_exists('storage/messages/' . $this->image)) {
            return asset('storage/messages/' . $this->image);
        }
        return '';
    }
}
