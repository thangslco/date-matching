<?php

namespace App\Repositories;

use App\Models\Post;
use App\Repositories\Contracts\PostRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class PostRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class PostRepositoryEloquent extends BaseRepository implements PostRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Post::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function search($conditions, $columns = ['*'], $pageName = 'page', $page = null)
    {
        $now = date(DATETIME_FORMAT);
        $this->applyCriteria();
        $this->applyScope();
        $memberBlocked = [];
        if (isset($conditions['member_blocked']) && is_array($conditions['member_blocked'])) {
            $memberBlocked = $conditions['member_blocked'];
            unset($conditions['member_blocked']);
        }
        $memberHided = [];
        if (isset($conditions['member_hided']) && is_array($conditions['member_hided'])) {
            $memberHided = $conditions['member_hided'];
            unset($conditions['member_hided']);
        }
        // $conditions['is_pause'] = 0;
        // $conditions['is_canceled'] = 0;
        $model = $this->model->with(['member' => function ($query) {
            $query->with(['images', 'categories', 'tags', 'favorites']);
        }])
        ->whereHas('member', function (Builder $query) use ($conditions) {
            $query->where($conditions);
        })
        ->where('status', 1)
        ->whereNotNull('date')
        // ->whereNotNull('type_time')
        // ->whereIn('type_time', array_keys(LIST_TYPE_TIME))
        ->whereNotNull('category1_id')
        ->whereNotNull('category2_id')
        ->whereNotNull('category3_id')
        ->whereRaw($this->getQueryFilterByDate($now));
        if (!empty($memberBlocked) && is_array($memberBlocked)) {
            $model = $model->whereNotIn('member_id', $memberBlocked);
        }
        if (!empty($memberHided) && is_array($memberHided)) {
            $model = $model->whereNotIn('member_id', $memberHided);
        }
        $results = $model->paginate(PAGINATE_DEFAULT, $columns, $pageName, $page);
        $results->appends(app('request')->query());
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function listMessageByPostId($conditions, $isPaging = true, $columns = ['*'], $pageName = 'page', $page = null)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with(['member' => function ($query) {
            $query->with(['images', 'categories', 'tags', 'favorites']);
        }])
        ->whereHas('member', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        });
        if (isset($conditions['member_blocked']) && is_array($conditions['member_blocked'])) {
            $model = $model->whereNotIn('member_id', $conditions['member_blocked']);
            unset($conditions['member_blocked']);
        }
        if (isset($conditions['member_hided']) && is_array($conditions['member_hided'])) {
            $model = $model->whereNotIn('member_id', $conditions['member_hided']);
            unset($conditions['member_hided']);
        }
        $model = $model->whereNull('category1_id')
            ->whereNull('category2_id')
            ->whereNull('category3_id')
            ->whereNull('date')
            ->whereNull('type_time')
            ->where($conditions);
        if ($isPaging) {
            $results = $model->paginate(PAGINATE_DEFAULT, $columns, $pageName, $page);
            $results->appends(app('request')->query());
        } else {
            $results = $model->get();
        }
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function listPosts($conditions, $columns = ['*'], $pageName = 'page', $page = null)
    {
        $this->applyCriteria();
        $this->applyScope();
        $now = date(DATETIME_FORMAT);
        $model = $this->model->with(['member' => function ($query) {
            $query->with(['images', 'categories', 'tags', 'favorites']);
        }])
        ->whereHas('member', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where('status', 1)
        ->whereNotNull('date')
        // ->whereNotNull('type_time')
        // ->whereIn('type_time', array_keys(LIST_TYPE_TIME))
        ->whereNotNull('category1_id')
        ->whereNotNull('category2_id')
        ->whereNotNull('category3_id')
        ->whereRaw($this->getQueryFilterByDate($now));
        if (isset($conditions['member_blocked']) && is_array($conditions['member_blocked'])) {
            $model = $model->whereNotIn('member_id', $conditions['member_blocked']);
        }
        if (isset($conditions['post_liked']) && is_array($conditions['post_liked'])) {
            $model = $model->whereNotIn('id', $conditions['post_liked']);
        }
        if (isset($conditions['member_hided']) && is_array($conditions['member_hided'])) {
            $model = $model->whereNotIn('member_id', $conditions['member_hided']);
        }
        if (isset($conditions['post_replied']) && is_array($conditions['post_replied'])) {
            $model = $model->whereNotIn('id', $conditions['post_replied']);
        }
        $model = $model->orderBy('id', 'DESC');
        $results = $model->paginate(PAGINATE_DEFAULT, $columns, $pageName, $page);
        $results->appends(app('request')->query());
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getPostLatest($memberId)
    {
        $this->applyCriteria();
        $this->applyScope();
        $now = date(DATETIME_FORMAT);
        $results = $this->model->with(['member' => function ($query) {
            $query->with(['images', 'categories', 'tags', 'favorites']);
        }])
        ->whereHas('member', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where('member_id', $memberId)
        ->where('status', 1)
        ->whereNotNull('date')
        // ->whereNotNull('type_time')
        // ->whereIn('type_time', array_keys(LIST_TYPE_TIME))
        ->whereNotNull('category1_id')
        ->whereNotNull('category2_id')
        ->whereNotNull('category3_id')
        ->whereRaw($this->getQueryFilterByDate($now))
        ->orderBy('updated_at', 'DESC')
        ->first();
        $this->resetModel();
        return $this->parserResult($results);
    }

    private function getQueryFilterByDate($now)
    {
        // WHEN DATE_FORMAT(date, '%Y-%m-%d') = DATE_FORMAT('$now', '%Y-%m-%d') AND date >= '$now' THEN 1
        return DB::raw("
            CASE type_time
                WHEN 1 THEN
                    CASE
                        WHEN DATE_FORMAT(DATE_ADD(created_at, INTERVAL 12 HOUR), '%Y-%m-%d') = DATE_FORMAT(created_at, '%Y-%m-%d') THEN DATE_ADD(created_at, INTERVAL 12 HOUR) >= '$now'
                        ELSE DATE_ADD(created_at, INTERVAL 24 HOUR) >= '$now'
                    END
                ELSE DATE_ADD(created_at, INTERVAL 24 HOUR) >= '$now'
                END
        ");
    }

    public function getTotalPosts()
    {
        $this->applyCriteria();
        $this->applyScope();
        $results = $this->model
        ->whereHas('member', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->whereNotNull('date')
        // ->whereNotNull('type_time')
        // ->whereIn('type_time', array_keys(LIST_TYPE_TIME))
        ->whereNotNull('category1_id')
        ->whereNotNull('category2_id')
        ->whereNotNull('category3_id')
        ->count();
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getTotalPostOnOneDay($post, $member)
    {
        $this->applyCriteria();
        $this->applyScope();
        $results = $this->model
        ->whereNull('date')
        ->whereNull('category1_id')
        ->whereNull('category2_id')
        ->whereNull('category3_id')
        ->where([
            'member_id' => $member->id
        ])
        ->whereDate('created_at', '=', date(DATE_FORMAT))
        ->count();
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getListPostReplied($member)
    {
        $this->applyCriteria();
        $this->applyScope();
        $results = $this->model
        ->whereNull('date')
        ->whereNull('category1_id')
        ->whereNull('category2_id')
        ->whereNull('category3_id')
        ->where([
            'member_id' => $member->id
        ])
        ->pluck('post_id');
        $this->resetModel();
        return $this->parserResult($results);
    }
}
