<?php

use Illuminate\Support\Facades\Auth;

if (!function_exists('user')) {
    function user()
    {
        return Auth::user();
    }
}

if (!function_exists('get_email')) {
    function get_email()
    {
        $user = Auth::user();
        if ($user) {
            return $user->email;
        }
        return '';
    }
}

if (!function_exists('generate_link_member_detail')) {
    function generate_link_member_detail($memberId, $gender)
    {
        if ($gender == 1) {
            return route('male.detail', $memberId);
        }
        return route('female.detail', $memberId);
    }
}
