<?php

namespace App\Jobs;

use App\Mail\QaMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailQa implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info('========== START Send mail QA ===============');
        \Log::info('MemberID: ' . $this->data['member_id']);
        \Log::info('Start Queue send mail QA');
        Mail::send(new QaMail($this->data));
        \Log::info('End Queue send mail QA');
        \Log::info('=========== END Send mail QA ==============');
    }
}
