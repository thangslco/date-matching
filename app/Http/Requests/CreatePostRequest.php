<?php

namespace App\Http\Requests;


class CreatePostRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category1_id' => 'required|category_post',
            'category2_id' => 'required|category_post',
            'category3_id' => 'required|category_post',
            'type_time' => 'nullable|in:1,2,3,4',
            'date' => 'nullable|date_format:Y-m-d'
        ];
    }
}
