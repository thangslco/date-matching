<?php

namespace App\Models;

/**
 * Class Setting.
 *
 * @package namespace App\Models;
 */
class Setting extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'value'];

    protected $dates = ['created_at', 'updated_at'];
}
