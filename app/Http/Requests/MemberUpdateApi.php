<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use DateTime;

class MemberUpdateApi extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nickname' => 'max:150|unique:members,nickname,' . $this->id,
            'email' => 'max:255|email|unique:members,email,' . $this->id,
            'birthday' => 'date|date_format:Y-m-d|before:now',
            'address' => 'string|max:255',
            'phone' => 'string|max:30|unique:members,phone,' . $this->id,
            'place_of_birth' => 'integer',
            'education' => 'integer',
            'height' => 'integer',
            'sake' => 'integer',
            'smoke' => 'integer',
            'blood_type' => 'integer',
            'thing_of_marriage' => 'integer',
            'annual_income' => 'integer',
            'job_type' => 'integer',
            'hair' => 'integer',
            'hair_color' => 'integer',
            'option_1' => 'integer',
            'option_2' => 'integer',
            'option_3' => 'integer',
            'option_4' => 'integer'
        ];
        $dataHumanForms = $this->get('human_forms');
        if (!is_null($dataHumanForms)) {
            foreach ($listKeysDataHumanForms as $key) {
                $rules['human_forms.' . $key] = 'nullable|in:' . implode(',', array_keys(LIST_HUMAN_FORM));
            }
        }
        $dataAppearances = $this->get('appearances');
        if (!is_null($dataAppearances)) {
            foreach ($listKeysDataAppearances as $key) {
                $rules['appearances.' . $key] = 'nullable|in:' . implode(',', array_keys(LIST_APPEARANCE));
            }
        }
        $dataImpressives = $this->get('impressives');
        if (!is_null($dataImpressives)) {
            foreach ($listKeysDataImpressives as $key) {
                $rules['impressives.' . $key] = 'nullable|in:' . implode(',', array_keys(LIST_IMPRESSIVE));
            }
        }

        $images = $this->file('images');
        $imagesKey = array_keys($images);
        foreach ($imagesKey as $key) {
            $rules['images.' . $key] = 'mimes:jpg,png,jpeg|max:10240';
        }
        return $rules;
    }

    public function messages()
    {
        $messages = [];
        $dataHumanForms = $this->get('human_forms');
        if (!is_null($dataHumanForms)) {
            $listKeysDataHumanForms = array_keys($dataHumanForms);
            foreach ($listKeysDataHumanForms as $key) {
                $messages['human_forms.' . $key] = "Value of 体格 at {$key} invalid";
            }
        }
        $dataAppearances = $this->get('appearances');
        if (!is_null($dataAppearances)) {
            $listKeysDataAppearances = array_keys($dataAppearances);
            foreach ($listKeysDataAppearances as $key) {
                $messages['appearances.' . $key] = "Value of 見た目 at {$key} invalid";
            }
        }
        $dataImpressives = $this->get('impressives');
        if (!is_null($dataImpressives)) {
            $listKeysDataImpressives = array_keys($dataImpressives);
            foreach ($listKeysDataImpressives as $key) {
                $messages['impressives.' . $key] = "Value of イメージ at {$key} invalid";
            }
        }
        $images = $this->file('images');
        $imagesKey = array_keys($images);
        foreach ($imagesKey as $key) {
            $messages['images.' . $key] = 'Image invalid';
        }
        return $messages;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $d = DateTime::createFromFormat('Y/m/d', $this->birthday);
        if ($d && $d->format('Y/m/d') === $this->birthday) {
            $this->merge([
                'birthday' => Carbon::parse($this->birthday)->format(DATE_FORMAT)
            ]);
        }
    }
}
