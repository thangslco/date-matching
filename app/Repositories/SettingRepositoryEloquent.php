<?php

namespace App\Repositories;

use App\Models\Setting;
use App\Repositories\Contracts\SettingRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class SettingRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class SettingRepositoryEloquent extends BaseRepository implements SettingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Setting::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getSettingByKey($key)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
                        ->where('key', $key)
                        ->first();
        $this->resetModel();
        return $this->parserResult($model);
    }
}
