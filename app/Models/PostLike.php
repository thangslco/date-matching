<?php

namespace App\Models;

/**
 * Class PostLike
 *
 * @package namespace App\Models;
 */
class PostLike extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['member_id', 'target_id', 'post_id'];

    protected $dates = ['created_at', 'updated_at'];

}
