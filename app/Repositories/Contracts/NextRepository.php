<?php

namespace App\Repositories\Contracts;

/**
 * Interface NextRepository
 *
 * @package namespace App\Repositories;
 */
interface NextRepository extends BaseRepositoryContract
{
}
