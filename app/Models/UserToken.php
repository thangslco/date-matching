<?php

namespace App\Models;


/**
 * Class UserToken
 *
 * @package namespace App\Models;
 */
class UserToken extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'token', 'apns_id'];

    protected $dates = ['created_at', 'updated_at'];

    public function member()
    {
        return $this->belongsTo('App\Models\Member', 'user_id', 'id');
    }
}
