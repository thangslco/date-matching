<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateImageTypeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_image_type';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update image type for all';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->updateImageTypeForMember();
        $this->info('Updated image type for member');
        $this->updateImageTypeForGroup();
        $this->info('Updated image type for group');
        $this->updateImageTypeForCategory();
        $this->info('Updated image type for category');

    }

    private function updateImageTypeForCategory()
    {
        $categories = DB::table('categories')->select('image_id')->pluck('image_id');
        if ($categories->count()) {
            DB::table('images')->whereIn('target_id', $categories->toArray())
                ->update(['type' => IMAGE_TYPE_CATEGORY]);
        }
    }

    private function updateImageTypeForGroup()
    {
        $groups = DB::table('groups')->select('image_id')->pluck('image_id');
        if ($groups->count()) {
            DB::table('images')->whereIn('target_id', $groups->toArray())
                ->update(['type' => IMAGE_TYPE_GROUP]);
        }
    }

    private function updateImageTypeForMember()
    {
        $members = DB::table('members')->select('id')->pluck('id');
        if ($members->count()) {
            DB::table('images')->whereIn('target_id', $members->toArray())
                ->update(['type' => IMAGE_TYPE_MEMBER]);
        }
    }
}
