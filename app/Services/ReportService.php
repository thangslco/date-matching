<?php

namespace App\Services;

use App\Repositories\Contracts\ReportRepository;

class ReportService extends BaseService
{
    /**
     * ReportService constructor.
     * @param ReportRepository $repository
     */
    public function __construct(
        ReportRepository $repository
    ) {
        $this->repository = $repository;
    }
}
