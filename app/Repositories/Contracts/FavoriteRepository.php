<?php

namespace App\Repositories\Contracts;

/**
 * Interface FavoriteRepository.
 *
 * @package namespace App\Repositories;
 */
interface FavoriteRepository extends BaseRepositoryContract
{
}
