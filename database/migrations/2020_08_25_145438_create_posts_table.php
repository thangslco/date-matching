<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id');
            $table->bigInteger('category1_id')->nullable();
            $table->bigInteger('category2_id')->nullable();
            $table->bigInteger('category3_id')->nullable();
            $table->text('content')->nullable();
            $table->dateTime('date')->nullable();
            $table->integer('status')->default(1)->comment('1: normal, 0: cancel');
            $table->tinyInteger('is_viewed')->default(0)->comment('0: not see, 1: seen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
