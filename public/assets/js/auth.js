$(document).ready(function() {
    $(document).on('click', '.approval', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        start_rotating_button();
        var $this = $(this);
        if (typeof id !== 'undefined' && id != '' && typeof urlApprove !== 'undefined' && urlApprove != '') {
            $.ajax({
                url: urlApprove,
                type: 'POST',
                data: {id: id},
                beforeSend: function() {
                    stop_rotating_button();
                },
                success: function(result) {
                    var message = '';
                    if (result.STATUS == true) {
                        message = "承認済みにしました。";
                        remove_item($this);
                    } else {
                        message = result.message;
                    }
                    $('#notice-modal').find('p').text(message);
                    $('#notice-modal').modal('show');
                },
                error: function() {
                    stop_rotating_button();
                }
            });
        }
    });
    $(document).on('click', '.rejection', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        start_rotating_button();
        var $this = $(this);
        if (typeof id !== 'undefined' && id != '' && typeof urlReject !== 'undefined' && urlReject != '') {
            $.ajax({
                url: urlReject,
                type: 'POST',
                data: {id: id},
                beforeSend: function() {
                    stop_rotating_button();
                },
                success: function(result) {
                    var message = '';
                    if (result.STATUS == true) {
                        message = "非承認にしました。";
                        remove_item($this);
                    } else {
                        message = result.message;
                    }
                    $('#notice-modal').find('p').text(message);
                    $('#notice-modal').modal('show');
                },
                error: function() {
                    stop_rotating_button();
                }
            });
        }
    });

    $('img.zoomable').css({ cursor: 'pointer' }).on('click', function () {
        var img = $(this);
        var bigImg = $('<img />').css({
            'width': '100%',
            'height': 'auto',
            'display': 'inline'
        });
        bigImg.attr({
            src: img.attr('src'),
            alt: img.attr('alt'),
            title: img.attr('title')
        });
        $('#zoom-image').find('p').html(bigImg);
        $('#zoom-image').modal('show');
    });
});
function start_rotating_button() {
    $('#reload_button').attr('disabled', 'disabled');
    $('#reload_button').addClass('rotate');
};
function stop_rotating_button() {
    $('#reload_button').removeAttr('disabled');
    $('#reload_button').removeClass('rotate');
};
function remove_item(item) {
    item.parents('tr').addClass('fadeout').hide();
};
function reload() {
    location.reload();
};