<?php

namespace App\Http\Requests;

use App\Helpers\Common;
use Illuminate\Support\Facades\DB;

class UpdateMemberApiRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nickname' => 'nullable|string|max:150|unique:members,nickname,' . $this->id,
            'email' => 'nullable|string|max:255|unique:members,email,' . $this->id,
            'birthday' => 'nullable|date',
            'address' => 'nullable|string',
            'place_of_birth' => 'nullable|in:' . implode(',', array_keys(LIST_PREFECTURE)),
            'education' => 'nullable|in:' . implode(',', array_keys(LIST_EDUCATION)),
            'blood_type' => 'nullable|in:' . implode(',', array_keys(LIST_BLOOD_TYPE)),
            'thing_of_marriage' => 'nullable|in:' . implode(',', array_keys(LIST_THING_OF_MARRIAGE)),
            'annual_income' => 'nullable|in:' . implode(',', array_keys(LIST_ANNUAL_INCOME)),
            'hair_color' => 'nullable|in:' . implode(',', array_keys(LIST_HAIR_COLOR)),
            'avatar' => 'nullable|image',
            'phone' => 'nullable|string',
            'gender' => 'nullable|in:0,1',
            'job_type' => 'nullable|in:' . implode(',', array_keys(LIST_JOB_TYPE)),
            'height' => 'nullable|in:' . implode(',', array_keys(Common::listHeight())),
            'smoke' => 'nullable|in:' . implode(',', array_keys(LIST_SMOKE)),
            'sake' => 'nullable|in:' . implode(',', array_keys(LIST_SAKE)),
            'option_1' => 'nullable|in:' . implode(',', array_keys(LIST_OPTION_1)),
            'option_2' => 'nullable|in:' . implode(',', array_keys(LIST_OPTION_2)),
            'option_3' => 'nullable|in:' . implode(',', array_keys(LIST_OPTION_3)),
            'option_4' => 'nullable|in:' . implode(',', array_keys(LIST_OPTION_4)),
            'option_5' => 'nullable|string',
            'groups_join' => 'nullable|array',
            'groups_join.*' => 'exists:groups,id',
            'categories' => 'nullable|array',
            'categories.*' => 'exists:categories,id',
            'tags' => 'nullable|array',
            'tags.*' => 'exists:tags,id',
            'favorites' => 'nullable|array',
            'favorites.*' => 'exists:favorites,id',
            'images' => 'nullable|array',
            'images.*' => 'image',
            'images_secondary' => 'nullable|array|max:4', // 4 image secondary
            'images_secondary.*' => 'image',
        ];
        $member = DB::table('members')->where('id', $this->id)->first();
        if ($member) {
            $gender = $member->gender;
            $listKeysHair = array_keys(LIST_HAIR_FEMALE);
            if ($gender == 1) {
                $listKeysHair = array_keys(LIST_HAIR_MALE);
            }
            $rules['hair'] = 'nullable|in:' . implode(',', $listKeysHair);
        }
        return $rules;
    }
}
