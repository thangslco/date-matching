<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIsShowToMemberBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_blocks', function (Blueprint $table) {
            $table->tinyInteger('is_show')->default(1)->comment('0: no show, 1: show');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_blocks', function (Blueprint $table) {
            $table->dropColumn('is_show');
        });
    }
}
