<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Common Class
 */
class Common
{
    public static function noImage()
    {
        return asset('/assets/no-image.png');
    }

    public static function listHeight($isDefault = true)
    {
        if ($isDefault) {
            $data = ['' => '選択しない'];
        } else {
            $data = [];
        }
        for ($i = 130; $i <= 210; $i++) {
            $data[$i] = $i . ' Cm';
        }
        return $data;
    }

    public static function setFieldMultiData($value)
    {
        if (!is_null($value) && is_array($value)) {
            return ',' . implode(',', $value) . ',';
        }
        return $value;
    }

    public static function checkMenuActive($menu, $menuActived)
    {
        if (!is_null($menuActived) && $menu == $menuActived) {
            return 'active';
        }
        return '';
    }

    public static function checkPermission()
    {
        $user = Auth::user();
        if (!is_null($user) && !is_null($user->type) && $user->type == TYPE_USER_MASTER) {
            return true;
        }
        return false;
    }

    public static function getCategoryTrend($limit = 3)
    {
        $listCategory3 = self::getAllCategory3();
        $items = DB::table('posts')->selectRaw('category3_id, COUNT(category3_id) AS total')
                                ->whereNotNull('category1_id')
                                ->whereNotNull('category2_id')
                                ->whereNotNull('category3_id')
                                ->whereIn('category3_id', array_keys($listCategory3))
                                ->groupBy('category3_id')
                                ->orderBy('total', 'DESC')
                                ->limit($limit)
                                ->get();
        return $items->pluck('total', 'category3_id')->toArray();
    }

    public static function getInfoMember($member, $dataAppend = [], $groupJoinedDetail = false)
    {
        $data = [
            'id' => $member->id,
            'user_id' => str_pad($member->id, 8, '0', STR_PAD_LEFT),
            'nickname' => $member->nickname,
            'email' => $member->email,
            'birthday' => $member->getBirthday(DATE_FORMAT),
            'age' => $member->getAge(),
            'address' => $member->address,
            'date_vip' => $member->date_vip ? $member->date_vip->format(DATETIME_FORMAT) : null,
            'date_paid' => $member->date_paid ? $member->date_paid->format(DATETIME_FORMAT) : null,
            'place_of_birth' => $member->getPrefecture(),
            'education' => $member->getEducation(),
            'blood_type' => $member->getBloodType(),
            'thing_of_marriage' => $member->getThingOfMarriage(),
            'annual_income' => $member->getAnnualIncome(),
            'hair' => $member->getHair(),
            'hair_color' => $member->getHairColor(),
            'avatar' => $member->getAvatarUrl(),
            'phone' => $member->phone,
            'gender' => $member->gender,
            'job_type' => $member->getJobTypes(),
            'height' => $member->getHeights(),
            'smoke' => $member->getSmokes(),
            'sake' => $member->getSakes(),
            'option_1' => $member->getOption1(),
            'option_2' => $member->getOption2(),
            'option_3' => $member->getOption3(),
            'option_4' => $member->getOption4(),
            'option_5' => $member->option_5,
            'groups_join' => $member->getGroupsJoin($groupJoinedDetail),
            'is_pause' => $member->is_pause,
            'reason_pause' => $member->reason_pause,
            'type_pause' => $member->type_pause,
            'created_at' => $member->created_at ? $member->created_at->format(DATETIME_FORMAT) : null
        ];
        $listCategories = !is_null($member->categories) ? $member->categories : [];
        $categories = [];
        if (!empty($listCategories)) {
            foreach ($listCategories as $item) {
                $categories[] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'image' => $item->getImageUrl()
                ];
            }
        }
        $data['categories'] = $categories;
        $listFavorites = !is_null($member->favorites) ? $member->favorites : [];
        $favorites = [];
        if (!empty($listFavorites)) {
            foreach ($listFavorites as $item) {
                $favorites[] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'type' => [
                        'index' => $item->type,
                        'name' => LIST_TYPE_FAVORITES[$item->type] ?? null
                    ]
                ];
            }
        }
        $data['favorites'] = $favorites;
        $listTags = !is_null($member->tags) ? $member->tags : [];
        $tags = [];
        if (!empty($listTags)) {
            foreach ($listTags as $item) {
                $tags[] = [
                    'id' => $item->id,
                    'name' => $item->name
                ];
            }
        }
        $data['tags'] = $tags;
        if ($member->gender == 1) {
            $data['image_verify'] = $member->getImageVerifyUrl(true);
            $listImages = !is_null($member->images) ? $member->images : [];
            $images = [];
            if (!empty($listImages)) {
                foreach ($listImages as $itemImage) {
                    $images[] = [
                        'id' => $itemImage->id,
                        'url' => $itemImage->getUrl()
                    ];
                }
            }
            $data['images'] = $images;
        }

        $listImagesSecondary = !is_null($member->imagesSecondary) ? $member->imagesSecondary : [];
        $imagesSecondary = [];
        if (!empty($listImagesSecondary)) {
            foreach ($listImagesSecondary as $itemImage) {
                $imagesSecondary[] = [
                    'id' => $itemImage->id,
                    'url' => $itemImage->getUrl()
                ];
            }
        }
        $data['images_secondary'] = $imagesSecondary;
        $data['human_forms'] = $member->getHumanForms(true);
        $data['appearances'] = $member->getAppearances(true);
        $data['impressives'] = $member->getImpressives(true);
        $data['status'] = $member->status;
        return array_merge($data, $dataAppend);
    }

    public static function getAllCategory2()
    {
        $data = [];
        foreach (LIST_CATEGORY_2 as $items) {
            foreach ($items as $key => $item) {
                $data[$key] = $item;
            }
        }
        return $data;
    }

    public static function getAllCategory3()
    {
        $data = [];
        foreach (LIST_CATEGORY_3 as $items) {
            foreach ($items as $key => $item) {
                $data[$key] = $item;
            }
        }
        return $data;
    }

    public static function getCategoryNameById($categoryId)
    {
        $listCategory2 = self::getAllCategory2();
        $listCategory3 = self::getAllCategory3();
        if (isset($listCategory2[$categoryId])) {
            return $listCategory2[$categoryId];
        } elseif (isset($listCategory3[$categoryId])) {
            return $listCategory3[$categoryId];
        } else {
            return null;
        }
    }

    public static function getListMemberBlocked($memberId, $memberCheckBlocked = null)
    {
        $list1 = DB::table('member_blocks')->where('user_blocked_id', $memberId)
                                        ->select('user_block_id AS member_id');
        $list2 = DB::table('member_blocks')->where('user_block_id', $memberId)
                                        ->select('user_blocked_id AS member_id')
                                        ->union($list1)
                                        ->pluck('member_id')
                                        ->toArray();
        $result = array_unique($list2);
        if (!empty($result) && !is_null($memberCheckBlocked) && in_array($memberCheckBlocked, $result)) {
            return $memberCheckBlocked;
        }
        return $result;
    }

    public static function getListMemberHided($memberId, $memberCheckHided = null)
    {
        $result = DB::table('member_hides')->where('user_id', $memberId)
                                        ->select('user_hided_id')
                                        ->pluck('user_hided_id')
                                        ->toArray();
        $result = array_unique($result);
        if (!empty($result) && !is_null($memberCheckHided) && in_array($memberCheckHided, $result)) {
            return $memberCheckHided;
        }
        return $result;
    }

    public static function getListPostLiked($memberId)
    {
        $result = DB::table('post_likes')->where('member_id', $memberId)
            ->distinct()
            ->select('post_id')
            ->pluck('post_id')
            ->toArray();
        return array_unique($result);
    }

    public static function getListMemberNexted($memberId)
    {
        $result = DB::table('nexts')->where('owner_id', $memberId)
                                        ->select('id_next')
                                        ->pluck('id_next')
                                        ->toArray();
        return array_unique($result);
    }

    public static function convertTimeToRemain($date1, $date2)
    {
        if (!($date1 instanceof Carbon)) {
            $date1 = Carbon::parse($date1);
        }
        if (!($date2 instanceof Carbon)) {
            $date2 = Carbon::parse($date2);
        }
        $tmp = $date1->diff($date2)->format('%Y|%m|%d|%H|%i|%s');
        $items = explode('|', $tmp);
        $time = '';
        foreach ($items as $key => $item) {
            if ($item > 0) {
                if ($key == 3) {
                    $time .= $item . '時';
                } elseif ($key == 4) {
                    $time .= $item . '分';
                }
            }
        }
        return $time;
    }

    public static function checkExistMember($memberId, $throwException = false)
    {
        $checkUser = DB::table('members')->where([
            'id' => $memberId
        ])->where(function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->first();
        if (!$checkUser) {
            if ($throwException) {
                throw new \Exception(trans('messages.member_not_found'), 400);
            }
            return false;
        }
        return $checkUser;
    }

    public static function checkNickName($value)
    {
        foreach (BLACK_LIST_NICKNAME as $ngWord) {
            if (strpos($value, $ngWord) !== false) {
                return false;
            }
        }
        return true;
    }

    public static function checkOption5($value)
    {
        foreach (BLACK_LIST_PR as $ngWord) {
            if (strpos($value, $ngWord) !== false) {
                return false;
            }
        }
        return true;
    }

    public static function array_sort_by_column(&$array, $column, $direction = SORT_ASC)
    {
        $reference_array = [];

        foreach ($array as $key => $row) {
            $reference_array[$key] = $row[$column];
        }

        array_multisort($reference_array, $direction, $array);
    }
}
