<?php

namespace App\Models;

/**
 * Class VerifyCode.
 *
 * @package namespace App\Models;
 */
class VerifyCode extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone',
        'code',
        'type',
    ];

    protected $dates = ['created_at', 'updated_at'];
}
