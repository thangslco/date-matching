<?php

namespace App\Models;

use App\Helpers\Common;
use Carbon\Carbon;

/**
 * Class Post
 *
 * @package namespace App\Models;
 */
class Post extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'content',
        'category1_id',
        'category2_id',
        'category3_id',
        'date',
        'status',
        'post_id',
        'owner_id',
        'is_viewed',
        'type_time',
        'is_liked',
        'member_id_liked',
    ];

    protected $dates = ['created_at', 'updated_at', 'date'];

    public function member()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'id');
    }

    public function postLikes()
    {
        return $this->hasMany(PostLike::class, 'post_id', 'id');
    }

    public function getCategory1()
    {
        if ($this->category1_id && array_key_exists($this->category1_id, LIST_CATEGORY_1)) {
            return LIST_CATEGORY_1[$this->category1_id];
        }
        return null;
    }

    public function getCategory2()
    {
        $listCategory2 = Common::getAllCategory2();
        if ($this->category2_id && array_key_exists($this->category2_id, $listCategory2)) {
            return $listCategory2[$this->category2_id];
        }
        return null;
    }

    public function getCategory3()
    {
        $listCategory3 = Common::getAllCategory3();
        if ($this->category3_id && array_key_exists($this->category3_id, $listCategory3)) {
            return $listCategory3[$this->category3_id];
        }
        return null;
    }

    public function getTypeTimeCurrent()
    {
        if (is_null($this->date) || is_null($this->created_at) || !($this->created_at instanceof Carbon)) {
            return null;
        }
        if ($this->created_at->isToday()) {
            return LIST_TYPE_TIME[$this->type_time] ?? null;
        }

        $now = now();
        $createdAt = $this->created_at;
        if ($this->type_time == TYPE_TIME_TOMORROW) {
            if ($createdAt->copy()->addHours(24)->greaterThanOrEqualTo($now)) {
                return LIST_TYPE_TIME[TYPE_TIME_TODAY];
            }
        } elseif ($this->type_time == TYPE_TIME_NEXT_WEEK) {
            if ($createdAt->copy()->addHours(24)->isCurrentWeek()) {
                return LIST_TYPE_TIME[TYPE_TIME_NEXT_WEEK];
            } elseif ($createdAt->copy()->addHours(24)->isNextWeek()) {
                return '今週';
            }
        } elseif ($this->type_time == TYPE_TIME_OTHER) {
            if ($createdAt->copy()->addHours(24)->greaterThanOrEqualTo($now)) {
                return LIST_TYPE_TIME[TYPE_TIME_OTHER];
            }
        }
        return null;
    }

    public function getTypeTimeText()
    {
        if (isset(LIST_TYPE_TIME[$this->type_time])) {
            return LIST_TYPE_TIME[$this->type_time];
        }
        return null;
    }

    public function getTimeRemain()
    {
        if (is_null($this->created_at) || !($this->created_at instanceof Carbon)) {
            return null;
        }
        if ($this->type_time == TYPE_TIME_TODAY) {
            $dateCompare = $this->created_at->endOfDay();
        } else {
            $dateCompare = $this->created_at->addHours(24);
        }
        return Common::convertTimeToRemain($dateCompare, $this->created_at);
    }

}
