<?php

namespace App\Repositories;

use App\Models\Next;
use App\Repositories\Contracts\NextRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class NextRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class NextRepositoryEloquent extends BaseRepository implements NextRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Next::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function listUsersNextedByConditions($conditions)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('userNext')
        ->has('userNext');
        if (isset($conditions['member_id'])) {
            $model->where([
                'owner_id' => $conditions['member_id']
            ]);
        }
        if (isset($conditions['id_next'])) {
            $model->where([
                'id_next' => $conditions['id_next']
            ]);
        }
        if (isset($conditions['member_blocked']) && !empty($conditions['member_blocked'])) {
            $model->whereNotIn('id_next', $conditions['member_blocked']);
        }
        if (isset($conditions['member_hided']) && !empty($conditions['member_hided'])) {
            $model->whereNotIn('id_next', $conditions['member_hided']);
        }
        $model = $model->get();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function listUsersNexted($conditions)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('userNext')
        ->whereHas('userNext', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        });
        if (isset($conditions['member_id'])) {
            $model->where([
                'owner_id' => $conditions['member_id']
            ]);
        }
        if (isset($conditions['member_blocked']) && !empty($conditions['member_blocked'])) {
            $model->whereNotIn('id_next', $conditions['member_blocked']);
        }
        if (isset($conditions['member_hided']) && !empty($conditions['member_hided'])) {
            $model->whereNotIn('id_next', $conditions['member_hided']);
        }
        $model = $model->get();
        $this->resetModel();
        return $this->parserResult($model);
    }

}
