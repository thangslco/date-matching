<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGroupRequest;
use App\Services\GroupService;
use Illuminate\Http\Request;
use Session;

class GroupController extends Controller
{
    protected $groupService;
    public function __construct(GroupService $service)
    {
        parent::__construct();
        $this->groupService = $service;
    }

    public function index(Request $request)
    {
        $menuActived = 'MENU_GROUP';
        $items = $this->groupService->getList($request);
        return view('group.list', compact('items', 'menuActived'));
    }

    public function create(Request $request)
    {
        $menuActived = 'MENU_GROUP';
        $action = route('groups.store');
        $title = __('messages.groups.lbl_heading_create');
        $imageUrl = null;
        return view('group.create', compact('menuActived', 'action', 'title', 'imageUrl'));
    }

    public function store(CreateGroupRequest $request)
    {
        $this->groupService->register($request);
        Session::flash('message', __('messages.create_group_success'));
        return redirect()->route('groups');
    }

    public function detail(Request $request, $id)
    {
        $menuActived = 'MENU_GROUP';
        $group = $this->groupService->find($id);
        if (!$group) {
            return redirect()->route('groups')->with('error', __('messages.group_not_found'));
        }
        $image = $group->image()->first();
        $imageUrl = null;
        if (!is_null($image)) {
            $imageUrl = $image->getUrl();
        }
        $request->merge($group->toArray());
        $action = route('groups.update', ['id' => $id]);
        $title = __('messages.groups.lbl_heading_update');
        return view('group.create', compact('menuActived', 'action', 'title', 'id', 'imageUrl'));
    }

    public function update(CreateGroupRequest $request, $id)
    {
        $group = $this->groupService->find($id);
        if (!$group) {
            return redirect()->route('groups')->with('error', __('messages.group_not_found'));
        }
        $this->groupService->updateGroup($request, $group);
        Session::flash('message', __('messages.update_group_success'));
        return redirect()->route('groups');
    }

    public function delete(Request $request)
    {
        try {
            $id = $request->get('id');
            if (!$id) {
                return response()->json(['status' => false, 'message' => __('messages.invalid_param')]);
            }
            $group = $this->groupService->find($id);
            if (!$group) {
                return response()->json(['status' => false, 'message' => __('messages.group_not_found')]);
            }
            $this->groupService->delete($id);
            return response()->json(['status' => true, 'message' => __('messages.delete_group_success')]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }
}
