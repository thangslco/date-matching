<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

/**
 * Class Notice
 *
 * @package namespace App\Models;
 */
class Notice extends BaseModel
{
    use SoftDeletes;
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'category'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getContent($limit = 100)
    {
        if ($this->content) {
            return Str::limit($this->content, $limit);
        }
        return null;
    }

    public function getCategoryName()
    {
        if ($this->category && in_array($this->category, array_keys(LIST_CATEGORY_NOTICES))) {
            return LIST_CATEGORY_NOTICES[$this->category];
        }
        return null;
    }
}
