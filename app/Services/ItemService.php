<?php

namespace App\Services;

use App\Repositories\Contracts\ItemRepository;
use App\Repositories\Contracts\MemberRepository;
use App\Repositories\Contracts\TransactionRepository;

class ItemService extends BaseService
{
    protected $memberRepository = null;
    protected $transactionRepository = null;
    /**
     * ItemService constructor.
     * @param ItemRepository $repository
     * @param MemberRepository $memberRepository
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(
        ItemRepository $repository,
        MemberRepository $memberRepository,
        TransactionRepository $transactionRepository
    ) {
        $this->repository = $repository;
        $this->memberRepository = $memberRepository;
        $this->transactionRepository = $transactionRepository;
    }

    public function getItemByUserId($userId)
    {
        return $this->repository->findWhere(['member_id' => $userId]);
    }

    public function addItem($request, $user)
    {
        $item = $this->repository->firstByConditions([
            'member_id' => $user->id,
            'type' => $request->get('item_id')
        ]);
        $data = [];
        $quantity = $request->get('quantity');
        $type = $request->get('item_id');
        if ($item) {
            $dataUpdate = [
                'total' => $item->total + $quantity
            ];
            $this->repository->forcedUpdate($dataUpdate, [
                'member_id' => $user->id,
                'type' => $type
            ]);
        } else {
            $data = [
                'member_id' => $user->id,
                'type' => $type,
                'total' => $quantity
            ];
            $this->repository->create($data);
        }
        $this->transactionRepository->create([
            'member_id' => $user->id,
            'amount' => 0,
            'quantity' => $quantity,
            'type' => TRANSACTION_TYPE_REWARD
        ]);
        return true;
    }

    public function useItem($request, $user)
    {
        $item = $this->repository->firstByConditions([
            'member_id' => $user->id,
            'type' => $request->get('item_id')
        ]);
        if ($item) {
            $quantity = $request->get('quantity');
            if ($item->total < $quantity) {
                throw new \Exception("Quantity item not enough", 400);
            }
            $dataUpdate = [
                'total' => $item->total - $quantity
            ];
            $itemType = $request->get('item_id');
            $result = $this->repository->forcedUpdate($dataUpdate, [
                'member_id' => $user->id,
                'type' => $itemType
            ]);
            if ($result) {
                $item = $this->repository->firstByConditions([
                    'member_id' => $user->id,
                    'type' => $request->get('item_id')
                ]);

                $item['total_remain'] = $item->total;
                $item = $item->toArray();
                unset($item['total']);
                if ($itemType == TYPE_ROCKET_ITEM) {
                    $member = $this->memberRepository->firstByConditions(['id' => $user->id]);
                    $itemRocketDate = $member->item_rocket_date;
                    if (is_null($itemRocketDate)) {
                        $itemRocketDate = now()->addMinutes(5)->format(DATETIME_FORMAT);
                    } elseif (!is_null($itemRocketDate) && $itemRocketDate->greaterThanOrEqualTo(now()->subMinutes(5))) {
                        $itemRocketDate = $itemRocketDate->addMinutes(5)->format(DATETIME_FORMAT);
                    }
                    $this->memberRepository->forcedUpdate([
                        'item_rocket_date' => $itemRocketDate
                    ], [
                        'id' => $user->id
                    ]);
                }
                return $item;
            }
        } else {
            throw new \Exception("Not found item", 400);
        }
        return null;
    }

    public function getTotalLikeItemByUser($memberId)
    {
        $this->repository->getTotalLikeItemByUser($memberId);
    }
}
