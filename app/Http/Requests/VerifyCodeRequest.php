<?php

namespace App\Http\Requests;

class VerifyCodeRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|max:30',
            'action' => 'required|in:' . implode(',', array_keys(LIST_TYPE_VERIFY_CODE)),
            'code' => 'required'
        ];
    }
}
