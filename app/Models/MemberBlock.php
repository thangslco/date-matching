<?php

namespace App\Models;

/**
 * Class MemberBlock
 *
 * @package namespace App\Models;
 */
class MemberBlock extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_block_id', 'user_blocked_id', 'is_show'];
    protected $dates = ['created_at', 'updated_at'];

    public function userBlock()
    {
        return $this->belongsTo('App\Models\Member', 'user_block_id', 'id');
    }

    public function userBlocked()
    {
        return $this->belongsTo('App\Models\Member', 'user_blocked_id', 'id');
    }
}
