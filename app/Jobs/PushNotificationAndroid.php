<?php

namespace App\Jobs;

use App\Services\SendPushNotificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PushNotificationAndroid implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data = null;
    protected $userId = null;
    protected $service = null;
    protected $limit = 100;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $userId = [])
    {
        $this->data = $data;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SendPushNotificationService $service)
    {
        $this->service = $service;
        if (!empty($this->userId)) {
            $this->sendByUserId($this->userId);
        } else {
            $lastId = 0;
            $this->send($lastId);
        }
    }

    private function send($lastId = 0)
    {
        $listToken = $this->service->getDataDevices('android', $this->limit, $lastId);
        if ($listToken) {
            $this->service->sendToAndroid($listToken, $this->data);
            $this->send($lastId + 1);
        } else {
            return false;
        }
    }

    private function sendByUserId($userId)
    {
        $listToken = $this->service->getDataDeviceByUserId($userId, 'android');
        if ($listToken) {
            $this->service->sendToAndroid($listToken, $this->data);
        }
    }
}
