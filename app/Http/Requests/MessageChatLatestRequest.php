<?php

namespace App\Http\Requests;

class MessageChatLatestRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mailbox_id' => 'required|exists:mail_boxes,id',
            'message_id' => 'nullable|exists:messages,id'
        ];
    }
}
