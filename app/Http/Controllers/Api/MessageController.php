<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateMessageRequest;
use App\Http\Requests\MessageChatLatestRequest;
use App\Services\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MessageController extends Controller
{
    protected $messageService;

    public function __construct(MessageService $service)
    {
        parent::__construct();
        $this->messageService = $service;
    }

    /**
     * Get list message by id
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listMessagesById(Request $request, $id)
    {
        try {
            $member = request()->user;
            $data = $this->messageService->listMessagesById($member->id, $id);
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API list message by id: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API send new message
     *
     * @param CreateMessageRequest $request
     * @return JsonResponse
     */
    public function send(CreateMessageRequest $request)
    {
        try {
            $member = request()->user;
            $data = $this->messageService->createMessage($member, $request);
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API send new message: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API get message latest
     *
     * @param MessageChatLatestRequest $request
     * @return JsonResponse
     */
    public function messageLatest(MessageChatLatestRequest $request)
    {
        try {
            $member = request()->user;
            // ID message latest
            $messageId = $request->get('message_id');
            $mailBoxId = $request->get('mailbox_id');
            $data = $this->messageService->messageLatest($member, $mailBoxId, $messageId);
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API get message latest: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }
}
