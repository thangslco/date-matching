<?php

namespace App\Repositories;

use App\Models\Like;
use App\Repositories\Contracts\LikeRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class LikeRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class LikeRepositoryEloquent extends BaseRepository implements LikeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Like::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getTotalLikedByUser($id, $hasMessage = false) {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
        ->whereHas('userTo', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where('id_to', $id);
        if ($hasMessage) {
            $model->whereNotNull('message')
                  ->where('is_viewed', 0);
        }
        $model = $model->count();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getTotalLiked() {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
        ->whereHas('userTo', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->count();
        $this->resetModel();
        return $this->parserResult($model);
    }

    // check 2 member matching or not: A like B and B like A => Count = 2
    // public function checkMemberLikedOrNot($memberId1, $memberId2) {
    //     $this->applyCriteria();
    //     $this->applyScope();
    //     $model = $this->model
    //     ->whereHas('userTo', function ($query) {
    //         $query->where('is_pause', 0)
    //               ->where('is_canceled', 0);
    //     })
    //     ->whereHas('userFrom', function ($query) {
    //         $query->where('is_pause', 0)
    //               ->where('is_canceled', 0);
    //     })
    //     ->where(function ($query) use ($memberId1, $memberId2) {
    //         $query->where('id_to', $memberId1)
    //               ->where('id_from', $memberId2);
    //     })
    //     ->orWhere(function ($query) use ($memberId1, $memberId2) {
    //         $query->where('id_to', $memberId2)
    //               ->where('id_from', $memberId1);
    //     })->count();
    //     $this->resetModel();
    //     return $this->parserResult($model);
    // }

    // A liked B. B like A => is matching
    public function checkMatching($memberIdB, $memberIdA) {
        $checkALikedB = $this->model
        ->whereHas('userTo', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->whereHas('userFrom', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where(function ($query) use ($memberIdA, $memberIdB) {
            $query->where('id_to', $memberIdA)
                  ->where('id_from', $memberIdB);
        })->first();

        $checkBLikedA = $this->model
        ->whereHas('userTo', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->whereHas('userFrom', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where(function ($query) use ($memberIdA, $memberIdB) {
            $query->where('id_to', $memberIdB)
                  ->where('id_from', $memberIdA);
        })->first();

        // A like B AND B like A => matching
        if ($checkALikedB && $checkBLikedA) {
            return 2;
        }
        // A like B = 1
        if ($checkALikedB) {
            return 1;
        }
        // B like A = 3
        if ($checkBLikedA) {
            return 3;
        }
        // not like, not matching
        return 0;
    }

    public function makedMatching($memberIdA, $memberIdB, &$dateMatching = null)
    {
        $dateMatching = date(DATETIME_FORMAT);
        return $this->model
        ->where(function ($query) use ($memberIdA, $memberIdB) {
            $query->where('id_to', $memberIdB)
                ->where('id_from', $memberIdA);
        })
        ->orWhere(function ($query) use ($memberIdA, $memberIdB) {
            $query->where('id_from', $memberIdB)
                ->where('id_to', $memberIdA);
        })->update([
            'date_matching' => $dateMatching
        ]);
    }

    // check member A liked member B => count = 1
    public function checkLikedMember($memberA, $memberB) {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
        ->whereHas('userTo', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->whereHas('userFrom', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where(function ($query) use ($memberA, $memberB) {
            $query->where('id_to', $memberA)
                  ->where('id_from', $memberB);
        })->count();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function listUsersLikedNotView($conditions, $hasMessage = false, $checkViewed = true)
    {
        $table = DB::table('likes');
        $whereIdFromNotIn = [];
        if (isset($conditions['member_blocked']) && !empty($conditions['member_blocked'])) {
            $whereIdFromNotIn = array_merge($whereIdFromNotIn, $conditions['member_blocked']);
            // $table = $table->whereNotIn('id_from', $conditions['member_blocked']);
            unset($conditions['member_blocked']);
        }
        if (isset($conditions['member_hided']) && !empty($conditions['member_hided'])) {
            $whereIdFromNotIn = array_merge($whereIdFromNotIn, $conditions['member_hided']);
            // $table = $table->whereNotIn('id_from', $conditions['member_hided']);
            unset($conditions['member_hided']);
        }
        if (isset($conditions['member_nexted']) && !empty($conditions['member_nexted'])) {
            $whereIdFromNotIn = array_merge($whereIdFromNotIn, $conditions['member_nexted']);
            // $table = $table->whereNotIn('id_from', $conditions['member_nexted']);
            unset($conditions['member_nexted']);
        }
        if (!empty($whereIdFromNotIn)) {
            $whereIdFromNotIn = array_unique($whereIdFromNotIn);
            $table = $table->whereNotIn('id_from', $whereIdFromNotIn);
        }
        $table->join('members', 'members.id', '=', 'likes.id_to')
            ->where(function ($query) {
                $query->where('members.is_pause', 0)
                      ->orWhere('members.is_canceled', 0);
            });
        if ($hasMessage !== -1 && $hasMessage == true) {
            $table = $table->whereNotNull('message');
        } elseif ($hasMessage !== -1 && $hasMessage == false) {
            $table = $table->whereNull('message');
        }
        $where = [
            'id_to' => $conditions['id_to'],
        ];
        if ($checkViewed) {
            $where['is_viewed'] = 0;
        }
        $list = $table->where($where)
        ->select('id_from AS member_id')
        ->distinct()
        ->pluck('member_id')
        ->toArray();
        return array_unique($list);
    }

    public function listUsersLikedNoMessage($conditions)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('userTo')
        ->whereHas('userTo', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where([
            'id_from' => $conditions['member_id']
        ])
        ->whereNull('message');
        if (isset($conditions['member_blocked']) && !empty($conditions['member_blocked'])) {
            $model->whereNotIn('id_to', $conditions['member_blocked']);
        }
        if (isset($conditions['member_hided']) && !empty($conditions['member_hided'])) {
            $model->whereNotIn('id_to', $conditions['member_hided']);
        }
        $model = $model->get();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function listUsersLikedHasMessage($conditions)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('userTo')
        ->whereHas('userTo', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where([
            'id_from' => $conditions['member_id']
        ])
        ->whereNotNull('message');
        $whereIdToNotIn = [];
        if (isset($conditions['member_blocked']) && !empty($conditions['member_blocked'])) {
            // $model->whereNotIn('id_to', $conditions['member_blocked']);
            $whereIdToNotIn = array_merge($whereIdToNotIn, $conditions['member_blocked']);
        }
        if (isset($conditions['member_hided']) && !empty($conditions['member_hided'])) {
            // $model->whereNotIn('id_to', $conditions['member_hided']);
            $whereIdToNotIn = array_merge($whereIdToNotIn, $conditions['member_hided']);
        }
        if (isset($conditions['member_nexted']) && !empty($conditions['member_nexted'])) {
            // $model->whereNotIn('id_to', $conditions['member_nexted']);
            $whereIdToNotIn = array_merge($whereIdToNotIn, $conditions['member_nexted']);
        }
        if (isset($conditions['member_not_like_again']) && !empty($conditions['member_not_like_again'])) {
            $whereIdToNotIn = array_merge($whereIdToNotIn, $conditions['member_not_like_again']);
        }
        if (!empty($whereIdToNotIn)) {
            $whereIdToNotIn = array_unique($whereIdToNotIn);
            $model->whereNotIn('id_to', $whereIdToNotIn);
        }
        $model = $model->groupBy('id_from')
            ->groupBy('id_to')
            ->get();
        $this->resetModel();
        return $this->parserResult($model);
    }

    // Get list members liked or member other like (A like B and B like A => matching)
    public function getMemberLikedOrOtherMemberLiked($memberId, $conditions = []) {
        $list = DB::table('likes')
            ->selectRaw(DB::raw('DISTINCT `id_to`, `id_from`'))
            ->where(function ($query) use ($memberId) {
                $query->orWhere('id_to', $memberId)
                    ->orWhere('id_from', $memberId);
            })
            ->where('is_ngword', 0);
        if (!empty($conditions)) {
            if (isset($conditions['date_matching'])) {
                $list = $list->whereNotNull('date_matching');
            }
            if (isset($conditions['is_viewed']) && $conditions['is_viewed'] === 0) {
                $list = $list->where('is_viewed', 0);
            }
        }
        $list = $list->get();
        $data = [];
        foreach ($list as $item) {
            if ($item->id_to != $memberId) {
                $total = isset($data[$item->id_to]) ? $data[$item->id_to] : 0;
                $data[$item->id_to] = $total + 1;
            }
            if ($item->id_from != $memberId) {
                $total = isset($data[$item->id_from]) ? $data[$item->id_from] : 0;
                $data[$item->id_from] = $total + 1;
            }
        }
        foreach ($data as $key => $value) {
            if ($value == 1) {
                unset($data[$key]);
            }
        }
        return array_keys($data);
    }

    public function getMemberSortedByDateMatching($memberId, $listMemberByMatching)
    {
        return DB::table('likes')
        ->selectRaw(DB::raw("
            DISTINCT
            CASE
                WHEN `id_to` = $memberId THEN `id_from`
                WHEN `id_from` = $memberId THEN `id_to`
            END AS `member_id`,
            `date_matching`
        "))
        ->whereNotNull('date_matching')
        ->where(function ($query) use ($memberId, $listMemberByMatching) {
            $query->where('id_to', $memberId);
            if (!empty($listMemberByMatching)) {
                $query->whereIn('id_from', $listMemberByMatching);
            }
        })
        ->orWhere(function ($query) use ($memberId, $listMemberByMatching) {
            $query->where('id_from', $memberId);
            if (!empty($listMemberByMatching)) {
                $query->whereIn('id_to', $listMemberByMatching);
            }
        })
        ->orderBy('date_matching', 'DESC')
        ->pluck('date_matching', 'member_id')
        ->toArray();
    }

    public function listUsersLikedByConditions($conditions)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('userTo')
        ->whereHas('userTo', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        });
        if (isset($conditions['member_id'])) {
            $model->where([
                'id_from' => $conditions['member_id']
            ]);
        }
        if (isset($conditions['id_to'])) {
            $model->where([
                'id_to' => $conditions['id_to']
            ]);
        }
        if (isset($conditions['member_blocked']) && !empty($conditions['member_blocked'])) {
            $model->whereNotIn('id_to', $conditions['member_blocked']);
        }
        if (isset($conditions['member_hided']) && !empty($conditions['member_hided'])) {
            $model->whereNotIn('id_to', $conditions['member_hided']);
        }
        $model = $model->get();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getTotalNgWordByMember($memberId)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
                        ->whereHas('userFrom', function ($query) {
                            $query->where('is_pause', 0)
                                  ->where('is_canceled', 0);
                        })
                        ->where([
                            ['id_from', '=', $memberId],
                            ['is_ngword', '>', 0]
                        ])
                        ->count();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getListMatching($conditions = [])
    {
        $items = DB::table('likes AS l1')
        ->select(['l1.id_from', 'l1.id_to'])
        ->join('likes as l2', function ($query) {
            $query->on('l1.id_from', '=', 'l2.id_to')
                ->on('l1.id_to', '=', 'l2.id_from');
        });
        if (!empty($conditions) && isset($conditions['member_id'])) {
            $items = $items->where('l1.id_from', $conditions['member_id'])
            ->orWhere('l1.id_to', $conditions['member_id']);
        }
        return $items->groupBy([
            'l1.id_from',
            'l1.id_to'
        ])
        ->get();
    }

    public function updateDateMatching($memberId1, $memberId2, $date)
    {
        return DB::table('likes')
        ->where(function ($query) use ($memberId1, $memberId2) {
            $query->where('id_from', $memberId1)
            ->where('id_to', $memberId2);
        })
        ->orWhere(function ($query) use ($memberId1, $memberId2) {
            $query->where('id_to', $memberId1)
            ->where('id_from', $memberId2);
        })
        ->update([
            'date_matching' => $date
        ]);
    }

    public function getDateLikeLatest($memberId1, $memberId2)
    {
        return DB::table('likes')
        ->selectRaw('MAX(created_at) AS date_matching')
        ->where(function ($query) use ($memberId1, $memberId2) {
            $query->where('id_from', $memberId1)
                ->where('id_to', $memberId2);
        })
        ->orWhere(function ($query) use ($memberId1, $memberId2) {
            $query->where('id_to', $memberId1)
                ->where('id_from', $memberId2);
        })
        ->first();
    }

    public function setViewedMatching($listIdViewed)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
            ->whereIn('id', $listIdViewed)
            ->update([
                'is_viewed' => 1
            ]);
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getMatchingMemberNotView($memberId1, $memberId2)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
            ->where(function ($query) use ($memberId1, $memberId2) {
                $query->where('id_from', $memberId1)
                    ->where('id_to', $memberId2);
            })
            ->whereNotNull('date_matching')
            ->whereNull('id_matching')
            ->where('is_viewed', 0)
            ->orderBy('created_at', 'DESC')
            ->first();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getListMembersMatchingNotView($conditions)
    {
        $table = DB::table('likes');
        $whereIdFromNotIn = [];
        if (isset($conditions['member_blocked']) && !empty($conditions['member_blocked'])) {
            $whereIdFromNotIn = array_merge($whereIdFromNotIn, $conditions['member_blocked']);
            unset($conditions['member_blocked']);
        }
        if (isset($conditions['member_hided']) && !empty($conditions['member_hided'])) {
            $whereIdFromNotIn = array_merge($whereIdFromNotIn, $conditions['member_hided']);
            unset($conditions['member_hided']);
        }
        if (isset($conditions['member_nexted']) && !empty($conditions['member_nexted'])) {
            $whereIdFromNotIn = array_merge($whereIdFromNotIn, $conditions['member_nexted']);
            unset($conditions['member_nexted']);
        }
        if (!empty($whereIdFromNotIn)) {
            $whereIdFromNotIn = array_unique($whereIdFromNotIn);
            $table = $table->whereNotIn('id_from', $whereIdFromNotIn);
        }
        $table->join('members', 'members.id', '=', 'likes.id_to')
        ->where(function ($query) {
            $query->where('members.is_pause', 0)
            ->orWhere('members.is_canceled', 0);
        })->whereNotNull('date_matching');
        $where = [
            'id_to' => $conditions['id_to'],
            'is_viewed' => 0,
        ];
        $list = $table->where($where)
            ->select('id_from AS member_id')
            ->distinct()
            ->pluck('member_id')
            ->toArray();
        return array_unique($list);
    }

    // A like B but B not like A
    public function getListMembersLikedButTheyNotLikeAgain($conditions)
    {
        $table = DB::table('likes');
        $whereIdFromNotIn = [];
        if (isset($conditions['member_blocked']) && !empty($conditions['member_blocked'])) {
            $whereIdFromNotIn = array_merge($whereIdFromNotIn, $conditions['member_blocked']);
            unset($conditions['member_blocked']);
        }
        if (isset($conditions['member_hided']) && !empty($conditions['member_hided'])) {
            $whereIdFromNotIn = array_merge($whereIdFromNotIn, $conditions['member_hided']);
            unset($conditions['member_hided']);
        }
        if (isset($conditions['member_nexted']) && !empty($conditions['member_nexted'])) {
            $whereIdFromNotIn = array_merge($whereIdFromNotIn, $conditions['member_nexted']);
            unset($conditions['member_nexted']);
        }
        if (!empty($whereIdFromNotIn)) {
            $whereIdFromNotIn = array_unique($whereIdFromNotIn);
            $table = $table->whereNotIn('id_from', $whereIdFromNotIn);
        }
        $table->join('members', 'members.id', '=', 'likes.id_to')
        ->where(function ($query) {
            $query->where('members.is_pause', 0)
            ->orWhere('members.is_canceled', 0);
        })->whereNull('date_matching');
        $where = [
            'id_to' => $conditions['id_to'],
        ];
        $list = $table->where($where)
            ->select('id_from AS member_id')
            ->distinct()
            ->pluck('member_id')
            ->toArray();
        return array_unique($list);
    }
}
