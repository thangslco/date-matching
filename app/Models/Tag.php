<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tag.
 *
 * @package namespace App\Models;
 */
class Tag extends BaseModel
{
    use SoftDeletes;
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
