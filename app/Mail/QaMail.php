<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QaMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        \Log::info('Start QaMail');
        try {
            $emailSender = env('EMAIL_SENDER');
            $emailAdmin = env('EMAIL_ADMIN');
            if (is_null($emailSender) || is_null($emailAdmin)) {
                throw new \Exception('Environment EMAIL_SENDER, EMAIL_ADMIN not set on .env', 400);
            }
            $mail = $this->from($emailSender, $emailSender)
                ->to($emailAdmin, 'Admin');
            if (isset($this->data['cc'])) {
                $mail->cc($this->data['cc']);
            }
            if (isset($this->data['bcc'])) {
                $mail->bcc($this->data['bcc']);
            }
            $mail->with($this->data)
            ->subject($this->data['title'])
            ->view('emails.qa');
            \Log::info('End QaMail');
        } catch (\Exception $e) {
            \Log::error('Exception send mail QA: ' . $e->getMessage());
        }
    }
}
