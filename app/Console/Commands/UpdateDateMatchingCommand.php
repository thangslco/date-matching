<?php

namespace App\Console\Commands;

use App\Services\LikeService;
use Illuminate\Console\Command;

class UpdateDateMatchingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-date-matching';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update date_matching field in likes table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(LikeService $likeService)
    {
        $likeService->updateDateMatching();
        $this->info('Success');
    }
}
