<?php

namespace App\Http\Requests;

class LoginPostApi extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:facebook,phone,email,apple',
            'password' => 'required_if:type,email',
            'phone' => 'required_if:type,phone',
            'code' => 'required_if:type,phone|verify_code',
            'email' => 'required_if:type,facebook,email',
            'apple_user_hash' => 'nullable|string'
        ];
    }
}
