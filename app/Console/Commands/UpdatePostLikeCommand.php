<?php

namespace App\Console\Commands;

use App\Services\PostService;
use Illuminate\Console\Command;

class UpdatePostLikeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-posts-liked';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Posts liked';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PostService $postService)
    {
        $postService->updatePostsLiked();
        $this->info('Success');
    }
}
