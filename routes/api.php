<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('member')->group(function () {
    Route::post('login', 'MemberController@login');
    Route::post('refresh_token', 'MemberController@refreshToken');
    Route::post('male/register', 'MemberController@maleRegister')->name('register.male');
    Route::post('female/register', 'MemberController@femaleRegister')->name('register.female');
    Route::get('find/{id}', 'MemberController@find')->where('id', '[0-9]+');
    Route::post('add-token', 'MemberController@addTokenFCM')->middleware('jwt.auth');
    Route::post('find-friends/{groupId?}', 'MemberController@findFriends')->middleware('jwt.auth');
    Route::post('next', 'MemberController@next')->middleware('jwt.auth');
    Route::get('list-nexted', 'MemberController@listUsersNexted')->middleware('jwt.auth');
    Route::post('like', 'MemberController@like')->middleware('jwt.auth');
    Route::get('list-liked-not-view', 'MemberController@listUsersLikedNotView')->middleware('jwt.auth');
    Route::get('list-liked-with-message', 'MemberController@listUsersLikedWithMessage')->middleware('jwt.auth');
    Route::get('list-liked-has-message', 'MemberController@listUsersLikedHasMessage')->middleware('jwt.auth');
    Route::get('list-liked', 'MemberController@listUsersLiked')->middleware('jwt.auth');
    Route::get('detail/{id}', 'MemberController@detail')->where('id', '[0-9]+')->middleware('jwt.auth');
    Route::get('profile', 'MemberController@profile')->middleware('jwt.auth');
    Route::post('update/{id}', 'MemberController@updateMember')->where('id', '[0-9]+');
    Route::get('groups', 'GroupController@listGroups')->middleware('jwt.auth');
    Route::get('group/detail/{id}', 'GroupController@detail')->where('id', '[0-9]+');
    Route::post('group/join/{id}', 'MemberController@joinGroup')->where('id', '[0-9]+')->middleware('jwt.auth');
    Route::post('posts/new', 'PostController@store')->middleware('jwt.auth');
    Route::get('mypage', 'MemberController@mypage')->middleware('jwt.auth');
    Route::post('block', 'MemberController@blockMember')->middleware('jwt.auth');
    Route::post('hide-blocked', 'MemberController@hideMemberBlocked')->middleware('jwt.auth');
    Route::post('un-blocked', 'MemberController@unBlockMember')->middleware('jwt.auth');
    Route::get('list-blocked', 'MemberController@listMemberBlocked')->middleware('jwt.auth');
    Route::post('hide', 'MemberController@hideMember')->middleware('jwt.auth');
    Route::post('un-hide', 'MemberController@unHideMember')->middleware('jwt.auth');
    Route::get('list-member-hided', 'MemberController@listMemberHided')->middleware('jwt.auth');
    Route::get('list-matching', 'MemberController@listMatching')->middleware('jwt.auth');
    Route::get('list-matching-not-view', 'MemberController@listMatchingNotView')->middleware('jwt.auth');
    Route::post('set-viewed-for-matching', 'MemberController@setViewedMatching')->middleware('jwt.auth');
    Route::post('buy/vip', 'MemberController@buyVip')->middleware('jwt.auth');
    Route::post('buy/paid', 'MemberController@buyPaid')->middleware('jwt.auth');
    Route::post('buy/like', 'MemberController@buyLike')->middleware('jwt.auth');
    Route::post('tranfer-like', 'MemberController@tranferLike')->middleware('jwt.auth');
    Route::post('pause', 'MemberController@pause')->middleware('jwt.auth');
    Route::post('unpause', 'MemberController@unpause')->middleware('jwt.auth');
    Route::post('save-address', 'MemberController@saveAddress')->middleware('jwt.auth');
    Route::post('save-email', 'MemberController@saveEmail')->middleware('jwt.auth');
    Route::post('save-password', 'MemberController@savePassword')->middleware('jwt.auth');
    Route::post('cancel', 'MemberController@cancel')->middleware('jwt.auth');
    Route::get('list-likes-bougth-or-reward', 'MemberController@listLikesBoughtAndReward')->middleware('jwt.auth');
    Route::post('upload-image-verify', 'MemberController@uploadImageVerify')->middleware('jwt.auth');
    Route::get('list-users-by-options', 'MemberController@listUsersByOption')->middleware('jwt.auth');
    Route::get('check-info', 'MemberController@checkInfo')->middleware('jwt.auth');
});
Route::prefix('groups')->group(function () {
    Route::get('all', 'GroupController@all')->name('api.groups.all');
});

Route::prefix('messages')->group(function () {
    Route::get('chat/{id}', 'MessageController@listMessagesById')->where('id', '[0-9]+')->middleware('jwt.auth');
    Route::post('send', 'MessageController@send')->middleware('jwt.auth');
    Route::get('latest', 'MessageController@messageLatest')->middleware('jwt.auth');
});
Route::prefix('posts')->group(function () {
    Route::get('list', 'PostController@listPosts')->name('api.post.list-post')->middleware('jwt.auth');
    Route::get('category-trend', 'PostController@categoryTrend')->name('api.post.category-trend');
    Route::post('update/{id}', 'PostController@update')->where('id', '[0-9]+')->name('api.post.update')->middleware('jwt.auth');
    Route::post('reply/{id}', 'PostController@reply')->where('id', '[0-9]+')->name('api.post.reply')->middleware('jwt.auth');
    Route::post('like/{id}', 'PostController@like')->where('id', '[0-9]+')->name('api.post.like')->middleware('jwt.auth');
    Route::post('viewed/{id}', 'PostController@viewed')->where('id', '[0-9]+')->name('api.post.viewed')->middleware('jwt.auth');
    Route::post('stop/{id}', 'PostController@stop')->where('id', '[0-9]+')->name('api.post.stop')->middleware('jwt.auth');
    Route::post('search', 'PostController@search')->name('api.post.search')->middleware('jwt.auth');
    Route::get('list/{id}', 'PostController@listMessageByPostId')->where('id', '[0-9]+')->name('api.post.list')->middleware('jwt.auth');
    Route::get('latest', 'PostController@latest')->middleware('jwt.auth');
});
Route::group(['prefix' => 'items', 'middleware' => 'jwt.auth'], function () {
    Route::post('add', 'ItemController@add')->name('api.items.add');
    Route::post('use-item', 'ItemController@useItem')->name('api.items.use-item');
});
Route::get('/user/get-user', 'MemberController@getUserByEmailPhone');
Route::get('/constants', 'CommonController@getConstants')->name('constants');

Route::group(['middleware' => 'jwt.auth'], function() {
    Route::get('users', function() {
        $users = \App\Models\Member::all();
        return response()->json($users);
    });
});

Route::prefix('reports')->group(function () {
    Route::post('report', 'ReportController@report')->middleware('jwt.auth');
});

Route::prefix('notices')->group(function () {
    Route::get('list', 'NoticeController@list')->middleware('jwt.auth');
    Route::get('detail/{id}', 'NoticeController@detail')->where('id', '[0-9]+')->middleware('jwt.auth');
});

Route::prefix('favorites')->group(function () {
    Route::get('list', 'FavoriteController@list')->middleware('jwt.auth');
});

Route::prefix('settings')->group(function () {
    Route::post('set-notifications', 'SettingController@setSettingNotification')->middleware('jwt.auth');
    Route::post('set-limit-like-has-message', 'SettingController@setLimitLikeHasMessage')->middleware('jwt.auth');
    Route::get('get-all', 'SettingController@getAllSettings')->middleware('jwt.auth');
});

Route::post('send-mail', 'CommonController@sendMail')->middleware('jwt.auth');
Route::post('send-verify', 'CommonController@sendVerify');
