<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_boxes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user1_id');
            $table->bigInteger('user2_id');
            $table->timestamps();
            $table->unique(['user1_id', 'user2_id'], 'mailboxes_user1_id_user2_id_uindex');
            $table->index('user1_id');
            $table->index('user2_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_boxes');
    }
}
