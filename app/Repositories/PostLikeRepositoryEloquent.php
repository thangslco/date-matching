<?php

namespace App\Repositories;

use App\Models\PostLike;
use App\Repositories\Contracts\PostLikeRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class PostLikeRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class PostLikeRepositoryEloquent extends BaseRepository implements PostLikeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PostLike::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
