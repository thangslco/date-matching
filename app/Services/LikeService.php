<?php

namespace App\Services;

use App\Repositories\Contracts\LikeRepository;

class LikeService extends BaseService
{
    protected $imageRepository;
    /**
     * LikeService constructor.
     * @param LikeRepository $repository
     */
    public function __construct(
        LikeRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function getTotalLiked()
    {
        return $this->repository->getTotalLiked();
    }

    public function getTotalLikedByUser($userId)
    {
        return $this->repository->getTotalLikedByUser($userId);
    }

    public function updateDateMatching()
    {
        $result = $this->repository->getListMatching();
        $data = [];
        foreach ($result as $item) {
            $key1 = $item->id_from . '-' . $item->id_to;
            $key2 = $item->id_to . '-' . $item->id_from;
            if (!in_array($key1, $data) && !in_array($key2, $data)) {
                $data[] = $key1;
            }
        }
        if (!empty($data)) {
            foreach ($data as $itemKey) {
                $itemData = explode('-', $itemKey);
                $id1 = $itemData[0];
                $id2 = $itemData[1];
                $like1 = $this->repository->getDateLikeLatest($id1, $id2);
                $this->repository->updateDateMatching($id1, $id2, $like1->date_matching);
            }
        }
    }
}
