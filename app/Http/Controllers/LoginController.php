<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginPost;
use App\Services\UserService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
    }

    protected $userService;

    /**
     * Create a new controller instance.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        parent::__construct();

        $this->userService = $userService;
        $this->middleware('guest:admin')->except('logout');
    }

    /**
     * View - Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Post - Login request
     *
     * @param LoginPost $request
     *
     * @return \Illuminate\Http\Response
     */
    public function login(LoginPost $request)
    {
        if ($this->userService->login($request)) {
            return redirect()->route('home');
        }
        return redirect()->back()->withInput()->withErrors([ 'message' => 'Login fail!' ]);
    }

    /**
     * Post - Logout request
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $this->userService->logout();
        return redirect()->route('login');
    }
}
