<?php

namespace App\Http\Requests;

class CreateMessageRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:members,id',
            'content' => 'required_without:image|string',
            'image' => 'required_without:content|mimes:jpg,png,jpeg|max:10240'
        ];
    }
}
