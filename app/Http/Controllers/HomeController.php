<?php

namespace App\Http\Controllers;

use App\Services\LikeService;
use App\Services\MemberService;
use App\Services\PostService;
use App\Services\TransactionService;

class HomeController extends Controller
{
    protected $postService;
    protected $likeService;
    protected $memberService;
    protected $transactionService;

    public function __construct(
        PostService $postService,
        LikeService $likeService,
        MemberService $memberService,
        TransactionService $transactionService
    )
    {
        parent::__construct();
        $this->postService = $postService;
        $this->likeService = $likeService;
        $this->memberService = $memberService;
        $this->transactionService = $transactionService;
    }

    public function index()
    {
        $menuActived = 'MENU_HOME';
        $totalPosts = $this->postService->getTotalPosts();
        $totalLiked = $this->likeService->getTotalLiked();
        $labelsAge = [
            '18歳~25歳' => '#f56954',
            '26歳~33歳' => '#00a65a',
            '34歳~41歳' => '#f39c12',
            '42歳~49歳' => '#00c0ef',
            '50歳~57歳' => '#3c8dbc',
            '58歳~65歳' => '#d2d6de',
        ];
        $dataAge = $this->memberService->statisticByAge(array_keys($labelsAge));
        $labelsGender = [
            '男性' => '#f56954',
            '女性' => '#00a65a',
        ];
        $dataGender = $this->memberService->statisticByGender(array_keys($labelsGender));
        $labelsLike = [
            'いいね（男性）' => '#f56954',
            'いいね（女性）' => '#00a65a',
        ];
        $dataLike = $this->memberService->statisticByLike(array_keys($labelsLike));
        $totalMoney = $this->transactionService->getTotalMoney();
        $currentYear = now()->year;
        $prevYear = now()->subYear(1)->year;
        $totalMoneyCurrentYear = $this->transactionService->getTotalMoneyByYear($currentYear);
        $totalMoneyPrevYear = $this->transactionService->getTotalMoneyByYear($prevYear);
        $rateYear = 0;
        if ($totalMoneyPrevYear > 0) {
            $rateYear = round(($totalMoneyCurrentYear - $totalMoneyPrevYear) / $totalMoneyPrevYear);
        }

        $currentMonth = now()->month;
        $prevMonth = now()->subMonth(1)->month;
        $totalMoneyCurrentMonth = $this->transactionService->getTotalMoneyByMonth($currentYear, $currentMonth);
        $totalMoneyPrevMonth = $this->transactionService->getTotalMoneyByMonth($currentYear, $prevMonth);
        $rateMonth = 0;
        if ($totalMoneyPrevMonth > 0) {
            $rateMonth = round(($totalMoneyCurrentMonth - $totalMoneyPrevMonth) / $totalMoneyPrevMonth);
        }

        // 12 months ago
        $monthLatest = now()->subMonths(12)->startOfMonth()->format('Y-m-d');
        $totalMoneyLike = $this->transactionService->getTotalMoneyByType(TRANSACTION_TYPE_LIKE, $monthLatest);
        $totalMoneyVip = $this->transactionService->getTotalMoneyByType(TRANSACTION_TYPE_VIP, $monthLatest);
        $totalMoneyPaid = $this->transactionService->getTotalMoneyByType(TRANSACTION_TYPE_PAID, $monthLatest);
        $totalMoney12MonthAgo = $totalMoneyLike + $totalMoneyVip + $totalMoneyPaid;

        list($labelsGraph, $dataGraph, $rangeDate) = $this->getDataByMonth();

        // Statistic users
        list(
            $totalUserHasFee,
            $totalUserNotHasFeeOrExpired,
            $totalUserBuyVIP,
            $totalUserFemaleVerified,
            $totalUserFemaleNotVerified
        ) = $this->getStatisticUser();
        return view('home.index', [
            'menuActived' => $menuActived,
            'totalPosts' => $totalPosts,
            'totalLiked' => $totalLiked,
            'dataAge' => $dataAge,
            'labelsAge' => $labelsAge,
            'dataGender' => $dataGender,
            'labelsGender' => $labelsGender,
            'dataLike' => $dataLike,
            'labelsLike' => $labelsLike,
            'totalMoney' => $totalMoney,
            'totalMoneyCurrentYear' => $totalMoneyCurrentYear,
            'rateYear' => $rateYear,
            'totalMoneyCurrentMonth' => $totalMoneyCurrentMonth,
            'rateMonth' => $rateMonth,
            'totalMoneyLike' => $totalMoneyLike,
            'totalMoneyVip' => $totalMoneyVip,
            'totalMoneyPaid' => $totalMoneyPaid,
            'totalMoney12MonthAgo' => $totalMoney12MonthAgo,
            'labelsGraph' => $labelsGraph,
            'dataGraph' => $dataGraph,
            'rangeDate' => $rangeDate,
            'totalUserHasFee' => $totalUserHasFee,
            'totalUserNotHasFeeOrExpired' => $totalUserNotHasFeeOrExpired,
            'totalUserBuyVIP' => $totalUserBuyVIP,
            'totalUserFemaleVerified' => $totalUserFemaleVerified,
            'totalUserFemaleNotVerified' => $totalUserFemaleNotVerified,
        ]);
    }

    // Get data by 12 months ago latest
    private function getDataByMonth()
    {
        $monthLatest = now()->subMonths(12)->startOfMonth()->format('Y-m-d');
        $rangeDate = $monthLatest . ' ～ ' . now()->format('Y-m-d');
        $data = [];
        $labels = [];
        for ($i = 12; $i >= 0; $i--) {
            if ($i == 0) {
                $date = now()->startOfMonth();
            } else {
                $date = now()->startOfMonth()->subMonths($i);
            }
            $month = $date->copy()->month;
            $keyMonth = $month;
            $keyData = $date->copy()->format('Y-m');
            if ($month == 1) {
                $keyMonth = $date->copy()->format('Y-m');
            } else {
                $keyMonth = str_pad($keyMonth, 2, '0', STR_PAD_LEFT);
            }
            $labels[] = $keyMonth;
            $data[$keyData] = $this->transactionService->getTotalMoneyByMonth($date->copy()->year, $date->copy()->month);
        }
        return [$labels, $data, $rangeDate];
    }

    private function getStatisticUser()
    {
        $totalUserHasFee = $this->memberService->getTotalUserHasFee();
        $totalUserNotHasFeeOrExpired = $this->memberService->getTotalUserNotHasFeeOrExpired();
        $totalUserBuyVIP = $this->memberService->getTotalUserBuyVIP();
        $totalUserFemaleVerified = $this->memberService->getTotalUserByVerify(1, 1);
        $totalUserFemaleNotVerified = $this->memberService->getTotalUserByVerify(1, 0);
        return [
            $totalUserHasFee,
            $totalUserNotHasFeeOrExpired,
            $totalUserBuyVIP,
            $totalUserFemaleVerified,
            $totalUserFemaleNotVerified
        ];
    }
}
