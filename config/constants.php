<?php

if (!defined('PAGINATE_DEFAULT')) {
    define('PAGINATE_DEFAULT', 20);
}

if (!defined('DATE_FORMAT')) {
    define('DATE_FORMAT', 'Y-m-d');
}

if (!defined('DATETIME_FORMAT')) {
    define('DATETIME_FORMAT', 'Y-m-d H:i:s');
}

if (!defined('GUARD_ADMIN')) {
    define('GUARD_ADMIN', 'admin');
}
if (!defined('STATUS_DEFAULT')) {
    define('STATUS_DEFAULT', -1);
}

if (!defined('STATUS_PENDING')) {
    define('STATUS_PENDING', 0);
}

if (!defined('STATUS_APROVED')) {
    define('STATUS_APROVED', 1);
}

if (!defined('STATUS_REJECTED')) {
    define('STATUS_REJECTED', 2);
}

if (!defined('STATUS_PENDING_LABEL')) {
    define('STATUS_PENDING_LABEL', '承認待ち');
}

if (!defined('STATUS_APROVED_LABEL')) {
    define('STATUS_APROVED_LABEL', '承認済み');
}

if (!defined('STATUS_REJECTED_LABEL')) {
    define('STATUS_REJECTED_LABEL', '非承認');
}

if (!defined('LIST_PREFECTURE')) {
    define('LIST_PREFECTURE', [
        '' => '選択しない',
        1 => '北海道',
        2 => '青森県',
        3 => '岩手県',
        4 => '宮城県',
        5 => '秋田県',
        6 => '山形県',
        7 => '福島県',
        8 => '茨城県',
        9 => '栃木県',
        10 => '群馬県',
        11 => '埼玉県',
        12 => '千葉県',
        13 => '東京都',
        14 => '神奈川県',
        15 => '新潟県',
        16 => '富山県',
        17 => '石川県',
        18 => '福井県',
        19 => '山梨県',
        20 => '長野県',
        21 => '岐阜県',
        22 => '静岡県',
        23 => '愛知県',
        24 => '三重県',
        25 => '滋賀県',
        26 => '京都府',
        27 => '大阪府',
        28 => '兵庫県',
        29 => '奈良県',
        30 => '和歌山県',
        31 => '鳥取県',
        32 => '島根県',
        33 => '岡山県',
        34 => '広島県',
        35 => '山口県',
        36 => '徳島県',
        37 => '香川県',
        38 => '愛媛県',
        39 => '高知県',
        40 => '福岡県',
        41 => '佐賀県',
        42 => '長崎県',
        43 => '熊本県',
        44 => '大分県',
        45 => '宮崎県',
        46 => '鹿児島県',
        47 => '沖縄県',
    ]);
}

if (!defined('LIST_EDUCATION')) {
    define('LIST_EDUCATION', [
        '' => '選択しない',
        1 => '高校卒',
        2 => '短大/専門学校卒',
        3 => '大学卒',
        4 => '大学院卒',
    ]);
}

if (!defined('LIST_HUMAN_FORM')) {
    define('LIST_HUMAN_FORM', [
        1 => 'スリム',
        2 => 'やや細め',
        3 => '普通',
        4 => 'グラマー',
        5 => '筋肉質',
        6 => 'ぽっちゃり',
    ]);
}

if (!defined('LIST_SAKE')) {
    define('LIST_SAKE', [
        '' => '選択しない',
        1 => '全く飲まない',
        2 => 'たまに飲む',
        3 => '時々飲む',
        4 => 'よく飲む',

    ]);
}

if (!defined('LIST_SMOKE')) {
    define('LIST_SMOKE', [
        '' => '選択しない',
        1 => '吸わない',
        2 => '吸う',
        3 => '吸う（電子タバコ）',
        4 => '時々吸う',
        5 => '相手が嫌なら辞める',

    ]);
}

if (!defined('LIST_BLOOD_TYPE')) {
    define('LIST_BLOOD_TYPE', [
        '' => '選択しない',
        1 => 'A型',
        2 => 'B型',
        3 => 'O型',
        4 => 'AB型',
        5 => 'わからない',
    ]);
}

if (!defined('LIST_THING_OF_MARRIAGE')) {
    define('LIST_THING_OF_MARRIAGE', [
        '' => '選択しない',
        1 => 'すぐにでもしたい',
        2 => '近いうちにしたい',
        3 => 'いい人がいればしたい',
        4 => '今は考えていない',
        5 => 'わからない',
    ]);
}

if (!defined('LIST_ANNUAL_INCOME')) {
    define('LIST_ANNUAL_INCOME', [
        '' => '選択しない',
        1 => '300万円未満',
        2 => '300万円〜500万円',
        3 => '500万円〜700万円',
        4 => '700万円〜900万円',
        5 => '900万円〜1100万円',
        6 => '1100万円以上',
        7 => 'その他',
    ]);
}

if (!defined('LIST_JOB_TYPE')) {
    define('LIST_JOB_TYPE', [
        '' => '選択しない',
        1 => '正社員',
        2 => '医療',
        3 => '公務員',
        4 => '経営者・役員',
        5 => '事務員',
        6 => '大手商社',
        7 => '大手企業',
        8 => '大手外資',
        9 => '外資金融',
        10 => 'クリエイター',
        11 => 'IT関連',
        12 => '航空',
        13 => '芸能・モデル',
        14 => 'イベント',
        15 => '秘書',
        16 => '保育士',
        17 => 'フリーランス',
        18 => '学生',
        19 => 'その他',

    ]);
}

if (!defined('LIST_HAIR_FEMALE')) {
    define('LIST_HAIR_FEMALE', [
        '' => '選択しない',
        1 => 'ベリーショート',
        2 => 'ショート',
        3 => 'セミロング',
        4 => 'ロング',
        5 => 'ストレート',
        6 => 'ボブ',
    ]);
}

if (!defined('LIST_HAIR_MALE')) {
    define('LIST_HAIR_MALE', [
        '' => '選択しない',
        1 => 'ツーブロック',
        2 => '短髪',
        3 => 'ミディアム',
        4 => 'ボウズ',
        5 => 'パーマ系',
        6 => 'マッシュ',
    ]);
}

if (!defined('LIST_HAIR_COLOR')) {
    define('LIST_HAIR_COLOR', [
        '' => '選択しない',
        1 => '黒色',
        2 => '茶髪',
        3 => '派手髪',
    ]);
}

if (!defined('LIST_OPTION_1')) {
    define('LIST_OPTION_1', [
        '' => '選択しない',
        1 => '一人暮らし',
        2 => '実家住み',
        3 => '友達と同居',
        4 => 'ペット',
        5 => 'その他',

    ]);
}

if (!defined('LIST_OPTION_2')) {
    define('LIST_OPTION_2', [
        '' => '選択しない',
        1 => 'すぐ会って話したい',
        2 => '気が合えば会いたい',
        3 => 'メッセージを重ねて会いたい',
    ]);
}

if (!defined('LIST_OPTION_3')) {
    define('LIST_OPTION_3', [
        '' => '選択しない',
        1 => '長男',
        2 => '長女',
        3 => '次男',
        4 => '次女',
        5 => '末っ子',
        6 => '一人っ子',
    ]);
}

if (!defined('LIST_OPTION_4')) {
    define('LIST_OPTION_4', [
        '' => '選択しない',
        1 => '子供は欲しい',
        2 => 'できれば欲しい',
        3 => '子供は欲しくない',
        4 => '相手と相談して決める',
        5 => 'わからない',
    ]);
}

if (!defined('LIST_APPEARANCE_MALE')) {
    define('LIST_APPEARANCE_MALE', [
        1 => 'ぱっちり二重',
        2 => 'イヌ顔',
        3 => 'キリっとした顔',
        4 => 'アイドル顔',
        5 => 'ネコ顔',
        6 => 'ちょい悪顔',
        7 => '茶髪',
        8 => '高身長',
        9 => '黒髪',
        10 => '色白系',
        11 => '色黒系',
        12 => 'スッキリ一重',
        13 => 'メガネ男子',
        14 => '濃いめの顔',
    ]);
}

if (!defined('LIST_APPEARANCE_FEMALE')) {
    define('LIST_APPEARANCE_FEMALE', [
        100 => 'ハーフ顔',
        101 => '小顔',
        102 => 'ぱっちり二重',
        103 => 'イヌ顔',
        104 => 'メガネ女子',
        105 => 'ギャル系',
        106 => '色白系',
        107 => 'ネコ顔',
        108 => 'おとなしめ',
        109 => 'えくぼ',
        110 => '八重歯',
    ]);
}

if (!defined('LIST_IMPRESSIVE_MALE')) {
    define('LIST_IMPRESSIVE_MALE', [
        1 => 'さわやか系',
        2 => '紳士系',
        3 => 'かわいい系',
        4 => '原宿系',
        5 => 'IT系',
        6 => 'インテリ系',
        // 7 => '短髪系',
        8 => 'スポーツマン系',
        9 => 'オレ様系',
        10 => 'ガテン系',
        11 => '韓流系',
        12 => 'V系',
    ]);
}

if (!defined('LIST_IMPRESSIVE_FEMALE')) {
    define('LIST_IMPRESSIVE_FEMALE', [
        100 => '素直',
        101 => '真面目',
        102 => 'お調子者',
        103 => '天然',
        104 => 'よく笑う',
        105 => 'ボーイッシュ',
        106 => '小動物系',
        107 => 'ドジっ子',
        108 => '精祖系',
        109 => 'お姉さん系',
        110 => 'スポーティー',
        111 => 'アイドル系',
        112 => '小悪魔',
        113 => '礼儀正しい',
        114 => '清楚系',
    ]);
}

if (!defined('LIST_TYPE_USER')) {
    define('LIST_TYPE_USER', [
        0 => 'Normal',
        1 => 'Admin',
        2 => 'Master'
    ]);
}
if (!defined('TYPE_USER_MASTER')) {
    define('TYPE_USER_MASTER', 2);
}
if (!defined('TYPE_USER_ADMIN')) {
    define('TYPE_USER_ADMIN', 1);
}
if (!defined('TYPE_USER_NORMAL')) {
    define('TYPE_USER_NORMAL', 0);
}

if (!defined('LIST_TYPE_ITEMS')) {
    define('LIST_TYPE_ITEMS', [
        1 => 'Like',
        2 => 'Rose',
        3 => 'Rocket'
    ]);
}
if (!defined('TYPE_LIKE_ITEM')) {
    define('TYPE_LIKE_ITEM', 1);
}
if (!defined('TYPE_ROSE_ITEM')) {
    define('TYPE_ROSE_ITEM', 2);
}
if (!defined('TYPE_ROCKET_ITEM')) {
    define('TYPE_ROCKET_ITEM', 3);
}

if (!defined('LIST_TYPE_FAVORITES')) {
    define('LIST_TYPE_FAVORITES', [
        1 => '動物',
        2 => 'アウトドア',
        3 => '趣味',
        4 => '休日',
        5 => '飲食'
    ]);
}

if (!defined('TYPE_FAVORITE_PINK')) {
    define('TYPE_FAVORITE_PINK', 1);
}
if (!defined('TYPE_FAVORITE_BLUE')) {
    define('TYPE_FAVORITE_BLUE', 2);
}
if (!defined('TYPE_FAVORITE_ORANGE')) {
    define('TYPE_FAVORITE_ORANGE', 3);
}
if (!defined('TYPE_FAVORITE_GREEN')) {
    define('TYPE_FAVORITE_GREEN', 4);
}
if (!defined('TYPE_FAVORITE_RED')) {
    define('TYPE_FAVORITE_RED', 5);
}
if (!defined('TIME_ACCESS_TOKEN')) {
    define('TIME_ACCESS_TOKEN', 24); // 24 hours
}
if (!defined('TIME_REFRESH_TOKEN')) {
    define('TIME_REFRESH_TOKEN', 30); // 30 days
}
if (!defined('LIST_CATEGORY_1')) {
    define('LIST_CATEGORY_1', [
        1 => 'おでかけ',
        2 => 'おしゃべり'
    ]);
}
if (!defined('LIST_CATEGORY_2')) {
    define('LIST_CATEGORY_2', [
        1 => [
            11 => 'トレンド',
            12 => 'グルメ',
            13 => '定番'
        ],
        2 => [
            21 => 'トレンド',
            22 => '定番',
            23 => 'SOS'
        ]
    ]);
}
if (!defined('LIST_CATEGORY_3')) {
    define('LIST_CATEGORY_3', [
        11 => [
        ],
        12 => [
            121 => 'ディナーに行こう',
            122 => 'ランチに行こう',
            123 => '飲み行こう',
            124 => '焼肉食べ行こう',
            125 => '寿司食べ行こう',
            126 => '映えスポットに行こう',
            127 => 'イルミネーション行きたい',
        ],
        13 => [
            131 => 'カフェでお茶しよう',
            132 => 'ショッピング付き合って！',
            133 => 'テーマパークに行こう',
            134 => '近場で軽く散歩♪',
            135 => '映画見に行こう',
            136 => 'ドライブに行こう'
        ],
        21 => [
        ],
        22 => [
            220 => 'こわい話しよう',
            221 => 'オンライン飲み会しよう',
            222 => '電話で話そう',
            223 => '面白い話して！',
            224 => '悩みを聞いて',
            225 => 'ゲームしよう',
        ],
        23 => [
            231 => '仕事のアドバイスが欲しい',
            232 => '勉強を教えて！'
        ]
    ]);
}

if (!defined('TYPE_LOGIN_PHONE')) {
    define('TYPE_LOGIN_PHONE', 'phone');
}

if (!defined('TYPE_LOGIN_FACEBOOK')) {
    define('TYPE_LOGIN_FACEBOOK', 'facebook');
}

if (!defined('TYPE_LOGIN_EMAIL')) {
    define('TYPE_LOGIN_EMAIL', 'email');
}

if (!defined('TYPE_LOGIN_APPLE')) {
    define('TYPE_LOGIN_APPLE', 'apple');
}

if (!defined('LIST_TYPE_TIME')) {
    define('LIST_TYPE_TIME', [
        1 => '今日',
        2 => '明日',
        3 => '来週',
        4 => '相談',
    ]);
}

if (!defined('TYPE_TIME_TODAY')) {
    define('TYPE_TIME_TODAY', 1);
}

if (!defined('TYPE_TIME_TOMORROW')) {
    define('TYPE_TIME_TOMORROW', 2);
}

if (!defined('TYPE_TIME_NEXT_WEEK')) {
    define('TYPE_TIME_NEXT_WEEK', 3);
}

if (!defined('TYPE_TIME_OTHER')) {
    define('TYPE_TIME_OTHER', 4);
}

if (!defined('IMAGE_TYPE_MEMBER')) {
    define('IMAGE_TYPE_MEMBER', 1);
}

if (!defined('IMAGE_TYPE_MEMBER_SECONDARY')) {
    define('IMAGE_TYPE_MEMBER_SECONDARY', 2);
}

if (!defined('IMAGE_TYPE_CATEGORY')) {
    define('IMAGE_TYPE_CATEGORY', 3);
}

if (!defined('IMAGE_TYPE_GROUP')) {
    define('IMAGE_TYPE_GROUP', 4);
}

if (!defined('LIST_POSITION_REPORT')) {
    define('LIST_POSITION_REPORT', [
        1 => '登録プロフィール内容',
        2 => '自己紹介文',
        3 => 'メッセージの内容',
        4 => 'amor以外の場所',
        5 => '実際に会った時',
        6 => 'その他',
    ]);
}

if (!defined('LIST_TYPE_REPORT')) {
    define('LIST_TYPE_REPORT', [
        1 => '肉体関係の目的',
        2 => '金銭目的',
        3 => 'ビジネスや団体への勧誘',
        4 => '別サイトへの勧誘',
        5 => '営業、求人目的',
        6 => '登録情報の詐称',
        7 => '誹謗中傷、卑わいな言動',
        8 => 'ドタキャンされた',
        9 => 'その他不快な行為',
    ]);
}

if (!defined('LIST_CATEGORY_NOTICES')) {
    define('LIST_CATEGORY_NOTICES', [
        1 => '更新',
        2 => 'アップデート',
        3 => '通知',
        4 => 'セール',
    ]);
}

if (!defined('LIST_REASON_PAUSE')) {
    define('LIST_REASON_PAUSE', [
        1 => 'amorで恋人ができた',
        2 => 'amor以外で恋人ができた',
        3 => 'amorからの通知が多い',
        4 => 'amorの使い方がよくわからない',
        5 => '気になる人がいなかった',
        6 => '気になる人と会えなかった',
        7 => '他のサービスを利用するため',
        8 => 'その他',
    ]);
}

if (!defined('LIST_REASON_CANCELED')) {
    define('LIST_REASON_CANCELED', [
        1 => 'amorで恋人ができた',
        2 => 'amor以外で恋人ができた',
        3 => 'amorからの通知が多い',
        4 => 'amorの使い方がよくわからない',
        5 => '気になる人がいなかった',
        6 => '気になる人と会えなかった',
        7 => '他のサービスを利用するため',
        8 => 'その他',
    ]);
}

if (!defined('BLACK_LIST_PR')) {
    define('BLACK_LIST_PR', [
        'SEX',
        'sex',
        'セックス',
        'せっくす',
        'エッチ',
        'えっち',
        'マンコ',
        'まんこ',
        'ちんこ',
        'チンコ',
        'おっぱい',
        'オッパイ',
        '早漏',
        '乱交',
        '淫乱',
        'フェラチオ',
        'フェラ',
        'アイディー',
        '乳首',
        'おっぱい',
        'オッパイ',
        '性行為',
        '乱交',
        '淫乱',
        'フェラチオ',
        'フェラ',
        'せふれ',
        'セフレ',
        '援助交際',
        '円光',
        '援交',
        'パパカツ',
        'パパ活',
        'お手当て',
        '割り切った関係',
        '割り切り',
        'ソフレ',
        'そふれ',
        'シネ',
        '殺す',
        'ころす',
        'コロス',
        'ID',
        'id',
    ]);
}
if (!defined('BLACK_LIST_NICKNAME')) {
    define('BLACK_LIST_NICKNAME', [
        'SEX',
        'sex',
        'セックス',
        'せっくす',
        'エッチ',
        'えっち',
        'マンコ',
        'まんこ',
        'ちんこ',
        'チンコ',
        'おっぱい',
        'オッパイ',
        '早漏',
        '乱交',
        '淫乱',
        'フェラチオ',
        'フェラ',
        'アイディー',
        '乳首',
        'おっぱい',
        'オッパイ',
        '性行為',
        '乱交',
        '淫乱',
        'フェラチオ',
        'フェラ',
        'せふれ',
        'セフレ',
        '援助交際',
        '円光',
        '援交',
        'パパカツ',
        'パパ活',
        'お手当て',
        '割り切った関係',
        '割り切り',
        'ソフレ',
        'そふれ',
        'シネ',
        '殺す',
        'ころす',
        'コロス',
        'ID',
        'id',
        'LINE',
        'ＬＩＮＥ',
        'line',
        'Line',
        'Instagram',
        'instagram',
        'Twitter',
        'twitter',
        'Gmail',
        'ライン',
        'らいん',
        'ラいン',
        'ライん',
        'インスタグラム',
        'インスタ',
        'インスタ',
        'いんすたぐらむ',
        'いんすた',
        'ツイッター',
        'ついったー',
        'アモル',
        'amor',
        '村田飛翔',
        '酒井龍太郎',
    ]);
}

if (!defined('LIST_NGWORD_LIKE_HAS_MESSAGE')) {
    define('LIST_NGWORD_LIKE_HAS_MESSAGE', [
        'LINE',
        'ＬＩＮＥ',
        'line',
        'Line',
        'Instagram',
        'instagram',
        'Twitter',
        'twitter',
        'Gmail',
        'SEX',
        'sex',
        'セックス',
        'せっくす',
        'エッチ',
        'えっち',
        'マンコ',
        'まんこ',
        'ちんこ',
        'チンコ',
        'おっぱい',
        'オッパイ',
        '性行為',
        '乱交',
        '淫乱',
        'フェラチオ',
        'フェラ',
        'ブス',
        '死ね',
        'ライン',
        'らいん',
        'ラいン',
        'ライん',
        'インスタグラム',
        'インスタ',
        'インスタ',
        'いんすたぐらむ',
        'いんすた',
        'ツイッター',
        'ついったー',
        'カカオ',
        '援助交際',
        '円光',
        '援交',
        'パパカツ',
        'パパ活',
        'お手当て',
        '割り切った関係',
        '割り切り',
        'ソフレ',
        'そふれ',
        'シネ',
        'キモい',
        'キモイ',
        'きもい',
        'ブサイク',
        'デブ',
        'ぶす',
        'でぶ',
        '乳首',
        '殺す',
        'ころす',
        'コロス',
        'せふれ',
        'セフレ',
        'しね',
    ]);
}

if (!defined('TRANSACTION_TYPE_LIKE')) {
    define('TRANSACTION_TYPE_LIKE', 1);
}

if (!defined('TRANSACTION_TYPE_VIP')) {
    define('TRANSACTION_TYPE_VIP', 2);
}

if (!defined('TRANSACTION_TYPE_PAID')) {
    define('TRANSACTION_TYPE_PAID', 3);
}

if (!defined('TRANSACTION_TYPE_REWARD')) {
    define('TRANSACTION_TYPE_REWARD', 4);
}

// Time verify code login (minustes)
if (!defined('TIME_VERIFY_LOGIN')) {
    define('TIME_VERIFY_LOGIN', 1);
}

// Time verify code register (minustes)
if (!defined('TIME_VERIFY_REGISTER')) {
    define('TIME_VERIFY_REGISTER', 5);
}

if (!defined('LIST_TYPE_VERIFY_CODE')) {
    define('LIST_TYPE_VERIFY_CODE', [
        1 => 'Login',
        2 => 'Register'
    ]);
}

if (!defined('LIKE_NUMBER_REGISTER_ACCOUNT')) {
    define('LIKE_NUMBER_REGISTER_ACCOUNT', 200);
}
