<?php

namespace App\Providers;

use Illuminate\Http\Response as HttpStatusCode;
use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        response()->macro('jsonSuccess', function ($data, $status = HttpStatusCode::HTTP_OK) {
            return response()->json($data, $status);
        });

        response()->macro('jsonError', function ($errorCode, $message = 'Error') {
            return response()->json(
                [
                    'code' => $errorCode,
                    'message' => $message,
                ],
                $errorCode
            );
        });

        response()->macro('jsonValidationError', function ($message) {
            return response()->json(
                [
                    'code' => 400,
                    'errors' => $message,
                ],
                HttpStatusCode::HTTP_BAD_REQUEST
            );
        });
    }
}
