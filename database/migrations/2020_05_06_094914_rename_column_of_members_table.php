<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnOfMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->renameColumn('appearance', 'appearances');
            $table->renameColumn('impressive', 'impressives');
            $table->renameColumn('human_form', 'human_forms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->renameColumn('appearances', 'appearance');
            $table->renameColumn('impressives', 'impressive');
            $table->renameColumn('human_forms', 'human_form');
        });
    }
}
