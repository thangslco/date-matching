@extends('layouts.default')

@section('title', '一時停止ユーザ一覧')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">一時停止ユーザ一覧</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">一時停止ユーザ一覧</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-4">
                            <div class="chart-responsive">
                                <canvas id="pieChart-paused" height="220"></canvas>
                            </div>
                            <!-- ./chart-responsive -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                            <ul class="chart-legend clearfix">
                                @foreach ($labelPaused as $label => $color)
                                <li><i class="far fa-circle" style="color: {{$color}}!important;"></i> {{$label}}</li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-md-2">
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools">
                            <form action="" method="GET">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="keyword" class="form-control float-right" value="{!! request()->keyword !!}" placeholder="Search...">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nickname</th>
                                    <th>Birthday</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item)
                                <tr class="{!! $item->getStatusBackground() !!}">
                                    <td>{{ $item->id }}</td>
                                    <td><a href="{{ $item->getMemberUrl() }}">{!! $item->nickname !!}</a></td>
                                    <td>{!! $item->birthday !!}</td>
                                    <td>{!! $item->getStatusText() !!}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        {!! $items->links() !!}
                    </div>
                </div>
                <!-- /.card -->
                <div class="modal fade" id="modal-approve">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Confirm approve</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure approve this request?</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="btn-ok-approve">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <div class="modal fade" id="modal-reject">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Confirm reject</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure reject this request?</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="btn-ok-reject">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('style')
    @parent
@endsection
@section('script')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            var id = null;
            $('.btn-approve').click(function(e) {
                $('#modal-approve').modal('show');
                id = $(this).data('id');
            });

            $('.btn-reject').click(function(e) {
                $('#modal-reject').modal('show');
                id = $(this).data('id');
            });
            $('#btn-ok-approve').click(function(e) {
                e.preventDefault();
                if (typeof id !== 'undefined' && id !== '') {
                    $.ajax({
                        url: '{{ route ('approve')}}',
                        type: 'POST',
                        data: {id: id},
                        success: function(result) {
                            console.log(result);
                            if (result.STATUS == true) {
                                alert(result.message);
                                setTimeout(function() {
                                    location.href = location.href;
                                }, 1000);
                            } else {
                                alert(result.message);
                            }
                        }
                    });
                }
            });

            $('#btn-ok-reject').click(function(e) {
                e.preventDefault();
                if (typeof id !== 'undefined' && id !== '') {
                    $.ajax({
                        url: '{{ route ('reject')}}',
                        type: 'POST',
                        data: {id: id},
                        success: function(result) {
                            if (result.STATUS == true) {
                                alert(result.message);
                                setTimeout(function() {
                                    location.href = location.href;
                                }, 1000);
                            } else {
                                alert(result.message);
                            }
                        }
                    });
                }
            });

            $('#modal-approve').on('hidden.bs.modal', function (e) {
                id = null;
            });
            $('#modal-reject').on('hidden.bs.modal', function (e) {
                id = null;
            });
        });
    </script>
@endsection
@section('script')
@parent
<script src="{{ url('/assets/js/Chart.min.js') }}"></script>
<script>
    var dataPaused = JSON.parse('{!!json_encode(array_values($dataPaused))!!}');
    var labelPaused = JSON.parse('{!!json_encode(array_values($labelPaused))!!}');
</script>
<script src="{{ url('/assets/js/dashboard.js') }}"></script>
<script type="text/javascript">
    create_pie_chart('paused', labelPaused, dataPaused);
</script>
@endsection
