<?php

namespace App\Http\Requests;

class TranferLikeRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $listType = [TYPE_ROSE_ITEM, TYPE_ROCKET_ITEM];
        return [
            'like_number' => 'required|integer|min:1',
            'type' => 'required|integer|in:' . implode(',', $listType),
            'item_number' => 'required|integer|min:1',
        ];
    }
}
