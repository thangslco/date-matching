<?php

namespace App\Http\Requests;

class CreateFavoriteRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $listKeyTypes = array_keys(LIST_TYPE_FAVORITES);
        if ($this->id) {
            return [
                'name' => 'required|max:255|unique:favorites,name,' . $this->id,
                'type' => 'required|in:' . implode(',', $listKeyTypes)
            ];
        }
        return [
            'name' => 'required|max:255|unique:favorites,name',
            'type' => 'required|in:' . implode(',', $listKeyTypes)
        ];
    }
}
