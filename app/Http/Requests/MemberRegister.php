<?php

namespace App\Http\Requests;

use Route;


class MemberRegister extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nickname' => 'required|max:150|unique:members,nickname',
            'birthday' => 'date|date_format:Y-m-d|before:now',
            'address' => 'string|max:255',
            // 'password' => 'required|max:255',
            'avatar' => 'mimes:jpg,png,jpeg|max:10240',
            'images' => 'nullable|array'
        ];
        if (!$this->get('apple_user_hash') && !$this->get('phone') && !$this->get('email')) {
            $rules['phone'] = 'required|string|max:30|unique:members,phone|check_member_cancel';
            $rules['email'] = 'required|string|max:255|unique:members,email|check_member_cancel';
            $rules['apple_user_hash'] = 'required|max:255|unique:members,apple_user_hash|check_member_cancel';
        } elseif($this->get('apple_user_hash') && !$this->get('phone') && !$this->get('email')) {
            $rules['apple_user_hash'] = 'required|max:255|unique:members,apple_user_hash|check_member_cancel';
        } elseif (!$this->get('apple_user_hash') && $this->get('phone') && !$this->get('email')) {
            $rules['phone'] = 'required|string|max:30|unique:members,phone|check_member_cancel';
            $rules['code'] = 'required|verify_code_register';
        } elseif (!$this->get('apple_user_hash') && !$this->get('phone') && $this->get('email')) {
            $rules['email'] = 'required|string|max:255|unique:members,email|check_member_cancel';
        } elseif ($this->get('apple_user_hash') && !$this->get('phone') && $this->get('email')) {
            $rules['email'] = 'required|string|max:255|unique:members,email|check_member_cancel';
        } elseif (!$this->get('apple_user_hash') && $this->get('phone') && $this->get('email')) {
            $rules['phone'] = 'required|string|max:30|unique:members,phone|check_member_cancel';
            $rules['code'] = 'required|verify_code_register';
        } elseif ($this->get('apple_user_hash') && $this->get('phone') && !$this->get('email')) {
            $rules['phone'] = 'required|string|max:30|unique:members,phone|check_member_cancel';
            $rules['code'] = 'required|verify_code_register';
        }

        $currentRoute = Route::currentRouteName();
        if ($currentRoute == 'register.female') {
            $dataHumanForms = $this->get('human_forms');
            if (!is_null($dataHumanForms)) {
                $listKeysDataHumanForms = array_keys($dataHumanForms);
                foreach ($listKeysDataHumanForms as $key) {
                    $rules['human_forms.' . $key] = 'nullable|in:' . implode(',', array_keys(LIST_HUMAN_FORM));
                }
            }
            $dataAppearances = $this->get('appearances');
            if (!is_null($dataAppearances)) {
                $listKeysDataAppearances = array_keys($dataAppearances);
                foreach ($listKeysDataAppearances as $key) {
                    $rules['appearances.' . $key] = 'nullable|in:' . implode(',', array_keys(LIST_APPEARANCE_FEMALE));
                }
            }
            $dataImpressives = $this->get('impressives');
            if (!is_null($dataImpressives)) {
                $listKeysDataImpressives = array_keys($dataImpressives);
                foreach ($listKeysDataImpressives as $key) {
                    $rules['impressives.' . $key] = 'nullable|in:' . implode(',', array_keys(LIST_IMPRESSIVE_FEMALE));
                }
            }
        } elseif ($currentRoute == 'register.male') {
            $rules['image_verify'] = 'nullable|mimes:jpg,png,jpeg|max:10240';
        }
        return $rules;
    }

    public function messages()
    {
        $messages = [];
        $currentRoute = Route::currentRouteName();
        if ($currentRoute == 'register.female') {
            $dataHumanForms = $this->get('human_forms');
            if (!is_null($dataHumanForms)) {
                $listKeysDataHumanForms = array_keys($dataHumanForms);
                foreach ($listKeysDataHumanForms as $key) {
                    $messages['human_forms.' . $key] = "Value of 体格 at {$key} invalid";
                }
            }
            $dataAppearances = $this->get('appearances');
            if (!is_null($dataAppearances)) {
                $listKeysDataAppearances = array_keys($dataAppearances);
                foreach ($listKeysDataAppearances as $key) {
                    $messages['appearances.' . $key] = "Value of 見た目 at {$key} invalid";
                }
            }
            $dataImpressives = $this->get('impressives');
            if (!is_null($dataImpressives)) {
                $listKeysDataImpressives = array_keys($dataImpressives);
                foreach ($listKeysDataImpressives as $key) {
                    $messages['impressives.' . $key] = "Value of イメージ at {$key} invalid";
                }
            }
        }
        return $messages;
    }
}
