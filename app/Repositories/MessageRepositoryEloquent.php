<?php

namespace App\Repositories;

use App\Models\Message;
use App\Repositories\Contracts\MessageRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class MessageRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MessageRepositoryEloquent extends BaseRepository implements MessageRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Message::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
    public function getList($conditions = [], $keyword = null)
    {
        if (!is_null($keyword)) {
            $this->applyCriteria();
            $this->applyScope();
            $model = $this->model->with('user')->where('content', 'LIKE', "%$keyword%");
            $results = $model->paginate(PAGINATE_DEFAULT);
            $results->appends(app('request')->query());
            $this->resetModel();
            return $this->parserResult($results);
        } else {
            $select = [
                'messages.*',
                'mail_boxes.id as mailbox_id',
                'user1.id as member_id_1',
                'user1.nickname as nickname_1',
                'user1.gender as gender_1',
                'user2.id as member_id_2',
                'user2.nickname as nickname_2',
                'user2.gender as gender_2',
            ];
            return DB::table('messages')
                ->selectRaw(implode(', ', $select))
                ->join('mail_boxes', function ($join) {
                    $join->on('mail_boxes.id', '=', 'messages.mailbox_id');
                })
                ->join('members AS user1', function ($join) {
                    $join->on('user1.id', '=', 'mail_boxes.user1_id')
                        ->where('user1.is_pause', 0)
                        ->where('user1.is_canceled', 0);
                })
                ->join('members AS user2', function ($join) {
                    $join->on('user2.id', '=', 'mail_boxes.user2_id')
                        ->where('user2.is_pause', 0)
                        ->where('user2.is_canceled', 0);
                })
                ->where($conditions)
                ->whereRaw(DB::raw('messages.id IN (SELECT
                MAX(m.id)
                FROM
                messages as m
                GROUP BY mailbox_id)'))
                ->orderBy('created_at', 'DESC')
                ->paginate(PAGINATE_DEFAULT);
        }
    }

    public function getListNgWord($conditions = [], $keyword = null)
    {
        $items = DB::table('messages')
            ->selectRaw('messages.*, mail_boxes.id as mailbox_id, user1.email as email_1, user2.email as email_2')
            ->join('mail_boxes', function ($join) {
                $join->on('mail_boxes.id', '=', 'messages.mailbox_id');
            })
            ->join('members AS user1', function ($join) {
                $join->on('user1.id', '=', 'mail_boxes.user1_id')
                    ->where('user1.is_pause', 0)
                    ->where('user1.is_canceled', 0);
            })
            ->join('members AS user2', function ($join) {
                $join->on('user2.id', '=', 'mail_boxes.user2_id')
                    ->where('user2.is_pause', 0)
                    ->where('user2.is_canceled', 0);
            })
            ->where($conditions)
            ->where('has_ng_word', 1);
        if (!is_null($keyword)) {
            $items->where('messages.content', 'LIKE', "%$keyword%");
        }
        return $items->orderBy('messages.created_at', 'DESC')
                       ->paginate(PAGINATE_DEFAULT);
    }

    public function getListByMailBoxId($mailBoxId, $isPaging = true, $conditions = [])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('user')
                        ->whereHas('user', function ($query) {
                            $query->where('is_pause', 0)
                                  ->where('is_canceled', 0);
                        })
                        ->where('mailbox_id', $mailBoxId);
        if (!empty($conditions) && is_array($conditions)) {
            $model = $model->where($conditions);
        }
        $model = $model->orderBy('id', 'ASC');
        if ($isPaging) {
            $results = $model->paginate(PAGINATE_DEFAULT);
            $results->appends(app('request')->query());
        } else {
            $results = $model->get();
        }
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getTotalNgWordByMember($memberId)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
                        ->whereHas('user', function ($query) {
                            $query->where('is_pause', 0)
                                  ->where('is_canceled', 0);
                        })
                        ->where([
                            ['user_no', '=', $memberId],
                            ['total_ng_words', '>', 0]
                        ])
                        ->sum('total_ng_words');
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getMessageLatest($listMailBoxId, $ownerId, $memberId)
    {
        $subQuery = "(SELECT COUNT(*) FROM messages as m WHERE m.user_no = $memberId AND m.mailbox_id = messages.mailbox_id AND m.viewed = 0 AND m.created_at <= messages.created_at GROUP BY m.user_no ORDER BY m.created_at DESC LIMIT 1) AS total";
        return DB::table('messages')
            ->selectRaw("messages.id,messages.mailbox_id,messages.user_no AS user_id,messages.content,messages.image,messages.created_at,messages.updated_at,messages.viewed, members.nickname, members.email, $subQuery")
            ->join('members', 'members.id', '=', 'messages.user_no')
            ->whereIn('messages.mailbox_id', $listMailBoxId)
            ->whereIn('messages.user_no', [$memberId, $ownerId])
            ->where(function ($query) {
                $query->where('members.is_pause', 0)
                      ->where('members.is_canceled', 0);
            })
            ->orderBy('messages.created_at', 'DESC')
            ->first();
    }

    public function deleteMessage($mailBoxId)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where('mailbox_id', $mailBoxId)->delete();
        $this->resetModel();
        return $this->parserResult($model);
    }
}
