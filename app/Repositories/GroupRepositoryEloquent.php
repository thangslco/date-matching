<?php

namespace App\Repositories;

use App\Models\Group;
use App\Repositories\Contracts\GroupRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class GroupRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class GroupRepositoryEloquent extends BaseRepository implements GroupRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Group::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getList($conditions = [])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('image')->where($conditions);
        $results = $model->paginate(PAGINATE_DEFAULT);
        $results->appends(app('request')->query());
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getAll()
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('image')->get();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function updateTotalJoined($groupJoined)
    {
        foreach ($groupJoined as $groupId) {
            $group = $this->firstByConditions(['id' => $groupId]);
            if ($group) {
                $total = $group->joined + 1;
                $this->forcedUpdate(['joined' => $total], ['id' => $groupId]);
            }
        }
    }

    /*
    $type = true: joined, false: not_join
    */
    public function getListGroups($conditions = [], $type = false)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('image');
        if ($type) {
            $model = $model->whereIn('id', $conditions);
        } else {
            $model = $model->whereNotIn('id', $conditions);
        }
        $results = $model->get();
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function detail($id)
    {
        $this->applyCriteria();
        $this->applyScope();
        $results = $this->model->with('image')
                        ->where('id', $id)
                        ->first();
        $this->resetModel();
        return $this->parserResult($results);
    }
}
