<?php

namespace App\Repositories;

use App\Models\Member;
use App\Repositories\Contracts\MemberRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class MemberRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MemberRepositoryEloquent extends BaseRepository implements MemberRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Member::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function login(array $conditions, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with(['images', 'categories', 'tags'])
                        ->where($conditions)->first($columns);
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getListByStatus($conditions, $columns = ['*'], $pageName = 'page', $page = null)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where($conditions);
        $results = $model->paginate(PAGINATE_DEFAULT, $columns, $pageName, $page);
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getMemberSortedByDateVip($listUsersLiked, $onlyDateVip = false)
    {
        $this->applyCriteria();
        $this->applyScope();
        if ($onlyDateVip) {
            $model = $this->model->whereIn('id', $listUsersLiked)
            ->whereNotNull('date_vip')
            ->where('date_vip', '>=', date(DATETIME_FORMAT));
        } else {
            $model = $this->model->whereIn('id', $listUsersLiked);
        }
        $results = $model->orderBy('date_vip', 'DESC')->get(['id']);
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function findMember($params)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with(['images', 'categories', 'tags']);
        foreach ($params as $key => $value) {
            if (empty($value)) {
                continue;
            }
            if (in_array($key, ['appearances', 'impressives', 'human_forms']) && is_array($value)) {
                $model = $model->whereIn($key, $value);
            } elseif ($key == 'liked' && is_array($value) && !empty($value)) {
                $model = $model->whereIn('id', $value);
                $model = $model->orderByRaw(DB::raw("FIELD(id, " . implode(',', $value) . " )"));
            } elseif (($key == 'member_blocked' || $key == 'member_hided') && is_array($value)) {
                $model = $model->whereNotIn('id', $value);
            } elseif ($key == 'address') {
                $model = $model->where($key, 'LIKE', "%{$value}%");
            } elseif ($key == 'age') {
                $year = now()->subYears($value)->format('Y');
                $model = $model->whereYear('birthday', $year);
            } else {
                $model = $model->where($key, $value);
            }
        }
        $model = $model->get();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getListByGender($conditions)
    {
        $this->applyCriteria();
        $this->applyScope();
        $where = $whereIn = $whereLike = $whereNotIn = $whereLikePercent = $whereBetween = [];
        foreach ($conditions as $key => $item) {
            if ($item[1] == '=' || $item[1] == '>=' || $item[1] == '<=') {
                $where[] = $item;
            } elseif ($item[1] == 'IN') {
                $whereIn[$key] = $item;
            } elseif ($item[1] == 'NOT-IN') {
                $whereNotIn[$key] = $item;
            } elseif ($item[1] == 'LIKE') {
                $whereLike[] = $item;
            } elseif ($item[1] == 'LIKE%%') {
                $whereLikePercent[] = $item;
            } elseif ($item[1] == 'BETWEEN') {
                $whereBetween[] = $item;
            }
        }
        $model = $this->model->with(['images', 'categories', 'tags']);
        if (!empty($where)) {
            $model = $model->where($where);
        }
        if (!empty($whereIn)) {
            foreach ($whereIn as $item) {
                if (is_array($item[2])) {
                    $model = $model->whereIn($item[0], $item[2]);
                }
            }
        }
        if (!empty($whereNotIn)) {
            foreach ($whereNotIn as $item) {
                if (is_array($item[2])) {
                    $model = $model->whereNotIn($item[0], $item[2]);
                }
            }
        }
        if (!empty($whereLike)) {
            foreach ($whereLike as $item) {
                $model = $model->where($item[0], $item[1], $item[2]);
            }
        }
        if (!empty($whereBetween)) {
            foreach ($whereBetween as $item) {
                $model = $model->whereBetween($item[0], $item[2]);
            }
        }
        if (!empty($whereLikePercent)) {
            foreach ($whereLikePercent as $item) {
                if (is_array($item[2])) {
                    $model = $model->where(function ($query) use ($item) {
                        foreach ($item[2] as $element) {
                            $query->orWhere($item[0], 'LIKE', "%$element%");
                        }
                    });
                } else {
                    $model = $model->where($item[0], 'LIKE', "%$item[2]%");
                }
            }
        }
        $model = $model->orderBy('item_rocket_date')
                       ->orderBy('id', 'DESC')
                       ->get();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function detail($id)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with(['images', 'imagesSecondary', 'categories', 'tags', 'favorites'])
            ->where('id', $id)
            ->where(function ($query) {
                $query->where('is_pause', 0)
                      ->where('is_canceled', 0);
            })
            ->first();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function statisticByAge($startDate, $endDate)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
            ->where([
                ['birthday', '>=', $startDate],
                ['birthday', '<=', $endDate],
            ])
            ->count();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function statisticByGender($gender)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model
            ->where('gender', $gender)
            ->count();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function statisticByLike($gender)
    {
        return DB::table('members')
        ->join('likes', 'likes.id_to', '=', 'members.id')
        ->where('members.gender', $gender)
        ->select('likes.id')
        ->count();
    }

    public function getListPaused()
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where('is_pause', 1);
        $results = $model->paginate(PAGINATE_DEFAULT);
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function statisticMemberPaused()
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where('is_pause', 1)
                            ->whereNotNull('type_pause')
                            ->where('type_pause', '!=', '');
        $model->selectRaw('type_pause, COUNT(id) AS total');
        $model->groupBy('type_pause');
        $results = $model->get();
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getListCanceled()
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where('is_canceled', 1);
        $results = $model->paginate(PAGINATE_DEFAULT);
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function statisticMemberCanceled()
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where('is_canceled', 1)
                            ->whereNotNull('type_canceled')
                            ->where('type_canceled', '!=', '');
        $model->selectRaw('type_canceled, COUNT(id) AS total');
        $model->groupBy('type_canceled');
        $results = $model->get();
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function findMemberByEmailPhone($params)
    {
        $email = $params['email'] ?? null;
        $phone = $params['phone'] ?? null;
        $appleUserHash = $params['apple_user_hash'] ?? null;
        if (is_null($email) && is_null($phone) && is_null($appleUserHash)) {
            return false;
        }
        $this->applyCriteria();
        $this->applyScope();
        $results = $this->model;
        if (!is_null($email)) {
            $results = $results->where('email', $email);
        } elseif (!is_null($appleUserHash)) {
            $results = $results->where('apple_user_hash', $appleUserHash);
        } elseif (!is_null($phone)) {
            $results = $results->where('phone', $phone);
        }
        $results = $results->first();
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getMembersByGroupId($groupId, $gender) {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->with('images')->where([
            'is_canceled' => 0,
            'is_pause' => 0,
            'is_block' => 0,
            'gender' => !$gender
        ])
        ->where('groups_join', 'LIKE', "%,$groupId,%");
        $results = $model->get();
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function listUsersByOption($listAppearances, $listImpressives)
    {
        $firstAppearances = array_shift($listAppearances);
        $firstImpressives = array_shift($listImpressives);
        if (empty($firstAppearances) || empty($firstImpressives)) {
            return [null, null];
        }
        $limit = 4;
        $listQueryAppearances = $this->model->with(['images', 'categories', 'tags'])
            ->whereNull('deleted_at')
            ->whereRaw("FIND_IN_SET('" . $firstAppearances . "', appearances)")
            ->limit($limit);
        foreach ($listAppearances as $itemAppearances) {
            $subQuery = $this->model->with(['images', 'categories', 'tags'])
                ->whereNull('deleted_at')
                ->whereRaw(DB::raw("FIND_IN_SET('" . $itemAppearances . "', appearances )"))
                ->limit($limit);
            $listQueryAppearances->unionAll($subQuery);
        }
        $listQueryImpressives = $this->model->with(['images', 'categories', 'tags'])
            ->whereNull('deleted_at')
            ->whereRaw("FIND_IN_SET('" . $firstImpressives . "', impressives)")
            ->limit($limit);
        foreach ($listImpressives as $itemImpressives) {
            $subQuery = $this->model->with(['images', 'categories', 'tags'])
            ->whereNull('deleted_at')
            ->whereRaw(DB::raw("FIND_IN_SET('" . $itemImpressives . "', impressives )"))
            ->limit($limit);
            $listQueryImpressives->unionAll($subQuery);
        }
        $impressives = $listQueryImpressives->get();
        $appearances = $listQueryAppearances->get();
        return [$appearances, $impressives];
    }

    public function findUserVip()
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where([
            'is_canceled' => 0,
            'is_pause' => 0,
            'is_block' => 0
        ])
        ->whereNotNull('date_vip')
        ->where('date_vip', '>=', date('Y-m-d H:i:s'));
        $results = $model->get('id');
        $this->resetModel();
        return $this->parserResult($results);
    }

    public function getListByConditions($conditions)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where([
            'is_canceled' => 0,
            'is_pause' => 0,
            'is_block' => 0
        ]);
        $where = $whereIn = $whereNotIn = $whereLike = $whereLikePercent = [];
        foreach ($conditions as $key => $item) {
            if ($item[1] == '=' || $item[1] == '>=' || $item[1] == '<=') {
                $where[] = $item;
            } elseif ($item[1] == 'IN') {
                $whereIn[$key] = $item;
            } elseif ($item[1] == 'NOT-IN') {
                $whereNotIn[$key] = $item;
            } elseif ($item[1] == 'LIKE') {
                $whereLike[] = $item;
            } elseif ($item[1] == 'LIKE%%') {
                $whereLikePercent[] = $item;
            }
        }
        if (!empty($where)) {
            $model = $model->where($where);
        }
        if (!empty($whereIn)) {
            foreach ($whereIn as $item) {
                if (is_array($item[2])) {
                    $model = $model->whereIn($item[0], $item[2]);
                }
            }
        }
        if (!empty($whereNotIn)) {
            foreach ($whereNotIn as $item) {
                if (is_array($item[2])) {
                    $model = $model->whereNotIn($item[0], $item[2]);
                }
            }
        }
        if (!empty($whereLike)) {
            foreach ($whereLike as $item) {
                $model = $model->where($item[0], $item[1], $item[2]);
            }
        }
        if (!empty($whereLikePercent)) {
            foreach ($whereLikePercent as $item) {
                if (is_array($item[2])) {
                    $model = $model->where(function ($query) use ($item) {
                        foreach ($item[2] as $element) {
                            $query->orWhere($item[0], 'LIKE', "%$element%");
                        }
                    });
                } else {
                    $model = $model->where($item[0], 'LIKE', "%$item[2]%");
                }
            }
        }
        $results = $model->paginate();
        $this->resetModel();
        return $this->parserResult($results);
    }
}
