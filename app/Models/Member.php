<?php

namespace App\Models;

use App\Helpers\Common;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

/**
 * Class Member.
 *
 * @package namespace App\Models;
 */
class Member extends BaseModel
{
    use SoftDeletes;
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname', 'email', 'birthday', 'address', 'notes', 'password', 'avatar_id', 'phone',
        'gender', 'status', 'created_at', 'updated_at', 'deleted_at', 'place_of_birth', 'education',
        'height', 'human_forms', 'sake', 'smoke', 'blood_type', 'thing_of_marriage', 'annual_income',
        'job_type', 'hair', 'hair_color', 'option_1', 'option_2', 'option_3', 'option_4', 'option_5',
        'appearances', 'impressives', 'last_login', 'is_block', 'groups_join', 'image_verify_id', 'type',
        'date_vip', 'date_paid', 'is_pause', 'reason_pause', 'type_pause', 'is_canceled', 'reason_canceled',
        'type_canceled', 'canceled_at', 'item_rocket_date', 'apple_user_hash'
    ];
    protected $hidden = ['password'];
    protected $dates = ['birthday', 'last_login', 'created_at', 'updated_at', 'deleted_at', 'date_vip', 'date_paid', 'canceled_at', 'item_rocket_date'];

    public function avatar()
    {
        return $this->belongsTo('App\Models\Image', 'avatar_id', 'id');
    }

    public function imageVerify()
    {
        return $this->belongsTo('App\Models\Image', 'image_verify_id', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'target_id', 'id')->where('images.type', '=', IMAGE_TYPE_MEMBER);
    }

    public function memberHided()
    {
        return $this->hasMany('App\Models\MemberHide', 'user_id', 'id');
    }

    public function imagesSecondary()
    {
        return $this->hasMany('App\Models\Image', 'target_id', 'id')->where('images.type', '=', IMAGE_TYPE_MEMBER_SECONDARY);
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'member_tag', 'member_id', 'tag_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'member_category', 'member_id', 'category_id');
    }

    public function favorites()
    {
        return $this->belongsToMany('App\Models\Favorite', 'member_favorite', 'member_id', 'favorite_id');
    }

    public function getStatusText($isHtml = true)
    {
        $statusText = '';
        switch ($this->status) {
            case 1:
                $statusText = STATUS_APROVED_LABEL;
                break;
            case 2:
                $statusText = STATUS_REJECTED_LABEL;
                break;
            default:
                $statusText = STATUS_PENDING_LABEL;
                break;
        }
        return $statusText;
    }

    public function getStatusBackground()
    {
        switch ($this->status) {
            case 1:
                return 'bg-white';
                break;
            case 2:
                return 'bg-gray';
                break;
            default:
                return 'bg-blue';
                break;
        }
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getAvatarUrl($isDefault = false)
    {
        if (!is_null($this->avatar)) {
            return $this->avatar->getUrl();
        }
        if ($isDefault) {
            return Common::noImage();
        }
        return null;
    }

    public function getImageVerifyUrl($isDefault = false)
    {
        if (!is_null($this->imageVerify)) {
            return $this->imageVerify->getUrl();
        }
        if ($isDefault) {
            return Common::noImage();
        }
        return null;
    }

    public function getHumanForms($api = false)
    {
        $items = [];
        if (!empty($this->human_forms)) {
            $humanForms = trim($this->human_forms, ',');
            $items = explode(',', $humanForms);
        }
        $data = [];
        foreach ($items as $value) {
            if (array_key_exists($value, LIST_HUMAN_FORM)) {
                if ($api) {
                    $data[] = [
                        'index' => $value,
                        'title' => LIST_HUMAN_FORM[$value]
                    ];
                } else {
                    $data[$value] = LIST_HUMAN_FORM[$value];
                }
            }
        }
        return $data;
    }

    public function getAppearances($api = false)
    {
        $items = [];
        if (!empty($this->appearances)) {
            $appearances = trim($this->appearances, ',');
            $items = explode(',', $appearances);
        }
        $data = [];
        $listAppearances = LIST_APPEARANCE_MALE;
        if ($this->gender != 1) {
            $listAppearances = LIST_APPEARANCE_FEMALE;
        }
        foreach ($items as $value) {
            if (array_key_exists($value, $listAppearances)) {
                if ($api) {
                    $data[] = [
                        'index' => $value,
                        'title' => $listAppearances[$value]
                    ];
                } else {
                    $data[$value] = $listAppearances[$value];
                }
            }
        }
        return $data;
    }

    public function getImpressives($api = false)
    {
        $items = [];
        if (!empty($this->impressives)) {
            $impressives = trim($this->impressives, ',');
            $items = explode(',', $impressives);
        }
        $data = [];
        $listImpressives = LIST_IMPRESSIVE_MALE;
        if ($this->gender != 1) {
            $listImpressives = LIST_IMPRESSIVE_FEMALE;
        }
        foreach ($items as $value) {
            if (array_key_exists($value, $listImpressives)) {
                if ($api) {
                    $data[] = [
                        'index' => $value,
                        'title' => $listImpressives[$value]
                    ];
                } else {
                    $data[$value] = $listImpressives[$value];
                }
            }
        }
        return $data;
    }

    public function getAge()
    {
        if (!is_null($this->birthday)) {
            return Carbon::parse($this->birthday)->age;
        }
        return 0;
    }

    public function getPhone()
    {
        if (!is_null($this->phone)) {
            return $this->phone;
        }
        return 'None';
    }

    public function getLastLogin($format = 'Y/m/d H:i:s')
    {
        if (!is_null($this->last_login)) {
            return $this->last_login->format($format);
        }
        return '';
    }

    public function getBirthday($format = 'Y/m/d')
    {
        if (!is_null($this->birthday)) {
            return $this->birthday->format($format);
        }
        return '';
    }

    public function getCreatedAt($format = 'Y/m/d H:i:s')
    {
        if (!is_null($this->created_at)) {
            return $this->created_at->format($format);
        }
        return '';
    }

    public function getUpdatedAt($format = 'Y/m/d H:i:s')
    {
        if (!is_null($this->updated_at)) {
            return $this->updated_at->format($format);
        }
        return '';
    }

    public function getDetailUrl()
    {
        if (!is_null($this->gender) && $this->gender == 1) {
            return route('male.detail', $this->id);
        } else {
            return route('female.detail', $this->id);
        }
    }

    public function getJobTypes()
    {
        $data = null;
        if (!empty($this->job_type) && array_key_exists($this->job_type, LIST_JOB_TYPE)) {
            $data = LIST_JOB_TYPE[$this->job_type];
        }
        return $data;
    }

    public function getHeights()
    {
        return $this->height;
    }

    public function getSmokes()
    {
        $data = null;
        if (!empty($this->smoke) && array_key_exists($this->smoke, LIST_SMOKE)) {
            $data[$this->smoke] = LIST_SMOKE[$this->smoke];
        }
        return $data;
    }

    public function getSakes()
    {
        $data = null;
        if (!empty($this->sake) && array_key_exists($this->sake, LIST_SAKE)) {
            $data[$this->sake] = LIST_SAKE[$this->sake];
        }
        return $data;
    }

    public function getOption1()
    {
        $data = null;
        if (!empty($this->option_1) && array_key_exists($this->option_1, LIST_OPTION_1)) {
            $data[$this->option_1] = LIST_OPTION_1[$this->option_1];
        }
        return $data;
    }

    public function getOption2()
    {
        $data = null;
        if (!empty($this->option_2) && array_key_exists($this->option_2, LIST_OPTION_2)) {
            $data[$this->option_2] = LIST_OPTION_2[$this->option_2];
        }
        return $data;
    }

    public function getOption3()
    {
        $data = null;
        if (!empty($this->option_3) && array_key_exists($this->option_3, LIST_OPTION_3)) {
            $data[$this->option_3] = LIST_OPTION_3[$this->option_3];
        }
        return $data;
    }

    public function getOption4()
    {
        $data = null;
        if (!empty($this->option_4) && array_key_exists($this->option_4, LIST_OPTION_4)) {
            $data[$this->option_4] = LIST_OPTION_4[$this->option_4];
        }
        return $data;
    }

    public function getPrefecture()
    {
        $data = null;
        if (!empty($this->place_of_birth) && array_key_exists($this->place_of_birth, LIST_PREFECTURE)) {
            $data[$this->place_of_birth] = LIST_PREFECTURE[$this->place_of_birth];
        }
        return $data;
    }

    public function getEducation()
    {
        $data = null;
        if (!empty($this->education) && array_key_exists($this->education, LIST_EDUCATION)) {
            $data[$this->education] = LIST_EDUCATION[$this->education];
        }
        return $data;
    }

    public function getBloodType()
    {
        $data = null;
        if (!empty($this->blood_type) && array_key_exists($this->blood_type, LIST_BLOOD_TYPE)) {
            $data[$this->blood_type] = LIST_BLOOD_TYPE[$this->blood_type];
        }
        return $data;
    }

    public function getThingOfMarriage()
    {
        $data = null;
        if (!empty($this->thing_of_marriage) && array_key_exists($this->thing_of_marriage, LIST_THING_OF_MARRIAGE)) {
            $data[$this->thing_of_marriage] = LIST_THING_OF_MARRIAGE[$this->thing_of_marriage];
        }
        return $data;
    }

    public function getAnnualIncome()
    {
        $data = null;
        if (!empty($this->annual_income) && array_key_exists($this->annual_income, LIST_ANNUAL_INCOME)) {
            $data[$this->annual_income] = LIST_ANNUAL_INCOME[$this->annual_income];
        }
        return $data;
    }

    public function getHair()
    {
        $data = null;
        if (!empty($this->hair) && array_key_exists($this->hair, LIST_HAIR_MALE)) {
            $data[$this->hair] = LIST_HAIR_MALE[$this->hair];
        }
        return $data;
    }

    public function getHairColor()
    {
        $data = null;
        if (!empty($this->hair_color) && array_key_exists($this->hair_color, LIST_HAIR_COLOR)) {
            $data[$this->hair_color] = LIST_HAIR_COLOR[$this->hair_color];
        }
        return $data;
    }

    public function getGroupsJoin($isDetail = false)
    {
        $items = [];
        if (!empty($this->groups_join)) {
            $groupJoins = trim($this->groups_join, ',');
            $items = explode(',', $groupJoins);
        }
        $data = [];
        foreach ($items as $value) {
            $group = $this->getGroupById($value);
            if (!$group) {
                continue;
            }
            if ($isDetail) {
                $data[] = [
                    'id' => $group['id'],
                    'name' => $group['name'],
                    'image' => $group['image']
                ];
            } else {
                $data[$value] = $group['name'];
            }
        }
        return $data;
    }

    public function getGroupById($id)
    {
        $groups = config('app.groups');
        foreach ($groups as $item) {
            if ($item['id'] == $id) {
                return $item;
            }
        }
        return null;
    }

    public function getDateVip()
    {
        if (!is_null($this->date_vip)) {
            return $this->date_vip->format(DATETIME_FORMAT);
        }
        return null;
    }

    public function getDatePaid()
    {
        if (!is_null($this->date_paid)) {
            return $this->date_paid->format(DATETIME_FORMAT);
        }
        return null;
    }

    public function getMemberUrl()
    {
        if ($this->gender == 1) {
            return route('male.detail', $this->id);
        }
        return route('female.detail', $this->id);
    }

    public function checkDateExpired($date)
    {
        if ($date->greaterThanOrEqualTo(now())) {
            return true;
        }
        return false;
    }

    public function isHasFee(&$date)
    {
        $checkVip = $checkPaid = null;
        if (!is_null($this->date_vip)) {
            $checkVip = $this->checkDateExpired($this->date_vip);
            if ($checkVip) {
                $date = $this->date_vip;
                return true;
            }
        }
        if (!is_null($this->date_paid)) {
            $checkPaid = $this->checkDateExpired($this->date_paid);
            if ($checkPaid) {
                $date = $this->date_paid;
                return true;
            }
        }
        if ($checkPaid === false && $checkVip === false) {
            $date = null;
            return false;
        }
        if (is_null($this->date_paid) && is_null($this->date_vip)) {
            $date = null;
            return false;
        }
        return false;
    }
}
