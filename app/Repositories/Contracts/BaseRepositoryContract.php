<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

interface BaseRepositoryContract extends RepositoryInterface
{
    /**
     * Check exists with given conditions
     *
     * @param array $conditions
     * @param array $orWhere
     * @return mixed
     */
    public function exists(array $conditions, array $orWhere = []);

    /**
     * Find first data with given conditions
     *
     * @param array $conditions
     * @param array $columns
     *
     * @return mixed
     */
    public function firstByConditions(array $conditions, $columns = ['*']);

    /**
     * Forced update function: for only update statement.
     *
     * @param $data
     * @param $conditions
     * @return mixed
     */
    public function forcedUpdate($data, $conditions);

    /**
     * Increase amount of a column
     *
     * @param $where
     * @param $column
     * @param $amount
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function increment($where, $column, $amount);

    /**
     * Update value when given conditions is satisfied
     *
     * @param array $where
     * @param array $value
     * @return mixed
     */
    public function whereUpdate(array $where, array $value);
}
