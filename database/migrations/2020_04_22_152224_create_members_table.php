<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nickname', 150);
            $table->string('email')->unique();
            $table->date('birthday')->nullable();
            $table->string('address', 255)->nullable();
            $table->text('notes')->nullable();
            $table->string('password');
            $table->integer('avatar_id')->nullable();
            $table->string('phone', 30)->nullable();
            $table->tinyInteger('gender')->default(1)->comment('0: female, 1: male');
            $table->integer('status')->default(0)->comment('0: pending, 1: approved, 2: canceled');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
