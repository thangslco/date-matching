<?php

namespace App\Models;

use App\Helpers\Common;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Image.
 *
 * @package namespace App\Models;
 */
class Image extends BaseModel
{
    use SoftDeletes;
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['filename', 'target_id', 'type'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getUrl($isDefault = false)
    {
        if (!is_null($this->filename) && file_exists('storage/avatars/' . $this->filename)) {
            return asset('storage/avatars/' . $this->filename);
        }
        if ($isDefault) {
            return Common::noImage();
        }
        return null;
    }
}
