<?php

namespace App\Providers;

use App\Services\Contracts\UserServiceContract;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register services for app
     *
     * @var array
     */
    protected $services = [
        UserServiceContract::class => UserService::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        die('111');
        foreach ($this->services as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }
}
