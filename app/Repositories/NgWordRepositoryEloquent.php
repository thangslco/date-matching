<?php

namespace App\Repositories;

use App\Models\NgWord;
use App\Repositories\Contracts\NgWordRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class NgWordRepositoryEloquent
 *
 * @package namespace App\Repositories;
 */
class NgWordRepositoryEloquent extends BaseRepository implements NgWordRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NgWord::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
