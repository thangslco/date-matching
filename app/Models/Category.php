<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category.
 *
 * @package namespace App\Models;
 */
class Category extends BaseModel
{
    use SoftDeletes;
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'image_id'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function image()
    {
        return $this->belongsTo('App\Models\Image', 'image_id', 'id');
    }

    public function getImageUrl()
    {
        if (!is_null($this->image_id) && !is_null($this->image)) {
            return $this->image->getUrl();
        }
        return null;
    }
}
