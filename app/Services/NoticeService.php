<?php

namespace App\Services;

use App\Jobs\PushNotificationAndroid;
use App\Jobs\PushNotificationIos;
use App\Repositories\Contracts\NoticeRepository;
use App\Repositories\Contracts\SettingRepository;

class NoticeService extends BaseService
{
    protected $settingRepository;
    /**
     * NoticeService constructor.
     * @param NoticeRepository $repository
     * @param SettingRepository $settingRepository
     */
    public function __construct(
        NoticeRepository $repository,
        SettingRepository $settingRepository
    ) {
        $this->repository = $repository;
        $this->settingRepository = $settingRepository;
    }

    public function register($request)
    {
        $params = $request->only('title', 'content', 'category');
        $dataInsert = $this->create($params);
        $isNotificationOther = $this->settingRepository->getSettingByKey('SETTING_NOTIFICATION_OTHER');
        if ($isNotificationOther && $isNotificationOther->value == 1) {
            $dataNotification = [
                'title' => 'お知らせが届きました。',
                'body' => 'お知らせが届きました。',
                'data_custom' => [
                    'type' => 'OTHER',
                ]
            ];
            // PushNotificationAndroid::dispatch($dataNotification);
            PushNotificationIos::dispatch($dataNotification);
        }
        return $dataInsert;
    }

    public function getList($request, $member = null)
    {
        $conditions = [];
        $keyword = $request->get('keyword');
        if ($keyword) {
            $conditions[] = ['title', 'LIKE', "%{$keyword}%"];
        }
        if (!is_null($member)) {
            $conditions[] = ['created_at', '>=', $member->created_at->format(DATETIME_FORMAT)];
        }
        return $this->repository->getList($conditions);
    }
}
