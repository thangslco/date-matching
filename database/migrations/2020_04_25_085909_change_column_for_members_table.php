<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeColumnForMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE members MODIFY place_of_birth INT;');
        DB::statement('ALTER TABLE members MODIFY education INT;');
        DB::statement('ALTER TABLE members MODIFY height INT;');
        DB::statement('ALTER TABLE members MODIFY human_form INT;');
        DB::statement('ALTER TABLE members MODIFY sake INT;');
        DB::statement('ALTER TABLE members MODIFY smoke INT;');
        DB::statement('ALTER TABLE members MODIFY blood_type INT;');
        DB::statement('ALTER TABLE members MODIFY thing_of_marriage INT;');
        DB::statement('ALTER TABLE members MODIFY annual_income INT;');
        DB::statement('ALTER TABLE members MODIFY job_type INT;');
        DB::statement('ALTER TABLE members MODIFY hair INT;');
        DB::statement('ALTER TABLE members MODIFY hair_color INT;');
        DB::statement('ALTER TABLE members MODIFY option_1 INT;');
        DB::statement('ALTER TABLE members MODIFY option_2 INT;');
        DB::statement('ALTER TABLE members MODIFY option_3 INT;');
        DB::statement('ALTER TABLE members MODIFY option_4 INT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
