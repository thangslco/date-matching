<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\UserService;

class UserController extends Controller
{
    protected $userService;
    public function __construct(UserService $service)
    {
        parent::__construct();
        $this->userService = $service;
    }
}
