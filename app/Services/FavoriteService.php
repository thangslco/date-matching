<?php

namespace App\Services;

use App\Repositories\Contracts\FavoriteRepository;
use Illuminate\Support\Facades\DB;

class FavoriteService extends BaseService
{
    /**
     * FavoriteService constructor.
     * @param FavoriteRepository $repository
     */
    public function __construct(
        FavoriteRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function getListByType($request, $type, $pageName = 'page')
    {
        $keyword = $request->get('keyword');
        $page = $request->get($pageName);
        $conditions = [
            ['type', '=', $type]
        ];
        if ($keyword) {
            $conditions[] = ['name', 'LIKE', "%{$keyword}%"];
        }
        return $this->repository->getListByType($conditions, ['*'], $pageName, $page);
    }

    public function register($request)
    {
        $params = $request->only('name', 'type');
        return $this->create($params);
    }

    public function updateFavorite($request, $favorite)
    {
        $params = $request->only('name', 'type');
        DB::beginTransaction();
        try {
            $result = $this->update($params, $favorite->id);
            DB::commit();
            return $result;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function getAll()
    {
        $results = $this->repository->getAll();
        $data = [];
        foreach ($results as $item) {
            $itemData = $item->toArray();
            $itemData['color'] = $item->getColor();
            $data[] = $itemData;
        }
        return $data;
    }

}
