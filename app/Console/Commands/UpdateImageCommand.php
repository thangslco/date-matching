<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateImageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_image {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update image for all';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $listImagesName = [];
        for ($i = 1; $i < 29; $i++) {
            $listImagesName[] = "$i.jpeg";
        }
        $type = $this->argument('type');
        if ($type == 'member') {
            $this->generateImageForMember($listImagesName);
        } elseif ($type == 'image_veriry') {
            $this->generateImageForVerify($listImagesName);
        }
        
    }

    private function generateImageForVerify($listImagesName) {
        $listMembers = DB::table('members')->select('id', 'image_verify_id')->whereNull('image_verify_id')->get();
        if ($listMembers->count()) {
            foreach ($listMembers as $item) {
                $image = $listImagesName[rand(0, 27)];
                $fileName = time() . '-' . $image;
                $sourcePath = public_path('img_tmp/' . $image);
                $targetPath = storage_path('app/public/avatars/' . $fileName);
                $result = File::copy($sourcePath, $targetPath);
                if ($result) {
                    $imageId = DB::table('images')->insertGetId([
                        'filename' => $fileName,
                        'created_at' => date(DATETIME_FORMAT),
                        'updated_at' => date(DATETIME_FORMAT)
                    ]);
                    $dataUpdate['image_verify_id'] = !is_null($imageId) ? $imageId : null;
                    DB::table('members')->where('id', $item->id)->update($dataUpdate);
                    \Log::info("Update memberID: " . $item->id . ' Image: ' . $targetPath . ' Image verify ID: ' . $imageId);
                }
            }
        }
        $this->info('Generated image for member');
    }

    private function generateImageForMember($listImagesName) {
        $listMembers = DB::table('members')->select('id', 'avatar_id')->where('avatar_id', 0)->get();
        if ($listMembers->count()) {
            foreach ($listMembers as $item) {
                $image = $listImagesName[rand(0, 27)];
                $fileName = time() . '-' . $image;
                $sourcePath = public_path('img_tmp/' . $image);
                $targetPath = storage_path('app/public/avatars/' . $fileName);
                $result = File::copy($sourcePath, $targetPath);
                if ($result) {
                    $imageId = DB::table('images')->insertGetId([
                        'filename' => $fileName,
                        'created_at' => date(DATETIME_FORMAT),
                        'updated_at' => date(DATETIME_FORMAT)
                    ]);
                    $dataUpdate['avatar_id'] = !is_null($imageId) ? $imageId : null;
                    DB::table('members')->where('id', $item->id)->update($dataUpdate);
                    \Log::info("Update memberID: " . $item->id . ' Image: ' . $targetPath . ' Avatar_id: ' . $imageId);
                }
            }
        }
        $this->info('Generated image for member');
    }
}
