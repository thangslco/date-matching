<?php

namespace App\Http\Requests;


class AddItemRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $listKeyItem = array_keys(LIST_TYPE_ITEMS);
        return [
            'item_id' => 'required|in:' . implode(',', $listKeyItem),
            'quantity' => 'required|numeric'
        ];
    }
}
