<?php

namespace App\Services;

use App\Http\Requests\LoginPost;
use App\Repositories\Contracts\UserRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserService extends BaseService
{
    /**
     * UserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(
        UserRepository $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * User login
     *
     * @param LoginPost $request
     * @return Response
     */
    public function login(LoginPost $request)
    {
        $rememberToken = $request->get('remember');
        $data = $request->only('email', 'password');
        if (Auth::guard(GUARD_ADMIN)->attempt($data, $rememberToken)) {
            $user = Auth::guard(GUARD_ADMIN)->user();
            $this->repository->forcedUpdate(['login_at' => date(DATETIME_FORMAT)], ['id' => $user->id]);
            return true;
        }
        return false;
    }

    /**
     * User logout
     *
     * @return Response
     */
    public function logout()
    {
        DB::beginTransaction();
        try {
            $user = Auth::guard(GUARD_ADMIN)->user();
            $this->repository->forcedUpdate(['logout_at' => date(DATETIME_FORMAT)], ['id' => $user->id]);
            Auth::guard(GUARD_ADMIN)->logout();
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function getList($request)
    {
        $keyword = $request->get('keyword');
        $page = $request->get('page');
        $conditions = [];
        if ($keyword) {
            $conditions[] = ['email', 'LIKE', "%{$keyword}%"];
        }
        $data = $this->repository->findWhere($conditions);
        return $this->repository->paging($data, $page, PAGINATE_DEFAULT);
    }
}
