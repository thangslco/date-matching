<?php

namespace App\Models;

/**
 * Class Favorite
 *
 * @package namespace App\Models;
 */
class Favorite extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'type'];

    protected $dates = ['created_at', 'updated_at'];

    public function getType()
    {
        if (!is_null($this->type) && isset(LIST_TYPE_FAVORITES[$this->type])) {
            return LIST_TYPE_FAVORITES[$this->type];
        }
        return null;
    }

    public function getColor()
    {
        switch ($this->type) {
            case TYPE_FAVORITE_PINK:
                return '#FFC0CB';
                break;
            case TYPE_FAVORITE_BLUE:
                return '#0000FF';
                break;
            case TYPE_FAVORITE_ORANGE:
                return '#FFA500';
                break;
            case TYPE_FAVORITE_GREEN:
                return '#008000';
                break;
            case TYPE_FAVORITE_RED:
                return '#FF0000';
                break;
            default:
                return null;
                break;
        }
    }

}
