<?php

namespace App\Repositories;

use App\Models\UserToken;
use App\Repositories\Contracts\UserTokenRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class UserTokenRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserTokenRepositoryEloquent extends BaseRepository implements UserTokenRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserToken::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getTokenByUserId($userId, $type)
    {
        $this->applyCriteria();
        $this->applyScope();
        if ($type == 'android') {
            $model = $this->model->where('token', '!=', null);
        } elseif ($type == 'ios') {
            $model = $this->model->where('apns_id', '!=', null);
        }
        if (is_array($userId)) {
            $model->whereIn('user_id', $userId);
        } else {
            $model->where('user_id', $userId);
        }
        $model = $model->whereHas('member', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->select('token', 'apns_id', 'id')
        ->orderBy('id')
        ->get();
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getDataForAndroid($limit, $lastId)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->whereHas('member', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where('token', '!=', null)
        ->where('id', '>=', $lastId)
        ->select('token', 'id')
        ->orderBy('id')
        ->limit($limit);
        $this->resetModel();
        return $this->parserResult($model);
    }

    public function getDataForIos($limit, $lastId)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->whereHas('member', function ($query) {
            $query->where('is_pause', 0)
                  ->where('is_canceled', 0);
        })
        ->where('apns_id', '!=', null)
        ->where('id', '>=', $lastId)
        ->select('apns_id', 'id')
        ->limit($limit);
        $this->resetModel();
        return $this->parserResult($model);
    }
}
