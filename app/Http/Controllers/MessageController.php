<?php

namespace App\Http\Controllers;

use App\Services\MessageService;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    protected $messageService;
    public function __construct(MessageService $service)
    {
        parent::__construct();
        $this->messageService = $service;
    }

    public function index(Request $request)
    {
        $menuActived = 'MENU_MESSAGE';
        $keyword = $request->get('keyword');
        if (!is_null($keyword)) {
            $items = $this->messageService->getList($request, $keyword);
        } else {
            $items = $this->messageService->getList($request);
        }
        return view('message.list', compact('items', 'menuActived', 'keyword'));
    }

    public function listMessageNgWord(Request $request)
    {
        $menuActived = 'MENU_MESSAGE';
        $keyword = $request->get('keyword');
        $items = $this->messageService->getListNgWord($request, $keyword);
        return view('message.list-ng-word', compact('items', 'menuActived', 'keyword'));
    }
    
    public function detail(Request $request, $mailBoxId)
    {
        $menuActived = 'MENU_MESSAGE';
        $items = $this->messageService->getListByMailBoxId($mailBoxId);
        return view('message.detail', compact('items', 'menuActived'));
    }

    public function delete(Request $request)
    {
        try {
            $id = $request->get('id');
            if (!$id) {
                return response()->json(['status' => false, 'message' => __('messages.invalid_param')]);
            }
            $result = $this->messageService->deleteMessage($id);
            if ($result) {
                return response()->json(['status' => true, 'message' => __('messages.delete_message_success')]);
            }
            return response()->json(['status' => false, 'message' => __('messages.delete_message_fail')]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }

    public function deleteForce(Request $request)
    {
        try {
            $id = $request->get('id');
            if (!$id) {
                return response()->json(['status' => false, 'message' => __('messages.invalid_param')]);
            }
            $result = $this->messageService->forceDeleteMessage($id);
            if ($result) {
                return response()->json(['status' => true, 'message' => __('messages.delete_message_success')]);
            }
            return response()->json(['status' => false, 'message' => __('messages.delete_message_fail')]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }

    public function rollback(Request $request)
    {
        try {
            $id = $request->get('id');
            if (!$id) {
                return response()->json(['status' => false, 'message' => __('messages.invalid_param')]);
            }
            $result = $this->messageService->forcedUpdate(['has_ng_word' => 0], ['id' => $id]);
            if ($result) {
                return response()->json(['status' => true, 'message' => __('messages.rollback_message_success')]);
            }
            return response()->json(['status' => false, 'message' => __('messages.rollback_message_fail')]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }
}
