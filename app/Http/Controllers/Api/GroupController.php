<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\GroupService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GroupController extends Controller
{
    protected $groupService;
    public function __construct(GroupService $service)
    {
        parent::__construct();
        $this->groupService = $service;
    }

    /**
     * Get all groups
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function all(Request $request)
    {
        try {
            $data = $this->groupService->getAll();
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API get all groups: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Get all groups for a member
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listGroups(Request $request)
    {
        try {
            $user = request()->user;
            $data = $this->groupService->getListGroups($request, $user);
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API get list groups by member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API detail group
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function detail(Request $request, $id)
    {
        try {
            $result = $this->groupService->detail($id);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API detail group: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }
}
