<?php

namespace App\Models;


/**
 * Class Report
 *
 * @package namespace App\Models;
 */
class Report extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_report_id',
        'user_reported_id',
        'type',
        'content',
        'position_id',
        'report_number'
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function userReport()
    {
        return $this->belongsTo('App\Models\Member', 'user_report_id', 'id');
    }

    public function userReported()
    {
        return $this->belongsTo('App\Models\Member', 'user_reported_id', 'id');
    }
}
