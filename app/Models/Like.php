<?php

namespace App\Models;

/**
 * Class Like
 *
 * @package namespace App\Models;
 */
class Like extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_from', 'id_to', 'message', 'is_viewed', 'is_ngword', 'date_matching', 'id_matching'];

    protected $dates = ['created_at', 'updated_at', 'date_matching'];

    public function userFrom()
    {
        return $this->belongsTo('App\Models\Member', 'id_from', 'id');
    }

    public function userTo()
    {
        return $this->belongsTo('App\Models\Member', 'id_to', 'id');
    }
}
