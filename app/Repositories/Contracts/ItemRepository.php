<?php

namespace App\Repositories\Contracts;

/**
 * Interface ItemRepository.
 *
 * @package namespace App\Repositories;
 */
interface ItemRepository extends BaseRepositoryContract
{
}
