<?php

namespace App\Models;

/**
 * Class Item
 *
 * @package namespace App\Models;
 */
class Item extends BaseModel
{
    /**
     * The primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['member_id', 'total', 'type'];

    protected $dates = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'id');
    }
}
