<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Response as HttpStatusCode;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        $isJson = $request->header('Accept');
        if (config('app.debug') && $isJson != 'application/json') {
            return parent::render($request, $exception);
        }

        if ($exception instanceof UnauthorizedHttpException) {
            return response()->json(['error' => $exception->getMessage()], $exception->getStatusCode());
        }
        /**
         * ValidationException: invalid body params
         * FatalThrowableError: invalid url params
         */
        if ($exception instanceof ValidationException || $exception instanceof FatalThrowableError) {
            return response()->jsonValidationError($exception->getMessage());
        }
        return response()->json(
            [
                'code' => HttpStatusCode::HTTP_BAD_REQUEST,
                'errors' => config('app.debug') ? $exception->getMessage() : 'Bad request!',
            ],
            HttpStatusCode::HTTP_BAD_REQUEST
        );
    }
}
