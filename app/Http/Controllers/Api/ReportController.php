<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReportMemberApiRequest;
use App\Services\ReportService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class ReportController extends Controller
{
    protected $reportService;

    public function __construct(ReportService $service)
    {
        parent::__construct();
        $this->reportService = $service;
    }

    /**
     * API report member
     *
     * @param ReportMemberApiRequest $request
     * @return JsonResponse
     */
    public function report(ReportMemberApiRequest $request)
    {
        try {
            $reportId = $request->get('user_reported_id');
            $memberReport = request()->user;
            $checkReport = $this->reportService->firstByConditions(['user_report_id' => $memberReport->id, 'user_reported_id' => $reportId]);
            if ($checkReport) {
                $this->reportService->update([
                    'report_number' => $checkReport->report_number + 1
                ], $checkReport->id);
                $data = $this->reportService->firstByConditions([
                    'id' => $checkReport->id
                ]);
            } else {
                $params = $request->only(['user_reported_id', 'type', 'content']);
                $params['position_id'] = $request->get('position');
                $params['user_report_id'] = $memberReport->id;
                $params['report_number'] = 1;
                $data = $this->reportService->create($params);
            }
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API report member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }
}
