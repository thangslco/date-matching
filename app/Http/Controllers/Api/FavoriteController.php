<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\FavoriteService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FavoriteController extends Controller
{
    protected $favoriteService;
    public function __construct(FavoriteService $service)
    {
        parent::__construct();
        $this->favoriteService = $service;
    }

    /**
     * Get all favorites
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request)
    {
        try {
            $data = $this->favoriteService->getAll();
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API Get all favorites: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }
}
