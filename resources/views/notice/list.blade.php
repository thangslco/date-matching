@extends('layouts.default')

@section('title', 'お知らせ')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">お知らせ</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">お知らせ</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-success">{{ Session::get('error') }}</div>
                @endif
                <div class="card">
                    <div class="card-header row">
                        <div class="col-md-6">
                            <a href="{{route('notices.create')}}" class="btn btn-success">新規作成</a>
                        </div>
                        <div class="col-md-6">
                            <div class="card-tools fa-pull-right">
                                <form action="" method="GET">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="keyword" class="form-control float-right" value="{!! request()->keyword !!}" placeholder="Search...">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap table-bordered">
                            <thead>
                                <tr>
                                    <th>お知らせID</th>
                                    <th>{{__('messages.notices.lbl_title')}}</th>
                                    <th>{{__('messages.notices.lbl_category')}}</th>
                                    <th>{{__('messages.notices.lbl_content')}}</th>
                                    <th>{{__('messages.lbl_created_at')}}</th>
                                    <th>{{__('messages.lbl_updated_at')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item)
                                <tr>
                                    <td><a href="{{ route('notices.detail', $item->id)}}">{{ $item->id }}</a></td>
                                    <td>{!! $item->title !!}</td>
                                    <td>{!! $item->getCategoryName() !!}</td>
                                    <td>{!! $item->getContent(200) !!}</td>
                                    <td>{!! $item->created_at !!}</td>
                                    <td>{!! $item->updated_at !!}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        {!! $items->links() !!}
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('style')
@parent
@endsection
@section('script')
@parent
@endsection