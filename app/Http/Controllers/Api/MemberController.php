<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Common;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlockMemberApiRequest;
use App\Http\Requests\BuyLikeRequest;
use App\Http\Requests\BuyPackRequest;
use App\Http\Requests\FCMTokenRequest;
use App\Http\Requests\FindFriendRequest;
use App\Http\Requests\HideMemberApiRequest;
use App\Http\Requests\HideMemberBlockedApiRequest;
use App\Http\Requests\ImageVerifyRequest;
use App\Http\Requests\LikeMemberRequest;
use App\Http\Requests\LoginPostApi;
use App\Http\Requests\MemberFindApi;
use App\Http\Requests\MemberRegister;
use App\Http\Requests\MemberUpdateApi;
use App\Http\Requests\NextMemberRequest;
use App\Http\Requests\PauseMemberRequest;
use App\Http\Requests\RefreshTokenRequest;
use App\Http\Requests\SaveEmailRequest;
use App\Http\Requests\SavePasswordRequest;
use App\Http\Requests\TranferLikeRequest;
use App\Http\Requests\UnBlockMemberApiRequest;
use App\Http\Requests\UnHideMemberApiRequest;
use App\Http\Requests\UpdateMemberApiRequest;
use App\Models\Member;
use App\Services\ItemService;
use App\Services\LikeService;
use App\Services\MemberService;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MemberController extends Controller
{
    protected $memberService;
    protected $itemService;
    protected $likeService;

    public function __construct(MemberService $service, ItemService $itemService, LikeService $likeService)
    {
        parent::__construct();
        $this->memberService = $service;
        $this->itemService = $itemService;
        $this->likeService = $likeService;
    }

    /**
     * Create member male
     *
     * @param MemberRegister $request
     * @return JsonResponse
     */
    public function maleRegister(MemberRegister $request)
    {
        try {
            // Check and Delete all data of member canceled > 14 days
            $this->memberService->checkMemberCancel($request);
            list($message, $code) = $this->checkNickNameAndOption5($request);
            if (!empty($message) && !empty($code)) {
                return response()->json([
                    'status' => false,
                    'code' => $code,
                    'message' => $message
                ], 400);
            }
            $member = $this->memberService->register($request, 1);
            $this->itemService->create([
                'member_id' => $member->id,
                'type' => TYPE_LIKE_ITEM,
                'total' => LIKE_NUMBER_REGISTER_ACCOUNT
            ]);
            $result = $this->getMemberDetailForLogin($member);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API register male member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Create member female
     *
     * @param MemberRegister $request
     * @return JsonResponse
     */
    public function femaleRegister(MemberRegister $request)
    {
        try {
            list($message, $code) = $this->checkNickNameAndOption5($request);
            if (!empty($message) && !empty($code)) {
                return response()->json([
                    'status' => false,
                    'code' => $code,
                    'message' => $message
                ], 400);
            }
            $member = $this->memberService->register($request, 0);
            $this->itemService->create([
                'member_id' => $member->id,
                'type' => TYPE_LIKE_ITEM,
                'total' => LIKE_NUMBER_REGISTER_ACCOUNT
            ]);
            $result = $this->getMemberDetailForLogin($member);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API register female member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Update member
     *
     * @param MemberUpdateApi $request
     * @return JsonResponse
     */
    // public function update(MemberUpdateApi $request, $id)
    // {
    //     dd($id);
    //     try {
    //         $result = $this->memberService->updateProfile($request, $id);
    //         if ($result) {
    //             return response()->jsonSuccess($result);
    //         }
    //         return response()->jsonError(400, 'Update profile fail!');
    //     } catch (\Exception $e) {
    //         Log::error('========== Exception ==============');
    //         Log::error('API update member: ' . $e->getMessage());
    //         Log::error('===================================');
    //         return response()->jsonError(400, $e->getMessage());
    //     }
    // }

    /**
     * Find member
     *
     * @param MemberFindApi $request
     * @return JsonResponse
     */
    public function find(MemberFindApi $request, $id)
    {
        try {
            $result = $this->memberService->findMember($request, $id, 0);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API find member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * Get user by email, phone
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserByEmailPhone(Request $request)
    {
        try {
            $params = $request->only(['email', 'phone']);
            $params['is_block'] = 0;
            $params['is_pause'] = 0;
            $params['is_canceled'] = 0;
            $data = $this->memberService->findWhere($params);
            if ($data) {
                $count = $data->count();
                if ($count > 1) {
                    return response()->jsonError(400, 'Duplicate data');
                }
                if ($count == 0) {
                    return response()->jsonError(400, 'Not found user');
                }
                $user = $data->first();
                $result = $this->memberService->getInfoMember($user, [], true);
                return response()->jsonSuccess($result);
            }
            return response()->jsonSuccess([]);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API getUserByEmailPhone: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API refreshToken
     *
     * @param RefreshTokenRequest $request
     * @return JsonResponse
     */
    public function refreshToken(RefreshTokenRequest $request)
    {
        $refreshToken = $request->get('refresh_token');
        try {
            $credentials = JWT::decode($refreshToken, env('JWT_SECRET'), ['HS256']);
            $user = $this->memberService->firstByConditions(['id' => $credentials->sub]);
            $expirationTimeToken = now()->addHours(TIME_ACCESS_TOKEN)->timestamp;
            $expirationTimeRefreshToken = now()->addDays(TIME_REFRESH_TOKEN)->timestamp;
            $token = $this->jwt($user, $expirationTimeToken);
            $refreshToken = $this->jwt($user, $expirationTimeRefreshToken);
            $result = [
                'token' => $token,
                'token_expire' => $expirationTimeToken,
                'refresh_token' => $refreshToken,
                'refresh_token_expire' => $expirationTimeRefreshToken,
            ];
            return response()->jsonSuccess($result);
        } catch(ExpiredException $e) {
            return response()->json([
                'error' => 'Provided refresh token is expired.'
            ], 401);
        } catch(\Exception $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 401);
        }
    }

    /**
     * API Login
     *
     * @param LoginPostApi $request
     * @return JsonResponse
     */
    public function login(LoginPostApi $request)
    {
        try {
            $member = $this->memberService->login($request);
            if ($member) {
                $result = $this->getMemberDetailForLogin($member);
                return response()->jsonSuccess($result);
            }
            return response()->jsonError(400, __('messages.user_not_found'));
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API login: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    private function getMemberDetailForLogin($member)
    {
        $expirationTimeToken = now()->addHours(TIME_ACCESS_TOKEN)->timestamp;
        $expirationTimeRefreshToken = now()->addDays(TIME_REFRESH_TOKEN)->timestamp;
        $token = $this->jwt($member, $expirationTimeToken);
        $refreshToken = $this->jwt($member, $expirationTimeRefreshToken);
        $items = $this->itemService->getItemByUserId($member->id);
        $totalLike = 0;
        $totalRose = 0;
        $totalRocket = 0;
        if ($items->count()) {
            foreach ($items as $item) {
                if ($item->type == TYPE_LIKE_ITEM) {
                    $totalLike = $item->total;
                } elseif ($item->type == TYPE_ROSE_ITEM) {
                    $totalRose = $item->total;
                } elseif ($item->type == TYPE_ROCKET_ITEM) {
                    $totalRocket = $item->total;
                }
            }
        }
        $totalLikeItems = 0;
        if (!is_null($member)) {
            $totalLikeItems = $this->itemService->getTotalLikeItemByUser($member->id);
        }

        $totalLiked = $this->likeService->getTotalLikedByUser($member->id);
        $dataAppend = [
            'token' => $token,
            'token_expire' => $expirationTimeToken,
            'refresh_token' => $refreshToken,
            'refresh_token_expire' => $expirationTimeRefreshToken,
            'total_can_like' => $totalLikeItems,
            'total_like' => $totalLike,
            'total_rose' => $totalRose,
            'total_rocket' => $totalRocket,
            'user_id' => str_pad($member->id, 8, '0', STR_PAD_LEFT),
            'total_liked' => $totalLiked,
            'date_vip' => $member->getDateVip(),
            'date_paid' => $member->getDatePaid(),
        ];
        return $this->memberService->getInfoMember($member, $dataAppend, true);
    }

    protected function jwt(Member $user, $timeExpiration = null)
    {
        if (is_null($timeExpiration)) {
            $timeExpiration = now()->addHours(TIME_ACCESS_TOKEN)->timestamp;
        }
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => $timeExpiration // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /**
     * API add token FCM to database
     *
     * @param FCMTokenRequest $request
     * @return JsonResponse
     */
    public function addTokenFCM(FCMTokenRequest $request)
    {
        $data = $request->all();
        $user = request()->user;
        if(array_key_exists('device_type', $data)) {
            $FCMTokenData = [
                'user_id' => $user->id,
                'apns_id' => $data['token']
            ];
            // Add the IOS device token to dbs
            $FCMTokenData = $this->memberService->createUserToken($FCMTokenData);
            if ($FCMTokenData) {
                return response()->json(['message' => 'Token IOS added successfully', 'data' => [$FCMTokenData]], 200);
            } else {
                return response()->json(['error' => 'Something went wrong!!'], 400);
            }
        } else {
            $FCMTokenData = [
                'user_id' => $user->id,
                'token' => $data['token']
            ];
            // Add the android device token to dbs
            $FCMTokenData = $this->memberService->createUserToken($FCMTokenData);
            if ($FCMTokenData) {
                return response()->json(['message' => 'Token Android added successfully', 'data' => [$FCMTokenData]], 200);
            } else {
                return response()->json(['error' => 'Something went wrong!!'], 400);
            }
        }
    }

    /**
     * API get list friends
     *
     * @param FindFriendRequest $request
     * @return JsonResponse
     */
    public function findFriends(FindFriendRequest $request, $groupId = false)
    {
        try {
            $user = request()->user;
            $gender = $user->gender == 1 ? 0 : 1;
            $result = $this->memberService->getListByGender($request, $gender, $groupId);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API get list friends: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API like
     *
     * @param LikeMemberRequest $request
     * @return JsonResponse
     */
    public function like(LikeMemberRequest $request)
    {
        try {
            $user = request()->user;
            $memberId = $request->get('member_id');
            $message = $request->get('message');
            $useRose = $request->get('use_rose');
            $result = $this->memberService->like($user, $memberId, $message, $useRose);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API like member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API next member
     *
     * @param NextMemberRequest $request
     * @return JsonResponse
     */
    public function next(NextMemberRequest $request)
    {
        try {
            $user = request()->user;
            $memberId = $request->get('member_id');
            $useRose = $request->get('use_rose');
            $result = $this->memberService->next($user, $memberId, $useRose);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API next member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 37: list users liked but don't view (no message)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listUsersLikedNotView(Request $request)
    {
        try {
            $user = request()->user;
            $result = $this->memberService->listUsersLikedNotView($user->id, false);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error("API list users liked but don't view [no message]: " . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 38: list users liked but don't view (has message)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listUsersLikedWithMessage(Request $request)
    {
        try {
            $user = request()->user;
            $result = $this->memberService->listUsersLikedNotView($user->id, true);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error("API list users liked but don't view [has message]: " . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API : list users liked (has message)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listUsersLikedHasMessage(Request $request)
    {
        try {
            $user = request()->user;
            $result = $this->memberService->listUsersLikedHasMessage($user->id);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error("API list users liked [has message]: " . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 46: list user liked no message
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listUsersLiked(Request $request)
    {
        try {
            $user = request()->user;
            $result = $this->memberService->listUsersLikedNoMessage($user->id, true);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error("API list users liked [no message]: " . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 49: List user nexted
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listUsersNexted(Request $request)
    {
        try {
            $user = request()->user;
            $result = $this->memberService->listUsersNexted($user->id);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error("API 49: List user nexted: " . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 12: detail member
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function detail(Request $request, $id)
    {
        try {
            $owner = request()->user;
            $result = $this->memberService->detail($id, $owner);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API detail member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 14: profile member
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function profile(Request $request)
    {
        try {
            $user = request()->user;
            $result = $this->memberService->detail($user->id);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API profile member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    private function checkNickNameAndOption5($request)
    {
        $checkNickName = Common::checkNickName($request->get('nickname'));
        $checkOption5 = Common::checkOption5($request->get('option_5'));
        $result = [false, false];
        if (!$checkNickName && !$checkOption5) {
            $result = ['Nickname and Option_5 has NG word', 4001];
        } elseif (!$checkNickName) {
            $result = ['Nickname has NG word', 4002];
        } elseif (!$checkOption5) {
            $result = ['Option_5 has NG word', 4003];
        }
        return $result;
    }

    /**
     * API update member
     *
     * @param UpdateMemberApiRequest $request
     * @return JsonResponse
     */
    public function updateMember(UpdateMemberApiRequest $request, $id)
    {
        try {
            list($message, $code) = $this->checkNickNameAndOption5($request);
            if (!empty($message) && !empty($code)) {
                return response()->json([
                    'status' => false,
                    'code' => $code,
                    'message' => $message
                ], 400);
            }
            $result = $this->memberService->updateMember($request, $id);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API update member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API join group
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function joinGroup(Request $request, $id)
    {
        try {
            $user = request()->user;
            Common::checkExistMember($user->id, true);
            $result = $this->memberService->joinGroup($user, $id);
            if ($result) {
                return response()->jsonSuccess(['status' => $result]);
            }
            return response()->jsonError(400, 'Not join group with ID: ' . $id);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API join group: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API mypage
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function mypage(Request $request)
    {
        try {
            $user = request()->user;
            Common::checkExistMember($user->id, true);
            $result = $this->memberService->mypage($user);
            if ($result) {
                return response()->jsonSuccess($result);
            }
            return response()->jsonError(400, 'Not found user');
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API mypage: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API block member
     *
     * @param BlockMemberApiRequest $request
     * @return JsonResponse
     */
    public function blockMember(BlockMemberApiRequest $request)
    {
        try {
            $memberBlock = request()->user;
            Common::checkExistMember($memberBlock->id, true);
            $memberBlockedId = $request->get('user_blocked_id');
            if ($memberBlock->id == $memberBlockedId) {
                throw new \Exception("Can't block yourself", 400);
            }
            $result = $this->memberService->blockMember($memberBlock->id, $memberBlockedId);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API block member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 50: hide member blocked
     *
     * @param HideMemberBlockedApiRequest $request
     * @return JsonResponse
     */
    public function hideMemberBlocked(HideMemberBlockedApiRequest $request)
    {
        try {
            $memberBlock = request()->user;
            Common::checkExistMember($memberBlock->id, true);
            $memberBlockedId = $request->get('user_blocked_id');
            if ($memberBlock->id == $memberBlockedId) {
                throw new \Exception("Can't hide yourself", 400);
            }
            $result = $this->memberService->hideMemberBlocked($memberBlock->id, $memberBlockedId);
            return response()->jsonSuccess(['status' => $result ? true : false]);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 50: hide member blocked: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 51: unblock member
     *
     * @param UnBlockMemberApiRequest $request
     * @return JsonResponse
     */
    public function unblockMember(UnBlockMemberApiRequest $request)
    {
        try {
            $memberBlock = request()->user;
            Common::checkExistMember($memberBlock->id, true);
            $memberBlockedId = $request->get('user_blocked_id');
            if ($memberBlock->id == $memberBlockedId) {
                throw new \Exception("Can't unblock yourself", 400);
            }
            $result = $this->memberService->unBlockMember($memberBlock->id, $memberBlockedId);
            return response()->jsonSuccess(['status' => $result ? true : false]);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 51: unblock member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 52: List member blocked
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listMemberBlocked(Request $request)
    {
        try {
            $memberBlock = request()->user;
            Common::checkExistMember($memberBlock->id, true);
            $result = $this->memberService->listMemberBlocked($memberBlock->id);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 52: List member blocked: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 36: hide member
     *
     * @param HideMemberApiRequest $request
     * @return JsonResponse
     */
    public function hideMember(HideMemberApiRequest $request)
    {
        try {
            $member = request()->user;
            Common::checkExistMember($member->id, true);
            $memberHideId = $request->get('user_hide_id');
            if ($member->id == $memberHideId) {
                throw new \Exception("Can't hide yourself", 400);
            }
            $result = $this->memberService->hideMember($member->id, $memberHideId);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API hide member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 44: Unhide member
     *
     * @param UnHideMemberApiRequest $request
     * @return JsonResponse
     */
    public function unHideMember(UnHideMemberApiRequest $request)
    {
        try {
            $member = request()->user;
            Common::checkExistMember($member->id, true);
            $memberHideId = $request->get('user_unhide_id');
            if ($member->id == $memberHideId) {
                throw new \Exception("Can't unhide yourself", 400);
            }
            $result = $this->memberService->unHideMember($member->id, $memberHideId);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API Unhide member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 45: List member hided
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listMemberHided(Request $request)
    {
        try {
            $member = request()->user;
            Common::checkExistMember($member->id, true);
            $result = $this->memberService->listMemberHided($member->id);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API Unhide member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 71: set viewed for matching
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function setViewedMatching(Request $request)
    {
        try {
            $member = request()->user;
            Common::checkExistMember($member->id, true);
            $ids = $request->get('ids');
            if (empty($ids)) {
                throw new \Exception('ID matching not found', 400);
            }
            $result = $this->memberService->setViewedMatching($member, $ids);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 71: set viewed for matching: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 70: list matching not view
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listMatchingNotView(Request $request)
    {
        try {
            $member = request()->user;
            Common::checkExistMember($member->id, true);
            $result = $this->memberService->listMatchingNotView($member);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 70: list matching not view: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 32: list matching Not view
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listMatching(Request $request)
    {
        try {
            $member = request()->user;
            Common::checkExistMember($member->id, true);
            $result = $this->memberService->listMatching($member);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API list matching: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 39: Buy VIP
     *
     * @param BuyPackRequest $request
     * @return JsonResponse
     */
    public function buyVip(BuyPackRequest $request)
    {
        try {
            $member = request()->user;
            Common::checkExistMember($member->id, true);
            $params = $request->only(['month', 'amount']);
            $result = $this->memberService->buy($member, $params, 1);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 39: Buy VIP: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 40: Buy has fee
     *
     * @param BuyPackRequest $request
     * @return JsonResponse
     */
    public function buyPaid(BuyPackRequest $request)
    {
        try {
            $member = request()->user;
            Common::checkExistMember($member->id, true);
            $params = $request->only(['month', 'amount', 'like_reward']);
            $result = $this->memberService->buy($member, $params, 2);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 40: Buy has fee: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 54: Buy like
     *
     * @param BuyLikeRequest $request
     * @return JsonResponse
     */
    public function buyLike(BuyLikeRequest $request)
    {
        try {
            $member = request()->user;
            Common::checkExistMember($member->id, true);
            $points = $request->get('points');
            $amount = $request->get('amount');
            $result = $this->memberService->buyLike($member, $points, $amount);
            return response()->jsonSuccess($result);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 54: Buy like: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 55: Tranfer like
     *
     * @param TranferLikeRequest $request
     * @return JsonResponse
     */
    public function tranferLike(TranferLikeRequest $request)
    {
        try {
            $member = request()->user;
            Common::checkExistMember($member->id, true);
            $params = $request->only([
                'like_number',
                'type',
                'item_number',
            ]);
            $result = $this->memberService->tranferLike($member, $params);
            return response()->jsonSuccess(['status' => true, 'data' => $result]);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 54: Tranfer like: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 56: Pause member
     *
     * @param PauseMemberRequest $request
     * @return JsonResponse
     */
    public function pause(PauseMemberRequest $request)
    {
        try {
            $member = request()->user;
            $typePause = $request->get('type');
            $this->memberService->update([
                'is_pause' => 1,
                'type_pause' => $typePause,
                'reason_pause' => isset(LIST_REASON_PAUSE[$typePause]) ? LIST_REASON_PAUSE[$typePause] : null
            ], $member->id);
            return response()->jsonSuccess(['status' => true, 'message' => 'Paused member is success']);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 56: Pause member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 57: UnPause member
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function unpause(Request $request)
    {
        try {
            $member = request()->user;
            $this->memberService->update([
                'is_pause' => 0,
                'reason_pause' => null,
                'type_pause' => null
            ], $member->id);
            return response()->jsonSuccess(['status' => true, 'message' => 'UnPaused member is success']);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 57: UnPause member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 60: Save address
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function saveAddress(Request $request)
    {
        try {
            $member = request()->user;
            $this->memberService->update([
                'address' => $request->get('address')
            ], $member->id);
            return response()->jsonSuccess(['status' => true, 'message' => 'Update 居住地 is success']);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 60: Save address: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 61: Save email
     *
     * @param SaveEmailRequest $request
     * @return JsonResponse
     */
    public function saveEmail(SaveEmailRequest $request)
    {
        try {
            $member = request()->user;
            $this->memberService->update([
                'email' => $request->get('email')
            ], $member->id);
            return response()->jsonSuccess(['status' => true, 'message' => 'Update email is success']);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 61: Save email: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 62: Save password
     *
     * @param SavePasswordRequest $request
     * @return JsonResponse
     */
    public function savePassword(SavePasswordRequest $request)
    {
        try {
            $member = request()->user;
            $this->memberService->update([
                'password' => $request->get('password')
            ], $member->id);
            return response()->jsonSuccess(['status' => true, 'message' => 'Update password is success']);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 62: Save password: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 64: Cancel member
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function cancel(Request $request)
    {
        try {
            $member = request()->user;
            $typeCancel = $request->get('type');
            $this->memberService->update([
                'is_canceled' => 1,
                'type_canceled' => $typeCancel,
                'reason_canceled' => isset(LIST_REASON_CANCELED[$typeCancel]) ? LIST_REASON_CANCELED[$typeCancel] : null,
                'canceled_at' => date('Y-m-d H:i:s')
            ], $member->id);
            return response()->jsonSuccess(['status' => true, 'message' => 'Canceled member is success']);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 64: Cancel member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 66: List likes (bought or reward)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listLikesBoughtAndReward(Request $request)
    {
        try {
            $member = request()->user;
            $totalLikeBought = $this->memberService->getTotalLikeByType($member->id, TRANSACTION_TYPE_LIKE);
            $totalReward = $this->memberService->getTotalLikeByType($member->id, TRANSACTION_TYPE_REWARD);
            return response()->jsonSuccess(['status' => true, 'data' => [
                'total_like_bought' => $totalLikeBought,
                'total_like_reward' => $totalReward,
            ]]);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 66: List likes (bought or reward): ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 67: Upload imag verify
     *
     * @param ImageVerifyRequest $request
     * @return JsonResponse
     */
    public function uploadImageVerify(ImageVerifyRequest $request)
    {
        try {
            $result = $this->memberService->uploadImageVerify($request);
            if (!is_null($result)) {
                return response()->jsonSuccess(['status' => true, 'data' => [
                    'member' => $result
                ]]);
            }
            return response()->jsonSuccess(['status' => false]);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 67: Upload imag verify: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 69: List users by option
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listUsersByOption(Request $request)
    {
        try {
            $result = $this->memberService->listUsersByOption($request);
            if (!is_null($result)) {
                return response()->jsonSuccess(['status' => true, 'data' => $result]);
            }
            return response()->jsonSuccess(['status' => false]);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 69: List users by option: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 72: Check info for member
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function checkInfo(Request $request)
    {
        try {
            $result = $this->memberService->checkInfo($request);
            if (!is_null($result)) {
                return response()->jsonSuccess(['status' => true, 'data' => $result]);
            }
            return response()->jsonSuccess(['status' => false]);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 72: Check info for member: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }
}
