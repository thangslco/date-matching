<?php

namespace App\Services;

use App\Repositories\Contracts\UserTokenRepository;
use Illuminate\Support\Facades\Log;

class SendPushNotificationService extends BaseService
{
    /**
     * SendPushNotificationService constructor.
     * @param UserTokenRepository $repository
     */
    public function __construct(
        UserTokenRepository $repository
    ) {
        $this->repository = $repository;
    }

    /**
    * Get device info
    * $type = android, ios
    *
    */
    public function getDataDeviceByUserId($userId, $type)
    {
        $userToken = $this->repository->getTokenByUserId($userId, $type);
        $dataToken = [];
        if ($userToken) {
            foreach ($userToken as $itemToken) {
                if ($type == 'android' && !empty($itemToken->token)) {
                    $dataToken[] = $itemToken->token;
                }
                if ($type == 'ios' && !empty($itemToken->apns_id)) {
                    $dataToken[] = $itemToken->apns_id;
                }
            }
        }
        return $dataToken;
    }
    /**
    * Get devices info
    *
    */
    public function getDataDevices($type, $limit = 100, &$lastId = 0)
    {
        // for Android
        if ($type == 'android') {
            $dataTokenAndroid = $this->repository->getDataForAndroid($limit, $lastId);
            if ($dataTokenAndroid) {
                $listToken = $dataTokenAndroid->pluck('token', 'id');
                if ($listToken) {
                    $listToken = $listToken->all();
                    if (!empty($listToken)) {
                        $lastId = max(array_keys($listToken));
                    }
                    return $listToken;
                }
            }
        }
        // for IOS
        if ($type == 'ios') {
            $dataTokenIos = $this->repository->getDataForIos($limit, $lastId);
            if ($dataTokenIos) {
                $listApnsId = $dataTokenIos->pluck('apns_id', 'id');
                if ($listApnsId) {
                    $listApnsId = $listApnsId->all();
                    if (!empty($listApnsId)) {
                        $lastId = max(array_keys($listApnsId));
                    }
                    return $listApnsId;
                }
            }
        }
    }

    public function sendToIos($listApnsId, $message)
    {
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = env('FCM_SERVER_KEY');
        $notification = [
            'title' => $message['title'],
            'text' => $message['body'],
            'sound' => 'default',
            'badge' => '1'
        ];
        $arrayToSend = [
            'registration_ids' => array_values($listApnsId),
            'notification' => $notification,
            'priority' => 'high'
        ];
        if (isset($message['data_custom']) && !empty($message['data_custom'])) {
            $arrayToSend['data'] = $message['data_custom'];
        }
        $json = json_encode($arrayToSend);
        $headers = [];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Send the request
        $result = curl_exec($ch);
        if ($result === false) {
            Log::error('========== PushNotification Ios exception ==================');
            Log::error('Message: ' . curl_error($ch));
        }
        $result = json_decode($result, true);
        $responseData['ios'] = [
            'result' => $result
        ];
        //Close request
        curl_close($ch);
        return $responseData;
    }

    public function sendToAndroid($listToken, $message)
    {
        $msg = [
            'body' => $message['body'],
            'title' => $message['title'],
            'subtitle' => 'This is a subtitle',
        ];
        $fields = [
            'registration_ids' => $listToken,
            'notification' => $msg
        ];
        $headers = [
            'Authorization: key=' . env('FCM_SERVER_KEY'),
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            Log::error('========== PushNotification Android exception ==================');
            Log::error('Message: ' . curl_error($ch));
        }
        $result = json_decode($result, true);
        $responseData['android'] = [
            'result' => $result
        ];
        curl_close($ch);
        return $responseData;
    }
}
