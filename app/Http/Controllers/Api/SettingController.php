<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LimitLikeHasMessageRequest;
use App\Services\SettingService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SettingController extends Controller
{
    protected $settingService;
    public function __construct(SettingService $service)
    {
        parent::__construct();
        $this->settingService = $service;
    }

    /**
     * API 58: Set setting for notification
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function setSettingNotification(Request $request)
    {
        try {
            $params = $request->only([
                'SETTING_NOTIFICATION_MESSAGE',
                'SETTING_NOTIFICATION_LIKE',
                'SETTING_NOTIFICATION_MATCHING',
                'SETTING_NOTIFICATION_OTHER',
            ]);
            if (count($params) < 1) {
                throw new \Exception("No setting. Please add key and value", 400);
            }
            $data = $this->settingService->saveSetting($params, 1);
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 58: Set setting for notification: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 59: Set limit like has message
     *
     * @param LimitLikeHasMessageRequest $request
     * @return JsonResponse
     */
    public function setLimitLikeHasMessage(LimitLikeHasMessageRequest $request)
    {
        try {
            $params = $request->only([
                'LIMIT_LIKE_HAS_MESSAGE'
            ]);
            if (count($params) < 1) {
                throw new \Exception("No setting. Please add key and value", 400);
            }
            $data = $this->settingService->saveSetting($params);
            return response()->jsonSuccess($data);
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 59: Set limit like has message: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }

    /**
     * API 63: Get all settings
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAllSettings(Request $request)
    {
        try {
            $data = $this->settingService->all(['key', 'value']);
            $data = $data->pluck('value', 'key');
            return response()->jsonSuccess($data->toArray());
        } catch (\Exception $e) {
            Log::error('========== Exception ==============');
            Log::error('API 63: Get all settings: ' . $e->getMessage());
            Log::error('===================================');
            return response()->jsonError(400, $e->getMessage());
        }
    }
}
