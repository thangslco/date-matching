<?php

namespace App\Console\Commands;

use App\Services\MemberService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RewardUserVipCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reward-user-vip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reward user VIP every day 10 like';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(MemberService $memberService)
    {
        Log::info('======================================');
        Log::info('Start run command RewardUserVipCommand');
        $memberService->rewardUserVip();
        Log::info('Finished run command RewardUserVipCommand');
        $this->info('Success');
    }
}
